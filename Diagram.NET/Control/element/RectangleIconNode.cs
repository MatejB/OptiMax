using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Plasmoid.Extensions;


namespace Dalssoft.DiagramNet
{
	[Serializable]
	public class RectangleIconNode : RectangleNode
	{
        protected Image icon = null;

        public Image Icon 
        {
            get { return icon; }
            set { icon = value; }
        }

        public RectangleIconNode(Image icon)
            : this(icon, 0, 0, 100, 100)
		{}

        public RectangleIconNode(Image icon, Rectangle rec)
            : this(icon, rec.Location, rec.Size)
        {}

        public RectangleIconNode(Image icon, Point l, Size s)
            : this(icon, l.X, l.Y, s.Width, s.Height) 
		{}

        public RectangleIconNode(Image icon, int top, int left, int width, int height)
        {
            this.icon = icon;

            rectangle = new RectangleElement(top, left, width, height);
            SyncContructors();
		}

       

        internal override void Draw(Graphics g)
        {
            IsInvalidated = false;

            Point center = new Point();
            center.X = location.X + size.Width / 2;
            center.Y = location.Y + size.Height / 2;

            //g.FillRectangle(Brushes.LightGray, new Rectangle(center, new Size(32, 16)));

            //rectangle.Draw(g);

            Point topLeft = new Point();
            topLeft.X = center.X - icon.Width / 2 + 1;
            topLeft.Y = center.Y - icon.Height / 2 + 1;

            Rectangle iconDestRect = new Rectangle(topLeft, icon.Size);
            Rectangle iconSourceRect = new Rectangle(new Point(0, 0), icon.Size);

            g.FillRoundedRectangle(Brushes.WhiteSmoke, this.GetRectangle(), 7);
            g.DrawImage(icon, iconDestRect, iconSourceRect, GraphicsUnit.Pixel);
            g.DrawRoundedRectangle(new Pen(borderColor, borderWidth), this.GetRectangle(), 7);

            // set label offset
            label.TextHeightOffset = -this.size.Height * 2 / 3;
        }
	}
}
