Optimax

OptiMax is a software package that allows the user to structure the design process, explore the design space and compute optimal designs according to specified constraints and objectives.