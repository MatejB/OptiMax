﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public enum FileIOType
    {
        Input,
        Output,
        Constant,
        Copy
    }

    [Serializable]
    public class FileResource : Item, IDependent
    {
        // Variables                                                                                                                
        private string fileName;
        private string fileNameWithoutPath;
        private FileIOType fileIOType;
        private string fileResourceToCopyFrom;
        private string[] itemNames;
        private int count;

        // Properties                                                                                                               
        [Category("Data")]
        [Description("The file resource file name.")]
        [DisplayName("File name")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string FileNameWithPath
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;

                if (fileName.Trim().Length == 0)
                    throw new Exception("The file name is not defined.");

                if ((fileIOType == FileIOType.Input || fileIOType == FileIOType.Constant) && fileName != EmptyFileName && !File.Exists(fileName))
                    throw new Exception("The file '" + fileName + "' does not exist.");
                
                this.fileNameWithoutPath = Path.GetFileName(fileName);

                if (fileIOType == FileIOType.Input && File.Exists(fileName))
                {
                    if (!CheckForBinary(fileName))
                    {
                        string data = File.ReadAllText(fileName);
                        string[] allParameterNames;
                        GetParameterNames(data, out itemNames, out allParameterNames);
                    }
                    else
                        throw new Exception("A binary file can not be an input file.");
                }
                else itemNames = null;
            }
        }

        [Browsable(false)]
        public string FileNameWithoutPath
        {
            get
            {
                return fileNameWithoutPath;
            }
        }

        [Browsable(false)]
        public FileIOType IOType 
        {
            get
            {
                return fileIOType;
            }
        }

        [Browsable(false)]
        public string FileResourceToCopyFrom { get { return fileResourceToCopyFrom; } set { fileResourceToCopyFrom = value; } }


        public static string EmptyFileName { get { return "Enter file name"; } }

        // Constructors                                                                                                             
        public FileResource(string name, string description, string fileName, FileIOType type, string fileResourceToCopyFrom = null) 
            : base(name, description)
        {
            this.fileIOType = type; // first set the file type
            FileNameWithPath = fileName;            

            if (fileIOType == FileIOType.Copy && fileResourceToCopyFrom == null)
                throw new Exception("The copy file resource '" + name + "' does not have a file resource defined to copy from.");
            this.fileResourceToCopyFrom = fileResourceToCopyFrom;

            count = 0;

            List<string> tmp = InputVariables;
        }

        // Methods                                                                                                                  
        public bool SetToFilesDirectory(string filesDirectory)
        {
            if (fileIOType == FileIOType.Input || fileIOType == FileIOType.Constant)
            {
                string projectFileName = Path.Combine(filesDirectory, fileNameWithoutPath);
                if (fileName != projectFileName)
                {
                    if (!Directory.Exists(filesDirectory)) Directory.CreateDirectory(filesDirectory);

                    if (File.Exists(projectFileName))
                    {
                        if (System.Windows.Forms.MessageBox.Show("Overwrite existing file '" + projectFileName + "'?", "Warning", System.Windows.Forms.MessageBoxButtons.OKCancel)
                            == System.Windows.Forms.DialogResult.OK)
                        {
                            File.Copy(fileName, projectFileName, true);
                        }
                        else return false;
                    }
                    else File.Copy(fileName, projectFileName, true);

                    fileName = projectFileName;
                }
            }

            return true;
        }
        public void PrepareData(string workingDirectory, bool addJobID = false)
        {
            count++;
            
            string[] tmp = workingDirectory.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            string jobID = tmp[tmp.Length - 2];

            string workFileName;
            if (addJobID)
                workFileName = Path.Combine(workingDirectory, jobID + "_" + fileNameWithoutPath);
            else
                workFileName = Path.Combine(workingDirectory, fileNameWithoutPath);

            if (fileIOType == FileIOType.Input)
            {
                string data = File.ReadAllText(fileName, Encoding.Default);
                ReplaceAllParameterNamesWithValues(ref data, baseFrameworkItemCollection);
                File.WriteAllText(workFileName, data);
            }
            else if (fileIOType == FileIOType.Constant)
            {
                if (File.Exists(workFileName)) File.Delete(workFileName);
                File.Copy(fileName, workFileName, true);
            }
            else if (fileIOType == FileIOType.Output)
            {
            }
            else if (fileIOType == FileIOType.Copy)
            {
                Item item = baseFrameworkItemCollection[fileResourceToCopyFrom];
                if (item is FileResource)
                {
                    FileResource fr = (FileResource)item;

                    if (File.Exists(workFileName)) File.Delete(workFileName);
                    File.Copy(fr.FileNameWithPath, workFileName, true);
                }
            }

            fileName = workFileName;
            fileNameWithoutPath = Path.GetFileName(fileName);
        }

        public static void GetParameterNames(string data, out string[] itemNames, out string[] allParameterNames)
        {
            itemNames = null;
            allParameterNames = null;
            if (data == null || data.Length == 0) return;

            string[] splitData = data.Split('@');
            

            List<string> listItemNames = new List<string>();
            List<string> listParameterNames = new List<string>();

            string[] tmp;
            int index;
            int start = data[0] == '@' ? 0 : 1;

            start = 1;

            for (int i = start; i < splitData.Length; i += 2)
            {
                tmp = splitData[i].Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
                if (tmp.Length == 2 && int.TryParse(tmp[1], out index))
                {
                    if (!listItemNames.Contains(tmp[0]))
                        listItemNames.Add(tmp[0]);
                }
                else listItemNames.Add(splitData[i]);

                listParameterNames.Add(splitData[i]);
            }

            itemNames = listItemNames.ToArray();
            allParameterNames = listParameterNames.ToArray();
        }
        public static void ReplaceAllParameterNamesWithValues(ref string data, Dictionary<string, Item> frameworkItemCollection)
        {
            string[] itemNames;
            string[] allParamaterNames;
            GetParameterNames(data, out itemNames, out allParamaterNames);
            ReplaceAllParameterNamesWithValues(ref data, frameworkItemCollection, itemNames, allParamaterNames);
        }
        public static void ReplaceAllParameterNamesWithValues(ref string data, Dictionary<string, Item> frameworkItemCollection, string[] itemNames, string[] allParameterNames)
        {
            string[] tmp;
            if (allParameterNames != null)
            {
                foreach (string parName in allParameterNames)
                {
                    int index;
                    tmp = parName.Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tmp.Length == 2 && int.TryParse(tmp[1], out index))
                    {
                        Item item = frameworkItemCollection[tmp[0]];
                        if (item is ArrayBase)
                        {
                            ArrayBase array = (ArrayBase)item;
                            data = data.Replace("@" + parName + "@", array.GetValueAsString(index));
                        }
                    }
                    else
                    {
                        Item item = frameworkItemCollection[parName];
                        if (item is VariableBase)
                        {
                            VariableBase variable = (VariableBase)item;
                            data = data.Replace("@" + parName + "@", variable.GetValueAsString());
                        }
                        else if (item is ArrayBase)
                        {
                            ArrayBase array = (ArrayBase)item;
                            string values = array.GetValuesAsString();
                            values = values.Replace("\\r\\n ", "\r\n");
                            values = values.Replace("\\r\\n", "\r\n");
                            data = data.Replace("@" + parName + "@", values);
                        }
                    }
                }
            }
        }


        // IDependent
        [Browsable(false)]
        public List<string> InputVariables
        {
            // called in the constructor to initialize "parameterNames" if necessary
            get
            {
                try
                {
                    if (fileIOType == FileIOType.Copy)
                    {
                        return new List<string> { fileResourceToCopyFrom };
                    }
                    else if (fileIOType == FileIOType.Input && File.Exists(fileName))
                    {
                        if (itemNames == null) return null;
                        else return itemNames.ToList();
                    }
                    else return null;
                }
                catch (Exception ex)
                {
                    throw new Exception("Exception in FileResource property InputVariables." + Environment.NewLine + ex.Message);
                }
            }
        }

        /// <summary>
        /// This method checks whether selected file is Binary file or not.
        /// </summary>     
        public bool CheckForBinary(string fileName)
        {
            //Stream objStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            //bool bFlag = true;

            // Iterate through stream & check ASCII value of each byte.
            string data = File.ReadAllText(fileName);
            int a;
            for (int i = 0; i < data.Length; i++)
            {
                a = data[i];

                if (!(a >= 0 && a <= 127))
                {
                    return true;
                }
            }

            //for (int nPosition = 0; nPosition < objStream.Length; nPosition++)
            //{
            //    int a = objStream.ReadByte();

            //    if (!(a >= 0 && a <= 127))
            //    {
            //        break;            // Binary File
            //    }
            //    else if (objStream.Position == (objStream.Length))
            //    {
            //        bFlag = false;    // Text File
            //    }
            //}
            //objStream.Dispose();

            return false;
        }

    }
}
