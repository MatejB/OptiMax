﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;
using System.Data;

namespace OptiMaxFramework
{
    [Serializable]
    public class MemRecorder : Recorder, IValueRecorder 
    {
        // Variables                                                                                                                
        private List<string> variablesToRecord;
        public Action<object[]> ReportData;
        private object myLock;


        // Properties                                                                                                               
        public string[] TitleRow
        {
            get
            {
                if (itemNames != null && itemNames.Count > 0)
                {
                    List<string> titles = new List<string>() { "ID" };
                    titles.AddRange(itemNames);
                    return titles.ToArray();
                }
                else return null;
            }
        }


        // Constructors                                                                                                             
        public MemRecorder(string name, string description) : base(name, description)
        {
            variablesToRecord = null;
            myLock = null;
        }


        // Methods                                                                                                                  
        public override void StartRecording()
        {
            try
            {
                myLock = new object();
                variablesToRecord = new List<string>();

                foreach (var itemName in itemNames)
                {
                    Item item = FrameworkItemCollection[itemName];
                    if (item is IRecordable) variablesToRecord.Add(item.Name);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in MemRecorder method StartRecording()." + Environment.NewLine + ex.Message);
            }
        }
        public override void Record(string recordID, Dictionary<string, Item> items)
        {
            // do not check for RecAll inside the recorder
            object[] record = new object[1 + variablesToRecord.Count];
            Item item;

            string valueStr;
            int valueInt;
            double valueDbl;

            record[0] = int.Parse(recordID);
            int count = 1;
            foreach (string variableName in variablesToRecord)
            {
                item = items[variableName];
                if (item is VariableBase vb)
                {
                    valueStr = vb.GetValueAsString(OptiMaxFramework.Globals.nfi);

                    if (vb.ValueType == VariableValueType.Integer && int.TryParse(valueStr, out valueInt))
                        record[count] = valueInt;
                    else if (vb.ValueType == VariableValueType.Double && double.TryParse(valueStr, System.Globalization.NumberStyles.Number, Globals.nfi, out valueDbl))
                        record[count] = valueDbl;
                    else
                        record[count] = valueStr;
                }
                else if (item is ArrayBase ab) record[count] = ab.GetValuesAsString(Globals.nfi);
                else if (item is ResponseSurfaceComponent rsc) record[count] = rsc.GetStatusData();

                count++;
            }
            lock (myLock) ReportData?.Invoke(record);
        }
        public void Record(string recordID, Dictionary<string, List<Item>> items)
        {
            // do not check for RecAll inside the recorder

            // to record the best resoults with more items - more items with the same name
            object[] record = new object[1 + variablesToRecord.Count];

            record[0] = int.Parse(recordID);    // Add JobId
            int count = 1;
            foreach (string variableName in variablesToRecord)
            {
                record[count] = "";
                foreach (var item in items[variableName])
                {
                    if (item is VariableBase vb) record[count] += vb.GetValueAsString(Globals.nfi) + " ";
                    else if (item is ArrayBase ab) record[count] += ab.GetValuesAsString(Globals.nfi) + " ";
                    else if (item is ResponseSurfaceComponent rsc) record[count] += rsc.GetStatusData() + " ";
                }
                count++;
            }
            lock (myLock) ReportData?.Invoke(record);
        }
        public override void StopRecording()
        {
            variablesToRecord = null;
            myLock = null;
        }


        // IDependable                                                                                                              
        public override List<string> InputVariables
        {
            get 
            {
                return itemNames;
            }
        }
      

    }
}
