﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OptiMaxFramework
{
    [Serializable]
    public class FileRecorder : Recorder
    {
        // Constructors                                                                                                             
        public FileRecorder(string name, string description)
            : base(name, description)
        { 
        }


        // Methods                                                                                                                  
        public override void StartRecording()
        {
            if (!Directory.Exists(resultsDirectory)) 
                Directory.CreateDirectory(resultsDirectory);
        }

        public override void Record(string recordID, Dictionary<string, Item> items)
        {
            foreach (string itemName in itemNames)
            {
                Item item = items[itemName];
                FileResource fr = null;
                if (item is FileResource) fr = (FileResource)item;
                else if (item is ExcelComponent) fr = ((ExcelComponent)item).ExcelFile;
                
                if (fr == null) return;

                string sourceFileName = fr.FileNameWithPath;
                string destFileName = Path.Combine(resultsDirectory, recordID + "_" + fr.FileNameWithoutPath);

                if (!Directory.Exists(resultsDirectory))
                    Directory.CreateDirectory(resultsDirectory);

                File.Copy(sourceFileName, destFileName);
            }
        }

        public override void StopRecording()
        {
        }

       

        // IDependable                                                                                                              
        public override List<string> InputVariables
        {
            get 
            {
                return itemNames;
            }
        }
    }
}
