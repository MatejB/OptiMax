﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class Recorder : Item, IDependent
    {
        // Variables                                                                                                                
        protected string resultsDirectory;
        protected List<string> itemNames;


        // Properties                                                                                                               
        public string ResultsDirectory
        {
            get
            {
                return resultsDirectory;
            }
            set
            {
                resultsDirectory = System.IO.Path.Combine(value, name);
            }
        }

        public bool RecordAll { get; set; }

        // Constructors                                                                                                             
        public Recorder(string name, string description)
            : base(name, description)
        {
            resultsDirectory = null;
            itemNames = new List<string>();
            RecordAll = true;
        }


        // Methods                                                                                                                  
        public void Add(string name)
        {
            itemNames.Add(name);
        }
        abstract public void StartRecording();
        abstract public void Record(string recordID, Dictionary<string, Item> items);
        abstract public void StopRecording();


        // IDependent
        abstract public List<string> InputVariables
        {
            get;
        }
    }
}
