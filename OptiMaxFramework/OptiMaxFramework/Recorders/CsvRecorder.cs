﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

namespace OptiMaxFramework
{
    [Serializable]
    public class CsvRecorder : Recorder, IValueRecorder
    {
        // Variables                                                                                                                
        private string csvFileName;
        private List<string> variablesToRecord;
        private System.Globalization.NumberFormatInfo nfi;
        private static char sepparator = ';';
        private object myLock;
        private System.DateTime lastWriteTime;
        private ConcurrentQueue<string> records;

        // Properties
        public string DecimalSeparator 
        {
            get
            {
                //if (nfi == null) nfi = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };
                return nfi.NumberDecimalSeparator;
            }
        }

        public string FileName
        {
            get
            {
                return csvFileName;
            }
        }


        // Constructors                                                                                                             
        public CsvRecorder(string name, string description, string decimalSeparator)
            : base(name, description)
        {
            nfi = new System.Globalization.NumberFormatInfo{ NumberDecimalSeparator = decimalSeparator };
            csvFileName = null;
            variablesToRecord = null;
            myLock = new object();
        }


        // Methods                                                                                                                  
        public override void StartRecording()
        {
            try
            {
                if (!Directory.Exists(resultsDirectory)) Directory.CreateDirectory(resultsDirectory);

                lastWriteTime = new DateTime(0);
                records = new ConcurrentQueue<string>();
                csvFileName = Path.Combine(resultsDirectory, name + ".csv");
                variablesToRecord = new List<string>();

                foreach (var itemName in itemNames)
                {
                    Item item = FrameworkItemCollection[itemName];
                    if (item is IRecordable) variablesToRecord.Add(item.Name);
                }

                string record = "\"ID\"";
                foreach (string variableName in variablesToRecord)
                {
                    record += sepparator + "\"" + variableName + "\"";
                }

                record += Environment.NewLine;
                File.WriteAllText(csvFileName, record);
                lastWriteTime = DateTime.Now;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in CsvRecorder method StartRecording()." + Environment.NewLine + ex.Message);
            }
        }

        public override void Record(string recordID, Dictionary<string, Item> items)
        {
            // do not check for RecAll inside the recorder
            StringBuilder record = new StringBuilder(recordID);
                
            Item item;
            string data = "";
            foreach (string variableName in variablesToRecord)
            {
                item = items[variableName];
                if (item is VariableBase vb) data = vb.GetValueAsString(nfi);
                else if (item is ArrayBase ab) data = ab.GetValuesAsString(nfi);
                else if (item is ResponseSurfaceComponent rsc) data = rsc.GetStatusData();
                record.AppendFormat("{0}{1}", sepparator, data);
            }
            records.Enqueue(record.ToString() + Environment.NewLine);

            WriteToFile();
        }

        
        public void Record(string recordID, Dictionary<string, List<Item>> items)
        {
            // do not check for RecAll inside the recorder

            // to record the best resoults with more items - more items with the same name
            string record = recordID;
            string data ;
            foreach (string variableName in variablesToRecord)
            {
                data = "";
                foreach (var item in items[variableName])
                {
                    if (item is VariableBase vb) data += vb.GetValueAsString(nfi) + " ";
                    else if (item is ArrayBase ab) data += ab.GetValuesAsString(nfi) + " ";
                    else if (item is ResponseSurfaceComponent rsc) data += rsc.GetStatusData() + " ";
                }
                record += sepparator + data;
            }

            records.Enqueue(record + Environment.NewLine);
            WriteToFile();
        }

        private void WriteToFile()
        {
            string tmp;
            string record;

            lock (myLock)
            {
                if ((DateTime.Now - lastWriteTime).TotalSeconds > 5)
                {
                    record = "";
                    while (records.Count > 0)
                    {
                        records.TryDequeue(out tmp);
                        record += tmp;
                    }
                    File.AppendAllText(csvFileName, record);
                    lastWriteTime = DateTime.Now;
                }
            }
        }

        public override void StopRecording()
        {
            if (records.Count > 0)
            {
                string tmp;
                string record = "";
                while (records.Count > 0)
                {
                    records.TryDequeue(out tmp);
                    record += tmp;
                }
                File.AppendAllText(csvFileName, record);
                lastWriteTime = DateTime.Now;
            }
            variablesToRecord = null;
        }
     


        // IDependable                                                                                                              
        public override List<string> InputVariables
        {
            get 
            {
                return itemNames;
            }
        }
    }
}
