﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    public interface IValueRecorder
    {
        void Record(string recordID, Dictionary<string, List<Item>> items);
    }
}
