﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public struct VariableExcelRangePair
    {
        // Variables                                                                                                                
        private string variable;
        private string range;
        

        // Properties                                                                                                               
        public string Variable 
        {
            get { return variable; }
        }

        public string Range
        {
            get { return range; }
        }


        // Constructor                                                                                                              
        public VariableExcelRangePair(string variable, string range)
        {
            this.variable = variable;
            this.range = range;
        }
    }

    [Serializable]
    public class ExcelComponent : Component
    {
        // Variables                                                                                                                
        private FileResource excelFile;
        private VariableExcelRangePair[] inpVarRangePairs;
        private int numOfConcurrentApps;
        private static SemaphoreSlim sem;
        private static object myLock = new object();


        // Properties                                                                                                               
        public FileResource ExcelFile 
        {
            get { return excelFile; }
        }
        public VariableExcelRangePair[] InpVariableRangePairs
        {
            get { return inpVarRangePairs; }
        }
        public int NumOfConcurrentApps 
        { 
            get { return numOfConcurrentApps; }
            set
            {
                numOfConcurrentApps = value;
                if (numOfConcurrentApps < 1) sem = null;
                else sem = new SemaphoreSlim(numOfConcurrentApps);
            }
        }
        public int TimeoutMilliSeconds
        {
            get
            {
                return timeoutMilliSeconds;
            }
            set
            {
                timeoutMilliSeconds = value;
            }
        }
        override public string Application
        {
            get
            {
                return "Excel";
            }
        }


        // Constructor                                                                                                              
        public ExcelComponent(string name, string descritption, int timeoutMilliSeconds, string excelFileNameWithPath, VariableExcelRangePair[] inpVariableRangePairs, string filesDirectory)
            : base(name, descritption, timeoutMilliSeconds)
        {
            kill = false;
            NumOfConcurrentApps = -1;

            if (!File.Exists(excelFileNameWithPath))
                throw new Exception("The file '" + excelFileNameWithPath + "' defined in Excel component '" + name + "' does not exist.");

            excelFile = new FileResource("Excel", "", excelFileNameWithPath, FileIOType.Constant); // must be constant! input searches for @vars@
            excelFile.SetToFilesDirectory(filesDirectory);

            if (inpVariableRangePairs == null) this.inpVarRangePairs = null;
            else this.inpVarRangePairs = (VariableExcelRangePair[])(inpVariableRangePairs.Clone());

            foreach (VariableExcelRangePair pair in this.inpVarRangePairs)
            {
                itemNames.Add(pair.Variable);
            }
        }

        // Methods                                                                                                                  
        public override void PrepareData()
        {
            PrepareFiles();
            excelFile.PrepareData(workingDirectory);
        }
        public override JobStatus Run()
        {
            lock (myLock)
            {
                if (sem == null && numOfConcurrentApps > 0) NumOfConcurrentApps = numOfConcurrentApps; //create the sem after read from file
                System.Threading.Thread.Sleep(10);
            }

            ExcelHelper excel = null;

            try
            {
                if (sem != null) 
                    sem.Wait();

                Stopwatch watch = new Stopwatch();
                watch.Start();

                // Check                                                                                                    
                bool write = false;
                bool read = false;

                if (inpVarRangePairs != null && inpVarRangePairs.Length > 0) write = true;

                VariableOutput varOut;
                ArrayOutput arrOut;

                foreach (Item item in baseFrameworkItemCollection.Values)
                {
                    if (item is VariableOutput)
                    {
                        varOut = (VariableOutput)item;
                        if (varOut.Template is DataMiningTemplateExcel)
                        {
                            if (varOut.OutputItemName == this.name) read = true;
                        }
                    }
                }

                if (!write && !read) return JobStatus.OK;

                // open excel                                                                                               
                excel = new ExcelHelper(excelFile.FileNameWithPath);
                excel.Open();

                // write to excel file                                                                                      
                if (write)
                {
                    foreach (VariableExcelRangePair pair in inpVarRangePairs)
                    {
                        if (kill) return JobStatus.Killed;
                        if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;


                        Item item = baseFrameworkItemCollection[pair.Variable];
                        if (item is VariableBase)
                        {
                            VariableBase variable = (VariableBase)item;
                            excel.WriteToCell(pair.Range, variable.GetValueAsString());
                        }
                        else if (item is ArrayBase)
                        {
                            ArrayBase array = (ArrayBase)item;

                            string[] cells = pair.Range.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                            if (cells[0] == cells[1])
                                excel.WriteToCell(cells[0], array.GetValuesAsString());
                            else
                                excel.WriteToCells(cells[0], cells[1], PrepareArrayValues(array, cells[0], cells[1]));
                        }
                    }
                }

                excel.Save();

                // read from excel file                                                                                     
                
                double valueDouble;
                double[] valuesDouble;

                string valueString;

                foreach (Item item in baseFrameworkItemCollection.Values)
                {
                    if (item is VariableOutput)
                    {
                        varOut = (VariableOutput)item;
                        if (varOut.Template is DataMiningTemplateExcel)
                        {
                            if (varOut.OutputItemName == this.name)
                            {
                                if (varOut.ValueType == VariableValueType.Double || varOut.ValueType == VariableValueType.Integer)
                                {
                                    valueDouble = excel.ReadFromCellDouble((varOut.Template as DataMiningTemplateExcel).CellRange);
                                    varOut.SetDoubleValueFromOutputItem(valueDouble);
                                }
                                else if (varOut.ValueType == VariableValueType.String)
                                {
                                    valueString = excel.ReadFromCellString((varOut.Template as DataMiningTemplateExcel).CellRange);
                                    varOut.SetStringValueFromOutputItem(valueString);
                                }
                            }
                        }
                    }
                    else if (item is ArrayOutput)
                    {
                        arrOut = (ArrayOutput)item;
                        if (arrOut.Template is DataMiningTemplateExcel)
                        {
                            if (arrOut.OutputItemName == this.name)
                            {
                                string[] cells = (arrOut.Template as DataMiningTemplateExcel).CellRange.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                                if (cells[0] == cells[1]) valuesDouble = new double[] { excel.ReadFromCellDouble(cells[0]) };
                                else valuesDouble = DataMiner.GetDoubleArrayFromObjectArray(excel.ReadFromCells(cells[0], cells[1]));

                                arrOut.SetValueFromOutputItem(valuesDouble);
                            }
                        }
                    }
                }

                return JobStatus.OK;
            }
            finally
            {
                if (excel != null)
                {
                    excel.Close();
                    excel.ReleaseObjectsAndKillExcel();
                    excel = null;
                }
                if (sem != null) sem.Release();
            }
        }
        public override void GatherData()
        {
        }
        public override void Kill()
        {
            kill = true;
            //throw new NotSupportedException();
        }
      
        private void PrepareFiles()
        {
            foreach (string itemName in itemNames)
            {
                Item item = baseFrameworkItemCollection[itemName];
                if (item is FileResource)
                {
                    ((FileResource)item).PrepareData(workingDirectory);
                }
            }
        }
        private double[,] PrepareArrayValues(ArrayBase array, string startIndex, string endIndex)
        {
            int rowStart, colStart, rowEnd, colEnd;
            ExcelHelper.GetColRowIndexFromRange(startIndex, out rowStart, out colStart);
            ExcelHelper.GetColRowIndexFromRange(endIndex, out rowEnd, out colEnd);
            string[] values = array.GetValuesAsString().Split(' ');
            double[,] cellValues = null;

            if (colStart == colEnd)
            {
                cellValues = new double[values.Length, 1];
                for (int i = 0; i < values.Length; i++)
                {
                    cellValues[i, 0] = double.Parse(values[i], OptiMaxFramework.Globals.nfi);
                }
            }
            else if (rowStart == rowEnd)
            {
                cellValues = new double[1, values.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    cellValues[0, i] = double.Parse(values[i], OptiMaxFramework.Globals.nfi);
                }
            }

            return cellValues;
        }
        private int GetNumOfRunningExcelProcesses()
        {
            int count = 0;
            Process[] processes = Process.GetProcesses();
            foreach (Process p in processes)
            {
                if (p.ProcessName.ToLower().StartsWith("excel"))
                    count++;
            }
            return count;
        }

        // IDependent
        public override List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }
    }
}
