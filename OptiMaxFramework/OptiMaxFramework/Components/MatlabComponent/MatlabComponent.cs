﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class MatlabComponent : DosComponent
    {
        // Variables                                                                                          
        private string inputFileResourceName;
        private static SemaphoreSlim sem = new SemaphoreSlim(1);
        private static DateTime lastAccesTime = DateTime.Now;


        // Properties                                                                                         
        public string InputFileResourceName
        {
            get
            {
                return inputFileResourceName;
            }
            set
            {
                if (itemNames.Contains(inputFileResourceName)) itemNames.Remove(inputFileResourceName);
                inputFileResourceName = value;
                itemNames.Add(value);
            }
        }
        public new string Argument 
        {
            get
            {
                try
                {
                    // -automation -nodisplay -nosplash -nodesktop -wait -r "try, run('@workDirectory@\readComputeWrite.m'), catch, exit, end, exit"
                    Item item = baseFrameworkItemCollection[inputFileResourceName];
                    if (item != null && item is FileResource)
                    {
                        FileResource input = (FileResource)item;

                        string directory = "@workDirectory@";
                        if (workingDirectory != null && workingDirectory.Length > 0) directory = workingDirectory;

                        string fileName = Path.Combine(directory, input.FileNameWithoutPath);

                        return string.Format("-automation -nodisplay -nosplash -nodesktop -wait -r \"try, run('{0}'), catch, exit, end, exit\"", fileName);
                    }
                    else throw new Exception("The input file resource name of the MatlabComponent does not represent a file resource name or does not exist.");
                }
                catch (Exception ex)
                {
                    return "Error in argument.";
                }
            }
        }


        // Constructors                                                                                       
        public MatlabComponent(string name, string descritption, string inputFileResourceName, int timeoutMilliSeconds)
            : base(name, descritption, "matlab.exe", false, timeoutMilliSeconds, null)
        {
            InputFileResourceName = inputFileResourceName;
            argument = null;
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public override void PrepareData()
        {
            Item item = baseFrameworkItemCollection[inputFileResourceName];
            if (item == null || !(item is FileResource))
                throw new Exception("The input file resource name of the CalculiXComponent does not represent a file resource name or does not exist.");

            base.PrepareData();
        }

        public override JobStatus Run()
        {
            WindowsDesktop.Desktop matlabDesktop = null;
            try
            {
                sem.Wait(); // run Matlab in 200 ms intervals
                if ((DateTime.Now - lastAccesTime).TotalMilliseconds < 200)
                {
                    System.Threading.Thread.Sleep(200);
                }
                lastAccesTime = DateTime.Now;
                sem.Release();

                argument = Argument;

                //JobStatus status = base.Run();

                matlabDesktop = new WindowsDesktop.Desktop("MATLAB");

                exe = matlabDesktop.CreateProcessInIt(executable + " " + argument);

                if (exe.WaitForExit(timeoutMilliSeconds))
                {
                    // Process completed. Check process.ExitCode here.
                }
                else
                {
                    // Timed out.
                    KillOnTimedout();
                    Debug.WriteLine(DateTime.Now + "   Timeout proces: " + executable + " in: " + workingDirectory);
                    return JobStatus.TimedOut;
                }
                exe.Close();

                matlabDesktop.SwitchToOrginal();
                matlabDesktop.Dispose();
                matlabDesktop = null;

                return JobStatus.OK;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (matlabDesktop != null)
                {
                    matlabDesktop.SwitchToOrginal();
                    matlabDesktop.Dispose();
                }
            }
        }

       
    }
}
