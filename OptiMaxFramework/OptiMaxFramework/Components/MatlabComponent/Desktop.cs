﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;


namespace WindowsDesktop
{
    #region Enumerators
    [Flags]
    public enum ACCESS_MASK : uint
    {
        DELETE = 0x00010000,
        READ_CONTROL = 0x00020000,
        WRITE_DAC = 0x00040000,
        WRITE_OWNER = 0x00080000,
        SYNCHRONIZE = 0x00100000,

        STANDARD_RIGHTS_REQUIRED = 0x000F0000,

        STANDARD_RIGHTS_READ = 0x00020000,
        STANDARD_RIGHTS_WRITE = 0x00020000,
        STANDARD_RIGHTS_EXECUTE = 0x00020000,

        STANDARD_RIGHTS_ALL = 0x001F0000,

        SPECIFIC_RIGHTS_ALL = 0x0000FFFF,

        ACCESS_SYSTEM_SECURITY = 0x01000000,

        MAXIMUM_ALLOWED = 0x02000000,

        GENERIC_READ = 0x80000000,
        GENERIC_WRITE = 0x40000000,
        GENERIC_EXECUTE = 0x20000000,
        GENERIC_ALL = 0x10000000,

        DESKTOP_READOBJECTS = 0x00000001,
        DESKTOP_CREATEWINDOW = 0x00000002,
        DESKTOP_CREATEMENU = 0x00000004,
        DESKTOP_HOOKCONTROL = 0x00000008,
        DESKTOP_JOURNALRECORD = 0x00000010,
        DESKTOP_JOURNALPLAYBACK = 0x00000020,
        DESKTOP_ENUMERATE = 0x00000040,
        DESKTOP_WRITEOBJECTS = 0x00000080,
        DESKTOP_SWITCHDESKTOP = 0x00000100,

        WINSTA_ENUMDESKTOPS = 0x00000001,
        WINSTA_READATTRIBUTES = 0x00000002,
        WINSTA_ACCESSCLIPBOARD = 0x00000004,
        WINSTA_CREATEDESKTOP = 0x00000008,
        WINSTA_WRITEATTRIBUTES = 0x00000010,
        WINSTA_ACCESSGLOBALATOMS = 0x00000020,
        WINSTA_EXITWINDOWS = 0x00000040,
        WINSTA_ENUMERATE = 0x00000100,
        WINSTA_READSCREEN = 0x00000200,

        WINSTA_ALL_ACCESS = 0x0000037F
    }

    [Flags]
    internal enum DESKTOP_ACCESS_MASK : uint
    {
        DESKTOP_NONE = 0,
        DESKTOP_READOBJECTS = 0x0001,
        DESKTOP_CREATEWINDOW = 0x0002,
        DESKTOP_CREATEMENU = 0x0004,
        DESKTOP_HOOKCONTROL = 0x0008,
        DESKTOP_JOURNALRECORD = 0x0010,
        DESKTOP_JOURNALPLAYBACK = 0x0020,
        DESKTOP_ENUMERATE = 0x0040,
        DESKTOP_WRITEOBJECTS = 0x0080,
        DESKTOP_SWITCHDESKTOP = 0x0100,

        GENERIC_ALL = (DESKTOP_READOBJECTS | DESKTOP_CREATEWINDOW | DESKTOP_CREATEMENU |
                        DESKTOP_HOOKCONTROL | DESKTOP_JOURNALRECORD | DESKTOP_JOURNALPLAYBACK |
                        DESKTOP_ENUMERATE | DESKTOP_WRITEOBJECTS | DESKTOP_SWITCHDESKTOP),
    }
    #endregion

    #region Structures
    [StructLayout(LayoutKind.Sequential)]
    struct PROCESS_INFORMATION
    {
        public IntPtr hProcess;
        public IntPtr hThread;
        public int dwProcessId;
        public int dwThreadId;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct STARTUPINFO
    {
        public int cb;
        public string lpReserved;
        public string lpDesktop;
        public string lpTitle;
        public int dwX;
        public int dwY;
        public int dwXSize;
        public int dwYSize;
        public int dwXCountChars;
        public int dwYCountChars;
        public int dwFillAttribute;
        public int dwFlags;
        public short wShowWindow;
        public short cbReserved2;
        public IntPtr lpReserved2;
        public IntPtr hStdInput;
        public IntPtr hStdOutput;
        public IntPtr hStdError;
    }
    #endregion

    #region Classes
    [StructLayout(LayoutKind.Sequential)]
    public class SECURITY_ATTRIBUTES
    {
        public int nLength;
        public IntPtr lpSecurityDescriptor;
        public int bInheritHandle;
    }

    public class Desktop : IDisposable
    {
        #region Delegates
        delegate bool EnumDesktopsDelegate(string desktop, IntPtr lParam);
        #endregion

        #region DLLs
        [DllImport("user32.dll", EntryPoint = "CreateDesktop", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr CreateDesktop(
                    [MarshalAs(UnmanagedType.LPWStr)] string desktopName,
                    [MarshalAs(UnmanagedType.LPWStr)] string device,        // must be null.
                    [MarshalAs(UnmanagedType.LPWStr)] string deviceMode,    // must be null,
                    [MarshalAs(UnmanagedType.U4)] int flags,                // use 0
                    [MarshalAs(UnmanagedType.U4)] ACCESS_MASK accessMask,
                    [In] ref SECURITY_ATTRIBUTES lpsa);

        [DllImport("user32.dll")]
        static extern IntPtr OpenDesktop(string lpszDesktop, uint dwFlags, bool fInherit, uint dwDesiredAccess);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr OpenInputDesktop(uint dwFlags,
                                                        bool fInherit,
                                                        uint dwDesiredAccess);

        [DllImport("user32.dll")]
        private static extern bool SwitchDesktop(IntPtr hDesktop);

        [DllImport("user32.dll", EntryPoint = "CloseDesktop", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CloseDesktop(IntPtr handle);

        [DllImport("user32.dll")]
        static extern bool EnumDesktops(IntPtr hwinsta, EnumDesktopsDelegate lpEnumFunc, IntPtr lParam);


        [DllImport("user32.dll")]
        public static extern bool SetThreadDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern IntPtr GetThreadDesktop(int dwThreadId);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();


        [DllImport("kernel32.dll")]
        private static extern bool CreateProcess(string lpApplicationName,
                                                   string lpCommandLine,
                                                   IntPtr lpProcessAttributes,
                                                   IntPtr lpThreadAttributes,
                                                   bool bInheritHandles,
                                                   int dwCreationFlags,
                                                   IntPtr lpEnvironment,
                                                   string lpCurrentDirectory,
                                                   ref STARTUPINFO lpStartupInfo,
                                                   ref PROCESS_INFORMATION lpProcessInformation
                                                   );
        [DllImport("user32.dll")]
        private static extern IntPtr GetProcessWindowStation();

        const int DELETE = 0x00010000;
        const int READ_CONTROL = 0x20000;
        const int WRITE_DAC = 0x40000;
        const int DESKTOP_WRITEOBJECTS = 0x80;
        const int DESKTOP_READOBJECTS = 0x1;
        const int GENERIC_ALL = 0x10000000;



        #endregion

        #region Dispose 
        public void Dispose()
        {
            SwitchToOrginal();
            ((IDisposable)this).Dispose();
        }

        /// <summary>
        /// Unterklassen können hier die Funktionalität der Objektzerstörung erweitern. 
        /// </summary>
        /// <param name="fDisposing"></param>
        protected virtual void Dispose(bool fDisposing)
        {
            if (fDisposing)
            {
                // Hier die verwalteten Ressourcen freigeben
                //BspVariable1 = null;
                bool result = CloseDesktop(_desktopPtr);
            }
            // Hier die unverwalteten Ressourcen freigeben
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this); //Fordert das System auf, den Finalizer für das angegebenen Objekt nicht aufzurufen
        }
        #endregion

        #region Variables
        IntPtr _hOrigDesktop;
        public IntPtr _desktopPtr;
        private string _sMyDesk;
        public string DesktopName
        {
            get
            {
                return (_sMyDesk);
            }
            set
            {
                _sMyDesk = value;
            }
        }

        private const int NORMAL_PRIORITY_CLASS = 0x00000020;
        #endregion

        #region Constructors
        public Desktop()
        {
            _sMyDesk = "";
        }

        public Desktop(string sDesktopName)
        {
            _hOrigDesktop = GetCurrentDesktopPtr();
            _sMyDesk = sDesktopName;
            _desktopPtr = CreateMyDesktop();
        }
        #endregion

        #region Methods
        
        public void Switch()
        {
            SetThreadDesktop(_desktopPtr);
            SwitchDesktop(_desktopPtr);
        }

        public void SwitchToOrginal()
        {
            SwitchDesktop(_hOrigDesktop);
            SetThreadDesktop(_hOrigDesktop);
        }

        private IntPtr CreateMyDesktop()
        {
            SECURITY_ATTRIBUTES s_a = null;
            return CreateDesktop(_sMyDesk, null, null, 0, ACCESS_MASK.GENERIC_ALL, ref s_a);
        }

        public IntPtr GetCurrentDesktopPtr()
        {
            return GetThreadDesktop(GetCurrentThreadId());
        }

        public Process CreateProcessInIt(string path)
        {
            STARTUPINFO si = new STARTUPINFO();
            si.cb = Marshal.SizeOf(si);
            si.lpDesktop = this.DesktopName;

            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();

            // start the process.
            bool result = CreateProcess(null, path, IntPtr.Zero, IntPtr.Zero, true, NORMAL_PRIORITY_CLASS, IntPtr.Zero, null, ref si, ref pi);

            // error?
            // if (!result) -- Error!

            // Get the process.
            return Process.GetProcessById(pi.dwProcessId);
        }

        public static void EnumerateDesktops()
        {
            IntPtr windowStation = GetProcessWindowStation();
            EnumDesktops(windowStation, new EnumDesktopsDelegate(EnumDesktop), IntPtr.Zero);
        }

        private static bool EnumDesktop(string desktop, IntPtr lParam)
        {
            Console.WriteLine(desktop);
            return true;
        }
        #endregion

    }
    #endregion
}
