﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public struct Command
    {
        public string[] rows;
        public override string ToString()
        {
            string command = "";
            foreach (string row in rows)
            {
                command += row + ";";
            }

            return command;
        }
    }
}
