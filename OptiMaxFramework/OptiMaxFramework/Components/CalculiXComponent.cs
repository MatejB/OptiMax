﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class CalculiXComponent : DosComponent
    {
        // Variables                                                                                          
        private string inputFileResourceName;

        // Properties                                                                                         
        public string InputFileResourceName
        {
            get
            {
                return inputFileResourceName;
            }
            set
            {
                if (itemNames.Contains(inputFileResourceName)) itemNames.Remove(inputFileResourceName);
                inputFileResourceName = value;
                itemNames.Add(value);
            }
        }

        public int NumOfCpus { get; set; }

        public new string Argument 
        {
            get
            {
                try
                {
                    Item item = baseFrameworkItemCollection[inputFileResourceName];
                    if (item != null && item is FileResource)
                    {
                        FileResource input = (FileResource)item;

                        return "-i " + System.IO.Path.GetFileNameWithoutExtension(input.FileNameWithoutPath);
                    }
                    else throw new Exception("The input file resource name of the CalculiXComponent does not represent a file resource name or does not exist.");
                }
                catch (Exception ex)
                {
                    return "Error in argument.";
                }
            }
        }


        // Constructors                                                                                       
        public CalculiXComponent(string name, string descritption, string executable, string inputFileResourceName, int timeoutMilliSeconds)
            : base(name, descritption, executable, false, timeoutMilliSeconds, null)
        {
            this.inputFileResourceName = inputFileResourceName;
            itemNames.Add(inputFileResourceName);
            
            NumOfCpus = 1;

            argument = null;
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public override void PrepareData()
        {
            Item item = baseFrameworkItemCollection[inputFileResourceName];
            if (item == null || !(item is FileResource))
                throw new Exception("The input file resource name of the CalculiXComponent does not represent a file resource name or does not exist.");

            base.PrepareData();
        }

        public override JobStatus Run()
        {
            Item item = baseFrameworkItemCollection[inputFileResourceName];
            FileResource input = (FileResource)item;
            
            JobStatus status = RunAnalysis(input);
            
            return status;
        }
               
        private JobStatus RunAnalysis(FileResource input)
        {
            argument = Argument;

            if (NumOfCpus > 1)
                EnvironmentVariables = new EnvironmentVariable[] { new EnvironmentVariable() { Include = true, Name = "OMP_NUM_THREADS", Value = NumOfCpus.ToString() } };

            return base.Run();
        }
    }
}
