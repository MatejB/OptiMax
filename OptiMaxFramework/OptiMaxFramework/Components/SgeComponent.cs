﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using Renci.SshNet;

namespace OptiMaxFramework
{
    

    [Serializable]
    public class SgeComponent : Component, IDeepCopy, ISshComponent
    {
        // Variables                                                                                                                
        protected string serverName;
        protected string serverUserName;
        protected string serverUserPass;
        protected string serverPath;
        protected string scriptFileResourceName;
        protected string argument;
        protected int jobId;
        
        [NonSerialized]
        protected Dictionary<string, SgeCommunicator> SgeCommunicators;


        // Properties                                                                                                               
        override public string Application
        {
            get
            {
                return "Qsub";
            }
        }
        public string ScriptFileResourceName
        {
            get
            {
                return scriptFileResourceName;
            }
        }
        public int TimeoutMilliSeconds
        {
            get
            {
                return timeoutMilliSeconds;
            }
        }
        public string Argument
        {
            get
            {
                return argument;
            }
            set
            {
                argument = value;
            }
        }
        public string ServerName
        {
            get
            {
                return serverName;
            }
        }
        public string ServerUserName
        {
            get
            {
                return serverUserName;
            }
        }
        public string ServerUserPass
        {
            get
            {
                return serverUserPass;
            }
        }
        public string ServerPath
        {
            get
            {
                return serverPath;
            }
        }
        public Dictionary<string, SgeCommunicator> SshCommunicators
        {
            get { return SgeCommunicators; }
            set
            {
                SgeCommunicators = value;
                if (!SgeCommunicators.ContainsKey(serverName)) SgeCommunicators.Add(serverName, new SgeCommunicator(serverName, serverUserName, serverUserPass));
            }
        }


        // Constructor                                                                                                              
        public SgeComponent(string name, string descritption, string serverName, string serverUserName, string serverUserPass, 
                             string serverPath, string scriptFileResourceName, int timeoutMilliSeconds)
                             : base(name, descritption, timeoutMilliSeconds)
        {
            this.serverName = serverName;
            this.serverUserName = serverUserName;
            this.serverUserPass = serverUserPass;
            this.serverPath = serverPath;
            this.scriptFileResourceName = scriptFileResourceName;
            this.argument = null;

            itemNames.Add(scriptFileResourceName);
        }


        // Methods                                                                                                                  
        public override void PrepareData()
        {
            PrepareFiles();
        }
        public override JobStatus Run()
        {
            JobStatus status;
            try
            {
                // for timeout we need start time
                DateTime start = DateTime.Now;

                Command command = GetRunCommand();
                string result = SgeCommunicators[serverName].RunCommnad(command);
                jobId = int.Parse(result.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)[2]);
                
                while (true)
                {
                    System.Threading.Thread.Sleep(SgeCommunicator.MilisecondsToWait);
                    status = SgeCommunicators[serverName].GetJobStatus(jobId);

                    if (status == JobStatus.OK) break;
                    else if (status == JobStatus.Failed)
                    {
                        SgeCommunicators[serverName].RunCommnad(GetKillCommnad());
                        throw new Exception("Job " + jobId + " was deleted because of an error.");
                    }
                    else if ((DateTime.Now - start).TotalMilliseconds > timeoutMilliSeconds)
                    {
                        status = JobStatus.TimedOut;
                        SgeCommunicators[serverName].RunCommnad(GetKillCommnad());
                        throw new Exception("Job " + jobId + " was deleted because of time limit.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (status != JobStatus.OK)
            {
                int i = 0;
            }
            else
            {
                int j = 1;
            }


            return status;
        }
        public override void Kill()
        {
            SgeCommunicators[serverName].RunCommnad(GetKillCommnad());
        }
        public override void GatherData()
        {
        }
        private void PrepareArgument()
        {
            foreach (string itemName in itemNames)
            {
                Item item = baseFrameworkItemCollection[itemName];
                if (item is VariableBase)
                {
                    VariableBase variable = (VariableBase)item;
                    argument = argument.Replace("@" + itemName + "@", variable.GetValueAsString());
                }
            }
        }
        private void PrepareFiles()
        {
            foreach (string itemName in itemNames)
            {
                Item item = baseFrameworkItemCollection[itemName];
                if (item is FileResource)
                {
                    ((FileResource)item).PrepareData(workingDirectory);
                }
            }
        }
        private Command GetRunCommand()
        {
            string outputDirectory = workingDirectory.Replace(Path.GetPathRoot(workingDirectory), "~/");
            outputDirectory = outputDirectory.Replace("\\", "/");

            string jobName = "job_" + workingDirectory.Remove(0, Directory.GetParent(workingDirectory).FullName.Length + 1);

            // get the script file resource item 
            Item item = FrameworkItemCollection[scriptFileResourceName];
            FileResource fileResource;
            if (item is FileResource)
            {
                fileResource = (FileResource)item;
            }
            else throw new Exception();

            Command command;
            command.rows = new string[2];

            command.rows[0] = "cd " + outputDirectory;
            command.rows[1] = "qsub";
            if (argument != null && argument.Length > 0) command.rows[1] += " " + argument;
            command.rows[1] += string.Format(" -N {0} {1}", jobName, fileResource.FileNameWithoutPath); // -N for adding job name; shell file

            return command;
        }
        private Command GetKillCommnad()
        {
            Command command;
            command.rows = new string[1];
            command.rows[0] = "qdel " + jobId;
            return command;
        }


        // IDependent
        public override List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }

        // IDeepCopy
        public Item DeepCopy()
        {
            SgeComponent copy = this.DeepClone();
            copy.SgeCommunicators = SgeCommunicators;
            return copy;
        }
        

    }
}
