﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public enum ReportType
    {
        Field,
        History
    }

    [Serializable]
    public class AbaqusComponent : DosComponent
    {
        // Variables                                                                                          
        static string abaqusBat = "abaqus.bat";
        private string inputFileResourceName;
        private string reportFileResourceName;
        private static SemaphoreSlim sem = new SemaphoreSlim(1);
        private static DateTime lastAccesTime = DateTime.Now;

        // Properties                                                                                         
        public string InputFileResourceName
        {
            get
            {
                return inputFileResourceName;
            }
            set
            {
                if (itemNames.Contains(inputFileResourceName)) itemNames.Remove(inputFileResourceName);
                inputFileResourceName = value;
                itemNames.Add(value);
            }
        }

        public string ReportFileResourceName
        {
            get
            {
                return reportFileResourceName;
            }
            set
            {
                if (itemNames.Contains(reportFileResourceName)) itemNames.Remove(reportFileResourceName);
                reportFileResourceName = value;
                itemNames.Add(value);
            }
        }

        public string JobName { get; set; }
        
        public int NumOfCpus { get; set; }

        public int MemorySizeGB { get; set; }

        public ReportType OdbReportType { get; set; }

        public new string Argument 
        {
            get
            {
                try
                {
                    Item item = baseFrameworkItemCollection[inputFileResourceName];
                    if (item != null && item is FileResource)
                    {
                        FileResource input = (FileResource)item;

                        if (ArgumentsToAdd == null) ArgumentsToAdd = "";

                        string scratchDirectory = "@workDirectory@";
                        if (workingDirectory != null && workingDirectory.Length > 0) scratchDirectory = workingDirectory;

                        return string.Format("job={0} analysis input={1} cpus={2} memory=\"{3} gb\" interactive scratch=\"{4}\" {5}",
                                              JobName,
                                              input.FileNameWithoutPath,
                                              NumOfCpus.ToString(),
                                              MemorySizeGB.ToString(),
                                              scratchDirectory,
                                              ArgumentsToAdd);
                    }
                    else throw new Exception("The input file resource name of the AbaqusComponent does not represent a file resource name or does not exist.");
                }
                catch (Exception ex)
                {
                    return "Error in argument.";
                }
            }
        }

        public string ArgumentsToAdd { get; set; }


        // Constructors                                                                                       
        public AbaqusComponent(string name, string descritption, string inputFileResourceName, string jobName, string reportFileResourceName, int timeoutMilliSeconds)
            : base(name, descritption, abaqusBat, false, timeoutMilliSeconds, null)
        {
            executable = abaqusBat;
            this.inputFileResourceName = inputFileResourceName;
            this.reportFileResourceName = reportFileResourceName;
            itemNames.Add(inputFileResourceName);
            itemNames.Add(reportFileResourceName);
            JobName = jobName;
            
            NumOfCpus = 1;
            MemorySizeGB = 1;
            OdbReportType = ReportType.Field;

            argument = null;
            ArgumentsToAdd = null;

            //if (sem == null) sem = new SemaphoreSlim(1);
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public override void PrepareData()
        {
            Item item = baseFrameworkItemCollection[inputFileResourceName];
            if (item == null || !(item is FileResource))
                throw new Exception("The input file resource name of the AbaqusComponent does not represent a file resource name or does not exist.");

            base.PrepareData();
        }

        public override JobStatus Run()
        {
            Item item = baseFrameworkItemCollection[inputFileResourceName];
            FileResource input = (FileResource)item;
            JobStatus status;

            sem.Wait(); // run Abaqus in 200 ms intervals
            if ((DateTime.Now - lastAccesTime).TotalMilliseconds < 200)
            {
                System.Threading.Thread.Sleep(200);
            }
            lastAccesTime = DateTime.Now;
            sem.Release();

            // analysis                                                                             
            status = RunAnalysis(input);

            if (status != JobStatus.OK)
            {
                return status;
            }
            else
            {
                // prepare results                                                                  
                status = RunOdbReport();
                return status;
            }
        }

        private JobStatus RunDataCheck(FileResource input)
        {
            argument = string.Format("job={0} datacheck input={1} scratch=\"{2}\"",
                                    JobName,
                                    input.FileNameWithoutPath,
                                    workingDirectory);
            return base.Run();
        }

        private JobStatus RunContinue(FileResource input)
        {
            argument = string.Format("job={0} continue input={1} cpus={2} memory=\"{3} gb\" interactive scratch=\"{4}\" {5}",
                                    JobName,
                                    input.FileNameWithoutPath,
                                    NumOfCpus.ToString(),
                                    MemorySizeGB.ToString(),
                                    workingDirectory,
                                    ArgumentsToAdd);
            return base.Run();
        }

        private JobStatus RunAnalysis(FileResource input)
        {
            argument = string.Format("job={0} analysis input={1} cpus={2} memory=\"{3} gb\" interactive scratch=\"{4}\" {5}",
                                    JobName,
                                    input.FileNameWithoutPath,
                                    NumOfCpus.ToString(),
                                    MemorySizeGB.ToString(),
                                    workingDirectory,
                                    ArgumentsToAdd);
            return base.Run();
        }

        private JobStatus RunOdbReport()
        {
            string output;
            if (OdbReportType == ReportType.Field) output = "field";
            else output = "history";

            argument = string.Format("odbreport job={0} {1}", JobName, output);
            return base.Run();
        }

        private void WaitForPreToExit()
        {
            string lockFile = System.IO.Path.Combine(workingDirectory, JobName + ".lck");
            int count;

            count = 0;
            int max = 6000;
            while (!System.IO.File.Exists(lockFile) && count < max)
            {
                System.Threading.Thread.Sleep(100);
                count++;
            }
            if (count >= max) throw new Exception();

            count = 0;
            while (System.IO.File.Exists(lockFile) && count < max)
            {
                System.Threading.Thread.Sleep(100);
                count++;
            }
            if (count >= max) throw new Exception();
        }

    }
}
