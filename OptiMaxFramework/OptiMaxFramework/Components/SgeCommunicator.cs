﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;


namespace OptiMaxFramework
{
    public class SgeCommunicator
    {
        // Variables                                                                                                                
        protected static object myLock;
        protected SshClient sshClient;
        protected DateTime statesTime;
        protected Dictionary<int, JobStatus> jobStates;
        public static int MilisecondsToWait = 1000;

        // Properties                                                                                                               
        // this does not work
        //public int MilisecondsToWait { get { return milisecondsToWait; } set { milisecondsToWait = value; } }
        // this does not work

        // Constructor                                                                                                              
        public SgeCommunicator(string serverName, string serverUserName, string serverUserPass)
        {
            myLock = new object();
            sshClient = new SshClient(serverName, serverUserName, serverUserPass);
            statesTime = DateTime.MinValue;
            jobStates = new Dictionary<int, JobStatus>();
        }


        // Methods                                                                                                                  
        private bool ConnectClientIfNotConnectd()
        {
            if (!sshClient.IsConnected)
            {
                try
                {
                    sshClient.Connect();
                }
                catch
                {
                    System.Threading.Thread.Sleep(1000);
                    sshClient.Connect();
                }
            }
            return sshClient.IsConnected;
        }
        private void GetJobStatesIfTimedout()
        {
            TimeSpan deltaTime = DateTime.Now - statesTime;
            if (deltaTime.TotalMilliseconds > MilisecondsToWait)
            {
                GetJobStates();
                statesTime = DateTime.Now;
            }
        }
        protected virtual void GetJobStates()
        {
            jobStates.Clear();

            var sshComm = sshClient.RunCommand("qstat");
            string result = sshComm.Result;
            string[] jobs = result.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            string[] jobData;

            for (int i = 2; i < jobs.Length; i++)
            {
                jobData = jobs[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (jobData[4].Contains("qw")) jobStates.Add(int.Parse(jobData[0]), JobStatus.InQueue);
                else if (jobData[4].Contains("r")) jobStates.Add(int.Parse(jobData[0]), JobStatus.Running);
                else if (jobData[4].Contains("E")) jobStates.Add(int.Parse(jobData[0]), JobStatus.Failed);
                else jobStates.Add(int.Parse(jobData[0]), JobStatus.OK);
            }
        }

        public string RunCommnad(Command command)
        {
            lock (myLock)
            {
                if (!ConnectClientIfNotConnectd()) throw new Exception("The ssh client can not be connected.");

                var sshComm = sshClient.RunCommand(command.ToString());
                System.Threading.Thread.Sleep(100);    // so that job gets put into queue
                return sshComm.Result;
            }
        }
        public JobStatus GetJobStatus(int jobId)
        {
            lock (myLock)
            {
                GetJobStatesIfTimedout();
            }

            if (jobStates.ContainsKey(jobId)) return jobStates[jobId];
            else return JobStatus.OK;
        }
        public void Disconnect()
        {
            lock (myLock)
            {
                if (sshClient.IsConnected) sshClient.Disconnect();
            }
        }
    }


}
