﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InterfaceDll;

namespace OptiMaxFramework
{
    [Serializable]
    public struct VariableDllNamePair
    {
        // Variables                                                                                                                
        private string variable;
        private string dllName;


        // Properties                                                                                                               
        public string Variable
        {
            get { return variable; }
        }

        public string DllName
        {
            get { return dllName; }
        }


        // Constructor                                                                                                              
        public VariableDllNamePair(string variable, string dllName)
        {
            this.variable = variable;
            this.dllName = dllName;
        }
    }

    [Serializable]
    public class DllComponent : Component
    {
        // Static
        public static ConcurrentDictionary<string, Type> typesDictionary = new ConcurrentDictionary<string, Type>();
        
        
        // Variables                                                                                                                
        private FileResource dllFile;
        private VariableDllNamePair[] inpVariableDllNamePairs;

        [NonSerialized]
        private Type managedDllType;
        [NonSerialized]
        private IManagedDll managedDll;
        [NonSerialized]
        private Thread workerThread;


        // Properties                                                                                                               
        public FileResource DllFile
        {
            get { return dllFile; }
        }
        public VariableDllNamePair[] InpVariableDllNamePairs
        {
            get { return inpVariableDllNamePairs; }
        }
        public int TimeoutMilliSeconds
        {
            get
            {
                return timeoutMilliSeconds;
            }
            set
            {
                timeoutMilliSeconds = value;
            }
        }
        override public string Application
        {
            get
            {
                return "Managed dll";
            }
        }


        // Constructor                                                                                                              
        public DllComponent(string name, string descritption, int timeoutMilliSeconds, string dllFileNameWithPath, VariableDllNamePair[] inpVariableDllNamePairs, string filesDirectory)
            : base(name, descritption, timeoutMilliSeconds)
        {
            kill = false;

            if (!File.Exists(dllFileNameWithPath))
                throw new Exception("Managed dll library '" + dllFileNameWithPath + "' defined in Dll component '" + name + "' does not exist.");

            dllFile = new FileResource("Dll", "", dllFileNameWithPath, FileIOType.Constant); // must be constant! input searches for @vars@
            dllFile.SetToFilesDirectory(filesDirectory);

            // Create dll
            managedDll = null;
            managedDllType = null;
            workerThread = null;
            

            if (inpVariableDllNamePairs == null) this.inpVariableDllNamePairs = null;
            else this.inpVariableDllNamePairs = (VariableDllNamePair[])(inpVariableDllNamePairs.Clone());

            foreach (VariableDllNamePair pair in this.inpVariableDllNamePairs) itemNames.Add(pair.Variable);
        }


        // Methods                                                                                                                  
        public static Type GetType(string fileName)
        {
            AppDomain domain = null;
            try
            {
                // Create application domain setup information.
                AppDomainSetup domainSetup = new AppDomainSetup();
                domainSetup.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
                domainSetup.ApplicationName = "Dll";

                //Create new domain with domain setup details
                domain = AppDomain.CreateDomain("Dll", null, domainSetup);

                //Create new instance of Loader class, where the dll is loaded.
                //This is loaded into the new application domian so it can be
                //unloaded later
                AssemblyLoader.Loader loader = (AssemblyLoader.Loader)domain.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName,
                                                                                                     "OptiMaxFramework.AssemblyLoader.Loader");

                //Call LoadDll method to load the assembly into the doamin
                Type type = loader.LoadAndReturnDllType(fileName, typeof(IManagedDll));

                return type;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                // Unload the application domain:
                if (domain != null) AppDomain.Unload(domain);
            }
        }
        public static IManagedDll GetDllObject(string fileName)
        {
            Type type = GetType(fileName);
            return (IManagedDll)Activator.CreateInstance(type);
        }
        public bool Check()
        {
            managedDllType = GetType(dllFile.FileNameWithPath);
            return managedDllType != null;
        }
        public override void PrepareData()
        {
            PrepareFiles();

            managedDllType = typesDictionary.GetOrAdd(dllFile.FileNameWithPath, GetType);
            
            //managedDllType = GetType(dllFile.FileNameWithPath);

            managedDll = (IManagedDll)Activator.CreateInstance(managedDllType);
            managedDll.SetWoringDirectory(workingDirectory);

            foreach (VariableDllNamePair pair in inpVariableDllNamePairs)
            {
                Item item = baseFrameworkItemCollection[pair.Variable];

                if (item is VariableBase vb) managedDll.SetVariableByName(pair.DllName, vb.GetValueAsString());
                else if (item is ArrayBase ab) managedDll.SetArrayByName(pair.DllName, ab.GetValuesAsString());
            }
        }
        public override JobStatus Run()
        {
            Boolean timeOut = false;
            Boolean finished = false;

            ManualResetEvent wait = new ManualResetEvent(false);
            // In the above code, we initialize the ManualResetEvent with false value,
            // that means all the threads which calls the WaitOne method will block until
            // some thread calls the Set() method.

            workerThread = new Thread(new ThreadStart(() =>
            {
                managedDll.Run();
                finished = true;
                wait.Set();
            }));

            workerThread.Start();
            wait.WaitOne(timeoutMilliSeconds);

            if (! finished && workerThread.IsAlive)
            {
                workerThread.Abort();
                timeOut = true;
            }

            if (kill) return JobStatus.Killed;
            else if (timeOut) return JobStatus.TimedOut;
            else return JobStatus.OK;
        }
        public override void GatherData()
        {
            double valueDouble;
            double[] valuesDouble;

            string valueString;

            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is VariableOutput vo)
                {
                    if (vo.Template is DataMiningTemplateDll dmt)
                    {
                        if (vo.OutputItemName == this.name)
                        {
                            valueString = managedDll.GetVariableByNameAsString(dmt.DllName, Globals.nfi);
                            if (valueString == null) throw new Exception("The variable '" + dmt.DllName + "' is null ater reading it from the dll.");

                            if (vo.ValueType == VariableValueType.Double || vo.ValueType == VariableValueType.Integer)
                            {
                                valueDouble = double.Parse(valueString, Globals.nfi);
                                vo.SetDoubleValueFromOutputItem(valueDouble);
                            }
                            else if (vo.ValueType == VariableValueType.String)
                            {
                                vo.SetStringValueFromOutputItem(valueString);
                            }
                        }
                    }
                }
                else if (item is ArrayOutput ao)
                {
                    ao = (ArrayOutput)item;
                    if (ao.Template is DataMiningTemplateDll dmt)
                    {
                        if (ao.OutputItemName == this.name)
                        {
                            valueString = managedDll.GetArrayByNameAsString(dmt.DllName, Globals.nfi);
                            if (valueString == null) throw new Exception("The variable '" + dmt.DllName + "' is null ater reading it from the dll.");

                            string[] tmp = valueString.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            valuesDouble = new double[tmp.Length];
                            for (int i = 0; i < tmp.Length; i++) valuesDouble[i] = double.Parse(tmp[i]);
                            ao.SetValueFromOutputItem(valuesDouble);
                        }
                    }
                }
            }
        }
        public override void Kill()
        {
            //if (workerThread != null && workerThread.IsAlive)
            //{
            //    workerThread.Abort();
            //}
            kill = true;
        }

        private void PrepareFiles()
        {
            foreach (string itemName in itemNames)
            {
                Item item = baseFrameworkItemCollection[itemName];
                if (item is FileResource)
                {
                    ((FileResource)item).PrepareData(workingDirectory);
                }
            }
        }

        // IDependent
        public override List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }
       
    }
}
