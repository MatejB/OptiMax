﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace OptiMaxFramework.AssemblyLoader
{
    [Serializable]
    class Loader
    {
        public Type LoadAndReturnDllType(string fileName, Type dllComponentType)
        {
            if (File.Exists(fileName))
            {
                //Loads file into Bytle array - prevent file locking - this does not work for parallel threads (semaphore)
                byte[] rawAssembly = LoadFile(fileName);
                //algorithmAssembly = Assembly.LoadFrom(filename);
                Assembly algorithmAssembly = Assembly.Load(rawAssembly);

                // this works for parallel threads, but does not release the dll after AppDpomain.Unload
                //AssemblyName an = AssemblyName.GetAssemblyName(fileName);
                //Assembly algorithmAssembly = Assembly.Load(an);


                foreach (Type type in algorithmAssembly.GetExportedTypes())
                {
                    if (type.IsInterface || type.IsAbstract) continue;
                    else
                    {
                        if (type.GetInterface(dllComponentType.FullName) != null)
                        {
                            return type;
                        }
                    }
                }
            }
            throw new Exception("Managed dll library '" + fileName + "' does not implement OptiMaxFramework.IManagedDll interface.");
        }

        // Loads the content of a file to a byte array. Prevent file locking, so user can modify dll while using the dll
        static byte[] LoadFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open);
            byte[] buffer = new byte[(int)fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            return buffer;
        }

    } 

}