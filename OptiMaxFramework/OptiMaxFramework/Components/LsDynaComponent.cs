﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class LsDynaComponent : DosComponent
    {
        // Variables                                                                                          
        //static string dynaSolver;
        private string inputFileResourceName;
        private string outputFileResourceName;

        // Properties                                                                                         
        public string InputFileResourceName
        {
            get
            {
                return inputFileResourceName;
            }
            set
            {
                if (itemNames.Contains(inputFileResourceName)) itemNames.Remove(inputFileResourceName);
                inputFileResourceName = value;
                itemNames.Add(value);
            }
        }

        public string OutputFileResourceName
        {
            get
            {
                return outputFileResourceName;
            }
            set
            {
                if (itemNames.Contains(outputFileResourceName)) itemNames.Remove(outputFileResourceName);
                outputFileResourceName = value;
                itemNames.Add(value);
            }
        }


        
        public int NumOfCpus { get; set; }

        public int MemorySizeWords { get; set; }

        public new string Argument 
        {
            get
            {
                if (baseFrameworkItemCollection.ContainsKey(inputFileResourceName))
                {
                    Item item = baseFrameworkItemCollection[inputFileResourceName];
                    if (item != null && item is FileResource)
                    {
                        FileResource input = (FileResource)item;

                        if (ArgumentsToAdd == null) ArgumentsToAdd = "";

                        return "i=" + input.FileNameWithoutPath + " ncpu=" + NumOfCpus.ToString()
                                + " memory=" + MemorySizeWords.ToString() + " "
                                + ArgumentsToAdd;
                    }
                    else throw new Exception("The input file resource name '" + inputFileResourceName + "' of the LsDynaComponent does not represent a file resource name.");
                }
                else throw new Exception("The input file resource name '" + inputFileResourceName + "' of the LsDynaComponent does not exist.");
            }
        }

        public string ArgumentsToAdd { get; set; }


        // Constructors                                                                                       
        public LsDynaComponent(string name, string descritption, string dynaSolver, string inputFileResourceName, string outputFileResourceName, int timeoutMilliSeconds)
            : base(name, descritption, dynaSolver, false, timeoutMilliSeconds, null)
        {
            executable = dynaSolver;
            this.inputFileResourceName = inputFileResourceName;
            this.outputFileResourceName = outputFileResourceName;
            itemNames.Add(inputFileResourceName);
            itemNames.Add(outputFileResourceName);
            
            NumOfCpus = 1;
            MemorySizeWords = 250000; // 1Gb on 32 bit machine

            argument = null;
            ArgumentsToAdd = null;
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public override void PrepareData()
        {
            base.Argument = Argument;

            base.PrepareData();
        }

        public override JobStatus Run()
        {
            // analysis
            return base.Run();
        }
    }
}
