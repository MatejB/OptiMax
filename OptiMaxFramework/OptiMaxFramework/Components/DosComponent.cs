﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;

namespace OptiMaxFramework
{
    [Serializable]
    public struct EnvironmentVariable
    {
        public bool Include;
        public string Name;
        public string Value;
    }

    [Serializable]
    public class DosComponent : Component
    {
        // Variables                                                                                                                
        protected string executable;
        protected string argument;
        protected bool useLocalExecutable;
        private EnvironmentVariable[] environmentVariables;
        private DataReceivedEventHandler dataReceivedHandler;
        private DataReceivedEventHandler errorReceivedHandler;

        [NonSerializedAttribute]
        protected Process exe;

        // Properties                                                                                                               
        public string Executable { get { return executable; } set { executable = value; } }
        public string Argument { get { return argument; } set { argument = value; } }
        public string[] ArgumentParameters
        {
            get
            {
                string[] itemNames;
                string[] parameterNames;
                FileResource.GetParameterNames(argument, out itemNames, out parameterNames);
                return itemNames;
            }
        }
        public int TimeoutMilliSeconds { get { return timeoutMilliSeconds; } set { timeoutMilliSeconds = value; } }
        public bool UseLocalExe { get { return useLocalExecutable; } set { useLocalExecutable = value; } }
        public EnvironmentVariable[] EnvironmentVariables { get { return environmentVariables; } set { environmentVariables = value; } }
        override public string Application { get { return executable; } }


        // Constructor                                                                                                              
        public DosComponent(string name, string descritption, string executable, bool useLocalExecutable, int timeoutMilliSeconds, EnvironmentVariable[] environmentVariables) 
            : base(name, descritption, timeoutMilliSeconds)
        {
            this.executable = executable;
            this.useLocalExecutable = useLocalExecutable;
            this.environmentVariables = environmentVariables;
            argument = null;
            exe = null;
        }


        // Methods                                                                                                                  
        public override void PrepareData()
        {
            if (argument != null)
            {
                if (argument.Contains("@workDirectory@") && workingDirectory != null && workingDirectory.Length > 0)
                    argument = argument.Replace("@workDirectory@", workingDirectory);

                PrepareArgument();
            }
            PrepareFiles();
        }
        public override JobStatus Run()
        {
            if (Globals.IsWindows10orNewer()) return Run_Win10();
            else return Run_OldWin();
        }
        public JobStatus Run_OldWin()
        {
            string workExecutable;
            if (useLocalExecutable) workExecutable = Path.Combine(workingDirectory, Path.GetFileName(executable));
            else workExecutable = executable;

            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = workExecutable;
            psi.Arguments = argument;
            psi.WorkingDirectory = workingDirectory;
            //psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;

            try
            {
                if (environmentVariables != null && environmentVariables.Length > 0)
                {
                    foreach (var envVariable in environmentVariables)
                    {
                        if (envVariable.Include)
                        {
                            psi.EnvironmentVariables.Add(envVariable.Name, envVariable.Value);
                        }
                    }

                }
            }
            catch
            {
                throw new Exception("To set Environment Variable the program must be run with elevated permisions (Run as administrator).");
            }

            string outputName = Path.GetFileName(workExecutable) + "_" + argument.Substring(0, Math.Min(argument.Length, 15));

            Debug.WriteLine(DateTime.Now + "   Start proces: " + outputName + " in: " + workingDirectory);

            kill = false;
            exe = new Process();
            exe.StartInfo = psi;
            exe.Start();

            if (exe.WaitForExit(timeoutMilliSeconds))
            {
                // Process completed. Check process.ExitCode here.
                // after Kill() _jobStatus is Killed
            }
            else
            {
                // Timed out.
                KillOnTimedout();
                Debug.WriteLine(DateTime.Now + "   Timeout proces: " + outputName + " in: " + workingDirectory);
                return JobStatus.TimedOut;
            }
            exe.Close();

            Debug.WriteLine(DateTime.Now + "   End proces: " + outputName + " in: " + workingDirectory);
            return JobStatus.OK;
        }
        public JobStatus Run_Win10()
        {
            string workExecutable;
            if (useLocalExecutable) workExecutable = Path.Combine(workingDirectory, Path.GetFileName(executable));
            else workExecutable = executable;

            ProcessStartInfo psi = new ProcessStartInfo();
            // Using CreateNoWindow requires UseShellExecute to be false, if true window style is used 
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.FileName = workExecutable;
            psi.Arguments = argument;
            psi.WorkingDirectory = workingDirectory;
            
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            
            try
            {
                if (environmentVariables != null && environmentVariables.Length > 0)
                {
                    foreach (var envVariable in environmentVariables)
                    {
                        if (envVariable.Include)
                        {
                            psi.EnvironmentVariables.Add(envVariable.Name, envVariable.Value);
                        }
                    }

                }
            }
            catch
            {
                throw new Exception("To set Environment Variable the program must be run with elevated permisions (Run as administrator).");
            }

            string outputName = Path.GetFileName(workExecutable);
            string outputFileName = Path.Combine(workingDirectory, "_output_" + outputName + ".txt");
            string errorFileName = Path.Combine(workingDirectory, "_error_" + outputName + ".txt");

            File.AppendAllText(outputFileName, "Command: " + workExecutable + " " + argument + Environment.NewLine + Environment.NewLine);

            Debug.WriteLine(DateTime.Now + "   Start proces: " + outputName + " in: " + workingDirectory);

            kill = false;
            exe = new Process();
            exe.StartInfo = psi;
            
            using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                dataReceivedHandler = (sender, e) =>
                {
                    if (e.Data == null)
                    {
                        // the safe wait handle closes on kill
                        if (!outputWaitHandle.SafeWaitHandle.IsClosed) outputWaitHandle.Set();
                    }
                    else
                    {
                        //output.AppendLine(e.Data);
                        File.AppendAllText(outputFileName, e.Data + Environment.NewLine);
                    }
                };
                this.exe.OutputDataReceived += dataReceivedHandler;

                errorReceivedHandler = (sender, e) =>
                {
                    if (e.Data == null)
                    {
                        // the safe wait handle closes on kill
                        if (!errorWaitHandle.SafeWaitHandle.IsClosed) errorWaitHandle.Set();
                    }
                    else
                    {
                        //error.AppendLine(e.Data);
                        File.AppendAllText(errorFileName, e.Data + Environment.NewLine);
                    }
                };
                this.exe.ErrorDataReceived += errorReceivedHandler;

                exe.Start();

                exe.BeginOutputReadLine();
                exe.BeginErrorReadLine();
                
                if (exe.WaitForExit(timeoutMilliSeconds) &&
                    outputWaitHandle.WaitOne(timeoutMilliSeconds) &&
                    errorWaitHandle.WaitOne(timeoutMilliSeconds))
                {
                    // Process completed. Check process.ExitCode here.
                }
                else
                {
                    // Timed out.
                    KillOnTimedout();
                    Debug.WriteLine(DateTime.Now + "   Timeout proces: " + outputName + " in: " + workingDirectory);
                    return JobStatus.TimedOut;
                }
                exe.Close();
            }

            Debug.WriteLine(DateTime.Now + "   End proces: " + outputName + " in: " + workingDirectory);
            return JobStatus.OK;
        }
        public void KillOnTimedout()
        {
            kill = true;
            Globals.KillAllProcessesSpawnedBy((UInt32)exe.Id);

            if (exe != null)
            {
                if (Process.GetProcesses().Any(x => x.Id == exe.Id))
                {
                    exe.Kill();
                    exe.Close();

                    this.exe.OutputDataReceived -= dataReceivedHandler;
                    this.exe.ErrorDataReceived -= errorReceivedHandler;
                }
            }

            dataReceivedHandler = null;
            errorReceivedHandler = null;
        }
        public override void Kill()
        {
            kill = true;
            Globals.KillAllProcessesSpawnedBy((UInt32)exe.Id);
            exe.Kill();
        }

        public override void GatherData()
        {
        }
        private void PrepareArgument()
        {
            FileResource.ReplaceAllParameterNamesWithValues(ref argument, baseFrameworkItemCollection);
        }
        private void PrepareFiles()
        {
            foreach (string itemName in itemNames)
            {
                Item item = baseFrameworkItemCollection[itemName];
                if (item is FileResource)
                {
                    ((FileResource)item).PrepareData(workingDirectory);
                }
            }
        }


        // IDependent
        public override List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }



       

    }
}



























