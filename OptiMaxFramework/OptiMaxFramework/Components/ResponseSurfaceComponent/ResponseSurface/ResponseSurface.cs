﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework;

namespace ResponseSurface
{
    [Serializable]
    public abstract class ResponseSurfaceBase
    {
        // Variables                                                                                                                
        protected int _numOfParameters;
        protected int _numOfDataValues;
        protected double[][] _parameterValues;
        protected double[] _functionValues;

        protected int _numOfCoefficients;
        protected double[] _coefficients;


        // Constructors                                                                                                             
        public ResponseSurfaceBase(double[][] parameterValues, double[] functionValues)
        {
            if (parameterValues.Length != functionValues.Length)
                throw new Exception("The number of rows for parameter values and function values is not the same.");

            int len = parameterValues[0].Length;
            for (int i = 1; i < parameterValues.Length; i++)
            {
                if (parameterValues[i].Length != len)
                    throw new Exception("The number of parameters is not the same in each row of the provided data.");
            }
            _parameterValues = parameterValues;
            _functionValues = functionValues;

            _numOfParameters = len;
            _numOfDataValues = _functionValues.Length;
        }


        // Methods                                                                                                                  
        public abstract double GetValue(double[] parameters);
        public abstract double CoefficientOfDetermination();
        public string GetEquation(string[] variableNames, int numOfSignificantDigits)
        {
            double max = -double.MaxValue;
            double epsilon = Math.Pow(10, -numOfSignificantDigits);
            for (int i = 0; i < _coefficients.Length; i++)
            {
                if (Math.Abs(_coefficients[i]) > max) max = Math.Abs(_coefficients[i]);
            }
            epsilon = max * epsilon;


            int count = 0;
            string equation = "";
            string[] partialEquations = GetPartialFunctionEquations(variableNames);
            for (int i = 0; i < _numOfCoefficients; i++)
            {
                //if (Math.Abs(_coefficients[i]) > epsilon)
                {
                    if (i == 0)
                    {
                        equation += _coefficients[i].RoundToSignificantDigits(numOfSignificantDigits).ToString(Globals.nfi);
                    }
                    else
                    {
                        if (count != 0 && _coefficients[i] > 0) equation += "+";
                        equation += _coefficients[i].RoundToSignificantDigits(numOfSignificantDigits).ToString(Globals.nfi) + "*" + partialEquations[i];
                    }
                    count++;
                }
            }
            return equation;
        }
        protected abstract string[] GetPartialFunctionEquations(string[] variableNames);

    }
}
