﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearRegression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResponseSurface
{
    [Serializable]
    public class LinearResponseSurface : ResponseSurfaceBase
    {
        // Variables                                                                                                                


        // Constructors                                                                                                             
        /// <summary>
        /// LinearResponseSurface computes linear response surface based on parameter and function values
        /// </summary>
        /// <param name="parameterValues">parameter values given in a row based matrix; first column represents first parameter...</param>
        /// <param name="functionValues">function values</param>
        public LinearResponseSurface(double[][] parameterValues, double[] functionValues)
            : base(parameterValues, functionValues)
        {
            _numOfCoefficients = (_numOfParameters + 1);

            if (functionValues.Length < _numOfCoefficients)
                throw new Exception("To few parameter/function values to fit the surface.");

            Fit();
        }


        // Methods                                                                                                                  
        private void Fit()
        {
            Matrix<double> m = new DenseMatrix(_numOfDataValues, _numOfCoefficients);
            for (int i = 0; i < _numOfDataValues; i++)
            {
                m.SetRow(i, GetPartialFunctionValues(_parameterValues[i]));
            }

            Vector<double> y = Vector.Build.DenseOfArray(_functionValues);

            _coefficients = MultipleRegression.QR(m, y).AsArray();
        }
        public override double CoefficientOfDetermination()
        {
            double[] modeledValues = new double[_functionValues.Length];
            for (int i = 0; i < modeledValues.Length; i++)
            {
                modeledValues[i] = GetValue(_parameterValues[i]);
            }

            double R2 = MathNet.Numerics.GoodnessOfFit.CoefficientOfDetermination(modeledValues, _functionValues);
            return R2;
        }
        public override double GetValue(double[] parameterValues)
        {
            if (parameterValues.Length != _numOfParameters)
                throw new Exception("The number of parameters is " + parameterValues.Length + " instead of " + _numOfParameters + ".");

            double result = 0;
            double[] partialFunctions = GetPartialFunctionValues(parameterValues);
            for (int i = 0; i < _numOfCoefficients; i++)
            {
                result += _coefficients[i] * partialFunctions[i];
            }
            return result;
        }

        private double[] GetPartialFunctionValues(double[] x)
        {
            int count = 0;
            double[] fx = new double[_numOfCoefficients];

            // constant
            fx[count++] = 1;
            // linear
            for (int i = 0; i < x.Length; i++) fx[count++] = x[i];

            return fx;
        }
        protected override string[] GetPartialFunctionEquations(string[] variableNames)
        {
            int count = 0;
            string[] names = new string[_numOfCoefficients];

            // constant
            names[count++] = "1";
            // linear
            for (int i = 0; i < variableNames.Length; i++) names[count++] = variableNames[i];

            return names;
        }
    }
}















