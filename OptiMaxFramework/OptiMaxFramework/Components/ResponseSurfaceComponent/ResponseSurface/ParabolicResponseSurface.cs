﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearRegression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework;

namespace ResponseSurface
{
    [Serializable]
    public class ParabolicResponseSurface : ResponseSurfaceBase
    {
        // Variables                                                                                                                


        // Constructors                                                                                                             
        /// <summary>
        /// ParabolicResponseSurface computes parabolic response surface based on parameter and function values
        /// </summary>
        /// <param name="parameterValues">parameter values given in a row based matrix; first column represents first parameter...</param>
        /// <param name="functionValues">function values</param>
        public ParabolicResponseSurface(double[][] parameterValues, double[] functionValues)
            : base(parameterValues, functionValues)
        {
            _numOfCoefficients = (_numOfParameters + 1) * (_numOfParameters + 2) / 2;

            if (functionValues.Length < _numOfCoefficients)
                throw new Exception("To few parameter/function values to fit the surface.");

            Fit();
        }


        // Methods                                                                                                                  
        private void Fit()
        {
            Matrix<double> m = new DenseMatrix(_numOfDataValues, _numOfCoefficients);
            for (int i = 0; i < _numOfDataValues; i++)
            {
                m.SetRow(i, GetPartialFunctionValues(_parameterValues[i]));
            }

            Vector<double> y = Vector.Build.DenseOfArray(_functionValues);

            _coefficients = MultipleRegression.QR(m, y).AsArray();
        }
        public override double CoefficientOfDetermination()
        {
            double[] modeledValues = new double[_functionValues.Length];
            for (int i = 0; i < modeledValues.Length; i++)
            {
                modeledValues[i] = GetValue(_parameterValues[i]);
            }

            double R2 = MathNet.Numerics.GoodnessOfFit.CoefficientOfDetermination(modeledValues, _functionValues);
            return R2;
        }
        public override double GetValue(double[] parameterValues)
        {
            if (parameterValues.Length != _numOfParameters)
                throw new Exception("The number of parameters is " + parameterValues.Length + " instead of " + _numOfParameters + ".");

            double result = 0;
            double[] partialFunctions = GetPartialFunctionValues(parameterValues);
            for (int i = 0; i < _numOfCoefficients; i++)
            {
                result += _coefficients[i] * partialFunctions[i];
            }
            return result;
        }

        private double[] GetPartialFunctionValues(double[] x)
        {
            int count = 0;
            double[] fx = new double[_numOfCoefficients];

            // constant
            fx[count++] = 1;
            // linear
            for (int i = 0; i < x.Length; i++) fx[count++] = x[i];
            // parabolic
            for (int i = 0; i < x.Length; i++) fx[count++] = Math.Pow(x[i], 2);
            // parabolic mixed
            for (int i = 0; i < x.Length - 1; i++)
            {
                for (int j = i + 1; j < x.Length; j++)
                {
                    fx[count++] = x[i] * x[j];
                }
            }

            return fx;
        }
        protected override string[] GetPartialFunctionEquations(string[] variableNames)
        {
            int count = 0;
            string[] names = new string[_numOfCoefficients];

            // constant
            names[count++] = "1";
            // linear
            for (int i = 0; i < variableNames.Length; i++) names[count++] = variableNames[i];
            // parabolic
            for (int i = 0; i < variableNames.Length; i++) names[count++] = variableNames[i] + "^2";
            // parabolic mixed
            for (int i = 0; i < variableNames.Length - 1; i++)
            {
                for (int j = i + 1; j < variableNames.Length; j++)
                {
                    names[count++] = variableNames[i] + "*" + variableNames[j];
                }
            }

            return names;
        }
    }
}












