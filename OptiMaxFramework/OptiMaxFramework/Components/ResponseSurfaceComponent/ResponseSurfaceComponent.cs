﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Helpers;
using ResponseSurface;

namespace OptiMaxFramework
{
    [Serializable]
    public enum ResponseSurfaceType
    {
        Linear,
        Parabolic
    }

    [Serializable]
    public struct VariableArrayPair
    {
        // Variables                                                                                                                
        private string variable;
        private string array;


        // Properties                                                                                                               
        public string Variable
        {
            get { return variable; }
        }

        public string Array
        {
            get { return array; }
        }


        // Constructor                                                                                                              
        public VariableArrayPair(string variable, string array)
        {
            this.variable = variable;
            this.array = array;
        }
    }

    [Serializable]
    public class ResponseSurfaceComponent : Component, IRecordable
    {
        // Variables                                                                                                                
        private VariableArrayPair[] inputVariableArrayPairs;
        private string[] outputArrayNames;
        private string[] outputVariableNames;
        private ResponseSurfaceType responseSurfaceType;
        private ResponseSurfaceBase[] responseSurfaces;
        private double[] results;

        private int numberOfInputArrays;
        private int numberOfOutputArrays;
        private int numberOfValues;

        private string statusData;

        // Properties                                                                                                               
        public int TimeoutMilliSeconds
        {
            get
            {
                return timeoutMilliSeconds;
            }
            set
            {
                timeoutMilliSeconds = value;
            }
        }
        override public string Application
        {
            get
            {
                return "Response Surface";
            }
        }
        public VariableArrayPair[] InputVariableArrayPairs { get { return inputVariableArrayPairs; } }
        public string[] OutputArrayNames { get { return outputArrayNames; } }
        public ResponseSurfaceType ResponseSurfaceType { get { return responseSurfaceType; } }
        

        // Constructor                                                                                                              
        public ResponseSurfaceComponent(string name, string descritption, int timeoutMilliSeconds,
                                        VariableArrayPair[] inputVariableArrayPairs, string[] outputArrayNames,
                                        ResponseSurfaceType responseSurfaceType)
            : base(name, descritption, timeoutMilliSeconds)
        {
            kill = false;

            this.inputVariableArrayPairs = inputVariableArrayPairs;
            this.outputArrayNames = outputArrayNames;
            this.responseSurfaceType = responseSurfaceType;
            this.responseSurfaces = null;
            this.results = null;
            this.statusData = null;

            numberOfInputArrays = inputVariableArrayPairs.Length;
            numberOfOutputArrays = outputArrayNames.Length;

            foreach (VariableArrayPair pair in this.inputVariableArrayPairs)
            {
                itemNames.Add(pair.Variable);
                itemNames.Add(pair.Array);
            }
            foreach (string arrayName in outputArrayNames)
            {
                itemNames.Add(arrayName);
            }
        }

        // Methods                                                                                                                  
        public override void PrepareData()
        {
            outputVariableNames = new string[outputArrayNames.Length];
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is VariableOutput varOut)
                {
                    if (varOut.Template is DataMiningTemplateResponseSurface dmtrs)
                    {
                        if (varOut.OutputItemName == this.name)
                        {
                            if (varOut.ValueType == VariableValueType.Double || varOut.ValueType == VariableValueType.Integer)
                            {
                                int id = -1;
                                for (int i = 0; i < outputArrayNames.Length; i++)
                                {
                                    if (dmtrs.OutputArrayName == outputArrayNames[i])
                                    {
                                        id = i;
                                        break;
                                    }
                                }
                                if (id == -1) throw new Exception("Variable output '" + varOut.Name + "' is linked to non existing item.");

                                outputVariableNames[id] = varOut.Name;
                            }
                            else throw new NotSupportedException();
                        }
                    }
                }
            }
        }
        public override JobStatus Run()
        {
            try
            {
                double[] parameterValues = new double[inputVariableArrayPairs.Length];

                Stopwatch watch = new Stopwatch();
                watch.Start();

                // get current parameter values
                for (int i = 0; i < inputVariableArrayPairs.Length; i++)
                {
                    if (kill) return JobStatus.Killed;
                    if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;

                    Item item = baseFrameworkItemCollection[inputVariableArrayPairs[i].Variable];
                    if (item is VariableBase)
                    {
                        VariableBase variable = (VariableBase)item;
                        parameterValues[i] = double.Parse(variable.GetValueAsString(), Globals.nfi);
                    }
                    else throw new NotSupportedException();
                }

                // create responce surfaces
                JobStatus status = CreateResponseSurfaces(watch);
                if (status != JobStatus.OK) return status;

                // prepare equations
                CreateStatusData();

                // gather results
                results = new double[numberOfOutputArrays];
                for (int i = 0; i < numberOfOutputArrays; i++)
                {
                    if (kill) return JobStatus.Killed;
                    if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;

                    results[i] = responseSurfaces[i].GetValue(parameterValues);
                }
                
                return JobStatus.OK;
            }
            finally
            {
            }
        }
        public override void GatherData()
        {
            double valueDouble;
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is VariableOutput varOut)
                {
                    if (varOut.Template is DataMiningTemplateResponseSurface dmtrs)
                    {
                        if (varOut.OutputItemName == this.name)
                        {
                            if (varOut.ValueType == VariableValueType.Double || varOut.ValueType == VariableValueType.Integer)
                            {
                                int id = -1;
                                for (int i = 0; i < outputArrayNames.Length; i++)
                                {
                                    if (dmtrs.OutputArrayName == outputArrayNames[i])
                                    {
                                        id = i;
                                        break;
                                    }
                                }
                                if (id == -1) throw new Exception("Variable output '" + varOut.Name + "' is linked to non existing item.");

                                valueDouble = results[id];
                                varOut.SetDoubleValueFromOutputItem(valueDouble);
                            }
                            else throw new NotSupportedException();
                        }
                    }
                }
            }
        }
        public override void Kill()
        {
            kill = true;
            //throw new NotSupportedException();
        }
        public string GetStatusData()
        {
            return statusData;
        }

        private JobStatus CreateResponseSurfaces(Stopwatch watch)
        {
            // get input values
            string[] stringValues;
            double[][] inputValues = new double[inputVariableArrayPairs.Length][];

            for (int i = 0; i < inputVariableArrayPairs.Length; i++)
            {
                if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;

                Item item = baseFrameworkItemCollection[inputVariableArrayPairs[i].Array];
                if (item is ArrayBase ab)
                {
                    // get all array values by calling the GetValuesAsString only once
                    stringValues = ab.GetValuesAsString().Split(' ');

                    if (i == 0) numberOfValues = stringValues.Length;
                    else if (numberOfValues != stringValues.Length)
                        throw new Exception("The number of values for each input array in Response surface component must be the same.");

                    inputValues[i] = new double[numberOfValues];
                    for (int j = 0; j < numberOfValues; j++)
                    {
                        inputValues[i][j] = double.Parse(stringValues[j], Globals.nfi);
                    }
                }
            }

            // get output values
            double[][] outputValues = new double[outputArrayNames.Length][];

            for (int i = 0; i < outputArrayNames.Length; i++)
            {
                if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;

                Item item = baseFrameworkItemCollection[outputArrayNames[i]];
                if (item is ArrayBase ab)
                {
                    // get all array values by calling the GetValuesAsString only once
                    stringValues = ab.GetValuesAsString().Split(' ');

                    if (numberOfValues != stringValues.Length)
                        throw new Exception("The number of values for each output array in Response surface component must " +
                                            "be the same and equal to the number of values in the input arrays.");

                    outputValues[i] = new double[numberOfValues];
                    for (int j = 0; j < numberOfValues; j++)
                    {
                        outputValues[i][j] = double.Parse(stringValues[j], Globals.nfi);
                    }
                }
            }

            // parameters
            double[][] parameterValues = new double[numberOfValues][];     // [valueId][inputVariableId]
            for (int i = 0; i < numberOfValues; i++)
            {
                parameterValues[i] = new double[numberOfInputArrays];
                for (int j = 0; j < numberOfInputArrays; j++)
                {
                    parameterValues[i][j] = inputValues[j][i];
                }
            }

            responseSurfaces = new ResponseSurfaceBase[numberOfOutputArrays];
            for (int i = 0; i < responseSurfaces.Length; i++)
            {
                if (watch.ElapsedMilliseconds > timeoutMilliSeconds) return JobStatus.TimedOut;

                if (responseSurfaceType == ResponseSurfaceType.Linear)
                    responseSurfaces[i] = new LinearResponseSurface(parameterValues, outputValues[i]);
                else if (responseSurfaceType == ResponseSurfaceType.Parabolic)
                    responseSurfaces[i] = new ParabolicResponseSurface(parameterValues, outputValues[i]);
                else throw new NotSupportedException();
            }

            return JobStatus.OK;
        }
      
        private void CreateStatusData()
        {
            string[] variables = new string[inputVariableArrayPairs.Length];
            for (int i = 0; i < inputVariableArrayPairs.Length; i++)
            {
                variables[i] = inputVariableArrayPairs[i].Variable;
            }

            int numOfSignificantDigits = 6;
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}:", name);
            for (int i = 0; i < responseSurfaces.Length; i++)
            {
                sb.Append("   ");
                sb.AppendFormat("{0}={1} R={2}", outputVariableNames[i], 
                                                 responseSurfaces[i].GetEquation(variables, numOfSignificantDigits),
                                                 responseSurfaces[i].CoefficientOfDetermination().RoundToSignificantDigits(numOfSignificantDigits));
            }
            statusData = sb.ToString();
        }

        
        // IDependent
        public override List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }
    }
}
