﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class Component : Item, IDependent
    {
        // Variables                                                                                                                
        protected string workingDirectory;
        protected List<string> itemNames;
        protected int timeoutMilliSeconds;
        protected bool kill;

        // Constructors                                                                                                             
        public Component(string name, string description, int timeoutMilliSeconds)
            : base(name, description)
        {
            this.timeoutMilliSeconds = timeoutMilliSeconds;
            workingDirectory = null;
            itemNames = new List<string>();
        }

        // Methods                                                                                                                  
        public void Add(string name)
        {
            itemNames.Add(name);
        }

        public JobStatus Execute(string workingDirectory)
        {
            this.workingDirectory = workingDirectory;

            PrepareData();
            JobStatus code = Run();
            GatherData();

            return code;
        }

        abstract public string Application { get; }

        abstract public void PrepareData();

        abstract public JobStatus Run();

        abstract public void Kill();

        abstract public void GatherData();

        // IDependent
        abstract public List<string> InputVariables
        {
            get;
        }
        
    }
}
