﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;


namespace OptiMaxFramework
{
    public class SlurmCommunicator : SgeCommunicator
    {
        // Variables                                                                                                                


        // Properties                                                                                                               


        // Constructor                                                                                                              
        public SlurmCommunicator(string serverName, string serverUserName, string serverUserPass)
            : base(serverName, serverUserName, serverUserPass)
        {
        }


        // Methods                                                                                                                  
        protected override void GetJobStates()
        {
            try
            {
                jobStates.Clear();
                var sshComm = sshClient.RunCommand("squeue");
                string result = sshComm.Result;
                string[] jobs = result.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                string[] jobData;

                for (int i = 1; i < jobs.Length; i++)
                {
                    jobData = jobs[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    if (jobData[4].Contains("PD")) jobStates.Add(int.Parse(jobData[0]), JobStatus.InQueue);
                    else if (jobData[4].Contains("R")) jobStates.Add(int.Parse(jobData[0]), JobStatus.Running);
                    else if (jobData[4].Contains("F")) jobStates.Add(int.Parse(jobData[0]), JobStatus.Failed);
                    else jobStates.Add(int.Parse(jobData[0]), JobStatus.OK);
                }
            }
            catch
            {

            }
        }
    }


}
