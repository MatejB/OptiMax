﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ItemContainer : Item, IDependent
    {
        // Variables                                                                                                                
        protected List<Item> items;


        // Properties                                                                                                               
        public List<Item> GetItems
        {
            get
            {
                return items;
            }
        }


        // Constructors                                                                                                             
        public ItemContainer(string name, string description) : base(name, description)
        {
            items = new List<Item>();
        }


        // Methods                                                                                                                  
        public void Add(Item item)
        {
            items.Add(item);
        }

        public bool Replace(Item oldItem, Item newItem)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i] == oldItem)
                {
                    items[i] = newItem;
                    return true;
                }
            }
            return false;
        }

        public List<string> InputVariables
        {
            // called in the constructor to initialize "parameterNames" if necessary
            get
            {
                try
                {
                    HashSet<string> allInputVariables = new HashSet<string>();
                    foreach (var item in items)
                    {
                        if (item is IDependent id && id.InputVariables != null) allInputVariables.UnionWith(id.InputVariables);
                    }
                    return allInputVariables.ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Exception in ItemContainer property InputVariables." + Environment.NewLine + ex.Message);
                }
            }
        }



    }
}
