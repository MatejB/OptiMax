﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ArrayOutput : ArrayBase, IDependent
    {
        // Variables                                                                                          
        private VariableValueType valueType;
        private string outputItemName;
        private DataMiningTemplate dataMiningTemplate;
        private DateTime lastWriteTime;
        private double[] values;
        private string valuesAsString;


        // Properties                                                                                         
        public VariableValueType ValueType
        {
            get
            {
                return valueType;
            }
        }
        
        public string OutputItemName
        {
            get
            {
                return outputItemName;
            }
        }

        public DataMiningTemplate Template
        {
            get
            {
                return dataMiningTemplate;
            }
        }

        override public int Length
        {
            get
            {
                UpdateValues();
                if (values != null) return values.Length;
                else return -1;
            }
        }


        // Constructors                                                                                       
        public ArrayOutput(string name, string description, VariableValueType valueType, string outputItemName, DataMiningTemplate dataMiningTemplate)
            : base(name, description)
        {
            this.valueType = valueType;
            this.outputItemName = outputItemName;
            this.dataMiningTemplate = dataMiningTemplate;

            // Check Excel cell range
            if (dataMiningTemplate is DataMiningTemplateExcel templateExcel)
            {
                if (!Helpers.ExcelHelper.IsThisArrayCellRangeValid(templateExcel.CellRange))
                    throw new Exception("The cell range '" + templateExcel.CellRange + "' of the output array '" + name + "' is not" + " a valid array cell range.");
            }

            lastWriteTime = new DateTime(0);
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public void SetValueFromOutputItem(double[] newValues)
        {
            values = newValues;
        }

        public override string GetValueAsString(int N)
        {
            return GetValueAsString(N, OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi)
        {
            UpdateValues();

            if (ValueType == VariableValueType.Double) return values[N].ToString(nfi);
            else if (ValueType == VariableValueType.Integer) return ((int)values[N]).ToString(nfi);
            else throw new Exception();
        }
        
        public override string GetValuesAsString()
        {
            return GetValuesAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValuesAsString(System.Globalization.NumberFormatInfo nfi)
        {
            try
            {
                UpdateValues(); // do not use if here !!!!
                
                valuesAsString = "";
                StringBuilder sb = new StringBuilder();
                if (ValueType == VariableValueType.Double)
                {
                    if (values.Length > 0) sb.Append(values[0].ToString(nfi));
                    for (int i = 1; i < values.Length; i++)
                    {
                        sb.AppendFormat(" {0}", values[i].ToString(nfi));
                    }
                }
                else if (ValueType == VariableValueType.Integer)
                {
                    if (values.Length > 0) sb.Append(((int)values[0]).ToString(nfi));
                    for (int i = 1; i < values.Length; i++)
                    {
                        sb.AppendFormat(" {0}", ((int)values[i]).ToString(nfi));
                    }
                }
                else throw new Exception();

                valuesAsString = sb.ToString();

                return valuesAsString;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in GetValuesAsString function in array output named '" + name + "'." + Environment.NewLine + Environment.NewLine + ex.Message);
            }
        }

        public override ArrayConstant ToArrayConstant()
        {
            ArrayConstant array = new ArrayConstant(name, description, valueType);
            UpdateValues();
            array.SetValues(values);
            return array;
        }

        private void UpdateValues()
        {
            if (dataMiningTemplate.FileBasedTemplate)
            {
                Item item = FrameworkItemCollection[outputItemName];
                FileResource fileResource;
                if (item is FileResource fr) fileResource = fr;
                else throw new Exception();

                DateTime currentWriteTime = System.IO.File.GetLastWriteTime(fileResource.FileNameWithPath);

                // update the values from File resources if necessary
                if (lastWriteTime < currentWriteTime)
                {
                    values = DataMiner.GetArrayDoubleData(fileResource, dataMiningTemplate);
                    lastWriteTime = currentWriteTime;
                }
            }
            else
            {
                // all values are already set from the output item
            }
        }

       

        // IDependent                                                                                         
        public List<string> InputVariables
        {
            get
            {
                return new List<string>() { OutputItemName };
            }
        }
    }
}
