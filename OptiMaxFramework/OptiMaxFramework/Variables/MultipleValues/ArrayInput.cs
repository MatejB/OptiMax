﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ArrayInput : ArrayBase
    {
        // Variables                                                                                          
        private VariableValueType valueType;
        private double[] valuesDouble;
        protected int stringLength;
        private System.Globalization.NumberFormatInfo oldNfi;
        private string oldDelimiter;
        private string valuesAsString;
        private bool upToDate;

        // Properties                                                                                         
        public VariableValueType ValueType
        {
            get
            {
                return valueType;
            }
        }
        override public int Length
        {
            get
            {
                if (ValueType == VariableValueType.Double || ValueType == VariableValueType.Integer) return valuesDouble.Length;
                else throw new Exception();
            }
        }
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }
        public int NumOfVariables
        {
            get
            {
                return valuesDouble.Length;
            }
        }


        // Constructors                                                                                       
        public ArrayInput(string name, string description, VariableValueType valueType, int numOfValues) : base(name, description)
        {
            this.valueType = valueType;
            valuesDouble = new double[numOfValues];
            stringLength = -1;
            oldNfi = null;
            oldDelimiter = null;
            valuesAsString = null;
            upToDate = false;
        }


        // Methods                                                                                            
        public void SetValue(double value, int valueId)
        {
            if (valueId < 0 || valueId >= valuesDouble.Length) throw new Exception("The valueId is invalid.");
            valuesDouble[valueId] = value;

            upToDate = false;
        }
        public void SetValue(int value, int valueId)
        {
            if (valueId < 0 || valueId >= valuesDouble.Length) throw new Exception("The valueId is invalid.");
            valuesDouble[valueId] = value;

            upToDate = false;
        }
        public void SetValues(double[] values)
        {
            if (values.Length != valuesDouble.Length) throw new Exception("Wrong array length.");
            if (valueType != VariableValueType.Double) throw new Exception("Wrong variable value type.");
            values.CopyTo(valuesDouble, 0);

            upToDate = false;
        }
        public void SetValues(int[] values)
        {
            if (values.Length != valuesDouble.Length) throw new Exception("Wrong array length.");
            if (valueType != VariableValueType.Integer) throw new Exception("Wrong variable value type.");
            for (int i = 0; i < values.Length; i++)
            {
                valuesDouble[i] = (double)values[i];
            }

            upToDate = false;
        }
        
        public override string GetValueAsString(int N)
        {
            return GetValueAsString(N, OptiMaxFramework.Globals.nfi);
        }
        public override string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi)
        {
            if (ValueType == VariableValueType.Double) return Globals.ConvertToString(valuesDouble[N], nfi, stringLength);
            else if (ValueType == VariableValueType.Integer) return Globals.ConvertToString((int)valuesDouble[N], stringLength);
            else throw new Exception();
        }
        public override string GetValuesAsString()
        {
            return GetValuesAsString(OptiMaxFramework.Globals.nfi);
        }
        public override string GetValuesAsString(System.Globalization.NumberFormatInfo nfi)
        {
            return GetValuesAsString(nfi, " ");
        }
        public string GetValuesAsString(System.Globalization.NumberFormatInfo nfi, string delimiter = " ")
        {
            if (!upToDate || oldNfi != nfi || oldDelimiter != delimiter) SetValuesAsString(nfi, delimiter);
            return valuesAsString;
        }

        private void SetValuesAsString(System.Globalization.NumberFormatInfo nfi, string delimiter = " ")
        {
            StringBuilder sb = new StringBuilder();
            if (ValueType == VariableValueType.Double)
            {
                if (valuesDouble.Length > 0) sb.Append(valuesDouble[0].ToString(nfi));
                for (int i = 1; i < valuesDouble.Length; i++)
                {
                    sb.AppendFormat("{0}{1}", delimiter, valuesDouble[i].ToString(nfi));
                }
            }
            else if (ValueType == VariableValueType.Integer)
            {
                if (valuesDouble.Length > 0) sb.Append(((int)valuesDouble[0]).ToString(nfi));
                for (int i = 1; i < valuesDouble.Length; i++)
                {
                    sb.AppendFormat("{0}{1}", delimiter, ((int)valuesDouble[i]).ToString(nfi));
                }
            }
            else throw new NotSupportedException();

            upToDate = true;
            oldNfi = nfi;
            oldDelimiter = delimiter;
            valuesAsString = sb.ToString();
        }

        public override ArrayConstant ToArrayConstant()
        {
            ArrayConstant array = new ArrayConstant(name, description, valueType);
            if (ValueType == VariableValueType.Double) array.SetValues(valuesDouble);
            else if (ValueType == VariableValueType.Integer)
            {
                int[] intValues = new int[valuesDouble.Length];
                for (int i = 0; i < intValues.Length; i++) intValues[i] = (int)Math.Round(valuesDouble[i]);
                array.SetValues(intValues);
            }
            else throw new Exception();
            return array;
        }
       
    }
}
