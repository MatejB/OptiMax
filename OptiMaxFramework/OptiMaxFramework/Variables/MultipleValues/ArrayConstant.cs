﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ArrayConstant : ArrayBase
    {
        // Variables                                                                                          
        private VariableValueType valueType;
        private double[] valuesDouble;
        private string[] valuesString;
        private System.Globalization.NumberFormatInfo oldNfi;
        private string oldDelimiter;
        private string valuesAsString;
        private bool upToDate;


        // Properties                                                                                         
        public VariableValueType ValueType
        {
            get
            {
                return valueType;
            }
        }

        override public int Length
        {
            get
            {
                if (ValueType == VariableValueType.Double || ValueType == VariableValueType.Integer) return valuesDouble.Length;
                if (ValueType == VariableValueType.String) return valuesString.Length;
                else throw new Exception();
            }
        }

        // Constructors                                                                                       
        public ArrayConstant(string name, string description, VariableValueType valueType) : base(name, description)
        {
            this.valueType = valueType;
            valuesDouble = null;
            valuesString = null;
            oldNfi = null;
            oldDelimiter = null;
            valuesAsString = null;
            upToDate = false;
        }

        public ArrayConstant(ArrayConstant array) : base (array.name, array.description)
        {
            valueType = array.valueType;
            valuesDouble = array.valuesDouble;
            valuesString = array.valuesString;

            upToDate = false;            
        }


        // Methods                                                                                            
        public void SetValues(double[] values)
        {
            if (valueType != VariableValueType.Double) throw new Exception("Wrong variable value type.");
            valuesDouble = values;
            valuesString = null;

            upToDate = false;
        }

        public void SetValues(int[] values)
        {
            if (valueType != VariableValueType.Integer) throw new Exception("Wrong variable value type.");
            valuesDouble = new double[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                valuesDouble[i] = (double)values[i];
            }
            valuesString = null;

            upToDate = false;
        }

        public void SetValues(string[] values)
        {
            if (valueType != VariableValueType.String) throw new Exception("Wrong variable value type.");
            valuesString = values;
            valuesDouble = null;

            upToDate = false;
        }

        public override string GetValueAsString(int N)
        {
            return GetValueAsString(N, OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi)
        {
            if (ValueType == VariableValueType.Double) return valuesDouble[N].ToString(nfi);
            else if (ValueType == VariableValueType.Integer) return ((int)valuesDouble[N]).ToString(nfi);
            else if (ValueType == VariableValueType.String) return valuesString[N];
            else throw new Exception();
        }

        public override string GetValuesAsString()
        {
            return GetValuesAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValuesAsString(System.Globalization.NumberFormatInfo nfi)
        {
            return GetValuesAsString(nfi, " ");
        }

        public string GetValuesAsString(System.Globalization.NumberFormatInfo nfi, string delimiter = " ")
        {
            if (!upToDate || oldNfi != nfi || oldDelimiter != delimiter) SetValuesAsString(nfi, delimiter);
            return valuesAsString;
        }


        private void SetValuesAsString(System.Globalization.NumberFormatInfo nfi, string delimiter = " ")
        {
            StringBuilder sb = new StringBuilder();
            if (ValueType == VariableValueType.Double)
            {
                if (valuesDouble.Length > 0) sb.Append(valuesDouble[0].ToString(nfi));
                for (int i = 1; i < valuesDouble.Length; i++)
                {
                    sb.AppendFormat("{0}{1}", delimiter, valuesDouble[i].ToString(nfi));
                }
            }
            else if (ValueType == VariableValueType.Integer)
            {
                if (valuesDouble.Length > 0) sb.Append(((int)valuesDouble[0]).ToString(nfi));
                for (int i = 1; i < valuesDouble.Length; i++)
                {
                    sb.AppendFormat("{0}{1}", delimiter, ((int)valuesDouble[i]).ToString(nfi));
                }
            }
            else if (ValueType == VariableValueType.String)
            {
                if (valuesString.Length > 0) sb.Append(valuesString[0]);
                for (int i = 1; i < valuesString.Length; i++)
                {
                    sb.AppendFormat("{0}{1}", delimiter, valuesString[i]);
                }
            }
            else throw new NotSupportedException();

            upToDate = true;
            oldNfi = nfi;
            oldDelimiter = delimiter;
            valuesAsString = sb.ToString();
        }

        public override ArrayConstant ToArrayConstant()
        {
            return new ArrayConstant(this);
        }
       
    }
}
