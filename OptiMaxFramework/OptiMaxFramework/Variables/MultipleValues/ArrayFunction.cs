﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ArrayFunction : ArrayBase, IDependent
    {
        // Variables                                                                                                                
        private VariableValueType valueType;
        private string equation;
        private List<string> functionParameters;
        private SymbolEquation symbolEquation;


        // Properties                                                                                                               
        public string Equation
        {
            get { return equation; }
        }
        private double[] Values
        {
            get
            {
                try
                {
                    Dictionary<string, string>[] variables = GetVariables();
                    double[] values = new double[variables.Length];
                    for (int i = 0; i < values.Length; i++) values[i] = symbolEquation.Solve(variables[i]);
                    return values;
                }
                catch (Exception ex)
                {
                    throw new Exception(ExceptionTools.FormatException(this, ex));
                }
            }
        }
        private string[] StringValues
        {
            get
            {
                try
                {
                    Dictionary<string, string>[] variables = GetVariables();
                    string[] values = new string[variables.Length];
                    string[] tmp = equation.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < tmp.Length; i++) tmp[i] = tmp[i].Replace(" ", "");
                    string result;
                    for (int i = 0; i < values.Length; i++) 
                    {
                        result = "";
                        foreach (var par in tmp)
                        {
                            result += variables[i][par];
                        }
                        values[i] = result; //.Replace("\\r\\n", "\r\n");
                    }
                    return values;
                }
                catch (Exception ex)
                {
                    throw new Exception(ExceptionTools.FormatException(this, ex));
                }
            }
        }
        override public int Length
        {
            get
            {
                return GetLength();
            }
        }


        // Constructors                                                                                                             
        public ArrayFunction(string name, string description, string equation)
            : base(name, description)
        {
            this.equation = equation;
            string[] tmp = equation.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
            if (tmp.Length >= 2)
            {
                valueType = VariableValueType.String;
                functionParameters = new List<string>();
                for (int i = 0; i < tmp.Length; i++)
                {
                    tmp[i] = tmp[i].Replace(" ", "");
                    if (!functionParameters.Contains(tmp[i])) functionParameters.Add(tmp[i]);
                }
            }
            else
            {
                valueType = VariableValueType.Double;
                symbolEquation = new SymbolEquation(equation);
                functionParameters = symbolEquation.GetParameterNames();
            }
        }


        // Methods                                                                                                                  
        private int GetLength()
        {
            int len = -1;
            Item item;
            int count = 0;

            if (functionParameters.Count > 0)
            {
                ArrayBase array;
                foreach (string parameter in functionParameters)
                {
                    item = FrameworkItemCollection[parameter];
                    if (item is ArrayBase)
                    {
                        array = (ArrayBase)item;
                        if (count == 0)
                            len = array.Length;
                        else
                        {
                            if (len != array.Length)
                                throw new Exception("Not all array parameters of the array function '" + name + "' have the same array length.");
                        }
                        count++;
                    }
                    else if (item is VariableBase)
                    {
                    }
                    else throw new Exception("Not all parameters of the array function '" + name + "' are variables or arrays.");
                }
            }
            if (count == 0)
                throw new Exception("The array function '" + name + "' does not use any array parameters.");
            if (len == -1)
                throw new Exception("The length of the array function '" + name + "' can not be determined.");

            return len;
        }
        private Dictionary<string, string>[] GetVariables()

        {
            Item item;
            int len = GetLength();

            // perform computations
            double[] values = new double[len];
            Dictionary<string, string>[] variables = new Dictionary<string, string>[len];

            // speed optimization
            string[][] stringValues = new string[functionParameters.Count][];
            string[] parNames = new string[functionParameters.Count];
            string variable;
            for (int n = 0; n < functionParameters.Count; n++)
            {
                item = baseFrameworkItemCollection[functionParameters[n]];
                parNames[n] = item.Name;
                // get all array values by calling the GetValuesAsString only once
                if (item is ArrayBase) stringValues[n] = ((ArrayBase)item).GetValuesAsString().Split(' ');
                // convert scalar in an array by calling the GetValueAsString only once
                else if (item is VariableBase)
                {
                    variable = ((VariableBase)item).GetValueAsString();
                    stringValues[n] = new string[len];
                    for (int i = 0; i < len; i++)
                    {
                        stringValues[n][i] = variable;
                    }
                }
            }

            for (int i = 0; i < len; i++)
            {
                variables[i] = new Dictionary<string, string>();
                for (int n = 0; n < functionParameters.Count; n++)
                {
                    variables[i].Add(parNames[n], stringValues[n][i]);
                }
            }

            return variables;
        }
        public override string GetValueAsString(int N)
        {
            return GetValueAsString(N, OptiMaxFramework.Globals.nfi);
        }
        public override string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi)
        {
            if (valueType == VariableValueType.String) return StringValues[N];
            else if (valueType == VariableValueType.Double) return Values[N].ToString(nfi);
            else throw new NotSupportedException();
        }
        public override string GetValuesAsString()
        {
            return GetValuesAsString(OptiMaxFramework.Globals.nfi);
        }
        public override string GetValuesAsString(System.Globalization.NumberFormatInfo nfi)
        {
            StringBuilder sb = new StringBuilder();

            if (valueType == VariableValueType.String)
            {
                string[] values = StringValues;
                if (values.Length > 0) sb.Append(values[0]);
                for (int i = 1; i < values.Length; i++)
                {
                    sb.AppendFormat(" {0}", values[i]);
                }
            }
            else if (valueType == VariableValueType.Double)
            {
                double[] values = Values;
                if (values.Length > 0) sb.Append(values[0].ToString(nfi));
                for (int i = 1; i < values.Length; i++)
                {
                    sb.AppendFormat(" {0}", values[i].ToString(nfi));
                }
            }
            else throw new NotSupportedException();
           
            return sb.ToString();
        }
        public override ArrayConstant ToArrayConstant()
        {
            if (valueType == VariableValueType.String)
            {
                ArrayConstant array = new ArrayConstant(name, description, VariableValueType.String);
                array.SetValues(StringValues);
                return array;
            }
            else if (valueType == VariableValueType.Double)
            {
                ArrayConstant array = new ArrayConstant(name, description, VariableValueType.Double);
                array.SetValues(Values);
                return array;
            }
            else throw new NotSupportedException();
        }
      

        // IDependent                                                                                                                  
        public List<string> InputVariables
        {
            get
            {
                return functionParameters;
            }
        }


    }
}
