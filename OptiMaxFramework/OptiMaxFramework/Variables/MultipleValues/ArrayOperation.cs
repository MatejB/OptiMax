﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;            // for [DescriptionAttribute class]
using System.Runtime.Serialization;

namespace OptiMaxFramework
{
    [Serializable]
    public enum ArrayOperationType
    {
        MovingAverage,
        MakeMonotonous,
        SubArray,
        CentralDifference,
        LowPassButterworthFilter
    }

    [Serializable]
    public static class ArrayOperationTypeHelper
    {
        public static Dictionary<ArrayOperationType, string> Description  = new Dictionary<ArrayOperationType, string>(){
            {ArrayOperationType.MovingAverage, "Moving average"},
            {ArrayOperationType.MakeMonotonous, "Make array monotonous"},
            {ArrayOperationType.SubArray, "Subarray"},
            {ArrayOperationType.CentralDifference, "Central difference"},
            {ArrayOperationType.LowPassButterworthFilter, "Low-pass Butterworth filter"}};

        public static Dictionary<ArrayOperationType, string> ParameterHelp = new Dictionary<ArrayOperationType, string>(){
            {ArrayOperationType.MovingAverage, "One parameter: the window size."},
            {ArrayOperationType.MakeMonotonous, "One parameter: positive value results in monotonously increasing array, negative value results in monotonously decreasing array."},
            {ArrayOperationType.SubArray, "Two parameters: the indices of the first and last element of the subarray."},
            {ArrayOperationType.CentralDifference, "One parameter: the step size (2h)."},
            {ArrayOperationType.LowPassButterworthFilter, "Two parameters: time delta between point samples [s], cutoff frequency [Hz]."}};
    }

    [Serializable]
    public class ArrayOperation : ArrayBase, IDependent
    {
        // Variables                                                                                                                
        private string arrayToOperateOn;
        private ArrayOperationType operationType;
        private double[] operationParameters;


        // Properties                                                                                                               
        private double[] Values
        {
            get
            {
                //string test = operationType.EnumDescription();
                Item item;
                ArrayBase array;

                int len = -1;
                if (arrayToOperateOn != null)
                {
                    item = FrameworkItemCollection[arrayToOperateOn];

                    if (item is ArrayBase)
                    {
                        array = (ArrayBase)item;
                        len = array.Length;
                    }
                    else throw new Exception("The array to operate on in an array operation '" + name + "' is not an array.");
                }
                else throw new Exception("The array to operate on in an array operation '" + name + "' is null.");


                // perform computations
                double[] values = new double[len];
                string[] stringValues = new string[len];

                stringValues = array.GetValuesAsString().Split(' ');

                for (int i = 0; i < len; i++)
                {
                    values[i] = double.Parse(stringValues[i], OptiMaxFramework.Globals.nfi);
                }

                if (operationType == ArrayOperationType.MovingAverage)
                {
                    int window = (int)operationParameters[0];
                    values = MovingAverage(values, window);
                }
                else if (operationType == ArrayOperationType.MakeMonotonous)
                {
                    double sign = operationParameters[0];
                    values = MakeMonotonous(values, sign);
                }
                else if (operationType == ArrayOperationType.SubArray)
                {
                    int first = (int)operationParameters[0];
                    int last = (int)operationParameters[1];
                    values = SubArray(values, first, last);
                }
                else if (operationType == ArrayOperationType.CentralDifference)
                {
                    double step = operationParameters[0];
                    values = CentralDifference(values, step);
                }
                else if (operationType == ArrayOperationType.LowPassButterworthFilter)
                {
                    double deltaTimeInSec = operationParameters[0];
                    double cutOff = operationParameters[1];
                    values = LowPassButterworth(values, deltaTimeInSec, cutOff);
                }
                else throw new Exception("The operation in an array operation '" + name + "' is unknown.");

                return values;
            }
        }

        override public int Length
        {
            get
            {
                return GetLength();
            }
        }

        public string ArrayToOperateOn
        {
            get
            {
                return arrayToOperateOn;
            }
        }

        public ArrayOperationType OperationType
        {
            get
            {
                return operationType;
            }
        }

        public double[] OperationParameters
        {
            get
            {
                return operationParameters;
            }
        } 
        

        // Constructors                                                                                                             
        public ArrayOperation(string name, string description, string arrayToOperateOn, ArrayOperationType operationType, double[] operationParameters)
            : base(name, description)
        {
            this.arrayToOperateOn = arrayToOperateOn;
            this.operationType = operationType;
            this.operationParameters = operationParameters;
        }


        // Methods                                                                                                                  
        private int GetLength()
        {
            int len = -1;
            if (arrayToOperateOn != null)
            {
                ArrayBase array;
                Item item = FrameworkItemCollection[arrayToOperateOn];

                if (item is ArrayBase)
                {
                    array = (ArrayBase)item;
                }
                else throw new Exception("The array to operate on in an array operation '" + name + "' is not an array.");

                if (operationType == ArrayOperationType.MovingAverage)
                {
                    if (operationParameters.Length != 1)
                        throw new Exception("The number of operation parameters in an array operation '" + name + "' is not appropriate.");
                    len = array.Length;
                }
                else if (operationType == ArrayOperationType.MakeMonotonous)
                {
                    if (operationParameters.Length != 1)
                        throw new Exception("The number of operation parameters in an array operation '" + name + "' is not appropriate.");
                    len = array.Length;
                }
                else if (operationType == ArrayOperationType.SubArray)
                {
                    if (operationParameters.Length != 2)
                        throw new Exception("The number of operation parameters in an array operation '" + name + "' is not appropriate.");

                    int first = (int)operationParameters[0];
                    int last = (int)operationParameters[1];

                    if (first < 0)
                        throw new Exception("The first operation parameter in an array operation '" + name + "' is a negative number.");
                    if (last >= array.Length)
                        throw new Exception("The second operation parameter in an array operation '" + name + "' is larger than the size of the array.");

                    len = last - first + 1;
                }
                else if (operationType == ArrayOperationType.CentralDifference)
                {
                    if (operationParameters.Length != 1)
                        throw new Exception("The number of operation parameters in an array operation '" + name + "' is not appropriate.");
                    len = array.Length - 2;
                }
                else if (operationType == ArrayOperationType.LowPassButterworthFilter)
                {
                    if (operationParameters.Length != 2)
                        throw new Exception("The number of operation parameters in an array operation '" + name + "' is not appropriate.");
                    len = array.Length;
                }
                else throw new Exception("The operation in an array operation '" + name + "' is unknown.");
            }
            else throw new Exception("The array to operate on in an array operation '" + name + "' is null.");

            return len;
        }

        public override string GetValueAsString(int N)
        {
            return GetValueAsString(N, OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi)
        {
            return Values[N].ToString(nfi);
        }
        
        public override string GetValuesAsString()
        {
            return GetValuesAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValuesAsString(System.Globalization.NumberFormatInfo nfi)
        {
            double[] values = Values;

            string valuesAsString = "";
            foreach (double value in values)
            {
                valuesAsString += value.ToString(nfi) + " ";
            }
            valuesAsString = valuesAsString.Substring(0, valuesAsString.Length - 1);

            return valuesAsString;
        }

        public override ArrayConstant ToArrayConstant()
        {
            ArrayConstant array = new ArrayConstant(name, description, VariableValueType.Double);
            array.SetValues(Values);
            return array;
        }


        //                                                                                                                         
        private double[] MovingAverage(double[] values, int window)
        {
            int N = values.Length;
            int width = ((int)window - 1) / 2;
            double[] average = new double[N];

            int w;
            int count;
            for (int i = 0; i < N; i++)
            {
                w = width;
                if (i < width) w = i;
                else if (i + width > N - 1) w = N - i - 1;

                count = 0;
                for (int j = i - w; j <= i + w; j++)
                {
                    average[i] += values[j];
                    count++;
                }

                average[i] /= count;
            }

            return average;
        }
        private double[] MakeMonotonous(double[] values, double sign)
        {
            int N = values.Length;
            double[] monotonous = new double[N];

            double min = double.MaxValue;
            double max = -double.MaxValue;
            for (int i = 0; i < N; i++)
            {
                if (values[i] < min) min = values[i];
                if (values[i] > max) max = values[i];
            }
            double delta = (max - min) / 1E6;
            if (delta == 0) throw new Exception("The input array of the array operation '" + name + "' has only equal values.");

            monotonous[0] = values[0];

            if (sign > 0)
            {
                for (int i = 1; i < N; i++)
                {
                    if (values[i] > monotonous[i - 1]) monotonous[i] = values[i];
                    else monotonous[i] = monotonous[i - 1] + delta;
                }
            }
            else if (sign < 0)
            {
                for (int i = 1; i < N; i++)
                {
                    if (values[i] < monotonous[i - 1]) monotonous[i] = values[i];
                    else monotonous[i] = monotonous[i - 1] - delta;
                }
            }
            else throw new Exception("The parameter of the array operation '" + name + "' must not be equal to 0.");

            return monotonous;
        }
        private double[] SubArray(double[] values, int first, int last)
        {
            double[] subArray = new double[last - first + 1];
            for (int i = 0; i < subArray.Length; i++)
            {
                subArray[i] = values[first + i];
            }
            return subArray;
        }
        private double[] CentralDifference(double[] values, double step)
        {
            int N = values.Length - 2;
            double[] diff = new double[N];

            for (int i = 0; i < N; i++)
            {
                diff[i] = (values[i + 2] - values[i]) / step;
            }

            return diff;
        }
        public static double[] LowPassButterworth(double[] indata, double deltaTimeinsec, double cutOff)
        {
            // https://www.codeproject.com/Tips/1092012/A-Butterworth-Filter-in-Csharp
            //--------------------------------------------------------------------------
            // This function returns the data filtered. Converted to C# 2 July 2014.
            // Original source written in VBA for Microsoft Excel, 2000 by Sam Van
            // Wassenbergh (University of Antwerp), 6 june 2007.
            //--------------------------------------------------------------------------

            if (indata == null) return null;
            if (cutOff == 0) return indata;

            double Samplingrate = 1 / deltaTimeinsec;
            long dF2 = indata.Length;        // The data range is set with dF2
            double[] Dat2 = new double[dF2 + 4]; // Array with 4 extra points front and back
            double[] data = indata; // Ptr., changes passed data

            // Copy indata to Dat2
            for (long r = 0; r < dF2; r++)
            {
                Dat2[2 + r] = indata[r];
            }
            // project points over indata[0]
            Dat2[0] = indata[0] - (indata[2] - indata[0]);
            Dat2[1] = indata[0] - (indata[1] - indata[0]);

            Dat2[dF2 + 2] = indata[dF2 - 1] + (indata[dF2 - 1] - indata[dF2 - 2]);
            Dat2[dF2 + 3] = indata[dF2 - 1] + (indata[dF2 - 1] - indata[dF2 - 3]);

            const double pi = 3.14159265358979;
            double wc = Math.Tan(cutOff * pi / Samplingrate);
            double k1 = 1.414213562 * wc; // Sqrt(2) * wc
            double k2 = wc * wc;
            double a = k2 / (1 + k1 + k2);
            double b = 2 * a;
            double c = a;
            double k3 = b / k2;
            double d = -2 * a + k3;
            double e = 1 - (2 * a) - k3;

            // RECURSIVE TRIGGERS - ENABLE filter is performed (first, last points constant)
            double[] DatYt = new double[dF2 + 4];
            DatYt[0] = Dat2[0];
            DatYt[1] = Dat2[1];
            for (long s = 2; s < dF2 + 2; s++)
            {
                DatYt[s] = a * Dat2[s] + b * Dat2[s - 1] + c * Dat2[s - 2]
                           + d * DatYt[s - 1] + e * DatYt[s - 2];
            }
            DatYt[dF2 + 2] = DatYt[dF2 + 1] + (DatYt[dF2 + 1] - DatYt[dF2 - 0]);
            DatYt[dF2 + 3] = DatYt[dF2 + 1] + (DatYt[dF2 + 1] - DatYt[dF2 - 1]);

            // FORWARD filter
            double[] DatZt = new double[dF2 + 2];
            DatZt[dF2] = DatYt[dF2 + 2];
            DatZt[dF2 + 1] = DatYt[dF2 + 3];
            for (long t = -dF2 + 1; t <= 0; t++)
            {
                DatZt[-t] = a * DatYt[-t + 2] + b * DatYt[-t + 3] + c * DatYt[-t + 4]
                            + d * DatZt[-t + 1] + e * DatZt[-t + 2];
            }

            // Calculated points copied for return
            for (long p = 0; p < dF2; p++)
            {
                data[p] = DatZt[p];
            }

            return data;
        }

        private double[] Butterworth(double[] input, double S)
        {
            double T = 1 / S;
            double CFR = 13;
            double wd = 2 * Math.PI * CFR;
            double wa = Math.Tan(wd * T / 2);
            double a0 = wa * wa / (1 + Math.Sqrt(2) * wa + wa * wa);
            double a1 = 2 * a0;
            double a2 = a0;
            double b1 = -2 * (wa * wa - 1) / (1 + Math.Sqrt(2) * wa + wa * wa);
            double b2 = (-1 + Math.Sqrt(2) * wa - wa * wa) / (1 + Math.Sqrt(2) * wa + wa * wa);

            double[] output = new double[input.Length];

            output[0] = a0 * input[0] + a1 * input[0] + a2 * input[0];
            output[1] = a0 * input[1] + a1 * input[0] + a2 * input[0] + b1 * output[0];

            for (int i = 2; i < input.Length; i++)
            {
                output[i] = a0 * input[i] + a1 * input[i - 1] + a2 * input[i - 2] + b1 * output[i - 1] + b2 * output[i - 2];
            }
            return output;
        }

        // IDependent                                                                                                               
        public List<string> InputVariables
        {
            get
            {
                return new List<string>() { arrayToOperateOn };
            }
        }

    }
}
