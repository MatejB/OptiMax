﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class ArrayBase : Item, IRecordable
    {
        // Properties
        abstract public int Length { get; }


        // Constructors                                                                                                                    
        public ArrayBase(string name, string description)
            : base(name, description)
        {
        }


        // Mathods                                                                                                                         
        abstract public string GetValueAsString(int N);

        abstract public string GetValueAsString(int N, System.Globalization.NumberFormatInfo nfi);

        abstract public string GetValuesAsString();

        abstract public string GetValuesAsString(System.Globalization.NumberFormatInfo nfi);

        abstract public ArrayConstant ToArrayConstant();
    }
}
