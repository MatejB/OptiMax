﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaNodoutReader
{

    // n o d a l p r i n t   o u t f o r   t i m e  s t e p       1                              (at time 0.0000000E+00 )
    //
    // nodal point  x-disp y-disp z-disp x-vel y-vel z-vel x-accl y-accl z-accl x-coor y-coor z-coor
    //   327002   0.0000E+00  0.0000E+00  0.0000E+00  2.7775E+04  2.9330E+02 -4.2346E-02  0.0000E+00  0.0000E+00 -0.0000E+00 -1.4814E+03 -5.1866E+03  5.0737E+02
    //
    //
    //
    // n o d a l p r i n t   o u t f o r   t i m e  s t e p       1                              (at time 0.0000000E+00 )
    //
    // nodal point  x-rot y-rot z-rot x-rot vel   y-rot vel   z-rot vel   x-rot acc   y-rot acc   z-rot acc
    //   327002   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00
    //
    //
    //
    // n o d a l p r i n t   o u t f o r   t i m e  s t e p     625                              (at time 4.9920001E-04 )
    //
    // nodal point  x-disp y-disp z-disp x-vel y-vel z-vel x-accl y-accl z-accl x-coor y-coor z-coor
    //   327002   1.3863E+01  1.4645E-01 -1.4435E-03  2.7768E+04  2.9115E+02 -4.5929E+00 -1.4565E+04 -4.3274E+03 -8.8442E+03 -1.4767E+03 -5.1735E+03  5.0737E+02


    public class LsDynaNodoutReader
    {
        private int numNodes;
        private List<double> time;
        private List<LsDynaNodalTimePoint>[] timePoints;
        static private string[] splitter = new string[] { " " };


        public LsDynaNodoutReader(string file)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-us");

            LoadData(File.ReadAllText(file));
        }

        public void LoadData(string data)
        {
            string[] lines = data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int startLine = -1;
            int firstDataLine = -1;
            int deltaLines = -1;

            for (int i = 0; i < lines.Length; i++)
            {
                if (startLine < 0 && lines[i].StartsWith(" n o d a l   p r i n t "))
                {
                    startLine = i;
                    break;
                }
            }

            for (int i = startLine + 1; i < lines.Length; i++)
            {
                if (lines[i].StartsWith(" nodal point "))
                {
                    firstDataLine = i + 1;
                    break;
                }
            }

            for (int i = firstDataLine + 1; i < lines.Length; i++)
            {
                if (lines[i].StartsWith(" n o d a l   p r i n t "))
                {
                    deltaLines = i - startLine;
                    break;
                }
            }

            time = new List<double>();
            numNodes = deltaLines - 2;
            timePoints = new List<LsDynaNodalTimePoint>[numNodes];
            for (int i = 0; i < numNodes; i++) timePoints[i] = new List<LsDynaNodalTimePoint>();

            int currLineId = startLine;
            int lineId1;
            int lineId2;
            LsDynaNodalTimePoint timePoint;
            while (currLineId < lines.Length)
            {
                // get time
                string[] tmp = lines[currLineId].Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                time.Add(double.Parse(tmp[tmp.Length - 2]));

                // get data
                lineId1 = currLineId + (firstDataLine - startLine);
                lineId2 = lineId1 + deltaLines;

                for (int i = 0; i < numNodes; i++)
                {
                    if (lineId1 < lines.Length && lineId2 < lines.Length) timePoint = new LsDynaNodalTimePoint(lines[lineId1], lines[lineId2]);
                    else break;

                    timePoints[i].Add(timePoint);

                    lineId1++;
                    lineId2++;
                }

                currLineId += 2 * deltaLines;
            }

        }
        public string[] GetAllVariableNames()
        {
            string[] allVariables = new string[] {"time", "x-disp", "y-disp", "z-disp", "x-vel", "y-vel", "z-vel", "x-accl", "y-accl", "z-accl", "x-coor", "y-coor", "z-coor",
                                              "x-rot", "y-rot", "z-rot", "x-rot vel", "y-rot vel", "z-rot vel", "x-rot acc", "y-rot acc", "z-rot acc"};

            string[] allNodes = new string[timePoints.Length];
            for (int i = 0; i < timePoints.Length; i++) allNodes[i] = timePoints[i][0].Id.ToString();

            int count = 0;
            string[] allNames = new string[allVariables.Length * allNodes.Length];
            for (int i = 0; i < allNodes.Length; i++)
            {
                for (int j = 0; j < allVariables.Length; j++)
                {
                    allNames[count++] = allNodes[i] + "@" + allVariables[j];
                }
            }

            return allNames;
        }
        public double[] GetVariableValues(string nodoutVariableName)
        {
            //1344@time
            //1344@x-disp

            string[] tmp = nodoutVariableName.Split(new string[] { "@" }, StringSplitOptions.RemoveEmptyEntries);
            int nodeId = int.Parse(tmp[0]);
            string variable = tmp[1];

            if (variable == "time")
                return time.ToArray();
            else
            {
                int timePointId = -1;
                for (int i = 0; i < timePoints.Length; i++)
                {
                    if (timePoints[i][0].Id == nodeId)
                    {
                        timePointId = i;
                        break;
                    }
                }
                if (timePointId < 0) throw new Exception("Nodout: the node number '" + tmp[0] + "' was not found.");

                double[] result = new double[time.Count];

                int count = 0;
                foreach (LsDynaNodalTimePoint point in timePoints[timePointId])
                {
                    result[count++] = point.GetResult(variable);
                }

                return result.ToArray();
            }
        }





    }
}
