﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaNodoutReader
{
    public class LsDynaNodalTimePoint
    {
        // nodal point  x-disp     y-disp      z-disp      x-vel       y-vel       z-vel      x-accl      y-accl      z-accl      x-coor      y-coor      z-coor
        // 2539   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  1.0000E+01 -7.5000E+01  0.0000E+00

        // nodal point  x-rot      y-rot       z-rot       x-rot vel   y-rot vel   z-rot vel   x-rot acc   y-rot acc   z-rot acc
        // 2539   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00
        public int Id;

        private double ux;
        private double uy;
        private double uz;
        private double vx;
        private double vy;
        private double vz;
        private double ax;
        private double ay;
        private double az;
        private double x;
        private double y;
        private double z;

        private double rx;
        private double ry;
        private double rz;
        private double vrx;
        private double vry;
        private double vrz;
        private double arx;
        private double ary;
        private double arz;

        static private string[] splitter = new string[] { " " };


        public LsDynaNodalTimePoint(string line1, string line2)
        {
            string[] tmp = line1.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            Id = int.Parse(tmp[0]);
            ux = double.Parse(tmp[1]);
            uy = double.Parse(tmp[2]);
            uz = double.Parse(tmp[3]);
            vx = double.Parse(tmp[4]);
            vy = double.Parse(tmp[5]);
            vz = double.Parse(tmp[6]);
            ax = double.Parse(tmp[7]);
            ay = double.Parse(tmp[8]);
            az = double.Parse(tmp[9]);
            x = double.Parse(tmp[10]);
            y = double.Parse(tmp[11]);
            z = double.Parse(tmp[12]);

            tmp = line2.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            if (Id != int.Parse(tmp[0])) throw new Exception("Bad nodout line format.");
            rx = double.Parse(tmp[1]);
            ry = double.Parse(tmp[2]);
            rz = double.Parse(tmp[3]);
            vrx = double.Parse(tmp[4]);
            vry = double.Parse(tmp[5]);
            vrz = double.Parse(tmp[6]);
            arx = double.Parse(tmp[7]);
            ary = double.Parse(tmp[8]);
            arz = double.Parse(tmp[9]);
        }


        public string[] GetAllHistoryOutputNames()
        {
            List<string> allNames = new List<string>() {"x-disp", "y-disp", "z-disp", "x-vel", "y-vel", "z-vel", "x-accl", "y-accl", "z-accl", "x-coor", "y-coor", "z-coor",
                                                        "x-rot", "y-rot", "z-rot", "x-rot vel", "y-rot vel", "z-rot vel", "x-rot acc", "y-rot acc", "z-rot acc"};
            
            return allNames.ToArray();
        }
        public double GetResult(string nodoutVariableName)
        {
            //{"time", "x-disp", "y-disp", "z-disp", "x-vel", "y-vel", "z-vel", "x-accl", "y-accl", "z-accl", "x-coor", "y-coor", "z-coor",
            //                                  "x-rot", "y-rot", "z-rot", "x-rot vel", "y-rot vel", "z-rot vel", "x-rot acc", "y-rot acc", "z-rot acc"};


            switch (nodoutVariableName)
            {
                case "x-disp":
                    return ux;
                case "y-disp":
                    return uy;
                case "z-disp":
                    return uz;

                case "x-vel":
                    return vx;
                case "y-vel":
                    return vy;
                case "z-vel":
                    return vz;

                case "x-accl":
                    return ax;
                case "y-accl":
                    return ay;
                case "z-accl":
                    return az;

                case "x-coor":
                    return x;
                case "y-coor":
                    return y;
                case "z-coor":
                    return z;

                case "x-rot":
                    return rx;
                case "y-rot":
                    return ry;
                case "z-rot":
                    return rz;

                case "x-rot vel":
                    return vrx;
                case "y-rot vel":
                    return vry;
                case "z-rot vel":
                    return vrz;

                case "x-rot acc":
                    return arx;
                case "y-rot acc":
                    return ary;
                case "z-rot acc":
                    return arz;
                default:
                    return -1;
            }
        }
       

       


    }
}
