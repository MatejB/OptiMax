﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaMatsumReader
{
    public class LsDynaMatsumReader
    {
        private Dictionary<int, string> legend;
        private List<double> time;
        private Dictionary<int, List<LsDynaMatData>> timePoints;
        static private string[] splitter1 = new string[] { " " };
        static private string[] splitter2 = new string[] { " ", "=" };


        public LsDynaMatsumReader(string file)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-us");

            LoadData(File.ReadAllText(file));
        }

        public void LoadData(string data)
        {
            string[] tmp;
            string[] lines = data.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int startLine = 0;
            List<int> dataLineIds = new List<int>();

            // {BEGIN LEGEND}
            // Entity #        Title
            // 1     BASE
            //
            // 21     MOST
            //{END LEGEND}

            legend = new Dictionary<int, string>();
            timePoints = new Dictionary<int, List<LsDynaMatData>>();
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("Entity #"))
                {
                    
                    i++;

                    while (!lines[i].Contains("{END LEGEND}") && i < lines.Length)
                    {
                        tmp = lines[i].Split(splitter1, StringSplitOptions.RemoveEmptyEntries);
                        legend.Add(int.Parse(tmp[0]), tmp[1]);
                        timePoints.Add(int.Parse(tmp[0]), new List<LsDynaMatData>());
                        i++;
                    }
                    startLine = i;
                }
            }

            time = new List<double>();
            for (int i = startLine; i < lines.Length; i++)
            {
                //time = 0.0000E+00
                //mat.#=    1             inten=   1.8070E+02     kinen=   0.0000E+00     eroded_ie=   0.0000E+00     eroded_ke=   0.0000E+00
                if (lines[i].Contains("time"))
                {
                    tmp = lines[i].Split(splitter2, StringSplitOptions.RemoveEmptyEntries);
                    time.Add(double.Parse(tmp[1]));
                    dataLineIds.Add(i);
                }
            }

            LsDynaMatData matData = null;
            int[] dataLineArrayIds = dataLineIds.ToArray();
            int endLine;
            List<int> matDataLineIds = new List<int>();
            int[] matDataLineArrayIds;
            int numOfMatDataLines = -1;
            for (int i = 0; i < dataLineArrayIds.Length; i++)
            {
                // get start and end line
                startLine = dataLineArrayIds[i] + 1;

                if (i >= dataLineArrayIds.Length - 1) endLine = lines.Length;
                else endLine = dataLineArrayIds[i + 1];

                // get the lines where mat data is written
                matDataLineIds.Clear();
                for (int j = startLine; j < endLine; j++)
                {
                    if (lines[j].Contains("mat.#")) matDataLineIds.Add(j);
                }
                matDataLineArrayIds = matDataLineIds.ToArray();

                // get number of lines used for mat data
                if (numOfMatDataLines == -1)
                {
                    if (matDataLineArrayIds.Length > 1) numOfMatDataLines = matDataLineArrayIds[1] - matDataLineArrayIds[0];
                    else numOfMatDataLines = endLine - matDataLineArrayIds[0];
                }

                for (int j = 0; j < matDataLineArrayIds.Length; j++)
                {
                    if (j + 1 < matDataLineArrayIds.Length)
                        numOfMatDataLines = matDataLineArrayIds[j + 1] - matDataLineArrayIds[j];
                    else
                        numOfMatDataLines = endLine - matDataLineArrayIds[j];

                    if (numOfMatDataLines == 3)
                    {
                        matData = new LsDynaMatData(lines[startLine], lines[startLine + 1], lines[startLine + 2]);
                    }
                    else if (numOfMatDataLines == 4)
                    {
                        matData = new LsDynaMatData(lines[startLine], lines[startLine + 1], lines[startLine + 2], lines[startLine + 3]);
                    }
                    else throw new Exception("The matsum file does not have the expected file format.");
                    timePoints[matData.Id].Add(matData);
                    startLine += numOfMatDataLines;
                }
            }
        }
        public string[] GetAllVariableNames()
        {
            string[] allVariables = new string[] {"inten", "kinen", "eroded_ie", "eroded_ke", "x-mom", "y-mom", "z-mom", "x-rbv", "y-rbv", "z-rbv" };

            int count = 0;
            string[] allNames = new string[allVariables.Length * legend.Count + 1];
            allNames[count++] = "time";

            foreach (var entry in legend)
            {
                for (int i = 0; i < allVariables.Length; i++)
                {
                    allNames[count++] = entry.Key + "@" + allVariables[i];
                }
            }
            return allNames;
        }
        public double[] GetVariableValues(string variableName)
        {
            try
            {
                // time
                // distancespacer@kinen
                variableName = variableName.ToLower();
                string[] tmp = variableName.Split(new string[] { "@" }, StringSplitOptions.RemoveEmptyEntries);

                if (tmp.Length == 1 && tmp[0] == "time")
                    return time.ToArray();
                else if (tmp.Length == 2)
                {
                    int id = int.Parse(tmp[0]);
                    double[] result = new double[time.Count];

                    int count = 0;
                    foreach (LsDynaMatData matData in timePoints[id])
                    {
                        result[count++] = matData.GetResult(tmp[1]);
                    }

                    return result;
                }
                else throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception("The given variable name in the matsum reader is not valid." + Environment.NewLine + ex.Message);
            }
        }
       

       


    }
}
