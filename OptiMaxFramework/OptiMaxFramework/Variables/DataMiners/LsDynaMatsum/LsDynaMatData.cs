﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsDynaMatsumReader
{
    class LsDynaMatData
    {
        //mat.#=    1             inten=   1.8070E+02     kinen=   0.0000E+00     eroded_ie=   0.0000E+00     eroded_ke=   0.0000E+00
        //x-mom=   0.0000E+00     y-mom=   0.0000E+00     z-mom=   0.0000E+00
        //x-rbv=   0.0000E+00     y-rbv=   0.0000E+00     z-rbv=   0.0000E+00
        //                                                +mass=   0.0000E+00
        public int Id;

        private double inten;
        private double kinen;
        private double eroded_ie;
        private double eroded_ke;
        private double x_mom;
        private double y_mom;
        private double z_mom;
        private double x_rbv;
        private double y_rbv;
        private double z_rbv;
        private double mass;

        static private string[] splitter = new string[] { " ", "=" };

        public LsDynaMatData(string line1, string line2, string line3, string line4)
            :this(line1, line2, line3)
        {
            string[] tmp;
            tmp = line4.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            mass = double.Parse(tmp[1]);
        }

        public LsDynaMatData(string line1, string line2, string line3)
        {
            string[] tmp;

            tmp = line1.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            Id = int.Parse(tmp[1]);
            inten = double.Parse(tmp[3]);
            kinen = double.Parse(tmp[5]);
            eroded_ie = double.Parse(tmp[7]);
            eroded_ke = double.Parse(tmp[9]);

            tmp = line2.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            x_mom = double.Parse(tmp[1]);
            y_mom = double.Parse(tmp[3]);
            z_mom = double.Parse(tmp[5]);

            tmp = line3.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            x_rbv = double.Parse(tmp[1]);
            y_rbv = double.Parse(tmp[3]);
            z_rbv = double.Parse(tmp[5]);

            mass = -1;
        }
        public string[] GetAllHistoryOutputNames()
        {
            List<string> allNames = new List<string>() {"inten", "kinen", "eroded_ie", "eroded_ke", "x-mom", "y-mom", "z-mom", "x-rbv", "y-rbv", "z-rbv" };
            return allNames.ToArray();
        }

        public double GetResult(string variableName)
        {
            switch (variableName)
            {
                case "inten":
                    return inten;
                case "kinen":
                    return kinen;
                case "eroded_ie":
                    return eroded_ie;
                case "eroded_ke":
                    return eroded_ke;

                case "x-mom":
                    return x_mom;
                case "y-mom":
                    return y_mom;
                case "z-mom":
                    return z_mom;

                case "x-rbv":
                    return x_rbv;
                case "y-rbv":
                    return y_rbv;
                case "z-rbv":
                    return z_rbv;
             
                default:
                    return -1;
            }
        }

    }
}
