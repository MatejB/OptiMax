﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AbaqusHistoryReader
{
    public class AbaqusHistoryReader
    {
        
        Dictionary<string, AbaqusStep> steps;

        public AbaqusHistoryReader(string file)
        {
            steps = new Dictionary<string, AbaqusStep>();
            LoadData(File.ReadAllText(file));
        }

        public void LoadData(string data)
        {
            string[] tmp = data.Split(new string[] { "Step name" }, StringSplitOptions.RemoveEmptyEntries);

            AbaqusStep abaqusStep;

            foreach (string stepData in tmp)
            {
                if (stepData.Contains("History Region "))
                {
                    abaqusStep = new AbaqusStep();
                    abaqusStep.Load(stepData);
                    steps.Add(abaqusStep.Name, abaqusStep);
                }
            }
        }
        public string[] GetExistingHistoryOutputNames()
        {
            List<string> allNames = new List<string>();
            foreach (var entry in steps)
            {
                allNames.AddRange(entry.Value.GetExistingHistoryOutputNames);
            }
            return allNames.ToArray();
        }
        public double[] GetResults(string historyOutputName)
        {
            //Step-1@Node ASSEMBLY.1@RF2
            //Node ASSEMBLY.1@RF2

            if (historyOutputName != null)
            {
                string[] tmp = historyOutputName.Split(new string[] { "@" }, StringSplitOptions.RemoveEmptyEntries);

                if (tmp.Length == 3) return GetResults(tmp[0], tmp[1], tmp[2]);
                else if (tmp.Length == 2)
                {
                    List<double> allData = new List<double>();
                    double[] data;
                    double lastTime;
                    foreach (var entry in steps)
                    {
                        data = entry.Value.GetResults(tmp[0], tmp[1]);

                        if (tmp[1] == "time")
                        {
                            if (allData.Count > 0) lastTime = allData.Last();
                            else lastTime = 0;

                            for (int i = 0; i < data.Length; i++) data[i] += lastTime;
                        }

                        if (data != null) allData.AddRange(data);
                    }
                    return allData.ToArray();
                }
            }
            return null;
        }
        public double[] GetResults(string stepName, string historyRegion, string historyOutput)
        {
            try
            {
                return steps[stepName].GetResults(historyRegion, historyOutput);
            }
            catch
            {
                return null;
            }
        }

       


    }
}
