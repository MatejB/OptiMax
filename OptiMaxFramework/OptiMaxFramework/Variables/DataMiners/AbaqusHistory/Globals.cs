﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbaqusHistoryReader
{
    internal static class Globals
    {
        public static System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };

        public static string GetRow(this String str, int index)
        {
            string row = "";
            string[] rows = str.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (index < rows.Length) row = rows[index];
            return row;
        }
    }
}
