﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbaqusHistoryReader
{
    class AbaqusHistoryOutput
    {
        private string name;
        List<double> frameValues;
        List<double> dataValues;

        public string Name { get { return name; } }
        public int Length { get { return frameValues.Count; } }

        public AbaqusHistoryOutput()
        {
            name = null;
            frameValues = new List<double>();
            dataValues = new List<double>();
        }

        public string Load(string data)
        {
            // 0  History Output 'ALLAE'
            // 1  description  : Artificial strain energy
            // 2  type         : SCALAR
            // 3 Frame value          Data
            // 4         0.0           0.0
            // 5         0.1     0.0600746
            // 6         0.2      0.242129
            //                            

            string[] rows = data.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            name = rows[0].Trim(new char[] { ' ', '\'' });

            string[] tmp;
            for (int i = 4; i < rows.Length; i++)
            {
                tmp = rows[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (tmp.Length == 2)
                {
                    frameValues.Add(double.Parse(tmp[0], OptiMaxFramework.Globals.nfi));
                    dataValues.Add(double.Parse(tmp[1], OptiMaxFramework.Globals.nfi));
                }
            }

            return name;
        }

        public string[] GetAllHistoryNames()
        {
            return new string[] { "frame", "data" };
        }

        public double[] GetResults(string frameOrData)
        {
            if (frameOrData == "frame") return frameValues.ToArray();
            else if (frameOrData == "data") return dataValues.ToArray();
            else throw new NotSupportedException();
        }
    }
}
