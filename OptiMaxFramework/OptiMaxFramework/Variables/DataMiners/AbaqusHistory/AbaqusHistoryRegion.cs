﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbaqusHistoryReader
{
    class AbaqusHistoryRegion
    {
        private string name;
        private Dictionary<string, AbaqusHistoryOutput> historyOutputs;

        public string Name { get { return name; } }

        public AbaqusHistoryRegion()
        {
            name = null;
            historyOutputs = new Dictionary<string, AbaqusHistoryOutput>();
        }

        public string Load(string data)
        {
            // 0 History Region 'Assembly ASSEMBLY'
            // 1 description  : Output at assembly ASSEMBLY
            // 2 History Point
            // 3 position     : WHOLE_MODEL
            // 4                               
            // 5 Number of history outputs = 24
            // 6                      
            // 7 History Output 'ALLAE'
            // 8 description  : Artificial strain energy

            string[] tmp = data.Split(new string[] { " History Output " }, StringSplitOptions.RemoveEmptyEntries);
            string[] rows = tmp[0].Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            name = rows[0].Trim(new char[] { ' ', '\'' });

            AbaqusHistoryOutput historyOutput;
            for (int i = 1; i < tmp.Length; i++)
            {
                historyOutput = new AbaqusHistoryOutput();
                historyOutput.Load(tmp[i]);
                historyOutputs.Add(historyOutput.Name, historyOutput);
            }

            return name;
        }

        public string[] GetExistingHistoryOutputNames()
        {
            List<string> allNames = new List<string>();

            foreach (var entry in historyOutputs)
            {
                if (entry.Value.Length > 0)
                {
                    if (allNames.Count == 0) allNames.Add("time");
                    allNames.Add(entry.Key);
                }
            }
            
            return allNames.ToArray();
        }

        public double[] GetResults(string historyOutput)
        {
            if (historyOutput == "time") return historyOutputs.Values.First().GetResults("frame");
            else if (historyOutputs.ContainsKey(historyOutput)) return historyOutputs[historyOutput].GetResults("data");
            else return null;
        }
    }
}
