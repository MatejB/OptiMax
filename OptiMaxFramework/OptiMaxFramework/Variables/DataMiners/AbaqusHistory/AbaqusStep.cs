﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbaqusHistoryReader
{
    class AbaqusStep
    {
        private string name;
        private Dictionary<string, AbaqusHistoryRegion> historyRegions;

        public string Name { get { return name; } }
        
        public AbaqusStep()
        {
            name = null;
            historyRegions = new Dictionary<string, AbaqusHistoryRegion>();
        }

        public string Load(string data)
        {
            // 0 Step name 'Step-1'
            // 1 description   : 
            // 2 number        : 1
            // 3 domain        : TIME
            // 4 procedure     : *STATIC
            // 5 timePeriod    : 1.0
            // 6 totalTime     : 0.0
            // 7 previousStep  : Initial
            // 8 nlgeom        : Yes
            // 9                  
            // 10 Number of history regions = 3
            // 11                 
            // 12 History Region 'Assembly ASSEMBLY'

            string[] tmp = data.Split(new string[] { " History Region " }, StringSplitOptions.RemoveEmptyEntries);
            string[] rows = tmp[0].Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            name = rows[0].Trim(new char[] {' ', '\''});

            AbaqusHistoryRegion historyRegion;
            for (int i = 1; i < tmp.Length; i++)
            {
                historyRegion = new AbaqusHistoryRegion();
                historyRegion.Load(tmp[i]);
                historyRegions.Add(historyRegion.Name, historyRegion);
            }

            return name;
        }

        public string[] GetExistingHistoryOutputNames
        {
            get
            {
                List<string> historyOutputNames = new List<string>();
                string[] names;

                if (historyRegions.Count > 0)
                {
                    foreach (var entry in historyRegions)
                    {
                        names = entry.Value.GetExistingHistoryOutputNames();

                        for (int i = 0; i < names.Length; i++)
                        {
                            historyOutputNames.Add(name + '@' + entry.Key + "@" + names[i]);
                        }
                    }
                }

                return historyOutputNames.ToArray();
            }
        }

        public double[] GetResults(string historyRegion, string historyOutput)
        {
            if (historyRegions.ContainsKey(historyRegion)) return historyRegions[historyRegion].GetResults(historyOutput);
            else return null;
        }
    }
}
