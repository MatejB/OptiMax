﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public static class DataMiner
    {
        // Methods                                                                                            
        public static double GetScalarDoubleData(FileResource file, DataMiningTemplate template)
        {
            if (template is DataMiningTemplateTxt) return GetScalarDoubleDataFromTxt(file, template as DataMiningTemplateTxt);
            else throw new NotSupportedException();
        }
        public static string GetScalarStringData(FileResource file, DataMiningTemplate template)
        {
            if (template is DataMiningTemplateTxt) return GetScalarStringDataFromTxt(file, template as DataMiningTemplateTxt);
            else throw new NotSupportedException();
        }
        public static double[] GetArrayDoubleData(FileResource file, DataMiningTemplate template)
        {
            if (template is DataMiningTemplateAbaqusReport) return GetArrayDoubleDataFromAbaqus(file, template as DataMiningTemplateAbaqusReport);
            else if (template is DataMiningTemplateLsDyna) return GetArrayDoubleDataFromLsDynaNodout(file, template as DataMiningTemplateLsDyna);
            else if (template is DataMiningTemplateTxt) return GetArrayDoubleDataFromTxt(file, template as DataMiningTemplateTxt);
            else throw new NotSupportedException();
        }


        // Abaqus miners                                                                                    
        private static double[] GetArrayDoubleDataFromAbaqus(FileResource file, DataMiningTemplateAbaqusReport template)
        {
            WaitForFile(file.FileNameWithPath);
            AbaqusHistoryReader.AbaqusHistoryReader abaqusReader = new AbaqusHistoryReader.AbaqusHistoryReader(file.FileNameWithPath);
            return abaqusReader.GetResults(template.HistoryVariableName);
        }


        // Ls-Dyna miners                                                                                    
        private static double[] GetArrayDoubleDataFromLsDynaNodout(FileResource file, DataMiningTemplateLsDyna template)
        {
            WaitForFile(file.FileNameWithPath);

            if (template.ResultFile == LsDynaResultFile.nodout)
            {
                LsDynaNodoutReader.LsDynaNodoutReader dynaReader = new LsDynaNodoutReader.LsDynaNodoutReader(file.FileNameWithPath);
                return dynaReader.GetVariableValues(template.VariableName);
            }
            else if (template.ResultFile == LsDynaResultFile.spcforc)
            {
                LsDynaSpcForcReader.SpcForcReader dynaReader = new LsDynaSpcForcReader.SpcForcReader(file.FileNameWithPath);
                return dynaReader.GetVariableValues(template.VariableName);
            }
            else if (template.ResultFile == LsDynaResultFile.matsum)
            {
                LsDynaMatsumReader.LsDynaMatsumReader dynaReader = new LsDynaMatsumReader.LsDynaMatsumReader(file.FileNameWithPath);
                return dynaReader.GetVariableValues(template.VariableName);
            }
            else if (template.ResultFile == LsDynaResultFile.elout)
            {
                LsDynaEloutReader.LsDynaEloutReader dynaReader = new LsDynaEloutReader.LsDynaEloutReader(file.FileNameWithPath);
                return dynaReader.GetVariableValues(template.VariableName);
            }
            else throw new Exception("Ls-Dyna miner exception.");
        }


        // Excel miners                                                                                
        private static double GetScalarDoubleDataFromExcel(FileResource file, DataMiningTemplateExcel template)
        {
            ExcelHelper excel = null;
            try
            {
                if (!ExcelHelper.IsThisSingleCellRangeValid(template.CellRange))
                    throw new Exception("The single cell range '" + template.CellRange + "' is not valid.");

                excel = new ExcelHelper(file.FileNameWithPath);
                excel.Open();

                return excel.ReadFromCellDouble(template.CellRange);
            }
            finally
            {
                if (excel != null)
                {
                    excel.Close();
                    excel.ReleaseObjectsAndKillExcel();
                }
            }
        }
        private static string GetScalarStringDataFromExcel(FileResource file, DataMiningTemplateExcel template)
        {
            ExcelHelper excel = null;
            try
            {
                if (!ExcelHelper.IsThisSingleCellRangeValid(template.CellRange))
                    throw new Exception("The single cell range '" + template.CellRange + "' is not valid.");

                excel = new ExcelHelper(file.FileNameWithPath);
                excel.Open();

                return excel.ReadFromCellString(template.CellRange);
            }
            finally
            {
                if (excel != null)
                {
                    excel.Close();
                    excel.ReleaseObjectsAndKillExcel();
                }
            }
        }
        private static double[] GetArrayDoubleDataFromExcel(FileResource file, DataMiningTemplateExcel template)
        {
            ExcelHelper excel = null;
            try
            {
                if (!ExcelHelper.IsThisArrayCellRangeValid(template.CellRange))
                    throw new Exception("The array cell range '" + template.CellRange + "' is not valid.");

                excel = new ExcelHelper(file.FileNameWithPath);
                excel.Open();

                string[] cells = template.CellRange.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                if (cells[0] == cells[1]) return new double[] { excel.ReadFromCellDouble(cells[0]) };
                else return GetDoubleArrayFromObjectArray(excel.ReadFromCells(cells[0], cells[1]));
            }
            finally
            {
                if (excel != null)
                {
                    excel.Close();
                    excel.ReleaseObjectsAndKillExcel();
                }
            }
        }
        public static double[] GetDoubleArrayFromObjectArray(object[,] values)
        {
            double[] result = null;
            object val;

            int rowMin = values.GetLowerBound(0);
            int rowMax = values.GetUpperBound(0) + 1;

            int colMin = values.GetLowerBound(1);
            int colMax = values.GetUpperBound(1) + 1;

            if (rowMax - rowMin == 1)
            {
                result = new double[colMax - colMin];
                for (int i = 0; i < result.Length; i++)
                {
                    val = values[rowMin, i + colMin];
                    if (val == null) val = 0.0;

                    if (val is string)
                    {
                        if ((string)val == "") result[i] = 0;
                        else result[i] = double.Parse((string)val);
                    }
                    else result[i] = (double)val;
                }
            }
            else if (colMax - colMin == 1)
            {
                result = new double[rowMax - rowMin];
                for (int i = 0; i < result.Length; i++)
                {
                    val = values[i + rowMin, colMin];
                    if (val == null) val = 0.0;

                    if (val is string)
                    {
                        if ((string)val == "") result[i] = 0;
                        else result[i] = double.Parse((string)val);
                    }
                    else result[i] = (double)val;
                }
            }
            else throw new Exception("The data read from the excel file is not an array.");

            return result;
        }


        // Txt miners                                                                                  
        private static double GetScalarDoubleDataFromTxt(FileResource file, DataMiningTemplateTxt template)
        {
            string data = WaitForFileAndReadAllText(file.FileNameWithPath);
            data = data.Replace("\r\n", "\n");
            data = data.Replace("\r", "\n");

            if (template.StartText != null)
            {
                string[] tmp = data.Split(new string[] { template.StartText }, StringSplitOptions.None);
                if (tmp.Length > 2) throw new Exception("The data minig start keyword '" + template.StartText + "'is not unique.");
                data = tmp[1];
            }

            string[] rows = data.Split(new char[] { '\n' });

            string row = rows[template.RowNumbers[0]];

            string[] words = row.Split(template.WordDelimiters, StringSplitOptions.RemoveEmptyEntries);

            NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = template.DecimalSeparator };

            double result = double.Parse(words[template.WordColIndex], nfi);

            return result;
        }
        private static string GetScalarStringDataFromTxt(FileResource file, DataMiningTemplateTxt template)
        {
            string data = WaitForFileAndReadAllText(file.FileNameWithPath);
            data = data.Replace("\r\n", "\n");
            data = data.Replace("\r", "\n");

            if (template.StartText != null)
            {
                string[] tmp = data.Split(new string[] { template.StartText }, StringSplitOptions.None);
                if (tmp.Length > 2) throw new Exception("The data minig start keyword '" + template.StartText + "'is not unique.");
                data = tmp[1];
            }

            string[] rows = data.Split(new char[] { '\n' });

            string row = rows[template.RowNumbers[0]];

            string[] words = row.Split(template.WordDelimiters, StringSplitOptions.RemoveEmptyEntries);

            NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = template.DecimalSeparator };

            return words[template.WordColIndex];
        }
        private static double[] GetArrayDoubleDataFromTxt(FileResource file, DataMiningTemplateTxt template)
        {
            string data = WaitForFileAndReadAllText(file.FileNameWithPath);
            data = data.Replace("\r\n", "\n");
            data = data.Replace("\r", "\n");

            if (template.StartText != null)
            {
                string[] tmp = data.Split(new string[] { template.StartText }, StringSplitOptions.None);
                if (tmp.Length > 2) throw new Exception("The data minig start keyword '" + template.StartText + "'is not unique.");
                data = tmp[1];
            }

            string[] rows = data.Split(new char[] { '\n' });

            double[] results = new double[template.RowNumbers.Length];

            string row;
            string[] words;
            NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = template.DecimalSeparator };
            int n = 0;
            foreach (int rowNumber in template.RowNumbers)
            {
                row = rows[rowNumber];

                words = row.Split(template.WordDelimiters, StringSplitOptions.RemoveEmptyEntries);

                results[n++] = double.Parse(words[template.WordColIndex], nfi);
            }

            return results;
        }
        
        
        // Tools
        private static string WaitForFileAndReadAllText(string fileName)
        {
            int count = 0;
            while (!System.IO.File.Exists(fileName) && count++ < 5) System.Threading.Thread.Sleep(500);
            return System.IO.File.ReadAllText(fileName);
        }
        private static void WaitForFile(string fileName)
        {
            int count = 0;
            while (!System.IO.File.Exists(fileName) && count++ < 5) System.Threading.Thread.Sleep(500);
        }
    }
}
