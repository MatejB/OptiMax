﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaSpcForcReader
{
    struct Triple
    {
        public double x;
        public double y;
        public double z;
    }

    struct SpcData
    {
        public Triple force;
        public Triple moment;
        public Int32 setId;
    }

    public class SpcForcReader
    {
        // Variables                                                                                                                
        static System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo() { NumberDecimalSeparator = "." };
        List<double> time;
        Dictionary<int, HashSet<int>> setIds;
        Dictionary<int, List<SpcData>> nodes;
        double[][] results;

        static string timeName = "Time";
        static string[] componentNames = new string[] { "Fx", "Fy", "Fz", "Mx", "My", "Mz" };

        // Constructors                                                                                                             
        public SpcForcReader(string file)
        {
            time = new List<double>();
            setIds = new Dictionary<int, HashSet<int>>();
            nodes = new Dictionary<int, List<SpcData>>();
            results = new double[0][]; 

            LoadData(file);
        }


        // Methods                                                                                                                  
        private void LoadData(string file)
        {
            string[] lines = File.ReadAllLines(file);
            string line;
            int firstLineId = 0;

            int n;
            string outputAtTime = "";

            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                firstLineId = i;

                if (line.TrimStart().StartsWith("output at time"))
                {
                    n = line.IndexOf('=');
                    outputAtTime = line.Substring(0, n + 1);
                    break;
                }
            }

            for (int i = firstLineId; i < lines.Length; i++)
            {
                double tmpTime = 0;
                line = lines[i];

                if (line.StartsWith(" output at time ="))
                {
                    tmpTime = ReadTime(line);
                    time.Add(tmpTime);
                }
                else if (line.Contains("forces"))
                {
                    AddForceData(line);
                }
                else if (line.Contains("moments"))
                {
                    AddMomentData(line);
                }
            }

            foreach (var entry in nodes)
            {
                setIds[entry.Value.First().setId].Add(entry.Key);
            }
        }
        private double ReadTime(string row)
        {
            string[] tmp = row.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            return Double.Parse(tmp[tmp.Length - 1], nfi);
        }
        private void AddForceData(string row)
        {
            string[] tmp = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int id = int.Parse(tmp[1]);

            if (!nodes.ContainsKey(id)) nodes.Add(id, new List<SpcData>());

            SpcData node = new SpcData();
            node.force.x = double.Parse(tmp[6], nfi);
            node.force.y = double.Parse(tmp[7], nfi);
            node.force.z = double.Parse(tmp[8], nfi);
           
            if (!tmp[tmp.Length - 1].Contains("setid")) node.setId = int.Parse(tmp[tmp.Length - 1], nfi);
            if (!setIds.ContainsKey(node.setId)) setIds.Add(node.setId, new HashSet<int>());

            nodes[id].Add(node);
        }
        private void AddMomentData(string row)
        {
            string[] tmp = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int id = int.Parse(tmp[1]);

            if (!nodes.ContainsKey(id)) nodes.Add(id, new List<SpcData>());

            SpcData node = nodes[id][nodes[id].Count - 1];

            node.moment.x = double.Parse(tmp[5], nfi);
            node.moment.y = double.Parse(tmp[6], nfi);
            node.moment.z = double.Parse(tmp[7], nfi);

            nodes[id][nodes[id].Count - 1] = node;
        }
        public void ExportSetIdAverage(int setId, string file)
        {
            ComputeSetIdAverage(setId);

            if (results != null && results.Length > 0 && results[0].Length > 0)
            {
                string row;

                StreamWriter sw = new StreamWriter(File.Open(file, FileMode.Create));

                for (int j = 0; j < results[0].Length; j++)
                {
                    row = "";
                    for (int i = 0; i < results.Length; i++)
                    {
                        row += results[i][j].ToString().PadLeft(30);
                    }
                    sw.WriteLine(row);
                }
                sw.Close();
            }
        }
        private void ComputeSetIdAverage(int setId)
        {
            double[] averageFz = new double[time.Count];
            int count = 0;

            foreach (int n in nodes.Keys)
            {
                if (n >= 90405 + 0 * 121 && n <= 90525 + 9 * 121)
                {
                    for (int timeStep = 0; timeStep < nodes[n].Count; timeStep++)
			        {
                        averageFz[timeStep] += nodes[n][timeStep].force.z;
			        }
                    count++;
                }
                
            }

            results = new double[2][];
            results[0] = time.ToArray();
            results[1] = averageFz;
        }


        public string[] GetAllVariableNames()
        {
            List<string> names = new List<string>();

            names.Add(timeName);
            foreach (var entry in setIds)
            {
                for (int i = 0; i < componentNames.Length; i++) names.Add(String.Format("{0}@{1}", entry.Key, componentNames[i]));

                foreach (var nodeId in entry.Value)
                {
                    for (int i = 0; i < componentNames.Length; i++) names.Add(String.Format("{0}@{1}@{2}", entry.Key, nodeId, componentNames[i]));
                }
            }

            return names.ToArray();
        }
        public double[] GetVariableValues(string variableName)
        {
            //"TIME"
            //"SET_ID@COMPONENT"
            //"SET_ID@NODE_ID@COMPONENT"

            string[] tmp = variableName.Trim().Split('@');

            if (tmp.Length == 1)
            {
                if (tmp[0].ToLower() == timeName.ToLower()) return time.ToArray();
                else throw new Exception("Spcforc data name '" + variableName + "' not supported.");
            }
            else if (tmp.Length == 2 && tmp[1].Length == 2)
            {
                int setId;
                if (int.TryParse(tmp[0], out setId))
                {
                    return GetNodeSetResultant(setId, tmp[1]);
                }
                else throw new Exception("Spcforc data name '" + variableName + "' not supported.");
            }
            else if (tmp.Length == 3 && tmp[2].Length == 2)
            {
                int setId;
                int nodeId;
                if (int.TryParse(tmp[0], out setId) && int.TryParse(tmp[1], out nodeId))
                {
                    return GetNodeData(setId, nodeId, tmp[2]);
                }
                else throw new Exception("Spcforc data name '" + variableName + "' not supported.");
            }

            return null;
        }
        private double[] GetNodeSetResultant(int setId, string componentName)
        {
            int n = nodes.First().Value.Count;
            double[] resultant = new double[n];

            int componentId = -1;
            for (int i = 0; i < componentNames.Length; i++)
			{
			    if (componentName.ToLower() == componentNames[i].ToLower())
                {
                    componentId = i;
                    break;
                }
			}
            if (componentId == -1) throw new Exception("Spcforc component name '" + componentName + "' not supported.");


            foreach (int nodeId in setIds[setId])
            {
                n = 0;
                foreach (SpcData data in nodes[nodeId])
                {
                    if (componentId == 0) resultant[n] += data.force.x;
                    else if (componentId == 1) resultant[n] += data.force.y;
                    else if (componentId == 2) resultant[n] += data.force.z;
                    else if (componentId == 3) resultant[n] += data.moment.x;
                    else if (componentId == 4) resultant[n] += data.moment.y;
                    else if (componentId == 5) resultant[n] += data.moment.z;
                    n++;
                }
            }

            return resultant;
        }
        private double[] GetNodeData(int setId, int nodeId, string componentName)
        {
            int n = nodes.First().Value.Count;
            double[] values = new double[n];

            int componentId = -1;
            for (int i = 0; i < componentNames.Length; i++)
            {
                if (componentName.ToLower() == componentNames[i].ToLower())
                {
                    componentId = i;
                    break;
                }
            }
            if (componentId == -1) throw new Exception("Spcforc component name '" + componentName + "' not supported.");


            n = 0;
            foreach (SpcData spcData in nodes[nodeId])
            {
                if (componentId == 0) values[n] += spcData.force.x;
                else if (componentId == 1) values[n] += spcData.force.y;
                else if (componentId == 2) values[n] += spcData.force.z;
                else if (componentId == 3) values[n] += spcData.moment.x;
                else if (componentId == 4) values[n] += spcData.moment.y;
                else if (componentId == 5) values[n] += spcData.moment.z;
                n++;
            }

            return values;
        }


    }
}
