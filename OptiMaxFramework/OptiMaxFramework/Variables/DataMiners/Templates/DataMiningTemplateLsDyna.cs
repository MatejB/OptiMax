﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public enum LsDynaResultFile
    {
        nodout,
        spcforc,
        matsum,
        elout
    }

    [Serializable]
    public class DataMiningTemplateLsDyna: DataMiningTemplate
    {
        // Variables                                                                                            
        public string VariableName;
        public LsDynaResultFile ResultFile;

        // Properties                                                                                        


        // Constructors                                                                                      
        public DataMiningTemplateLsDyna(LsDynaResultFile resultFile, string variableName)
            : base(true)
        {
            ResultFile = resultFile;
            VariableName = variableName;
        }
    }

    
}
