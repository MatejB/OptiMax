﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public class DataMiningTemplateExcel : DataMiningTemplate
    {
        // Variables                                                                                            
        public string CellRange;


        // Properties                                                                                        


        // Constructors                                                                                      
        public DataMiningTemplateExcel(string cellRange)
            : base(false)
        {
            CellRange = cellRange;
        }
    }

    
}
