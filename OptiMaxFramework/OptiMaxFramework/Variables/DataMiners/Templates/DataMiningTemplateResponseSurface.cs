﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public class DataMiningTemplateResponseSurface : DataMiningTemplate
    {
        // Variables                                                                                            
        public string OutputArrayName;


        // Properties                                                                                        


        // Constructors                                                                                      
        public DataMiningTemplateResponseSurface(string outputArrayName)
            : base(false)
        {
            OutputArrayName = outputArrayName;
        }
    }

    
}
