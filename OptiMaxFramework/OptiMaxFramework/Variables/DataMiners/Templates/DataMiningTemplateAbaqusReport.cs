﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public class DataMiningTemplateAbaqusReport : DataMiningTemplate
    {
        // Variables                                                                                            
        public string HistoryVariableName;


        // Properties                                                                                        


        // Constructors                                                                                      
        public DataMiningTemplateAbaqusReport(string historyVariableName)
            : base(true)
        {
            HistoryVariableName = historyVariableName;
        }
    }

    
}
