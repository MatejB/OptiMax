﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public class DataMiningTemplateDll : DataMiningTemplate
    {
        // Variables                                                                                            
        public string DllName;


        // Properties                                                                                        


        // Constructors                                                                                      
        public DataMiningTemplateDll(string dllName)
            : base(false)
        {
            DllName = dllName;
        }
    }

    
}
