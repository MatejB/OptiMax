﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public class DataMiningTemplateTxt : DataMiningTemplate
    {
        // Variables                                                                                            
        public string FileName;
        public string StartText;
        public int[] RowNumbers;
        public int WordColIndex;
        public string[] WordDelimiters;
        public string DecimalSeparator;


        // Properties                                                                                           


        // Constructors                                                                                         
        public DataMiningTemplateTxt()
            : base(true)
        {
            FileName = null;
            StartText = null;
            RowNumbers = null;
            WordColIndex = -1;
            WordDelimiters = null;
            DecimalSeparator = null;
        }

        public DataMiningTemplateTxt(string startText, int[] rowNumbers, int wordColIndex, string[] wordDelimiters, string decimalSeparator)
            : base(true)
        {
            if (rowNumbers != null && rowNumbers.Length == 0) throw new Exception("At least one row number must be defined for the DataMiningTemplate.");

            FileName = null;
            StartText = startText;
            RowNumbers = rowNumbers;
            WordColIndex = wordColIndex;
            WordDelimiters = wordDelimiters;
            DecimalSeparator = decimalSeparator;
        }
    }

    
}
