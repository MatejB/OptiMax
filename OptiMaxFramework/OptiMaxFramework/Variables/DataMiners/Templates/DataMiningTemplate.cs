﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Helpers;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class DataMiningTemplate
    {
        // Variables                                                                                                                


        // Properties                                                                                                               
        public bool FileBasedTemplate { get; set; }


        // Constructors                                                                                                             
        public DataMiningTemplate(bool fileBasedTemplate)
        {
            FileBasedTemplate = fileBasedTemplate;
        }
    }
}
