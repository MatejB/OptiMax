﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaEloutReader
{

     //   e l e m e n t s t r e s s c a l c u l a t i o n s f o r   t i m e  s t e p        1   (at time 0.00000E+00 )

     //element materl
     //    ipt stress       sig-xx sig-yy sig-zz sig-xy sig-yz sig-zx yield
     //          state effsg      function
     //  28933-      2
     //      1 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00    0.0000E+00
     //  35749-      3
     //      1 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00    0.0000E+00
     //  39837-      4
     //      1 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00    0.0000E+00

     //r e s u l t a n t s   a n d s t r e s s e s f o r   t i m e  s t e p        1   (at time 0.00000E+00 )

     //beam/truss # =   40481      material #  =        15

     //resultants      axial shear-s shear-t moment-s moment-t torsion
     //            0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00

     //beam/truss # =   40482      material #  =        15

     //resultants      axial shear-s shear-t moment-s moment-t torsion
     //            0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00

     //beam/truss # =   40483      material #  =        15

     //resultants      axial shear-s shear-t moment-s moment-t torsion
     //            0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00  0.000E+00

     //e l e m e n t s t r e s s c a l c u l a t i o n s f o r   t i m e  s t e p        1   (at time 0.00000E+00 )

     //element materl(local)
     //ipt-shl stress       sig-xx sig-yy sig-zz sig-xy sig-yz sig-zx plastic
     //          state strain
     //  40480-     16
     //  1- 16 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00
     //  2- 16 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00
     //  40484-     16
     //  1- 16 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00
     //  2- 16 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00
     //  40485-     18
     //  1-  2 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00
     //  2-  2 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00

     //e l e m e n t s t r e s s c a l c u l a t i o n s f o r   t i m e  s t e p      105   (at time 9.92043E-07 )

     //element materl
     //    ipt stress       sig-xx sig-yy sig-zz sig-xy sig-yz sig-zx yield
     //          state effsg      function
     //  28933-      2
     //      1 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00    0.0000E+00


   
    public class LsDynaEloutReader
    {
        string[] splitterDataSet = new string[] { "e l e m e n t   s t r e s s   c a l c u l a t i o n s", "r e s u l t a n t s   a n d   s t r e s s e s" };
        char[] lineSplitter = new char[] { '\r', '\n' };

        Dictionary<int, List<LsDynaSolidTimePoint>> allSolidData;
        List<double> time;

        public LsDynaEloutReader(string file)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-us");

            allSolidData = new Dictionary<int, List<LsDynaSolidTimePoint>>();

            LoadData(File.ReadAllText(file));
        }

        public void LoadData(string stringData)
        {
            string[] dataSets = stringData.Split(splitterDataSet, StringSplitOptions.RemoveEmptyEntries);
            double t;
            List<LsDynaSolidTimePoint> solidData;
            List<LsDynaSolidTimePoint> tmpData;
            
            time = new List<double>();

            foreach (var dataSet in dataSets)
            {
                if (dataSet.Contains("yield"))  // solid element
                {
                    ReadSolidData(dataSet, out t, out solidData);

                    time.Add(t);

                    foreach (LsDynaSolidTimePoint data in solidData)
                    {
                        if (allSolidData.TryGetValue(data.Id, out tmpData)) tmpData.Add(data);
                        else allSolidData.Add(data.Id, new List<LsDynaSolidTimePoint>() { data });
                    }
                }
            }
        }

        private void ReadSolidData(string dataSet, out double time, out List<LsDynaSolidTimePoint> solidData)
        {
            string[] splitter = new string[] { "at time", ")" };
            string[] lines = dataSet.Split(lineSplitter, StringSplitOptions.RemoveEmptyEntries);

            string[] tmp = lines[0].Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            time = double.Parse(tmp[tmp.Length - 1]);

            int first = 4;
            solidData = new List<LsDynaSolidTimePoint>();

            for (int i = first; i + 1 < lines.Length; i+= 2)
            {
                solidData.Add(new LsDynaSolidTimePoint(lines[i], lines[i + 1]));
            }
        }


        public string[] GetAllVariableNames()
        {
            List<string> allNames = new List<string>() { "time" };
            List<string> names = new List<string>();

            if (allSolidData.Count > 0)
            {
                foreach (int elemId in allSolidData.Keys)
                {
                    foreach (string name in LsDynaSolidTimePoint.AllOutputNames)
                    {
                        allNames.Add(elemId + "@" + name);
                    }
                }
            }

            return allNames.ToArray();
        }

        public double[] GetVariableValues(string nodoutVariableName)
        {
            //time
            //1344@x-disp

            string[] tmp = nodoutVariableName.Split(new string[] { "@" }, StringSplitOptions.RemoveEmptyEntries);

            if (tmp.Length == 1)
                return time.ToArray();
            else
            {
                int n;
                double[] result = null;
                List<LsDynaSolidTimePoint> tmpData;
                
                int elementId = int.Parse(tmp[0]);
                string variable = tmp[1];

                if (allSolidData.TryGetValue(elementId, out tmpData))
                {
                    result = new double[tmpData.Count];
                    n = 0;
                    foreach (var data in tmpData)
                    {
                        result[n++] = data.GetResult(variable);
                    }
                }
                return result.ToArray();
            }
        }





    }
}
