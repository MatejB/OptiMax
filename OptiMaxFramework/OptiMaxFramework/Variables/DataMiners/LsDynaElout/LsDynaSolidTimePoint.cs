﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LsDynaEloutReader
{
    // e l e m e n t s t r e s s c a l c u l a t i o n s f o r   t i m e  s t e p        1   (at time 0.00000E+00 )

    // element materl
    //     ipt stress       sig-xx      sig-yy      sig-zz      sig-xy      sig-yz      sig-zx                       yield
    //          state                                                                                  effsg      function
    //  28933-      2
    //      1 elastic   0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00    0.0000E+00    0.0000E+00

    public class LsDynaSolidTimePoint
    {
        // Variables                                                                                                                
        public int Id;
        private double sxx;
        private double syy;
        private double szz;
        private double sxy;
        private double syz;
        private double szx;
        private double effsg;
        private double yield;

        static private string[] idSplitter = new string[] { " ", "\t", "-" };
        static private string[] splitter = new string[] { " ", "\t"};

        // Properties                                                                                                               
        public static string[] AllOutputNames = new string[] { "sig-xx", "sig-yy", "sig-zz", "sig-xy", "sig-yz", "sig-zx", "effsg", "yield_function" };


        // Constructors                                                                                                             
        public LsDynaSolidTimePoint(string line1, string line2)
        {
            string[] tmp = line1.Split(idSplitter, StringSplitOptions.RemoveEmptyEntries);
            Id = int.Parse(tmp[0]);

            line2 = line2.Substring(16, line2.Length - 16 - 1);
            tmp = line2.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            sxx = double.Parse(tmp[0]);
            syy = double.Parse(tmp[1]);
            szz = double.Parse(tmp[2]);
            sxy = double.Parse(tmp[3]);
            syz = double.Parse(tmp[4]);
            szx = double.Parse(tmp[5]);
            effsg = double.Parse(tmp[6]);
            yield = double.Parse(tmp[7]);
        }


        public double GetResult(string elementVariableName)
        {
            // "sig-xx", "sig-yy", "sig-zz", "sig-xy", "sig-yz", "sig-zx", "effsg", "yield_function" 
            switch (elementVariableName)
            {
                case "sig-xx":
                    return sxx;
                case "sig-yy":
                    return syy;
                case "sig-zz":
                    return szz;
                case "sig-xy":
                    return sxy;
                case "sig-yz":
                    return syz;
                case "sig-zx":
                    return szx;
                case "effsg":
                    return effsg;
                case "yield_function":
                    return yield;
                default:
                    return -1;
            }
        }







    }


   


}
