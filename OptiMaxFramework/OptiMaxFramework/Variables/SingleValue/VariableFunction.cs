﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    [HumanReadableClassName("Variable function")]
    public class VariableFunction : VariableBase, IDependent
    {
        // Variables                                                                                                                
        private List<string> functionParameters;    // is used instead of symbolEquation.GetParameterNames() as a speed optimization
        private SymbolEquation symbolEquation;
        private int stringLength;


        // Properties                                                                                                               
        [Browsable(false)]
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        override public string Name
        {
            get { return name; }
            set
            {
                if (functionParameters != null && functionParameters.Contains(value))
                    throw new Exception("The variable functions '" + Name + "' equation contains a self-reference.");

                base.Name = value;
            }
        }
        [CategoryAttribute("Data"),
        DisplayName("Equation"),
        DescriptionAttribute("The equation to be evaluated.")]
        public string Equation
        {
            get { return symbolEquation.Equation; }
            set
            {
                symbolEquation = new SymbolEquation(value);
                functionParameters = symbolEquation.GetParameterNames();
                if (functionParameters.Contains(name))
                    throw new Exception("The variable functions '" + Name + "' equation contains a self-reference.");
            }
        }

        private double Value
        {
            get
            {
                try
                {
                    return symbolEquation.Solve(baseFrameworkItemCollection);
                }
                catch (Exception ex)
                {
                    throw new Exception(ExceptionTools.FormatException(this, ex));
                }
            }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        [CategoryAttribute("Data"),
        DisplayName("Output string length"),
        DescriptionAttribute("Default output string length: -1\r\nTo change the strig length set the value between 6 and 20.")]
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }

        public static string HumanReadableTypeName
        {
            get
            {
                object[] attributes = typeof(VariableFunction).GetCustomAttributes(typeof(HumanReadableClassNameAttribute), false);
                return ((HumanReadableClassNameAttribute)attributes[0]).Name;
            }
        }

        // Constructors                                                                                                             
        public VariableFunction(string name, string description, string equation)
            : base(name, description, VariableValueType.Double)
        {
            Equation = equation;
            stringLength = -1;
        }


        // Methods                                                                                                                  
        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            return Globals.ConvertToString(Value, nfi, stringLength);
        }

        public override VariableConstant ToVariableConstant()
        {
            VariableConstant variable = new VariableConstant(name, description, VariableValueType.Double);
            variable.SetValue(Value);
            variable.StringLength = stringLength;
            return variable;
        }

        public override Item DeepCopy()
        {
            VariableFunction variable = (VariableFunction)this.MemberwiseClone();
            variable.symbolEquation = new SymbolEquation(symbolEquation.Equation);
            variable.functionParameters = new List<string>(functionParameters);
            variable.baseFrameworkItemCollection = null;
            return variable;
        }

        // IDependent                                                                                                                  
        [BrowsableAttribute(false)]
        public List<string> InputVariables
        {
            get
            {
                return functionParameters;
            }
        }

      

    }
}
