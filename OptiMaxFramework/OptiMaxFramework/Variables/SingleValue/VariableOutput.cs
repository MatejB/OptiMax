﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class VariableOutput : VariableBase, IDependent
    {
        // Variables                                                                                          
        private string outputItemName;
        private DataMiningTemplate dataMiningTemplate;
        private DateTime lastWriteTime;
        private double valueDouble;
        private string valueString;
        private int stringLength;


        // Properties                                                                                         
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        public string OutputItemName
        {
            get
            {
                return outputItemName;
            }
        }

        public DataMiningTemplate Template
        {
            get
            {
                return dataMiningTemplate;
            }
        }
        
        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }


        // Constructors                                                                                       
        public VariableOutput(string name, string description, VariableValueType valueType, string outputItemName, DataMiningTemplate dataMiningTemplate)
            : base(name, description, valueType)
        {
            this.outputItemName = outputItemName;
            this.dataMiningTemplate = dataMiningTemplate;

            if (dataMiningTemplate is DataMiningTemplateExcel)
            {
                if (!Helpers.ExcelHelper.IsThisSingleCellRangeValid((dataMiningTemplate as DataMiningTemplateExcel).CellRange))
                    throw new Exception("The cell range '" + (dataMiningTemplate as DataMiningTemplateExcel).CellRange + "' of the output variable '" + name + "' is not" +
                                        " a valid single cell range.");
            }

            //valueDouble = -1;
            //valueString = "-1";

            lastWriteTime = new DateTime(0);
            stringLength = -1;
        }


        // Methods                                                                                            
        public void SetDoubleValueFromOutputItem(double newValue)
        {
            valueDouble = newValue;
        }
        public void SetStringValueFromOutputItem(string newValue)
        {
            valueString = newValue;
        }

        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }
        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            if (dataMiningTemplate.FileBasedTemplate)
            {
                Item item = FrameworkItemCollection[outputItemName];
                FileResource fileResource;
                if (item is FileResource fs) fileResource = fs;
                else throw new Exception();

                DateTime current = System.IO.File.GetLastWriteTime(fileResource.FileNameWithPath);

                // update the value from the File resource if necessary
                if (lastWriteTime < current)
                {
                    if (valueType == VariableValueType.Double || valueType == VariableValueType.Integer)
                        valueDouble = DataMiner.GetScalarDoubleData(fileResource, dataMiningTemplate);
                    else if (valueType == VariableValueType.String)
                        valueString = DataMiner.GetScalarStringData(fileResource, dataMiningTemplate);

                    lastWriteTime = current;
                }
            }
            else
            {
               // all values are already set from the output item
            }

            if (valueType == VariableValueType.Double)
                return Globals.ConvertToString(valueDouble, nfi, stringLength);
            else if (valueType == VariableValueType.Integer)
                return Globals.ConvertToString((int)Math.Round(valueDouble, 0), stringLength);
            else if (valueType == VariableValueType.String)
                return Globals.ConvertToString(valueString, stringLength);
            else throw new Exception();
        }
        public override VariableConstant ToVariableConstant()
        {
            VariableConstant variable = new VariableConstant(name, description, valueType);
            if (valueType == VariableValueType.Double)
                variable.SetValue(valueDouble);
            else if (valueType == VariableValueType.Integer)
                variable.SetValue((int)Math.Round(valueDouble, 0));
            else //if (valueType == VariableValueType.String)
                variable.SetValue(valueString);
            
            variable.StringLength = stringLength;
            return variable;
        }

        public override Item DeepCopy()
        {
            //VariableOutput variable = new VariableOutput(name, description, valueType, outputFileResourceName, dataMiningTemplate);
            //variable.lastWriteTime = lastWriteTime;
            //variable.value = value;
            //variable.stringLength = stringLength;

            VariableOutput variable = (VariableOutput)this.MemberwiseClone();
            variable.baseFrameworkItemCollection = null;
            return variable;
        }

        // IDependent                                                                                                                  
        public List<string> InputVariables
        {
            get
            {
                return new List<string>() { outputItemName };
            }
        }

       
    }
}
