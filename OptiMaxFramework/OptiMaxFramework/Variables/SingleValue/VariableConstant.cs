﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    [HumanReadableClassName("Constant variable")]
    public class VariableConstant : VariableBase
    {
        // Variables                                                                                          
        private double valueDouble;
        private int valueInteger;
        private string valueString;
        private int stringLength;


        // Properties                                                                                         
        [CategoryAttribute("Data"),
        DisplayName("Data type"),
        DescriptionAttribute("The type of the constant variable data.")]
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        [CategoryAttribute("Data"),
        DisplayName("Output string length"),
        DescriptionAttribute("Default output string length: -1\r\nTo change the strig length set the value between 6 and 20.")]
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }

        [CategoryAttribute("Data"),
        DisplayName("Value"),
        DescriptionAttribute("The value of the constant variable.")]
        public string Value
        {
            get
            {
                return GetValueAsString();
            }
            set
            {
                if (valueType == VariableValueType.Double)
                    SetValue(double.Parse(value, OptiMaxFramework.Globals.nfi));
                else if (valueType == VariableValueType.Integer)
                    SetValue(int.Parse(value));
                else if (valueType == VariableValueType.String)
                    SetValue(value);
                else throw new Exception("The supplied value type is not supported.");
            }

        }

        public static string HumanReadableTypeName
        {
            get
            {
                object[] attributes = typeof(VariableConstant).GetCustomAttributes(typeof(HumanReadableClassNameAttribute), false);
                return ((HumanReadableClassNameAttribute)attributes[0]).Name;
            }
        }


        // Constructors                                                                                       
        public VariableConstant(string name, string description, VariableValueType valueType)
            : base(name, description, valueType)
        {
            this.valueType = valueType;
            valueDouble = 0;
            valueInteger = 0;
            valueString = "";
            stringLength = -1;
        }

        public VariableConstant(VariableConstant variable) 
            : base(variable.Name, variable.Description, variable.ValueType)
        {
            valueDouble = variable.valueDouble;
            valueInteger = variable.valueInteger;
            valueString = variable.valueString;

            stringLength = variable.StringLength;
        }


        // Methods                                                                                            
        
        public void SetValue(double value)
        {
            if (valueType != VariableValueType.Double) throw new Exception("Wrong variable value type.");
            valueDouble = value;
        }

        public void SetValue(int value)
        {
            if (valueType != VariableValueType.Integer) throw new Exception("Wrong variable value type.");
            valueInteger = value;
        }

        public void SetValue(string value)
        {
            if (valueType != VariableValueType.String) throw new Exception("Wrong variable value type.");
            valueString = value;
        }

        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            if (valueType == VariableValueType.Double)
                return Globals.ConvertToString(valueDouble, nfi, stringLength);
            else if (valueType == VariableValueType.Integer)
                return Globals.ConvertToString(valueInteger, stringLength);
            else return valueString;
        }

        public override VariableConstant ToVariableConstant()
        {
            return new VariableConstant(this);
        }

        public override Item DeepCopy()
        {
            return new VariableConstant(this);
        }
       

    }
}
