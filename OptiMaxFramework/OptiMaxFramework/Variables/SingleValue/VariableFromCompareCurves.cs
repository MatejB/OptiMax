﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public enum CurveCompareMethod
    {
        MeanSquareError,
        RelativeMeanSquareError
    }

    [Serializable]
    public class VariableFromCompareCurves : VariableBase, IDependent
    {
        // Variables                                                                                                                
        private string dataXtarget;
        private string dataYtarget;
        private bool normalizeXtarget;
        private string dataXcomputed;
        private string dataYcomputed;
        private bool normalizeXcomputed;
        private CurveCompareMethod method;
        private bool extrapolateWith0;
        private List<string> allParameters;         // is used instead of symbolEquation.GetParameterNames() as a speed optimization
        private int stringLength;


        // Properties                                                                                                               
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        public string DataXtargetName
        {
            get
            {
                return dataXtarget;
            }
        }
        public string DataYtargetName
        {
            get
            {
                return dataYtarget;
            }
        }
        public bool NormalizeXtarget
        {
            get
            {
                return normalizeXtarget;
            }
        }
        public string DataXcomputedName
        {
            get
            {
                return dataXcomputed;
            }
        }
        public string DataYcomputedName
        {
            get
            {
                return dataYcomputed;
            }
        }
        public bool NormalizeXcomputed
        {
            get
            {
                return normalizeXcomputed;
            }
        }
        public CurveCompareMethod Method
        {
            get
            {
                return method;
            }
        }
        public bool ExtrapolateWith0
        {
            get
            {
                return extrapolateWith0;
            }
        }
        private double Value
        {
            get
            {
                double[] xTargetArray = GetNumericArrayFromArrayName(dataXtarget);
                double[] yTargetArray = GetNumericArrayFromArrayName(dataYtarget);
                double[] xComputedArray = GetNumericArrayFromArrayName(dataXcomputed);
                double[] yComputedArray = GetNumericArrayFromArrayName(dataYcomputed);

                if (xTargetArray.Length < 2 || yTargetArray.Length < 2 || xComputedArray.Length < 2 || yComputedArray.Length < 2)
                    throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. Both curves should contain al least 2 points.");

                if (xTargetArray.Length != yTargetArray.Length)
                    throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. X and Y arrays of curve 1 must have equal number of elements.");

                if (xComputedArray.Length != yComputedArray.Length)
                    throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. X and Y arrays of curve 2 must have equal number of elements.");

                if (method == CurveCompareMethod.MeanSquareError)
                {
                    if (IsArrayMonotonouslyDecreasing(xTargetArray))
                    {
                        // invert x and y
                        InvertArray(xTargetArray);
                        InvertArray(yTargetArray);
                    }
                    else if (!IsArrayMonotonouslyIncreasing(xTargetArray))
                        throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. Data X1 is not monotonously increasing or decreasing.");

                    if (IsArrayMonotonouslyDecreasing(xComputedArray))
                    {
                        // invert x and y
                        InvertArray(xComputedArray);
                        InvertArray(yComputedArray);
                    }
                    else if (!IsArrayMonotonouslyIncreasing(xComputedArray))
                        throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. Data X2 is not monotonously increasing or decresing.");
                }

                // normalize x axes
                if (normalizeXtarget) NormalizeArray(xTargetArray);
                if (normalizeXcomputed) NormalizeArray(xComputedArray);

                if (method == CurveCompareMethod.MeanSquareError)
                {
                    double error = 0;
                    double yTarget;
                    double yComputed;
                    double x;
                    int startIndexComputed = 0;
                    int startIndexTarget = 0;

                    double minX = Math.Max(xTargetArray[0], xComputedArray[0]);
                    double targetMaxX = xTargetArray.Last();
                    double computedMaxX = xComputedArray.Last();
                    double maxX;
                    if (extrapolateWith0) maxX = Math.Max(targetMaxX, computedMaxX);
                    else maxX = Math.Min(targetMaxX, computedMaxX);

                    int numOfPoints = 1000;     // ##############################################################

                    double delta = (maxX - minX) / (numOfPoints - 1);

                    if (delta > 0)
                    {
                        for (int i = 0; i < numOfPoints; i++)
                        {
                            x = minX + i * delta;

                            // computed
                            if (x <= computedMaxX) yComputed = GetInterpolatedYValueAtX(x, xComputedArray, yComputedArray, ref startIndexComputed);
                            else yComputed = 0;

                            // target
                            if (x <= targetMaxX) yTarget = GetInterpolatedYValueAtX(x, xTargetArray, yTargetArray, ref startIndexTarget);
                            else yTarget = 0;

                            error += Math.Pow(yComputed - yTarget, 2);
                        }
                    }
                    else throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' property Value. The X axes of the curves 1 and 2 do not overlap.");

                    return error / numOfPoints;
                }
                else throw new NotSupportedException();
            }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }


        // Constructors                                                                                       
        public VariableFromCompareCurves(string name, string description, VariableValueType valueType,
                                         string dataX1, string dataY1, bool normalizeX1,
                                         string dataX2, string dataY2, bool normalizeX2,
                                         CurveCompareMethod method, bool extrapolateWith0)
            : base(name, description, valueType)
        {
            this.dataXtarget = dataX1;
            this.dataYtarget = dataY1;
            this.normalizeXtarget = normalizeX1;
            this.dataXcomputed = dataX2;
            this.dataYcomputed = dataY2;
            this.normalizeXcomputed = normalizeX2;
            this.method = method;
            this.extrapolateWith0 = extrapolateWith0;
            allParameters = new List<string>() { dataX1, dataY1, dataX2, dataY2 };
        }

        // Methods                                                                                            
        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }
        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            if (valueType == VariableValueType.Double)
                return Globals.ConvertToString(Value, nfi, stringLength);
            else if (valueType == VariableValueType.Integer)
                return Globals.ConvertToString((int)Math.Round(Value, 0), stringLength);
            else throw new Exception();
        }
        public override VariableConstant ToVariableConstant()
        {
            VariableConstant variable = new VariableConstant(name, description, valueType);
            variable.SetValue(Value);
            variable.StringLength = stringLength;
            return variable;
        }
        public override Item DeepCopy()
        {
            VariableFromCompareCurves variable = (VariableFromCompareCurves)this.MemberwiseClone();
            variable.allParameters = new List<string>(allParameters);
            variable.baseFrameworkItemCollection = null;
            return variable;
        }

        private double[] GetNumericArrayFromArrayName(string arrayName)
        {
            double[] valuesNumeric = null;
            string[] valuesString = null;

            Item item = FrameworkItemCollection[arrayName];
            ArrayBase array;
            if (item is ArrayBase) array = (ArrayBase)item;
            else throw new Exception();

            int len = array.Length;
            if (valueType == VariableValueType.Double || valueType == VariableValueType.Integer)
            {
                valuesNumeric = new double[len];
                valuesString = array.GetValuesAsString().Split(' ');
                for (int i = 0; i < len; i++)
                {
                    valuesNumeric[i] = double.Parse(valuesString[i], OptiMaxFramework.Globals.nfi);
                }
            }
            else throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The value type '" +
                                      valueType.ToString() + "' is not supported.");

            return valuesNumeric;
        }
        private void SwapArrays(double[] array1, double[] array2)
        {
            double[] tmp = array1.ToArray();
            array1 = array2;
            array2 = tmp;
        }
        private bool IsArrayMonotonouslyIncreasing(double[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] < array[i - 1])
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsArrayMonotonouslyDecreasing(double[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] > array[i - 1])
                {
                    return false;
                }
            }
            return true;
        }
        private void NormalizeArray(double[] array)
        {
            // normalize curves
            double minValue = array[0];
            double maxValue = array[array.Length - 1];
            double delta = maxValue - minValue;
            if (delta == 0) throw new Exception("Exception in VariableFromCurveCompare '" + Name + "' method NormalizeArray. Delta = 0.");

            array[0] = 0;
            array[array.Length - 1] = 1;

            for (int i = 1; i < array.Length - 1; i++)
            {
                array[i] = (array[i] - minValue) / delta;
            }
        }
        private void InvertArray(double[] array)
        {
            double[] copy = array.ToArray();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = copy[array.Length - 1 - i];
            }
        }
        private double GetInterpolatedYValueAtX(double x, double[] arrayX, double[] arrayY, ref int startIndex)
        {
            double x1 = 0;
            double x2 = 0;
            double y = 0;

            int endIndex = arrayY.Length;

            for (int i = startIndex; i < endIndex; i++)
            {
                if (arrayX[i] > x)
                {
                    x2 = arrayX[i];
                    startIndex = Math.Max(0, i - 1);
                    x1 = arrayX[startIndex];
                    break;
                }
                if (i == endIndex - 1)  // the interval was not found - take the last interval and extrapolate
                {
                    startIndex = Math.Max(0, endIndex - 2);
                    x1 = arrayX[startIndex];
                    x2 = arrayX[endIndex - 1];
                }
            }

            if (x1 == x2)
                y = arrayY[startIndex];
            else
            {
                double y1 = arrayY[startIndex];
                double y2 = arrayY[startIndex + 1];

                double k = (y2 - y1) / (x2 - x1);
                y = y1 + k * (x - x1);
            }

            return y;
        }

        // IDependent                                                                                         
        public List<string> InputVariables
        {
            get
            {
                return allParameters;
            }
        }
        public static new string ToString()
        {
            return "Variable from curve compare";
        }
    }
}
