﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public enum VariableValueType
    {
        Double,
        Integer,
        String
    }

    [Serializable]
    public abstract class VariableBase : Item , IDeepCopy, IRecordable
    {
        // Variables
        protected VariableValueType valueType;


        // Properties
        abstract public VariableValueType ValueType { get; set; }


        // Constructors                                                                                                                    
        public VariableBase(string name, string description, VariableValueType variableValueType)
            : base(name, description)
        {
            valueType = variableValueType;
        }


        // Mathods                                                                                                                         
        abstract public string GetValueAsString();

        abstract public string GetValueAsString(System.Globalization.NumberFormatInfo nfi);

        abstract public Item DeepCopy();

        abstract public VariableConstant ToVariableConstant();
    }
}
