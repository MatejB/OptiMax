﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    [HumanReadableClassName("Input variable")]
    public class VariableInput : VariableBase
    {
        // Variables                                                                                          
        protected double valueDouble;
        protected int valueInteger;
        protected int stringLength;


        // Properties                                                                                         
        [CategoryAttribute("Data"),
        DisplayName("Data type"),
        DescriptionAttribute("The type of the input variable data.")]
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }


        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        [CategoryAttribute("Data"),
        DisplayName("Output string length"),
        DescriptionAttribute("Default output string length: -1. To change the strig length set the value between 6 and 20.")]
        public int StringLength
        { 
            get 
            { 
                return stringLength; 
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }

        public static string HumanReadableTypeName
        {
            get
            {
                object[] attributes =  typeof(VariableInput).GetCustomAttributes(typeof(HumanReadableClassNameAttribute), false);
                return ((HumanReadableClassNameAttribute)attributes[0]).Name;
            }
        }


        // Constructors                                                                                       
        public VariableInput(string name, string description, VariableValueType valueType)
            : base(name, description, valueType)
        {
            valueDouble = 0;
            valueInteger = 0;
            stringLength = -1;
        }


        // Methods                                                                                            
        public void SetValue(double value)
        {
            if (valueType != VariableValueType.Double) throw new Exception("Wrong variable value type.");
            valueDouble = value;
        }
        public void SetValue(int value)
        {
            if (valueType != VariableValueType.Integer) throw new Exception("Wrong variable value type.");
            valueInteger = value;
        }
        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }
        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            if (valueType == VariableValueType.Double) return Globals.ConvertToString(valueDouble, nfi, stringLength);
            else if (valueType == VariableValueType.Integer) return Globals.ConvertToString(valueInteger, stringLength);
            else throw new Exception();
        }
        public override VariableConstant ToVariableConstant()
        {
            VariableConstant variable = new VariableConstant(name, description, valueType);
            if (valueType == VariableValueType.Double) variable.SetValue(valueDouble);
            else if (valueType == VariableValueType.Integer) variable.SetValue(valueInteger);
            variable.StringLength = stringLength;
            return variable;
        }
        public override Item DeepCopy()
        {
            VariableInput variable = (VariableInput)this.MemberwiseClone();
            variable.baseFrameworkItemCollection = null;
            return variable;
        }
      

        
    }
}
