﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public enum ArrayToVariableOperation
    {
        Average,
        Min,
        MinID,
        Max,
        MaxID,
        Sum,
        GetElementByNumber
    }

    [Serializable]
    public class VariableFromArray : VariableBase, IDependent
    {
        // Variables                                                                                                                
        private string arrayName;
        private ArrayToVariableOperation operation;
        private SymbolEquation elementNumberEq;
        private List<string> allParameters;         // is used instead of symbolEquation.GetParameterNames() as a speed optimization
        private int stringLength;


        // Properties                                                                                                               
        public override VariableValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        public string ArrayName
        {
            get
            {
                return arrayName;
            }
        }

        public ArrayToVariableOperation Operation
        {
            get
            {
                return operation;
            }
        }

        public string Equation
        {
            get { return elementNumberEq.Equation; }
        }

        private object Value
        {
            get
            {
                double[] valuesNumeric = null;
                string[] valuesString = null;

                Item item = FrameworkItemCollection[arrayName];
                ArrayBase array;
                if (item is ArrayBase) array = (ArrayBase)item;
                else throw new Exception();

                if (valueType == VariableValueType.Double || valueType == VariableValueType.Integer)
                {
                    valuesString = array.GetValuesAsString().Split(' ');
                    int len = valuesString.Length;
                    
                    valuesNumeric = new double[len];
                    for (int i = 0; i < len; i++)
                    {
                        valuesNumeric[i] = double.Parse(valuesString[i], OptiMaxFramework.Globals.nfi);
                    }

                    double result = double.NaN;
                    int id = -1;
                    int count = 0;

                    switch (operation)
                    {
                        case ArrayToVariableOperation.Average:
                            result = 0;
                            foreach (double value in valuesNumeric)
                            {
                                result += value;
                            }
                            result /= len;
                            break;
                        
                        case ArrayToVariableOperation.Min:
                            result = double.MaxValue;
                            foreach (double value in valuesNumeric)
                            {
                                if (value < result) result = value;
                            }
                            break;
                        
                        case ArrayToVariableOperation.MinID:
                            result = double.MaxValue;
                            foreach (double value in valuesNumeric)
                            {
                                if (value < result)
                                {
                                    result = value;
                                    id = count;
                                }
                                count++;
                            }
                            break;
                        
                        case ArrayToVariableOperation.Max:
                            result = -double.MaxValue;
                            foreach (double value in valuesNumeric)
                            {
                                if (value > result) result = value;
                            }
                            break;
                        
                        case ArrayToVariableOperation.MaxID:
                            result = -double.MaxValue;
                            foreach (double value in valuesNumeric)
                            {
                                if (value > result)
                                {
                                    result = value;
                                    id = count;
                                }
                                count++;
                            }
                            break;
                        
                        case ArrayToVariableOperation.Sum:
                            result = 0;
                            foreach (double value in valuesNumeric)
                            {
                                result += value;
                            }
                            break;
                        
                        case ArrayToVariableOperation.GetElementByNumber:
                            result = 0;
                            int elementNumber = (int)Math.Round(elementNumberEq.Solve(baseFrameworkItemCollection), 0);

                            if (elementNumber >= 0 && elementNumber < valuesNumeric.Length)
                                result = valuesNumeric[elementNumber];
                            else
                                throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The element number '" + elementNumber.ToString() + "' does not exsist.");
                            break;
                        
                        default:
                            throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The operation '" + operation.ToString() + "' is not supported.");
                            //break;
                    }

                    if (double.IsNaN(result)) throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The result was not found.");
                    else if (id != -1) return id;
                    else return result;
                }
                else if (valueType == VariableValueType.String)
                {
                    if (operation == ArrayToVariableOperation.GetElementByNumber)
                    {
                        valuesString = array.GetValuesAsString().Split(' ');
                        int len = valuesString.Length;

                        int elementNumber = (int)Math.Round(elementNumberEq.Solve(baseFrameworkItemCollection), 0);

                        if (elementNumber >= 0 && elementNumber < valuesString.Length)
                            return valuesString[elementNumber];
                        else
                            throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The element number '" + elementNumber.ToString() + "' does not exsist.");
                    }
                    else
                        throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The operation '" + operation.ToString() + "' is not supported for variables of type string.");
                }
                else
                    throw new Exception("Exception in VariableFromArray '" + Name + "' property Value. The selected valu type is not supported.");
            }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public int StringLength
        {
            get
            {
                return stringLength;
            }
            set
            {
                if (value >= 6) stringLength = value;
                else stringLength = -1;
            }
        }


        // Constructors                                                                                       
        public VariableFromArray(string name, string description, VariableValueType valueType, string arrayName, 
            ArrayToVariableOperation operation, string elementNumberEq = "")
            : base(name, description, valueType)
        {
            this.arrayName = arrayName;
            this.operation = operation;
            this.elementNumberEq = new SymbolEquation(elementNumberEq);
            allParameters = this.elementNumberEq.GetParameterNames();
            allParameters.Add(arrayName);
        }


        // Event handling                                                                                     


        // Methods                                                                                            
        public override string GetValueAsString()
        {
            return GetValueAsString(OptiMaxFramework.Globals.nfi);
        }

        public override string GetValueAsString(System.Globalization.NumberFormatInfo nfi)
        {
            if (valueType == VariableValueType.Double)
                return Globals.ConvertToString((double)Value, nfi, stringLength);
            else if (valueType == VariableValueType.Integer)
                return Globals.ConvertToString((int)Math.Round((double)Value, 0), stringLength);
            else if (valueType == VariableValueType.String)
                return (string)Value;
            else throw new Exception();
        }

        public override VariableConstant ToVariableConstant()
        {
            VariableConstant variable = new VariableConstant(name, description, valueType);
            if (valueType == VariableValueType.Double) variable.SetValue((double)Value);
            else if (valueType == VariableValueType.Integer) variable.SetValue((int)Math.Round((double)Value, 0));
            else if (valueType == VariableValueType.String) variable.SetValue((string)Value);
            variable.StringLength = stringLength;
            return variable;
        }

        public override Item DeepCopy()
        {
            VariableFromArray variable = (VariableFromArray)this.MemberwiseClone();
            variable.elementNumberEq = new SymbolEquation(elementNumberEq.Equation);
            variable.allParameters = new List<string>(allParameters);
            variable.baseFrameworkItemCollection = null;
            return variable;
        }

        // IDependent                                                                                         
        public List<string> InputVariables
        {
            get
            {
                return allParameters;
            }
        }

        public static new string ToString()
        {
            return "Variable from array";
        }
    }
}
