﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OptiMaxFramework.NSGAII;

using JMetalCSharp.Core;
using JMetalCSharp.Operators.Crossover;
using JMetalCSharp.Operators.Mutation;
using JMetalCSharp.Operators.Selection;
using JMetalCSharp.Problems;
using JMetalCSharp.QualityIndicator;
using JMetalCSharp.Utils;

namespace OptiMaxFramework
{
    [Serializable]
    public class NSGAIIDriver : Driver, IDependent
    {
        // Variables                                                                                                                
        private int recCount;
        private int populationSize;
        private int numOfGenerations;
        private double crossoverProb;
        private double mutationProb;
        private string initialPopulationArrayName;

        private BindingList<ConstraintVariableName> constraintNamesBl;
        private BindingList<ObjectiveVariableName> objectiveNamesBl;


        // Properties                                                                                                                
        [CategoryAttribute("Control parameters"),
         DisplayName("Population size"),
         DescriptionAttribute("The number of individuals in a population.")]
        public int PopulationSize
        {
            get { return populationSize; }
            set {
                if (value < 2) throw new Exception("Population size must be larger than 1.");
                populationSize = value; 
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Number of generations"),
        DescriptionAttribute("The number of iterations of the NSGAII algorithm.")]
        public int NumOfGenerations
        {
            get { return numOfGenerations; }
            set {
                if (value < 1) throw new Exception("Number of generations must be larger than 0.");
                numOfGenerations = value; 
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Crossover probability"),
        DescriptionAttribute("The crossover probability of the SBX crossover of the NSGAII algorithm.")]        
        public double CrossoverProb
        {
            get
            {
                return crossoverProb;
            }
            set
            {
                crossoverProb = value;
                if (crossoverProb <= 0) crossoverProb = 0;
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Mutation probability"),
        DescriptionAttribute("The mutaion probability of the polinomial mutation of the NSGAII algorithm.")]
        public double MutationProb
        {
            get
            {
                return mutationProb;
            }
            set
            {
                mutationProb = value;
                if (mutationProb <= 0) mutationProb = 0;
            }
        }


        [CategoryAttribute("Objective variables"),
        DisplayName("List of objective variables"),
        DescriptionAttribute("Select the variables for the list of objective functions.")]
        public BindingList<ObjectiveVariableName> ObjectiveNames
        {
            get
            {
                UpdateResultItemNames(); // the set property is not called by property grid
                return objectiveNamesBl;
            }
            set
            {
                objectiveNamesBl = value;
                UpdateResultItemNames();
            }
        }

        [CategoryAttribute("Constraints"),
        DisplayName("List of constraints"),
        DescriptionAttribute("Select the variables for the list of constraints.")]
        public BindingList<ConstraintVariableName> ConstraintNames
        {
            get
            {
                UpdateResultItemNames(); // the set property is not called by property grid
                return constraintNamesBl;
            }
            set
            {
                constraintNamesBl = value;
                UpdateResultItemNames();
            }
        }

        [CategoryAttribute("Initial population"),
        DisplayName("Initial population array"),
        DescriptionAttribute("Select the array for the initial population. Each row of the array represents one individual. Parameters must be separated by a space character."),
        TypeConverter(typeof(ArrayNamesWithEmptyTypeConverter))]
        public string InitialpopulationArrayName
        {
            get
            {
                if (initialPopulationArrayName == null) return "";
                else return initialPopulationArrayName;
            }
            set
            {
                initialPopulationArrayName = value;
                if (initialPopulationArrayName == "") initialPopulationArrayName = null;
            }
        }
        


        // Constructors                                                                                                             
        public NSGAIIDriver(string name, string description)
            : this(name, description, null, null, 10, 10, null)
        {
        }

        public NSGAIIDriver(string name, string description, string[] objectiveVariableNames, string[] constraintVariableNames, int populationSize, int numOfGenerations,
                            string initialPopulationArrayName)
            : base(name, description)
        {
            crossoverProb = 0.9;
            mutationProb = 1.0;

            objectiveNamesBl = new BindingList<ObjectiveVariableName>();
            if (objectiveVariableNames != null)
            {
                foreach (var objName in objectiveVariableNames) objectiveNamesBl.Add(new ObjectiveVariableName() {Name = objName });
            }
            
            constraintNamesBl = new BindingList<ConstraintVariableName>();
            if (constraintVariableNames != null)
            {
                foreach (var constrName in constraintVariableNames) constraintNamesBl.Add(new ConstraintVariableName() { Name = constrName });
            }

            UpdateResultItemNames();

            this.populationSize = populationSize;
            this.numOfGenerations = numOfGenerations;
            this.initialPopulationArrayName = initialPopulationArrayName;

            objectiveNamesBl = new BindingList<ObjectiveVariableName>();
            constraintNamesBl = new BindingList<ConstraintVariableName>();
        }
        

        // Methods                                                                                                                  
        public override void RunEvaluation()
        {
            bool err = CorrectlyDefined(baseFrameworkItemCollection);

            int numOfEvals = populationSize * numOfGenerations;
            numOfPlaces = (numOfEvals + 1).ToString().Length;

            string solutionID;
            jobStatusData = new JobStatusData[populationSize];
            for (int i = 0; i < jobStatusData.Length; i++)
            {
                solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                jobStatusData[i] = new JobStatusData() { ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0) };
            }

            recCount = 0;

            NSGAII();
        }
        public void NSGAII()
        {
            ///usage: NSGAII(problemName paretoFrontFile)
            
            Problem problem; // The problem to solve
            Algorithm algorithm; // The algorithm to use
            Operator crossover; // Crossover operator
            Operator mutation; // Mutation operator
            Operator selection; // Selection operator

            Dictionary<string, object> operatorParameters; // Operator parameters

            QualityIndicator indicators; // Object to get quality indicators

            int numOfConstraints = 0;
            if (constraintNamesBl != null) numOfConstraints = constraintNamesBl.Count;

            indicators = null;
            problem = new OptiMaxProblem(parameters.Length, objectiveNamesBl.Count, numOfConstraints, ObjectiveFunction);

            algorithm = new NSGAIIOptiMax(problem, OnRecordTheBestSolutions);

            // Algorithm parameters
            algorithm.SetInputParameter("populationSize", populationSize);
            algorithm.SetInputParameter("maxGenerationsCount", numOfGenerations);

            double[][] initialPopulation = GetInitialPopulation();
            algorithm.SetInputParameter("initialPopulation", initialPopulation);

            if (false) // testing for binary
            {
                // Mutation and Crossover for binary codification 
                operatorParameters = new Dictionary<string, object>();
                operatorParameters.Add("probability", 0.9);
                operatorParameters.Add("distributionIndex", 20.0);
                crossover = CrossoverFactory.GetCrossoverOperator("SinglePointCrossover", operatorParameters);

                operatorParameters = new Dictionary<string, object>();
                operatorParameters.Add("probability", 1.0 / problem.NumberOfVariables);
                operatorParameters.Add("distributionIndex", 20.0);
                mutation = MutationFactory.GetMutationOperator("BitFlipMutation", operatorParameters);
            }
            else
            {
                // Mutation and Crossover for Real codification 
                operatorParameters = new Dictionary<string, object>();
                operatorParameters.Add("probability", crossoverProb);
                operatorParameters.Add("distributionIndex", 20.0);
                crossover = CrossoverFactory.GetCrossoverOperator("SBXCrossover", operatorParameters);

                operatorParameters = new Dictionary<string, object>();
                operatorParameters.Add("probability", mutationProb / problem.NumberOfVariables);
                operatorParameters.Add("distributionIndex", 20.0);
                mutation = MutationFactory.GetMutationOperator("PolynomialMutation", operatorParameters);
            }
            

            // Selection Operator 
            operatorParameters = null;
            selection = SelectionFactory.GetSelectionOperator("BinaryTournament2", operatorParameters);

            // Add the operators to the algorithm
            algorithm.AddOperator("crossover", crossover);
            algorithm.AddOperator("mutation", mutation);
            algorithm.AddOperator("selection", selection);

            // Add the indicator object to the algorithm
            algorithm.SetInputParameter("indicators", indicators);

            // Execute the Algorithm
            long initTime = Environment.TickCount;
            SolutionSet paretoPopulation = algorithm.Execute();
            long estimatedTime = Environment.TickCount - initTime;

            // Result messages 
            //paretoPopulation.PrintVariablesToFile("VAR");
            //paretoPopulation.PrintObjectivesToFile("FUN");
        }

        private double[][] GetInitialPopulation()
        {
            string[] tmp;
            double[][] initialPopulation = null;

            if (initialPopulationArrayName != null)
            {
                ArrayBase array = baseFrameworkItemCollection[initialPopulationArrayName] as ArrayBase;
                int length = array.Length;
                double value;

                if (length < populationSize) throw new Exception("The initial population defined in '" + initialPopulationArrayName +
                                                                  "' does not have " + populationSize + " individual(s).");
                else if (length > populationSize)
                    base.OnReportStatusData("Warning: Only first " + populationSize + " individual(s) will be used for optimization.");

                initialPopulation = new double[length][];

                for (int i = 0; i < length; i++)
                {
                    tmp = array.GetValueAsString(i).Split(new string[] { ", ", " " }, StringSplitOptions.RemoveEmptyEntries);

                    if (tmp.Length != parameters.Length)
                        throw new Exception("The initial individual in row " + (i + i) + " defined in '" + initialPopulationArrayName +
                                            "' does not have exactly " + parameters.Length + " parameter(s).");

                    initialPopulation[i] = new double[tmp.Length];
                    for (int j = 0; j < tmp.Length; j++)
                    {
                        if (double.TryParse(tmp[j], System.Globalization.NumberStyles.Number, Globals.nfi, out value))
                        {
                            value = (value - parameters[j].Min) / (parameters[j].Max - parameters[j].Min);
                            if (value < 0 || value > 1)
                            {
                                throw new Exception("The initial individual in row " + (i + i) + " defined in '" + initialPopulationArrayName +
                                                     "' contains a value that is ouside of the prescribed interval [min, max].");
                            }
                            initialPopulation[i][j] = value;
                        }
                        else
                        {
                            throw new Exception("The initial individual in row " + (i + i) + " defined in '" + initialPopulationArrayName +
                                                "' can not be converted to numeric values.");
                        }
                    }
                }
            }

            return initialPopulation;
        }
        private double[][] ObjectiveFunction(double[][] allSamples)
        {
            // reset the job status
            for (int i = 0; i < jobStatusData.Length; i++)
                jobStatusData[i].jobStatus = JobStatus.InQueue;

            int numSam = allSamples.GetLength(0);
            int numPar = allSamples[0].Length;

            string[][] resultsData;
            double[][] results = new double[numSam][];

            // evaluate all samples
            resultsData = EvaluateAllSamples(allSamples);
            for (int i = 0; i < numSam; i++)
            {
                results[i] = new double[resultsData[i].Length];
                for (int j = 0; j < results[i].Length; j++)
                {
                    results[i][j] = double.Parse(resultsData[i][j], OptiMaxFramework.Globals.nfi);
                }
            }

            return results;
        }
        private void OnRecordTheBestSolutions(SolutionSet paretoFront)
        {
            recCount++;
            if (theBestHistoryItemsToExtract.Length > 0)
            {
                JMetalCSharp.Utils.Wrapper.XReal vars;
                Dictionary<string, List<Item>> theBest = new Dictionary<string, List<Item>>();

                foreach (var itemToRecord in theBestHistoryItemsToExtract)
                {
                    theBest.Add(itemToRecord, new List<Item>());
                }

                double[] values;
                foreach (var solution in paretoFront.SolutionsList)
                {
                    vars = new JMetalCSharp.Utils.Wrapper.XReal(solution);
                    values = new double[vars.Size()];
                    for (int i = 0; i < vars.Size(); i++) values[i] = vars.GetValue(i);

                    Item item;
                    HistoryItem historyItem = history[values];
                    foreach (var itemToRecord in theBestHistoryItemsToExtract)
                    {
                        item = historyItem.ItemValues[itemToRecord];
                        theBest[itemToRecord].Add(item);
                    }
                }
                RecordMergedValueResult((recCount).ToString(), theBest, false);
            }

            // hypervolume
            int count = 0;
            double[][] objectives = new double[paretoFront.SolutionsList.Count][];
            foreach (var solution in paretoFront.SolutionsList)
            {
                objectives[count++] = solution.Objective;
            }
            JMetalCSharp.QualityIndicator.HyperVolume hv = new JMetalCSharp.QualityIndicator.HyperVolume();
            double current = hv.CalculateHypervolume(objectives, objectives.Length, objectives[0].Length);
            if (current < bestValue) bestValue = current;

            OnReportStatusData("Generation " + recCount.ToString() + ", Hypervolume " + current);
            OnReportConverganceData(new double[] { bestValue, current });
        }
        new private void OnReportStatusData(string data)
        {
            base.OnReportStatusData(data + ": Number of skipped evaluations: " + base.countHistory);
        }
        private void UpdateResultItemNames()
        {
            int N = 0;
            if (objectiveNamesBl != null) N += objectiveNamesBl.Count;
            if (constraintNamesBl != null) N += constraintNamesBl.Count;

            this.resultItemNames = new string[N];
            int count = 0;

            foreach (var objVar in objectiveNamesBl)
            {
                resultItemNames[count++] = objVar.Name;
            }

            foreach (var conVar in constraintNamesBl)
            {
                resultItemNames[count++] = conVar.Name;
            }
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (objectiveNamesBl == null || objectiveNamesBl.Count <= 1)
                throw new Exception("At least two objective variables must be defined for the NSGAII driver.");

            string[] uniqueNames = resultItemNames.Distinct().ToArray();
            if (resultItemNames.Length != uniqueNames.Length)
                throw new Exception("At least one objective function or one constraint is used twice.");

            foreach (var name in uniqueNames)
            {
                if (!itemCollection.ContainsKey(name))
                {
                    System.Windows.Forms.MessageBox.Show("The item '" + name + "' used in NSGAII driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                    return false;
                }
            }

            if (initialPopulationArrayName != null && !itemCollection.ContainsKey(initialPopulationArrayName))
            {
                System.Windows.Forms.MessageBox.Show("The item '" + initialPopulationArrayName + "' used in NSGAII driver is missing.", "Error"
                                                     , System.Windows.Forms.MessageBoxButtons.OK);
                return false;
            }

            return true;
        }


        // IDependent
        [Browsable(false)]
        public List<string> InputVariables
        {
            get
            {
                if (initialPopulationArrayName != null && initialPopulationArrayName != "") return new List<string>() { initialPopulationArrayName };
                else return new List<string>();
            }
        }
    }
}
