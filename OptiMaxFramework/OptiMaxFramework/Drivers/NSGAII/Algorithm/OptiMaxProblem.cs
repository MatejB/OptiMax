﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JMetalCSharp.Core;

using JMetalCSharp.Encoding.SolutionType;
using JMetalCSharp.Utils;
using JMetalCSharp.Utils.Wrapper;

namespace OptiMaxFramework.NSGAII
{
    class OptiMaxProblem : Problem 
    {
        // Variables                                                                                          
        private Func<double[][], double[][]> ObjectiveFunction;


        // Constructors                                                                                       
        public OptiMaxProblem(int numberOfVariables, int numOfObjectives, int numOfConstraints, Func<double[][], 
                              double[][]> objectiveFunction)
		{
			NumberOfVariables = numberOfVariables;
            NumberOfObjectives = numOfObjectives;
            NumberOfConstraints = numOfConstraints;
            ProblemName = "OptiMaxProblem";

			UpperLimit = new double[NumberOfVariables];
			LowerLimit = new double[NumberOfVariables];

			for (int i = 0; i < NumberOfVariables; i++)
			{
				LowerLimit[i] = 0;
				UpperLimit[i] = 1;
			}

            if (false) // testing for binary
            {
                SolutionType = new BinarySolutionType(this);

                this.Length = new int[NumberOfVariables];
                for (int i = 0; i < numberOfVariables; i++) Length[i] = 1;
            }
            else
            {
                SolutionType = new RealSolutionType(this);
            }

            ObjectiveFunction = objectiveFunction;
		}


        // Methods                                                                                            
        public override void Evaluate(Solution solution)
        {
            // all the work is done in EvaluateAllObjAndConstr
        }

        public override void EvaluateAllObjAndConstr(SolutionSet solutions)
        {
            double[][] samples = new double[solutions.SolutionsList.Count][];
            double[][] results;
            // Prepare all samples to evaluate
            XReal vars;
            int count = 0;
            foreach (var sol in solutions.SolutionsList)
            {
                vars = new XReal(sol);
                samples[count] = new double[NumberOfVariables];
                //
                for (int i = 0; i < NumberOfVariables; i++)
                    samples[count][i] = vars.GetValue(i);
                //
                count++;
            }
            // Evaluate all samples
            results = ObjectiveFunction(samples);
            // Overwrite sample values if changed
            count = 0;
            foreach (var sol in solutions.SolutionsList)
            {
                vars = new XReal(sol);
                //
                for (int i = 0; i < NumberOfVariables; i++)
                    vars.SetValue(i, samples[count][i]);
                //
                count++;
            }
            // Copy the samples results back
            Solution currSolution;
            double constraint;
            double total;
            int number;
            int numberOfViolations = 0;
            count = 0;
            foreach (var sol in solutions.SolutionsList)
            {
                currSolution = sol;
                // Objectives
                for (int i = 0; i < NumberOfObjectives; i++)
                {
                    currSolution.Objective[i] = results[count][i];
                }
                // Constraints
                total = 0;
                number = 0;
                for (int i = 0; i < NumberOfConstraints; i++)
                {
                    constraint = -results[count][i + NumberOfObjectives];
                    if (constraint < 0)
                    {
                        total += constraint;
                        number++;
                        numberOfViolations++;
                    }
                }
                //
                currSolution.OverallConstraintViolation = total;
                currSolution.NumberOfViolatedConstraints = number;
                //
                count++;
            }
            
        }


    }
}
