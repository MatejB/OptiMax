﻿using JMetalCSharp.Core;
using JMetalCSharp.Utils;
using JMetalCSharp.Utils.Comparators;
using System;

namespace OptiMaxFramework.NSGAII
{
	public class NSGAIIOptiMax : Algorithm
	{
        // Variables                                                                                          
        private Action<SolutionSet> ReportTheBestSolutions;

        public NSGAIIOptiMax(Problem problem, Action<SolutionSet> ReportTheBestSolutions)
			: base(problem)
		{
            this.ReportTheBestSolutions = ReportTheBestSolutions;
		}


		/// <summary>
		/// Runs the NSGA-II algorithm.
		/// </summary>
		/// <returns>a <code>SolutionSet</code> that is a set of non dominated solutions as a result of the algorithm execution</returns>
		public override SolutionSet Execute()
		{
			int populationSize = -1;
			int maxGenerationsCount = -1;
			int generationsCount;

            JMetalCSharp.QualityIndicator.QualityIndicator indicators = null; // QualityIndicator object
			int requiredEvaluations; // Use in the example of use of the indicators object (see below)
            
            SolutionSet population;
			SolutionSet offspringPopulation;
			SolutionSet union;

			Operator mutationOperator;
			Operator crossoverOperator;
			Operator selectionOperator;

			Distance distance = new Distance();

			//Read the parameters
			JMetalCSharp.Utils.Utils.GetIntValueFromParameter(this.InputParameters, "maxGenerationsCount", ref maxGenerationsCount);
			JMetalCSharp.Utils.Utils.GetIntValueFromParameter(this.InputParameters, "populationSize", ref populationSize);
			JMetalCSharp.Utils.Utils.GetIndicatorsFromParameters(this.InputParameters, "indicators", ref indicators);

			//Initialize the variables
			population = new SolutionSet(populationSize);
            generationsCount = 0;
            requiredEvaluations = 0;

            //Read the operators
            mutationOperator = Operators["mutation"];
			crossoverOperator = Operators["crossover"];
			selectionOperator = Operators["selection"];

			Random random = new Random(4);
			JMetalRandom.SetRandom(random);

            // Create the initial solutionSet
            JMetalCSharp.Core.Variable[][] initialPopulationVariables = GetInitialPopulationVariables(populationSize);

            if (initialPopulationVariables == null)
            {
                for (int i = 0; i < populationSize; i++) population.Add(new Solution(Problem));
            }
            else
            {
                for (int i = 0; i < populationSize; i++) population.Add(new Solution(Problem, initialPopulationVariables[i]));
            }

            Ranking ranking;
            Problem.EvaluateAllObjAndConstr(population);
            generationsCount++;

            ranking = new Ranking(population);
            ReportTheBestSolutions(ranking.GetSubfront(0));
            
			while (generationsCount < maxGenerationsCount)
			{
				// Create the offSpring solutionSet      
                offspringPopulation = new SolutionSet(populationSize);  // could have 1 more free location, but it will not be evaluated
				Solution[] parents = new Solution[2];
                for (int i = 0; i < (populationSize / 2 + 1); i++)      // +1 for odd number of samples like 3, 5, 7, ...
				{
					//obtain parents
					parents[0] = (Solution)selectionOperator.Execute(population);
					parents[1] = (Solution)selectionOperator.Execute(population);
					Solution[] offSpring = (Solution[])crossoverOperator.Execute(parents);
					mutationOperator.Execute(offSpring[0]);
                    offspringPopulation.Add(offSpring[0]);

                    if (offspringPopulation.SolutionsList.Count < offspringPopulation.Capacity) // check for odd number of samples
                    {
                        mutationOperator.Execute(offSpring[1]);
                        offspringPopulation.Add(offSpring[1]);
                    }
				}
                Problem.EvaluateAllObjAndConstr(offspringPopulation);
                generationsCount++;

                // Create the solutionSet union of solutionSet and offSpring
                union = ((SolutionSet)population).Union(offspringPopulation);

				// Ranking the union
				ranking = new Ranking(union);

				int remain = populationSize;
				int index = 0;
				SolutionSet front = null;
				population.Clear();

				// Obtain the next front
				front = ranking.GetSubfront(index);

				while ((remain > 0) && (remain >= front.Size()))
				{
					//Assign crowding distance to individuals
					distance.CrowdingDistanceAssignment(front, Problem.NumberOfObjectives);
					//Add the individuals of this front
					for (int k = 0; k < front.Size(); k++)
					{
						population.Add(front.Get(k));
					}

					//Decrement remain
					remain = remain - front.Size();

					//Obtain the next front
					index++;
					if (remain > 0)
					{
						front = ranking.GetSubfront(index);
					}
				}

				// Remain is less than front(index).size, insert only the best one
				if (remain > 0)
				{  // front contains individuals to insert                        
					distance.CrowdingDistanceAssignment(front, Problem.NumberOfObjectives);
					front.Sort(new CrowdingComparator());
					for (int k = 0; k < remain; k++)
					{
						population.Add(front.Get(k));
					}

					remain = 0;
				}

                ranking = new Ranking(population);
                ReportTheBestSolutions(ranking.GetSubfront(0));

				// This piece of code shows how to use the indicator object into the code
				// of NSGA-II. In particular, it finds the number of evaluations required
				// by the algorithm to obtain a Pareto front with a hypervolume higher
				// than the hypervolume of the true Pareto front.
				if ((indicators != null) && (requiredEvaluations == 0))
				{
					double HV = indicators.GetHypervolume(population);
					if (HV >= (0.98 * indicators.TrueParetoFrontHypervolume))
					{
                        requiredEvaluations = generationsCount * populationSize;
					}
				}
			}

			// Return as output parameter the required evaluations
			SetOutputParameter("evaluations", requiredEvaluations);

			// Return the first non-dominated front
			Ranking rank = new Ranking(population);

			Result = rank.GetSubfront(0);

			return Result;
		}

        private JMetalCSharp.Core.Variable[][] GetInitialPopulationVariables(int populationSize)
        {
            object initialPopulationObject;
            double[][] initialPopulation;
            JMetalCSharp.Core.Variable[][] initialPopulationVariables;

            if (this.InputParameters.TryGetValue("initialPopulation", out initialPopulationObject) && initialPopulationObject is double[][])
            {
                initialPopulation = initialPopulationObject as double[][];
                if (initialPopulation.GetLength(0) < populationSize) throw new ArgumentOutOfRangeException();

                initialPopulationVariables = new JMetalCSharp.Core.Variable[populationSize][];
                for (int i = 0; i < populationSize; i++)
                {
                    if (initialPopulation[i].Length != Problem.NumberOfVariables) throw new ArgumentOutOfRangeException();

                    initialPopulationVariables[i] = new JMetalCSharp.Core.Variable[initialPopulation[i].Length];
                    for (int j = 0; j < initialPopulation[i].Length; j++)
                    {
                        initialPopulationVariables[i][j] = new JMetalCSharp.Encoding.Variable.Real(Problem.LowerLimit[j], Problem.UpperLimit[j], initialPopulation[i][j]);
                    }
                }
            }
            else initialPopulationVariables = null;

            return initialPopulationVariables;
        }

       
	}
}
