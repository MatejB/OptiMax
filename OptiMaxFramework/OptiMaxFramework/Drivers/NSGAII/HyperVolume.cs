﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    public class HyperVolume
    {
        //
        // https://ls11-www.cs.tu-dortmund.de/people/beume/publications/hoy.cpp
        //

        /* global variables */
        int dataNumber;
        int dimension;
        double dSqrtDataNumber;
        double volume;

        public HyperVolume()
        {

        }

        public double Evaluate(int dimension, int numOfPoints, List<double[]> pointsInitial, double[] referencePoint)
        {
            int i, j;
            this.dimension = dimension;
            this.dataNumber = numOfPoints;

            /* check parameters */
            if (dimension < 3) throw new Exception("Number of dimensoins must be larger or equal to 3.");

            // initialize volume
            volume = 0.0;
            // sqrt of dataNumber
            dSqrtDataNumber = Math.Sqrt(dataNumber);

            // initialize region
            double[] regionLow = new double[dimension - 1];
            double[] regionUp = new double[dimension - 1];
            for (j = 0; j < dimension - 1; j++)
            {
                // determine minimal j coordinate
                double min = double.MaxValue;
                for (i = 0; i < dataNumber; i++)
                {
                    if (pointsInitial[i][j] < min) min = pointsInitial[i][j];
                }
                regionLow[j] = min;
                regionUp[j] = referencePoint[j];
            }

            // sort pointList according to d-th dimension
            //sort(pointsInitial.begin(), pointsInitial.end(), cmp);
            pointsInitial.Sort((x, y) => x[dimension - 1].CompareTo(y[dimension - 1]));

            // call stream initially
            stream(regionLow, regionUp, pointsInitial, 0, referencePoint[dimension - 1]);

            // print hypervolume
            return volume;
        }
        private bool cmp(double[] a, double[] b)
        {
            return (a[dimension - 1] < b[dimension - 1]);
        }
        private bool covers(double[] cub, double[] regLow)
        {
            for (int i = 0; i < dimension - 1; i++)
            {
                if (cub[i] > regLow[i]) return false;
            }
            return true;
        }
        private bool partCovers(double[] cub, double[] regUp)
        {
            for (int i = 0; i < dimension - 1; i++)
            {
                if (cub[i] >= regUp[i]) return false;
            }
            return true;
        }
        private int containsBoundary(double[] cub, double[] regLow, int split)
        {
            // condition only checked for split>0
            if (regLow[split] >= cub[split])
            {
                // boundary in dimension split not contained in region, thus
                // boundary is no candidate for the splitting line
                return -1;
            }
            else
            {

                for (int j = 0; j < split; j++)
                { // check boundaries
                    if (regLow[j] < cub[j])
                    {
                        // boundary contained in region
                        return 1;
                    }
                }
            }
            // no boundary contained in region
            return 0;
        }
        double getMeasure(double[] regLow, double[] regUp)
        {
            double vol = 1.0;
            for (int i = 0; i < dimension - 1; i++)  vol *= (regUp[i] - regLow[i]);
            return vol;
        }
        private int isPile(double[] cub, double[] regLow, double[] regU)
        {
            int pile = dimension;
            // check all dimensions of the node
            for (int k = 0; k < dimension - 1; k++)
            {
                // k-boundary of the node's region contained in the cuboid? 
                if (cub[k] > regLow[k])
                {
                    if (pile != dimension)
                    {
                        // second dimension occured that is not completely covered
                        // ==> cuboid is no pile
                        return -1;
                    }
                    pile = k;
                }
            }
            // if pile == this.dimension then
            // cuboid completely covers region
            // case is not possible since covering cuboids have been removed before

            // region in only one dimenison not completly covered 
            // ==> cuboid is a pile 
            return pile;
        }
        private double computeTrellis(double[] regLow, double[] regUp, double[] trellis)
        {
            int i, j;
            double vol = 0;
            int numberSummands = 0;
            double summand = 0;
            int[] bitvector = new int[16];
            string binary;

            // calculate number of summands
            numberSummands = (int)Math.Pow(2, dimension - 1) - 1;

            double[] valueTrellis = new double[dimension - 1];
            double[] valueRegion = new double[dimension - 1];
            for (i = 0; i < dimension - 1; i++) valueTrellis[i] = trellis[i] - regUp[i];
            for (i = 0; i < dimension - 1; i++) valueRegion[i] = regUp[i] - regLow[i];

            double[] dTemp = new double[numberSummands / 2 + 1];

            // sum
            for (i = 1; i <= numberSummands / 2; i++)
            {
                // set bitvector length to fixed value 16
                // TODO Warning: dimension-1 <= 16 is assumed

                //bitvector = (long) i;
                binary = Convert.ToString(i, 2);
                for (j = 0; j < bitvector.Length; j++)
                {
                    if (j < binary.Length) bitvector[j] = int.Parse(binary[j].ToString());
                    else bitvector[j] = 0;
                }

                // construct summand
                // 0: take factor from region
                // 1: take factor from cuboid
                summand = 1.0;
                for (j = 0; j < dimension - 2; j++)
                {
                    if (bitvector[j] == 1) summand *= valueTrellis[j];
                    else summand *= valueRegion[j];
                }
                summand *= valueRegion[dimension - 2];

                // determine sign of summand
                vol -= summand;
                dTemp[i] = -summand;

                // add summand to sum
                // sign = (int) pow((double)-1, (double)counterOnes+1);
                // vol += (sign * summand); 
            }

            //bitvector = (long) i;
            binary = Convert.ToString(i, 2);
            for (j = 0; j < bitvector.Length; j++)
            {
                if (j < binary.Length) bitvector[j] = int.Parse(binary[j].ToString());
                else bitvector[j] = 0;
            }

            summand = 1.0;
            for (j = 0; j < dimension - 1; j++)
            {
                if (bitvector[j] == 1) summand *= valueTrellis[j];
                else summand *= valueRegion[j];
            }
            vol -= summand;

            for (i = 1; i <= numberSummands / 2; i++)
            {
                summand = dTemp[i];
                summand *= regUp[dimension - 2] - trellis[dimension - 2];
                summand /= valueRegion[dimension - 2];
                vol -= summand;
            }

            return vol;
        }

        // return median of the list of boundaries considered as a set
        // TODO linear implementation
        private double getMedian(List<double> bounds)
        {
            // do not filter duplicates
            if (bounds.Count == 1) return bounds[0];
            else if (bounds.Count() == 2) return bounds[1];
            bounds.Sort();
            return bounds[bounds.Count() / 2];
        }

        // recursive calculation of hypervolume
        private void stream(double[] regionLow, double[] regionUp, List<double[]> points, int split, double cover)
        {
            //--- init --------------------------------------------------------------//
            double coverOld;
            coverOld = cover;
            int coverIndex = 0;
            int c;

            //--- cover -------------------------------------------------------------//

            // identify first covering cuboid
            double dMeasure = getMeasure(regionLow, regionUp);
            while (cover == coverOld && coverIndex < points.Count())
            {
                if (covers(points[coverIndex], regionLow))
                {
                    // new cover value
                    cover = points[coverIndex][dimension - 1];
                    volume += dMeasure * (coverOld - cover);
                }
                else coverIndex++;
            }

            /* coverIndex shall be the index of the first point in points which
             * is ignored in the remaining process
             * 
             * It may occur that that some points in front of coverIndex have the same 
             * d-th coordinate as the point at coverIndex. This points must be discarded
             * and therefore the following for-loop checks for this points and reduces
             * coverIndex if necessary.
             */
            for (c = coverIndex; c > 0; c--)
            {
                if (points[c - 1][dimension - 1] == cover) coverIndex--;
            }

            // abort if points is empty
            if (coverIndex == 0) return;

            // Note: in the remainder points is only considered to index coverIndex

            //--- allPiles  ---------------------------------------------------------//
            bool allPiles = true;
            int i;

            int[] piles = new int[coverIndex];
            for (i = 0; i < coverIndex; i++)
            {
                piles[i] = isPile(points[i], regionLow, regionUp);
                if (piles[i] == -1)
                {
                    allPiles = false;
                    break;
                }
            }

            /*
             * trellis[i] contains the values of the minimal i-coordinate of 
             * the i-piles.
             * If there is no i-pile the default value is the upper bpund of the region.
             * The 1-dimensional KMP of the i-piles is: reg[1][i] - trellis[i]
             * 
             */

            if (allPiles)
            { // sweep

                // initialize trellis with region's upper bound
                double[] trellis = new double[dimension - 1];
                for (c = 0; c < dimension - 1; c++) trellis[c] = regionUp[c];

                double current = 0.0;
                double next = 0.0;
                i = 0;
                do
                { // while(next != coverNew)
                    current = points[i][dimension - 1];
                    do
                    { // while(next == current)
                        if (points[i][piles[i]] < trellis[piles[i]]) trellis[piles[i]] = points[i][piles[i]];
                        i++; // index of next point
                        if (i < coverIndex) next = points[i][dimension - 1];
                        else next = cover;

                    } while (next == current);

                    volume += computeTrellis(regionLow, regionUp, trellis) * (next - current);
                } while (next != cover);
            }

            //--- split -------------------------------------------------------------//
            // inner node of partition tree
            else
            {
                double bound = -1.0;
                List<double> boundaries = new List<double>();
                List<double> noBoundaries = new List<double>();

                do
                {
                    for (i = 0; i < coverIndex; i++)
                    {
                        int contained = containsBoundary(points[i], regionLow, split);
                        if (contained == 1) boundaries.Add(points[i][split]);
                        else if (contained == 0) noBoundaries.Add(points[i][split]);
                    }

                    if (boundaries.Count > 0) bound = getMedian(boundaries);
                    else if (noBoundaries.Count > dSqrtDataNumber) bound = getMedian(noBoundaries);
                    else split++;
                } while (bound == -1.0);

                double dLast;
                
                //pointsChild.reserve(coverIndex);
                List<double[]> pointsChild = new List<double[]>(coverIndex);

                // left child
                // reduce maxPoint
                dLast = regionUp[split];
                regionUp[split] = bound;
                for (i = 0; i < coverIndex; i++)
                {
                    if (partCovers(points[i], regionUp)) pointsChild.Add(points[i]);
                }
                if (pointsChild.Count > 0) stream(regionLow, regionUp, pointsChild, split, cover);

                // right child
                // increase minPoint
                pointsChild.Clear();
                regionUp[split] = dLast;
                dLast = regionLow[split];
                regionLow[split] = bound;
                for (i = 0; i < coverIndex; i++)
                {
                    if (partCovers(points[i], regionUp)) pointsChild.Add(points[i]);
                }
                if (pointsChild.Count > 0) stream(regionLow, regionUp, pointsChild, split, cover);
                regionLow[split] = dLast;

            }// end inner node

        } // end stream






















    }
}
