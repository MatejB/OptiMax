﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simplex;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    public class NMDriver : Driver
    {
        // Variables                                                                                                                
        private int maxNumOfIncrements;

        // Properties                                                                                                               
        [CategoryAttribute("Control parameters"),
        DisplayName("Relative convergance tolerance"),
        DescriptionAttribute("The relative difference between the successive solutions when the optimization is regarded as converged.")]
        public double Tolerance { get; set; }

        [CategoryAttribute("Control parameters"),
        DisplayName("Max number of increments"),
        DescriptionAttribute("The number of optimization increments after which the optimization exits.")]
        public int MaxIncrements
        {
            get { return maxNumOfIncrements; }
            set
            {
                if (value <= 0) throw new Exception("Max number of increments must be larger than 0.");
                else maxNumOfIncrements = value;
            }
        }

        [CategoryAttribute("Objective variable"),
        DisplayName("Objective variable"),
        DescriptionAttribute("Select the variable for the objective function."),
        TypeConverter(typeof(VariableNamesTypeConverter))]
        public string ObjectiveVariableName 
        {
            get
            {
                if (resultItemNames == null || resultItemNames.Length == 0 || resultItemNames[0] == null)
                {
                    if (Driver_GlobalVars.listOfVariableNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfVariableNames.Sort();
                        resultItemNames = new string[] { Driver_GlobalVars.listOfVariableNames[0] };
                    }
                    else return "";
                }
                return resultItemNames[0];
            }
            set
            {
                resultItemNames = new string[] { value };
            }
        }

        [CategoryAttribute("Execution"),
        DisplayName("Number of threads"),
        ReadOnly(true),
        DescriptionAttribute("Input a value larger than 1 to use parallel workflow execution.")]
        public override int NumOfThreads
        {
            get
            {
                return base.NumOfThreads;
            }
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (ObjectiveVariableName == null)
                throw new Exception("The NM driver objective variable must be defined.");

            if (!itemCollection.ContainsKey(ObjectiveVariableName))
            {
                System.Windows.Forms.MessageBox.Show("The item '" + ObjectiveVariableName + "' used in NM driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                return false;
            }
            
            return true;
        }
       

        // Constructors                                                                                                             
        public NMDriver(string name, string description)
            : this(name, description, 0.001, 10)
        {
        }

        public NMDriver(string name, string description, double tolerance, int maxIncrements, string resultVariableName = null)
            : base(name, description)
        {
            Tolerance = tolerance;
            MaxIncrements = maxIncrements;
            this.resultItemNames = new string[] { resultVariableName };
        }

        // Methods                                                                                                                  
        public override void RunEvaluation()
        {
            bool err = CorrectlyDefined(baseFrameworkItemCollection);

            double value, delta;
            SimplexConstant[] constants = new SimplexConstant[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
			{
                value = (parameters[i].Value - parameters[i].Min) / (parameters[i].Max - parameters[i].Min);
                delta = parameters[i].Delta / (parameters[i].Max - parameters[i].Min);
                constants[i] = new SimplexConstant(value, delta);
			}

            // in one increment there is a reflection and expansion or contraction (2x); initiation and maybe also shriking (5x)
            numOfPlaces = (2 * MaxIncrements + 5 * (parameters.Length + 1)).ToString().Length;

            string solutionID;
            jobStatusData = new JobStatusData[1];
            for (int i = 0; i < jobStatusData.Length; i++)
            {
                solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                jobStatusData[i] = new JobStatusData() { ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0) };
            }

            ObjectiveFunctionDelegate objFunction = new ObjectiveFunctionDelegate(ObjectiveFunction);
            AlgorithmResult result = NelderMeadSimplex.Regress(constants, Tolerance, MaxIncrements, objFunction, 
                                                               OnReportStatusData, OnReportConverganceData);

            if (result.TerminationReason == TerminationReason.MaxFunctionEvaluations)
                OnReportStatusData("Maximum number of increments reached.");
            else if (result.TerminationReason == TerminationReason.Converged)
                OnReportStatusData("The solution converged.");
            else 
                OnReportStatusData("Unspecified.");
        }

        private double ObjectiveFunction(double[] sample)
        {
            // interval limits
            for (int i = 0; i < sample.Length; i++)
            {
                if (sample[i] < 0) sample[i] = 0;
                else if (sample[i] > 1) sample[i] = 1;
            }

            double[][] allSamples = new double[][] { sample };
            string[][] results;
            double[] fitness = new double[allSamples.Length];

            // evaluate all samples
            results = EvaluateAllSamples(allSamples);
            for (int i = 0; i < results.GetLength(0); i++)
            {
                fitness[i] = double.Parse(results[i][0], OptiMaxFramework.Globals.nfi);
            }

            return fitness[0];
        }

       
    }
}
