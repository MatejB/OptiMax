﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simplex
{
    public sealed class AlgorithmResult
    {
        private TerminationReason _terminationReason;
        private double[] _parameters;
        private double _errorValue;
        private int _evaluationCount;

        public AlgorithmResult(TerminationReason terminationReason, double[] parameters, double errorValue, int evaluationCount)
        {
            _terminationReason = terminationReason;
            _parameters = parameters;
            _errorValue = errorValue;
            _evaluationCount = evaluationCount;
        }

        public TerminationReason TerminationReason
        {
            get { return _terminationReason; }
        }

        public double[] Constants
        {
            get { return _parameters; }
        }

        public double ErrorValue
        {
            get { return _errorValue; }
        }

        public int EvaluationCount
        {
            get { return _evaluationCount; }
        }
    }

    public enum TerminationReason
    {
        MaxFunctionEvaluations,
        Converged,
        Unspecified
    }

}