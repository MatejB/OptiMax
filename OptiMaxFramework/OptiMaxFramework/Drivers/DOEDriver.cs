﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    public class DOEDriver : Driver
    {
        //                                                                                                                          


        //                                                                                                                          
        public DOEDriver()
            : base("driver", "DOE Driver")
        {
        }

        //                                                                                                                          
        public override void Run(string workingDirectory)
        {
            List<double[]> experiments = GenerateExperiments();
            int count = 0;

            int numOfPlaces = (experiments.Count + 1).ToString().Length;

            foreach (var experiment in experiments)
            {
                string solutionID = (count + 1).ToString().PadLeft(numOfPlaces, '0');

                string workingDirectoryForSolution;
                if (workingDirectory != null) workingDirectoryForSolution = System.IO.Path.Combine(workingDirectory, solutionID);
                else workingDirectoryForSolution = null;

                EvaluateSolution(solutionID, experiment, workingDirectoryForSolution); 

                count++;
            }
        }

        private List<double[]> GenerateExperiments()
        {
            int numOfExperiments = parameters[0].NumOfFractions;
            for (int i = 1; i < parameters.Length; i++)
            {
                numOfExperiments *= parameters[i].NumOfFractions;
            }

            List<double[]> experiments = new List<double[]>(numOfExperiments);
            double[] experiment = new double[parameters.Length];
            GenerateExperimentsRecursive(experiments, ref experiment, 0);
            return experiments;
        }

        private void GenerateExperimentsRecursive(List<double[]> experiments, ref double[] experiment, int parID)
        {
            if (parID == parameters.Length)
            {
                experiments.Add((double[])experiment.Clone());
            }
            else
            {
                double delta;
                int numOfFractions = parameters[parID].NumOfFractions;

                if (numOfFractions <= 1) delta = 0;
                else delta = 1d / (numOfFractions - 1);

                for (int i = 0; i < numOfFractions; i++)
                {
                    experiment[parID] = delta * i;
                    GenerateExperimentsRecursive(experiments, ref experiment, parID + 1);
                }
            }
        }

        
    }
}
