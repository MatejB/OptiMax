﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class Workflow : IDependent
    {
        // Variables                                                                                                                
        private List<string> itemNames;
        private bool kill;
        private List<Component> runningComponents;
        private Object myLock;

        // Constructors                                                                                                             
        public Workflow()
        {
            itemNames = new List<string>();
            kill = false;
            runningComponents = new List<Component>();
            myLock = new Object();
        }


        // Methods                                                                                                                  
        public void Add(string item)
        {
            itemNames.Add(item);
        }
        public void Clear()
        {
            itemNames.Clear();
        }
        public JobStatus Execute(string workingDirectory, Dictionary<string, Item> itemsClone, Parameter[] parameters)
        {
            if (myLock == null) myLock = new Object();

            SetValues(itemsClone, parameters);

            kill = false;
            JobStatus jobStatus = JobStatus.OK;

            string componentDirectory;
            Component currComponent;
            
            foreach (string itemName in itemNames)
            {
                if (kill) return JobStatus.Killed;

                var item = itemsClone[itemName];
                if (item is Component)
                {
                    currComponent = (Component)item;
                    lock (myLock) runningComponents.Add(currComponent);

                    if (currComponent is ISshComponent)
                    {
                        string solutionId = workingDirectory.Split('\\').Last();
                        componentDirectory = System.IO.Path.Combine((currComponent as ISshComponent).ServerPath, solutionId);
                        componentDirectory = System.IO.Path.Combine(componentDirectory, item.Name);
                    }
                    else
                        componentDirectory = System.IO.Path.Combine(workingDirectory, item.Name);

                    System.IO.Directory.CreateDirectory(componentDirectory);

                    jobStatus = currComponent.Execute(componentDirectory);

                    // Try again
                    //if (jobStatus == JobStatus.TimedOut)
                    //{
                    //    Globals.DeleteDirectoryRecursive(componentDirectory);
                    //    System.IO.Directory.CreateDirectory(componentDirectory);

                    //    jobStatus = currComponent.Execute(componentDirectory);
                    //}

                    lock (myLock) runningComponents.Remove(currComponent);

                    if (jobStatus != JobStatus.OK) return jobStatus;
                }
            }

            // must be repeated after the loop
            if (kill) return JobStatus.Killed;

            return JobStatus.OK;
        }
        public void Kill()
        {
            this.kill = true;

            lock (myLock)
            {
                if (runningComponents != null)
                {
                    foreach (Component c in runningComponents)
                    {
                        c.Kill();
                    }
                }
                runningComponents = new List<Component>();
            }
        }
        private void SetValues(Dictionary<string, Item> items, Parameter[] parameters)
        {
            string[] tmp;
            string name;
            int id;
            for (int i = 0; i < parameters.Length; i++)
            {
                tmp = parameters[i].Name.Split(new string[] { Globals.InputArraySeparator }, StringSplitOptions.None);
                name = tmp[0];

                var item = items[name];
                if (item is VariableInput)
                {
                    VariableInput variable = (VariableInput)item;
                    if (variable.ValueType == VariableValueType.Double) variable.SetValue(parameters[i].Value);
                    else if (variable.ValueType == VariableValueType.Integer) variable.SetValue((int)Math.Round(parameters[i].Value, 0));
                    else throw new Exception("Can not set value from parameter.");
                }
                else if (item is ArrayInput && tmp.Length == 2)
                {
                    id = int.Parse(tmp[1]);
                    ArrayInput array = (ArrayInput)item;
                    if (array.ValueType == VariableValueType.Double) array.SetValue(parameters[i].Value, id);
                    else if (array.ValueType == VariableValueType.Integer) array.SetValue((int)Math.Round(parameters[i].Value, 0), id);
                    else throw new Exception("Can not set value from parameter.");
                }
            }
        }


        // Idependable                                                                                                                         
        public List<string> InputVariables
        {
            get
            {
                return itemNames;
            }
        }
        
    }
}
