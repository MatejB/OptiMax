﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public enum JobStatus
    {
        InQueue,
        Running,
        Retrying,
        OK,
        Killed,
        TimedOut,
        Failed
    }

    [Serializable]
    public struct JobStatusData
    {
        public string ID;
        public int numOfRetries;
        public JobStatus jobStatus;
        public DateTime startTime;
        public TimeSpan elapsedTime;
    }
}
