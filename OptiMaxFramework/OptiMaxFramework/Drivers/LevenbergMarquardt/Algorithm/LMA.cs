﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetMatrix;

//                                                              
//                                                              
// http://www.kniaz.net/software/LMA.aspx                       
//                                                              
//                                                              

namespace LevenbergMarquardt
{
    public delegate double[][] ObjectiveFunctionDelegate(double[][] allSamples);

    public class LMA
    {
        private ObjectiveFunctionDelegate ObjectiveFunction;
        
        private double[] currentSample;
        private double[][] dataPoints;
        private double[] weights;
        private double derivativeDelta;
        private double relativeConverganceObjFunction;
        private int maxIretations;

        private double[][] allSamples;                  // [sample ID][parameter ID]
        private double[][] allFunctionResults;          // [function ID][array element ID]
        private double[][] allDerivatives;              // [parameter ID][array element ID]
        private double[][] nextAllSamples;              // [sample ID][parameter ID]
        private double[][] nextAllFunctionResults;      // [function ID][array element ID]
        private GeneralMatrix J;
        private GeneralMatrix A;
        private GeneralMatrix g;
        private GeneralMatrix h;
        private double lambda;
        private double tao = 1E-3;
        private double bestValue;
        private TerminationReason terminationReason;

        public Action<double[]> ReportConverganceData;
        public Action<string> ReportStatusData;

        // Constructors                                                                                                             

        public LMA(ObjectiveFunctionDelegate objectiveFunction, double[] currentSample, double[][] dataPoints, 
                   double[] weights, double derivativeDelta, double relativeConverganceObjFunction, int maxIterations) 
		{
			if (dataPoints[0].Length != dataPoints[1].Length) 
				throw new ArgumentException("Data must have the same number of x and y points.");
			
			if (dataPoints.Length != 2) 
				throw new ArgumentException("Data point array must be 2 x N");

            this.ObjectiveFunction = objectiveFunction;
            this.currentSample = currentSample;
            this.dataPoints = dataPoints;
            this.weights = CheckWeights(dataPoints[0].Length, weights);
            this.derivativeDelta = derivativeDelta;
            this.relativeConverganceObjFunction = relativeConverganceObjFunction;
            this.maxIretations = maxIterations;
            this.nextAllSamples = new double[1][];
            this.nextAllSamples[0] = new double[currentSample.Length];
            this.nextAllFunctionResults = new double[1][];


            this.A = new GeneralMatrix(currentSample.Length, currentSample.Length);
            this.J = new GeneralMatrix(dataPoints[0].Length, currentSample.Length); // (row, col)
            this.g = new GeneralMatrix(currentSample.Length, 1); // (row, col)

            terminationReason = TerminationReason.Unspecified;
		}

        public AlgorithmResult Fit()
        {
            double ni;
            double gama;
            double F;
            double Fnew = -1;
            GeneralMatrix f;
            GeneralMatrix fnew;
            double denominator;
            bestValue = double.MaxValue;

            int iterationCount = 0;

            f = ComputeFunction();
            F = 0.5 * AbsSquared(f);

            double current = 2 * F;
            if (current < bestValue) bestValue = current;
            ReportStatusData("Iteration " + iterationCount.ToString() + ", Current value " + current.ToString());
            ReportConverganceData(new double[] { bestValue, current });

            ni = 2;
            lambda = tao * GetMaxDiagTerm(A);

            while (!Stop(iterationCount))
            {
                iterationCount++;
                h = ComputeNextStep();     
               
                if (!Stop(iterationCount))
                {
                    nextAllSamples = PrepareNextSample();
                    allFunctionResults = ObjectiveFunction(nextAllSamples);

                    fnew = ComputeFunctionVector(allFunctionResults[0]);
                    Fnew = 0.5 * AbsSquared(fnew);
                    
                    denominator = 0.5 * h.Transpose().Multiply(h.Multiply(lambda).Subtract(g)).GetElement(0, 0);
                    
                    gama = (F - Fnew) / denominator;

                    if (gama > 0) // step acceptable
                    {
                        nextAllSamples[0].CopyTo(currentSample, 0);
                        f = ComputeFunction();
                        F = 0.5 * AbsSquared(f);

                        current = 2 * F;
                        if (current < bestValue) bestValue = current;

                        ReportStatusData("Iteration " + iterationCount.ToString() + ", Current value " + current.ToString());
                        ReportConverganceData(new double[] { bestValue, current });

                        lambda = lambda * Math.Max(1.0 / 3, 1 - Math.Pow(2 * gama - 1, 3));
                        ni = 2;
                    }
                    else
                    {
                        lambda *= ni;
                        ni *= 2;
                    }
                }
            }

            return new AlgorithmResult(terminationReason, currentSample, 2 * F, iterationCount);
        }

        private GeneralMatrix ComputeFunction()
        {
            allSamples = PrepareAllSamples();
            allFunctionResults = ObjectiveFunction(allSamples);
            GeneralMatrix f = ComputeFunctionVector(allFunctionResults[0]);
            
            allDerivatives = ComputeAllDerivatives();

            UpdateJ();
            A = J.Transpose().Multiply(J);
            g = J.Transpose().Multiply(f);
            return f;
        }

        private GeneralMatrix ComputeFunctionVector(double[] functionResults)
        {
            GeneralMatrix f = new GeneralMatrix(dataPoints[0].Length, 1);
            for (int i = 0; i < dataPoints[0].Length; i++)
            {
                f.SetElement(i, 0, weights[i] * (functionResults[i] - dataPoints[1][i]));
            }
            return f;
        }

        private GeneralMatrix ComputeNextStep()
        {
            // (A + lambda*I)*h=-g
            GeneralMatrix I = GeneralMatrix.Identity(A.RowDimension, A.ColumnDimension);
            return (A.Add(I.Multiply(lambda))).Inverse().Multiply(g.UnaryMinus());
        }

        private bool Stop(int iterationCount)
        {
            double eps1 = relativeConverganceObjFunction;
            double eps2 = relativeConverganceObjFunction;

            if (iterationCount > maxIretations)
            {
                terminationReason = TerminationReason.MaxFunctionEvaluations;
                return true;
            }
            else if (Abs(g) <= eps1)
            {
                terminationReason = TerminationReason.ConvergedToZero;
                return true;
            }
            else if (h != null)
            {
                GeneralMatrix x = VectorMatrix(currentSample);
                if (Abs(h) < eps2 * (Abs(x) * eps2))                                    // ||h|| < eps2 * (||x|| + eps2)
                {
                    terminationReason = TerminationReason.ConvergedDueToSmallStep;
                    return true;
                }
            }

            return false;
        }

        private double[][] PrepareAllSamples()
        {
            int N = 2 * currentSample.Length + 1;
            double[][] allSamples = new double[N][];

            allSamples[0] = new double[currentSample.Length];
            currentSample.CopyTo(allSamples[0], 0);
            for (int i = 0; i < currentSample.Length; i ++)
            {
                allSamples[2 * i + 1] = new double[currentSample.Length];
                currentSample.CopyTo(allSamples[2 * i + 1], 0);
                allSamples[2 * i + 1][i] += derivativeDelta;

                allSamples[2 * i + 2] = new double[currentSample.Length];
                currentSample.CopyTo(allSamples[2 * i + 2], 0);
                allSamples[2 * i + 2][i] -= derivativeDelta;
            }

            return allSamples;
        }

        private double[][] PrepareNextSample()
        {
            int N = currentSample.Length;
            double[][] sample = new double[1][];
            sample[0] = new double[currentSample.Length];

            for (int i = 0; i < currentSample.Length; i++)
            {
                sample[0][i] = currentSample[i] + h.GetElement(i, 0);
            }

            return sample;
        }

        private double[][] ComputeAllDerivatives()
        {
            int N = currentSample.Length;
            double[][] derivative = new double[N][];

            int plus, minus;
            for (int i = 0; i < N; i++)
            {
                plus = 2 * i + 1;
                minus = 2 * i + 2;

                derivative[i] = new double[allFunctionResults[0].Length];

                for (int j = 0; j < allFunctionResults[0].Length; j++)
                {
                    derivative[i][j] = (allFunctionResults[plus][j] - allFunctionResults[minus][j]) / (2 * derivativeDelta);
                }
            }

            return derivative;
        }

        private void UpdateJ()
        {
            int N = J.ColumnDimension;
            int M = J.RowDimension;

            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    J.SetElement(i, j, allDerivatives[j][i]);
                }
            }
        }



        protected double[] CheckWeights(int length, double[] weights)
        {
            bool damaged = false;
            // check for null
            if (weights == null)
            {
                damaged = true;
                weights = new double[length];
            }
            // check if all elements are zeros or if there are negative, NaN or Infinite elements
            else
            {
                bool allZero = true;
                bool illegalElement = false;
                for (int i = 0; i < weights.Length && !illegalElement; i++)
                {
                    if (weights[i] < 0 || Double.IsNaN(weights[i]) || Double.IsInfinity(weights[i])) illegalElement = true;
                    allZero = (weights[i] == 0) && allZero;
                }
                damaged = allZero || illegalElement;
            }

            if (damaged)
            {
                for (int i = 0; i < weights.Length; i++)
                {
                    weights[i] = 1;
                }
            }

            return weights;
        }



        private GeneralMatrix VectorMatrix(double[] array)
        {
            GeneralMatrix v = new GeneralMatrix(array.Length, 1);
            for (int i = 0; i < array.Length; i++)
            {
                v.SetElement(i, 0, array[i]);
            }
            return v;
        }

        private double GetMaxDiagTerm(GeneralMatrix m)
        {
            double max = -double.MaxValue;
            for (int i = 0; i < m.RowDimension; i++)
            {
                if (m.GetElement(i, i) > max) max = m.GetElement(i, i);
            }
            return max;
        }

        private double GetMaxTerm(GeneralMatrix m)
        {
            double max = -double.MaxValue;
            for (int i = 0; i < m.RowDimension; i++)
            {
                for (int j = 0; j < m.ColumnDimension; j++)
                {
                    if (m.GetElement(i, j) > max) max = m.GetElement(i, j);
                }
            }
            return max;
        }

        private double Abs(GeneralMatrix m)
        {
            return Math.Sqrt(m.Transpose().Multiply(m).GetElement(0, 0));
        }

        private double AbsSquared(GeneralMatrix m)
        {
            return m.Transpose().Multiply(m).GetElement(0, 0);
        }
    }
}
