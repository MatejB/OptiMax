﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetMatrix;

namespace LevenbergMarquardt
{
    public delegate double[][] ObjectiveFunctionDelegate(double[][] allSamples);
    
    public class LMA
    {
        private ObjectiveFunctionDelegate objectiveFunction;
        private double[] currentSample;
        private double[][] dataPoints;
        private double[] weights;
        private double derivativeDelta;
        private double relativeConverganceObjFunction;
        private int maxEvaluations;

        private int evaluationCount;
        private double[][] allSamples;                  // [sample ID][parameter ID]
        private double[][] allFunctionResults;          // [function ID][array element ID]
        private double[][] allDerivatives;              // [parameter ID][array element ID]
        private double[][] nextAllSamples;              // [sample ID][parameter ID]
        private double[][] nextAllFunctionResults;      // [function ID][array element ID]
        private GeneralMatrix J;
        private GeneralMatrix A;
        private GeneralMatrix g;
        private double[] beta;
        private double[] da;
        private double lambda;
        private double tao = 1E-3;

        private double fError;
        private double nextFError;
        private TerminationReason terminationReason;

        
        public LMA(ObjectiveFunctionDelegate objectiveFunction, double[] currentSample, double[][] dataPoints, double[] weights,
                   double derivativeDelta, double relativeConverganceObjFunction, int maxEvaluations) 
		{
			if (dataPoints[0].Length != dataPoints[1].Length) 
				throw new ArgumentException("Data must have the same number of x and y points.");
			
			if (dataPoints.Length != 2) 
				throw new ArgumentException("Data point array must be 2 x N");

            this.objectiveFunction = objectiveFunction;
            this.currentSample = currentSample;
            this.dataPoints = dataPoints;
            this.weights = CheckWeights(dataPoints[0].Length, weights);
            this.derivativeDelta = derivativeDelta;
            this.relativeConverganceObjFunction = relativeConverganceObjFunction;
            this.maxEvaluations = maxEvaluations;
            this.nextAllSamples = new double[1][];
            this.nextAllSamples[0] = new double[currentSample.Length];
            this.nextAllFunctionResults = new double[1][];


            this.A = new GeneralMatrix(currentSample.Length, currentSample.Length);
            this.J = new GeneralMatrix(dataPoints[0].Length, currentSample.Length); // (row, col)
            this.g = new GeneralMatrix(currentSample.Length, 1); // (row, col)
            this.beta = new double[currentSample.Length];
            this.da = new double[currentSample.Length];

            terminationReason = TerminationReason.Unspecified;
		}

        public AlgorithmResult Fit()
        {
            int iterationCount = 0;
            evaluationCount = 0;
            lambda = -1;
            bool goodGuess = true;

            do
            {
                allSamples = PrepareAllSamples();
                // call the objective function
                if (goodGuess)
                {
                    allFunctionResults = objectiveFunction(allSamples);
                    evaluationCount += allSamples.Length;
                }
                
                allDerivatives = ComputeAllDerivatives();
                fError = ComputeteError(allFunctionResults[0]);

                UpdateAlpha();
                UpdateBeta();

                // set initial lambda value to max(diag(alpha))
                if (iterationCount == 0) SetLambda();

                for (int i = 0; i < A.RowDimension; i++)
                {
                    A.SetElement(i, i, A.GetElement(i, i) * (1 + lambda));
                }

                nextAllSamples[0] = ComputeNextSample();
                // call the objective function
                nextAllFunctionResults = objectiveFunction(nextAllSamples);
                evaluationCount += nextAllSamples.Length;
                nextFError = ComputeteError(nextAllFunctionResults[0]);

                if (nextFError >= fError)
                {
                    goodGuess = false;
                    lambda *= 2;
                }
                // The guess results to better nextFError - move and make the step larger
                else
                {
                    goodGuess = true;
                    lambda /= 2;
                    nextAllSamples[0].CopyTo(currentSample, 0);
                }
                iterationCount++;
            } 
            while (Continue());

            AlgorithmResult result;

            if (nextFError < fError) result = new AlgorithmResult(terminationReason, nextAllSamples[0], nextFError, evaluationCount);
            else result = new AlgorithmResult(terminationReason, currentSample, fError, evaluationCount);
            return result;
        }

        private bool Continue()
        {
            if (nextFError == 0 || Math.Abs(nextFError - fError) < relativeConverganceObjFunction * nextFError)
            {
                terminationReason = TerminationReason.Converged;
                return false;
            }
            else if (evaluationCount + allSamples.Length + 1 > maxEvaluations)
            {
                terminationReason = TerminationReason.MaxFunctionEvaluations;
                return false;
            }
            else return true;
        }

        private double[][] PrepareAllSamples()
        {
            int N = 2 * currentSample.Length + 1;
            double[][] allSamples = new double[N][];

            allSamples[0] = new double[currentSample.Length];
            currentSample.CopyTo(allSamples[0], 0);
            for (int i = 0; i < currentSample.Length; i ++)
            {
                allSamples[2 * i + 1] = new double[currentSample.Length];
                currentSample.CopyTo(allSamples[2 * i + 1], 0);
                allSamples[2 * i + 1][i] += derivativeDelta;

                allSamples[2 * i + 2] = new double[currentSample.Length];
                currentSample.CopyTo(allSamples[2 * i + 2], 0);
                allSamples[2 * i + 2][i] -= derivativeDelta;
            }

            return allSamples;
        }

        private double[][] ComputeAllDerivatives()
        {
            int N = currentSample.Length;
            double[][] derivative = new double[N][];

            int plus, minus;
            for (int i = 0; i < N; i++)
            {
                plus = 2 * i + 1;
                minus = 2* i + 2;

                derivative[i] = new double[allFunctionResults[0].Length];

                for (int j = 0; j < allFunctionResults[0].Length; j++)
                {
                    derivative[i][j] = (allFunctionResults[plus][j] - allFunctionResults[minus][j]) / (2 * derivativeDelta);
                }
            }

            return derivative;
        }

        private double ComputeteError(double[] functionResults)
        {
            double dy;
            double result = 0;
            for (int i = 0; i < functionResults.Length; i++)
            {
                dy = dataPoints[1][i] - functionResults[i];
                result += weights[i] * Math.Pow(dy, 2);
            }
            return result;
        }

        private void UpdateJ()
        {
            int N = J.ColumnDimension;
            int M = J.RowDimension;

            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    J.SetElement(i, j, allDerivatives[j][i]);
                }
            }
        }

        private void UpdateAlpha()
        {
            int N = currentSample.Length;
            double alphaElement;

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    alphaElement = 0;
                    for (int k = 0; k < allDerivatives[0].Length; k++)
                    {
                        alphaElement += weights[k] * allDerivatives[i][k] * allDerivatives[j][k];
                    }
                    //if (i == j) alphaElement *= (1 + lambda);

                    A.SetElement(i, j, alphaElement);
                }
            }
        }

        private void SetLambda()
        {
            // http://www.brnt.eu/phd/node10.html
            for (int i = 0; i < A.RowDimension; i++)
            {
                if (A.GetElement(i, i) > lambda) lambda = A.GetElement(i, i);
            }
            // book "Methods for Non-linear Least Squares Problems K. Madsen"
            lambda *= tao;
        }

        private void UpdateBeta()
        {
            int N = currentSample.Length;
            double betaElement;

            for (int i = 0; i < N; i++)
            {
                betaElement = 0;
                for (int j = 0; j < allDerivatives[0].Length; j++)
                {
                    betaElement += weights[j] * (dataPoints[1][j] - allFunctionResults[0][j]) * allDerivatives[i][j];
                }
                beta[i] = betaElement;
            }
        }

        private double[] ComputeNextSample()
        {
            try
            {
                //use the GeneralMatrix package to invert alpha
                //one could also use 
                //double[] da = DoubleMatrix.solve(alpha, beta);

                GeneralMatrix m = A.Inverse();
                //set alpha with inverted matrix
                A.SetMatrix(0, A.RowDimension - 1, 0, A.ColumnDimension - 1, m);
            }
            catch (Exception e)
            {
                throw new Exception("Error occured in the 'ComputeIncrements' function of the 'LMA' object." + Environment.NewLine + e.Message);
            }

            for (int i = 0; i < A.RowDimension; i++)
            {
                da[i] = 0;
                for (int j = 0; j < A.ColumnDimension; j++)
                {
                    da[i] += A.GetElement(i, j) * beta[j];
                }
            }

            double[] incrementedSample = new double[currentSample.Length];

            for (int i = 0; i < currentSample.Length; i++)
            {
                //if (!Double.isNaN(da[i]) && !Double.isInfinite(da[i]))
                incrementedSample[i] = currentSample[i] + da[i];
            }

            return incrementedSample;
        }



        private static bool Stop()
        {
            //return System.Math.Abs(chi2 - incrementedChi2) < minDeltaChi2 || iterationCount > maxIterations;
            return true;
        }

        protected double[] CheckWeights(int length, double[] weights)
        {
            bool damaged = false;
            // check for null
            if (weights == null)
            {
                damaged = true;
                weights = new double[length];
            }
            // check if all elements are zeros or if there are negative, NaN or Infinite elements
            else
            {
                bool allZero = true;
                bool illegalElement = false;
                for (int i = 0; i < weights.Length && !illegalElement; i++)
                {
                    if (weights[i] < 0 || Double.IsNaN(weights[i]) || Double.IsInfinity(weights[i])) illegalElement = true;
                    allZero = (weights[i] == 0) && allZero;
                }
                damaged = allZero || illegalElement;
            }

            if (damaged)
            {
                for (int i = 0; i < weights.Length; i++)
                {
                    weights[i] = 1;
                }
            }

            return weights;
        }

        private GeneralMatrix VectorMatrix(double[] array)
        {
            GeneralMatrix v = new GeneralMatrix(array.Length, 1);
            for (int i = 0; i < array.Length; i++)
            {
                v.SetElement(i, 0, array[i]);
            }
            return v;
        }

        private double GetMaxDiagTerm(GeneralMatrix m)
        {
            double max = -double.MaxValue;
            for (int i = 0; i < m.RowDimension; i++)
            {
                if (m.GetElement(i, i) > max) max = m.GetElement(i, i);
            }
            return max;
        }

        private double GetMaxTerm(GeneralMatrix m)
        {
            double max = -double.MaxValue;
            for (int i = 0; i < m.RowDimension; i++)
            {
                for (int j = 0; j < m.ColumnDimension; j++)
                {
                    if (m.GetElement(i, j) > max) max = m.GetElement(i, j);
                }
            }
            return max;
        }

        private double Abs(GeneralMatrix m)
        {
            return Math.Sqrt(m.Transpose().Multiply(m).GetElement(0, 0));
        }
    }
}
