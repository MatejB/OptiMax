﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LevenbergMarquardt;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    public class LMDriver : Driver
    {
        // Variables                                                                                                                
        private string targetArrayName;
        private int maxNumOfIterations;

        // Properties                                                                                                               
        [CategoryAttribute("Control parameters"),
        DisplayName("Relative convergance tolerance"),
        DescriptionAttribute("The relative difference between the successive solutions when the optimization is regarded as converged.")]
        public double ConvergenceTolerance { get; set; }

        [CategoryAttribute("Control parameters"),
        DisplayName("Max number of increments"),
        DescriptionAttribute("The number of optimization increments after which the optimization exits.")]
        public int MaxIterations
        {
            get { return maxNumOfIterations; }
            set
            {
                if (value <= 0) throw new Exception("Number of increments must be larger than 0.");
                else maxNumOfIterations = value;
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Relative derivative delta"),
        DescriptionAttribute("The relative width of the interval used for the computation of the derivative.")]
        public double DerivativeDelta { get; set; }

        [CategoryAttribute("Arrays"),
        DisplayName("Objective array"),
        DescriptionAttribute("Select the array for the objective array."),
        TypeConverter(typeof(ArrayNamesTypeConverter))]
        public string ObjectiveArrayName 
        {
            get
            {
                if (resultItemNames == null || resultItemNames.Length == 0 || resultItemNames[0] == null)
                {
                    if (Driver_GlobalVars.listOfArrayNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfArrayNames.Sort();
                        resultItemNames = new string[] { Driver_GlobalVars.listOfArrayNames[0] };
                    }
                    else return "";
                }
                return resultItemNames[0];
            }
            set
            {
                resultItemNames = new string[] { value };
            }
        }

        [CategoryAttribute("Arrays"),
        DisplayName("Target array"),
        DescriptionAttribute("Select the array for the target array."),
        TypeConverter(typeof(ArrayNamesTypeConverter))]
        public string TargetArrayName 
        {
            get
            {
                if (targetArrayName == null)
                {
                    if (Driver_GlobalVars.listOfArrayNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfArrayNames.Sort();
                        targetArrayName = Driver_GlobalVars.listOfArrayNames[0];
                    }
                    else return "";
                }
                return targetArrayName;
            }
            set
            {
                targetArrayName = value;
            }
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (ObjectiveArrayName == null)
                throw new Exception("The LM driver objective array must be defined.");
            else if (targetArrayName == null)
                throw new Exception("The LM driver target array must be defined.");
            else if (targetArrayName == ObjectiveArrayName)
                throw new Exception("The LM driver objective and target array can not be the same.");

            if (!itemCollection.ContainsKey(ObjectiveArrayName))
            {
                System.Windows.Forms.MessageBox.Show("The item '" + ObjectiveArrayName + "' used in LM driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                return false;
            }
            else if (!itemCollection.ContainsKey(targetArrayName))
            {
                System.Windows.Forms.MessageBox.Show("The item '" + targetArrayName + "' used in LM driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                return false;
            }

            return true;
        }

        // Constructors                                                                                                             
        public LMDriver(string name, string description)
            : this(name, description, null, null, 0.001, 10, 0.001)
        {
        }

        public LMDriver(string name, string description, string objectiveArrayName, string targetArrayName, double converganceTolerance, int maxEvaluations, double derivativeDelta)
            : base(name, description)
        {
            this.resultItemNames = new string[] { objectiveArrayName };
            this.targetArrayName = targetArrayName;
            ConvergenceTolerance = converganceTolerance;
            MaxIterations = maxEvaluations;
            DerivativeDelta = derivativeDelta;
        }


        // Methods                                                                                                                  
        public override void RunEvaluation()
        {
            bool err = CorrectlyDefined(baseFrameworkItemCollection);

            double[] startParameters = new double[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
			{
                startParameters[i] = (parameters[i].Value - parameters[i].Min) / (parameters[i].Max - parameters[i].Min);
			}

            numOfPlaces = ((MaxIterations + 1) * (parameters.Length * 2)).ToString().Length;

            string solutionID;
            jobStatusData = new JobStatusData[parameters.Length * 2 + 1]; // only for the central difference derivation
            for (int i = 0; i < jobStatusData.Length; i++)
            {
                solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                jobStatusData[i] = new JobStatusData() { ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0) };
            }

            double[][] dataPoints = new double[2][];
            Item item = baseFrameworkItemCollection[targetArrayName];
            if (item is ArrayBase)
            {
                ArrayBase array = (ArrayBase)item;
                string[] data = array.GetValuesAsString().Split(' ');
                dataPoints[0] = new double[data.Length];
                dataPoints[1] = new double[data.Length];
                for (int i = 0; i < data.Length; i++)
                {
                    dataPoints[1][i] = double.Parse(data[i], OptiMaxFramework.Globals.nfi);
                }
            }

            ObjectiveFunctionDelegate objFunction = new ObjectiveFunctionDelegate(ObjectiveFunction);

            LMA lma = new LMA(objFunction, startParameters, dataPoints, null, DerivativeDelta, ConvergenceTolerance, maxNumOfIterations);
            lma.ReportStatusData = OnReportStatusData;
            lma.ReportConverganceData = OnReportConverganceData;
            AlgorithmResult result = lma.Fit();
            lma = null;

            // report data
            OnReportStatusData("Result " + result.ErrorValue + ", Termination reason: " + result.TerminationReason.ToString());
        }

        private double[][] ObjectiveFunction(double[][] allSamples)
        {
            int numSam = allSamples.GetLength(0);
            int numPar = allSamples[0].Length;

            for (int i = 0; i < numSam; i++)
            {
                for (int j = 0; j < numPar; j++)
                {
                    if (allSamples[i][j] < 0) allSamples[i][j] = 0;
                    else if (allSamples[i][j] > 1) allSamples[i][j] = 1;
                }
            }

            string[] data;
            string[][] resultsData;
            double[][] results = new double[numSam][];

            // evaluate all samples
            resultsData = EvaluateAllSamples(allSamples);
            for (int i = 0; i < numSam; i++)
            {
                data = resultsData[i][0].Split(' ');
                results[i] = new double[data.Length];
                for (int j = 0; j < data.Length; j++)
                {
                    results[i][j] = double.Parse(data[j], OptiMaxFramework.Globals.nfi);
                }
            }

            // report data
            //OnReportData("Evaluation " + (count + 1).ToString() + " completed.");

            return results;
        }

      
    }
}
