﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    public struct HistoryItem
    {
        public int ID;
        public string[] Results;
        public Dictionary<string, Item> ItemValues;     // key: item name; value: constant variable or array

        public HistoryItem(string ID, string[] results, string[] itemsToExtract, Dictionary<string, Item> items)
        {
            this.ID = int.Parse(ID);
            this.Results = results;

            this.ItemValues = new Dictionary<string, Item>();
            Item item;
            foreach (var variableName in itemsToExtract)
            {
                item = items[variableName];

                if (item is VariableBase)
                    this.ItemValues.Add(variableName, ((VariableBase)items[variableName]).ToVariableConstant());
                else if (item is ArrayBase)
                    this.ItemValues.Add(variableName, ((ArrayBase)items[variableName]).ToArrayConstant());
                else if (item is ResponseSurfaceComponent rsc)
                {
                    VariableConstant vc = new VariableConstant(variableName, "", VariableValueType.String);
                    vc.SetValue(rsc.GetStatusData());
                    this.ItemValues.Add(variableName, vc);
                }
            }
        }
    }
}
