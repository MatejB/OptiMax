﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public struct Parameter
    {
        public string Name;
        public double Min;
        public double Max;
        public double Delta;
        private double value;
        public int NumOfValues;
        public VariableValueType ValueType;


        public double Value { get { return value; } }
        
        // Constructors                                                                                                             
        public Parameter(string name, VariableValueType valueType, double min, double max, int numOfValues = -1, double delta = 0)
        {
            if (valueType == VariableValueType.String) throw new Exception("The string value type is not supported.");
            this.Name = name;
            this.ValueType = valueType;
            this.Min = min;
            this.Max = max;
            this.value = min;
            this.NumOfValues = numOfValues;
            this.Delta = delta;
        }
        

        // Methods                                                                                                                  
        public void SetValue(double inputValue)
        {
            value = inputValue;

            if (ValueType == VariableValueType.Integer) value = Math.Round(value, 0);
        }
        public void SetValue(double intervalValue, double intervalMin, double intervalMax)
        {
            value = intervalValue;

            // convert to interval 0 ... 1
            value = (value - intervalMin) / (intervalMax - intervalMin);

            // convert to interval Min ... Max
            value = value * (this.Max - this.Min) + Min;

            if (ValueType == VariableValueType.Integer) value = Math.Round(value, 0);
        }
    }
}
