﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    [DefaultPropertyAttribute("ObjectiveVariableName")]
    public class GADriver : Driver
    {
        // Variables                                                                                                                
        private int numOfGenerations;
        private GA ga;


        // Properties                                                                                                               
        [CategoryAttribute("Control parameters"),
        DisplayName("Population size"),
        DescriptionAttribute("The number of individuals in a population.")]
        public int PopulationSize 
        { 
            get { return ga.PopulationSize; }
            set { ga.PopulationSize = value; }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Number of generations"),
        DescriptionAttribute("The number of iterations of the genetic algorithm.")]
        public int NumOfGenerations
        {
            get { return numOfGenerations; }
            set
            {
                if (value <= 0) throw new Exception("Number of generations must be larger than 0.");
                else numOfGenerations = value;
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Crossover rate"),
        DescriptionAttribute("The probability (0...1) that selectet individuals will have offspring.")]
        public double CrossOverRate
        {
            get { return ga.CrossoverRate; }
            set { ga.CrossoverRate = value; }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Mutation rate"),
        DescriptionAttribute("The probability (0...1) that selectet individual will mutate.")]
        public double MutationRate
        {
            get { return ga.MutationRate; }
            set { ga.MutationRate = value; }
        }


        [CategoryAttribute("Control parameters"),
        DisplayName("Elitism"),
        DescriptionAttribute("If elitism is enabled the best individual in a current generation will survive.")]
        public bool Elitism
        {
            get { return ga.Elitism; }
            set { ga.Elitism = value; }
        }

        [CategoryAttribute("Objective variable"),
        DisplayName("Objective variable"),
        DescriptionAttribute("Select the variable for the objective function."),
        TypeConverter(typeof(VariableNamesTypeConverter))]
        public string ObjectiveVariableName 
        {
            get 
            {
                if (resultItemNames == null || resultItemNames.Length == 0 || resultItemNames[0] == null) 
                {
                    if (Driver_GlobalVars.listOfVariableNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfVariableNames.Sort();
                        resultItemNames = new string[] { Driver_GlobalVars.listOfVariableNames[0] };
                    }
                    else return "";
                }
                return resultItemNames[0];
            }
            set
            {
                resultItemNames = new string[] { value };
            }
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (ObjectiveVariableName == null)
                throw new Exception("The GA driver objective variable must be defined.");
            if (!itemCollection.ContainsKey(ObjectiveVariableName))
            {
                System.Windows.Forms.MessageBox.Show("The item '" + ObjectiveVariableName + "' used in GA driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                return false;
            }
            return true;
        }


        // Constructors                                                                                                             
        public GADriver(string name, string description)
            : this(name, description, 10, null)
        {
        }
        public GADriver(string name, string description, int numOfGenerations, string objectiveVariableName)
            : base(name, description)
        {
            this.numOfGenerations = numOfGenerations;
            this.resultItemNames = new string[] { objectiveVariableName };

            ga = new GA();
        }


        // Methods                                                                                                                  
        public override void RunEvaluation()
        {
            bool err = CorrectlyDefined(baseFrameworkItemCollection);
            //
            int numOfGenomes = ga.PopulationSize * numOfGenerations;
            numOfPlaces = (numOfGenomes + 1).ToString().Length;
            //
            string solutionID;
            jobStatusData = new JobStatusData[ga.PopulationSize];
            for (int i = 0; i < jobStatusData.Length; i++)
            {
                solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                jobStatusData[i] = new JobStatusData() { ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0) };
            }
            // First population
            double[][] population = ga.GenerateNewPopulation(parameters.Length);
            string[][] results;
            double[] fitness = new double[ga.PopulationSize];
            // Loop
            for (int n = 0; n < numOfGenerations; n++)
            {
                if (kill) break;
                // Generate next population
                if (n > 0) population = ga.GenerateNextPopulation(population, fitness);
                // Reset the job status
                for (int i = 0; i < jobStatusData.Length; i++) jobStatusData[i].jobStatus = JobStatus.InQueue;
                // Evaluate all samples
                results = EvaluateAllSamples(population);
                // Invert the rasult to search for the min
                for (int i = 0; i < results.Length; i++)
			    {
                    fitness[i] = -double.Parse(results[i][0], OptiMaxFramework.Globals.nfi);
			    }
                // Report data
                ReportStatusData(n, parameters.Length, population, fitness);
                ReportConverganceData(fitness);
            }
        }
        private void ReportStatusData(int generation, int numParameters, double[][] population, double[] fitness)
        {
            double max = -double.MaxValue;
            int maxID = -1;

            int numOfSamples = fitness.Length;
            double[] average = new double[numParameters];
            double[] stdDev = new double[numParameters];

            for (int i = 0; i < numParameters; i++)
            {
                average[i] = 0;
                stdDev[i] = 0;
            }

            // average                                              
            for (int i = 0; i < numOfSamples; i++)
            {
                for (int j = 0; j < numParameters; j++)
                {
                    average[j] += population[i][j];
                }
            }

            for (int i = 0; i < numParameters; i++)
            {
                average[i] /= numOfSamples;
            }

            // stdev                                                
            for (int i = 0; i < numOfSamples; i++)
            {
                for (int j = 0; j < numParameters; j++)
                {
                    stdDev[j] += Math.Pow(population[i][j] - average[j], 2);
                }
            }

            for (int i = 0; i < numParameters; i++)
            {
                stdDev[i] = Math.Sqrt(stdDev[i]/numOfSamples);
            }

            //

            for (int i = 0; i < numOfSamples; i++)
            {
                if (fitness[i] > max)
                {
                    max = fitness[i];
                    maxID = i;
                }
            }


            string data = "";
            for (int i = 0; i < numParameters; i++)
            {
                data += average[i].ToString("G3", OptiMaxFramework.Globals.nfi) + "(" + stdDev[i].ToString("G3", OptiMaxFramework.Globals.nfi) + ") ";
            }

            OnReportStatusData("Generation " + (generation + 1).ToString() + ": Best fitness = " + (-fitness[maxID]).ToString("G4", OptiMaxFramework.Globals.nfi) + " " + data + " N. skipped: " + base.countHistory);
        }
        private void ReportConverganceData(double[] fitness)
        {
            double max = -double.MaxValue;
            int numOfSamples = fitness.Length;

            for (int i = 0; i < numOfSamples; i++)
            {
                if (fitness[i] > max)
                {
                    max = fitness[i];
                }
            }

            double current = -max;
            if (current < bestValue) bestValue = current;

            OnReportConverganceData(new double[] { bestValue, current });
        }
       
        
    }
}
