using System;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace OptiMaxFramework
{
    [Serializable]
	public class GA
    {
        // Variables                                                                        
        private static Random m_random = new Random();
        private int m_genomeSize;
        private int m_populationSize;
        private double m_crossoverRate;
        private double m_mutationRate;
        private bool m_elitism;
        private double m_rouletteWheelSize;
        private ArrayList m_thisGeneration;
        private ArrayList m_nextGeneration;
        private ArrayList m_fitnessTable;


        // Properties                                                                       
        public int PopulationSize
        {
            get
            {
                return m_populationSize;
            }
            set
            {
                m_populationSize = value;
            }
        }

        public double CrossoverRate
        {
            get
            {
                return m_crossoverRate;
            }
            set
            {
                m_crossoverRate = value;
            }
        }

        public double MutationRate
        {
            get
            {
                return m_mutationRate;
            }
            set
            {
                m_mutationRate = value;
            }
        }

        public bool Elitism
        {
            get
            {
                return m_elitism;
            }
            set
            {
                m_elitism = value;
            }
        }


        // Constructors                                                                     
		public GA()
		{
			DefaultValues();
		}

		public GA(int numOfParameters, int populationSize, double crossoverRate, double mutationRate, bool elitism)
		{
			DefaultValues();

            m_genomeSize = numOfParameters;
            PopulationSize = populationSize;
            CrossoverRate = crossoverRate;
         	MutationRate = mutationRate;
            m_elitism = elitism;
		}


        // Methods                                                                          

        private void DefaultValues()
        {
            Elitism = true;
            MutationRate = 0.05;
            CrossoverRate = 0.80;
            PopulationSize = 100;

            PrepareData();
        }

        private void PrepareData()
        {
            // Create the fitness table.
            m_fitnessTable = new ArrayList();
            m_thisGeneration = new ArrayList();
            m_nextGeneration = new ArrayList();

            Genome.MutationRate = m_mutationRate;
        }

        public double[][] GenerateNewPopulation(int numOfParameters)
        {
            Genome g;
            m_genomeSize = numOfParameters;

            double[][] population = new double[m_populationSize][];
            
            for (int i = 0; i < m_populationSize; i++)
            {
                g = new Genome(m_genomeSize);
                population[i] = g.Genes();
            }
            return population;
        }

        public double[][] GenerateNextPopulation(double[][] currentPopulation, double[] fitnesses)
        {
            Genome g;
            m_thisGeneration.Clear();
            for (int i = 0; i < m_populationSize; i++)
            {
                g = new Genome(currentPopulation[i]);
                g.Fitness = fitnesses[i];
                m_thisGeneration.Add(g);
            }

            //Sort in order of fitness.
            m_thisGeneration.Sort(new GenomeComparer());

            //Now construct the roulette wheel for parental selection.
            //Conventionally, the size of each section of the roulette wheel is proportional
            //to the fitness. This requires some modification to work for negative fitnesses
            //so I have changed it so that the size of each section is proportional to 
            //(fitness - worst_fitness). One side-effect of this is that the worst genome gets 
            //a size of 0, and will therefore never be selected as for parenthood. Another is
            //that the method will fail if the population becomes degenerate.
            double worst_fitness = ((Genome)m_thisGeneration[0]).Fitness; //minimum fitness in generation
            m_fitnessTable.Clear();
            m_rouletteWheelSize = 0d;
            for (int i = 0; i < m_populationSize; i++)
            {
                double fitness = ((Genome)m_thisGeneration[i]).Fitness;
                double section_size = (fitness - worst_fitness); //section size = (fitness - worst fitness).
                m_rouletteWheelSize += section_size;             //cumulative section sizes.
                m_fitnessTable.Add((double)m_rouletteWheelSize); //store cumulative section sizes in a table for later use in the RouletteSelection() method.
            }





            m_nextGeneration.Clear();

            //Breed next gen
            for (int i = 0; i < m_populationSize; i += 2)
            {
                int pidx1 = RouletteSelection();
                int pidx2 = RouletteSelection();

                int count = 0;
                while (count++ < 1000 & !((Genome)m_thisGeneration[pidx1]).DiffersFrom((Genome)m_thisGeneration[pidx2]))
                {
                    pidx2 = RouletteSelection();
                }

                Genome parent1, parent2, child1, child2;
                parent1 = ((Genome)m_thisGeneration[pidx1]).DeepClone();
                parent2 = ((Genome)m_thisGeneration[pidx2]).DeepClone();

                if (m_random.NextDouble() < m_crossoverRate)
                {
                    parent1.Crossover(ref parent2, out child1, out child2);
                }
                else
                {
                    child1 = parent1;
                    child2 = parent2;
                }

                child1.Mutate();
                child2.Mutate();

                child1.Fitness = 0;
                child2.Fitness = 0;

                m_nextGeneration.Add(child1);
                m_nextGeneration.Add(child2);
            }

            //Do elitism
            if (m_elitism)
            {
                Genome best = ((Genome)m_thisGeneration[m_populationSize - 1]);
                //Genome secondBest = ((Genome)m_thisGeneration[m_populationSize - 2]);
                m_nextGeneration[0] = best.DeepClone();
                //m_nextGeneration[1] = secondBest.DeepClone();
            }

            double[][] newPopulation = new double[m_populationSize][];

            for (int i = 0; i < m_populationSize; i++)
            {
                newPopulation[i] = ((Genome)m_nextGeneration[i]).Genes();
            }
            return newPopulation;
        }

		private int RouletteSelection()
		{
			int idx = -1;
            int first = 0;
            int last = m_populationSize - 1;

            if (m_rouletteWheelSize > 0d)
            {
                //ArrayList's BinarySearch is for exact values only so do this by hand.
                //Note that the below code assumes that the roulette wheel is positive.
                double randomFitness = m_random.NextDouble() * m_rouletteWheelSize;
			    int mid = (last - first)/2;
                while (idx == -1 && first <= last)
                {
                    if (randomFitness < (double)m_fitnessTable[mid])
                    {
                        last = mid;
                    }
                    else if (randomFitness > (double)m_fitnessTable[mid])
                    {
                        first = mid;
                    }
                    mid = (first + last) / 2;
                    if ((last - first) == 1) 
                        idx = last; 
                }
            }
            else
            {
                //The roulette wheel can have zero size when all members contian
                //the same fitness. I.e. the population is degenerate or homogenous.
                //In this case roulette selection should just be totally random.
                idx = m_random.Next(last);
            }

			return idx;
		}
        
    }
}
