﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class MonteCarloGenerator : DOEGeneratorBase
    {
        // Constructors                                                                                                             
        public MonteCarloGenerator()
            : this(10)
        {
        }
        public MonteCarloGenerator(int numberOfExperiments)
        {
            Name = "Monte Carlo";
            defineNumOfExperimentsDirectly = true;
            this.numberOfExperiments = numberOfExperiments;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            List<double[]> experiments = new List<double[]>(numberOfExperiments);

            double[] experiment = new double[parameters.Length];

            Random rand = new Random();
            for (int i = 0; i < NumberOfExperiments; i++)
            {
                for (int j = 0; j < experiment.Length; j++)
                {
                    experiment[j] = rand.NextDouble();
                }
                experiments.Add(experiment.ToArray());
            }
           
            return experiments;
        }
    }
}
