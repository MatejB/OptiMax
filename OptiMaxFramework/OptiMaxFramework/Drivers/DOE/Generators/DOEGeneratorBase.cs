﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class DOEGeneratorBase
    {
        // Variables                                                                                                                
        protected bool defineNumOfExperimentsDirectly;
        protected int numberOfExperiments;


        // Properties
        public string Name { get; set; }
        public bool ShowMinValuesColumn { get; set; }
        public bool ShowMaxValuesColumn { get; set; }
        public bool ShowParameterNumOfValuesColumn { get; set; }
        public bool ShowParameterValuesColumn { get; set; }
        public bool ShowParameterDeltaColumn { get; set; }

        public bool DefineNumOfExperimentsDirectly
        {
            get { return defineNumOfExperimentsDirectly; }
            set { defineNumOfExperimentsDirectly = value; }
        }
        public int NumberOfExperiments { get { return numberOfExperiments; } set { numberOfExperiments = Math.Max(1, value); } }


        // Methods                                                                                                                  
        public abstract List<double[]> GenerateExperiments(Parameter[] parameters);
        protected void EvaluateExperiments(List<double[]> experiments, out double phiP, out double minDist)
        {
            double sum = 0;
            double dist;
            minDist = double.MaxValue;
            for (int i = 0; i < experiments.Count - 1; i++)
            {
                for (int j = i + 1; j < experiments.Count; j++)
                {
                    dist = 0;
                    for (int k = 0; k < experiments[i].Length; k++)
                    {
                        dist += Math.Pow(experiments[i][k] - experiments[j][k], 2);
                    }
                    dist = Math.Sqrt(dist);
                    sum += dist;

                    if (dist < minDist) minDist = dist;
                }
            }
            phiP = sum;
        }
    }
}

