﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class FullFactorialGenerator : DOEGeneratorBase
    {
        // Constructors                                                                                                             
        public FullFactorialGenerator()
        {
            Name = "Full Factorial";
            defineNumOfExperimentsDirectly = false;
            numberOfExperiments = -1;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            numberOfExperiments = parameters[0].NumOfValues;
            for (int i = 1; i < parameters.Length; i++)
            {
                numberOfExperiments *= parameters[i].NumOfValues;
            }

            List<double[]> experiments = new List<double[]>(numberOfExperiments);
            double[] experiment = new double[parameters.Length];
            GenerateExperimentsRecursive(experiments, ref experiment, parameters, 0);

            //double phiP;
            //double minDist;
            //EvaluateExperiments(experiments, out phiP, out minDist);

            return experiments;
        }
        private void GenerateExperimentsRecursive(List<double[]> experiments, ref double[] experiment, Parameter[] parameters, int parID)
        {
            if (parID == parameters.Length)
            {
                experiments.Add((double[])experiment.Clone());
            }
            else
            {
                double delta;
                int numOfFractions = parameters[parID].NumOfValues;

                if (numOfFractions <= 1) delta = 0.5d;
                else delta = 1d / (numOfFractions - 1);

                for (int i = 0; i < numOfFractions; i++)
                {
                    experiment[parID] = delta * i;
                    GenerateExperimentsRecursive(experiments, ref experiment, parameters, parID + 1);
                }
            }
        }
    }
}
