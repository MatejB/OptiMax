﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class ParameterFactorialGenerator : DOEGeneratorBase
    {
        // Constructors                                                                                                             
        public ParameterFactorialGenerator()
        {
            Name = "Parameter Factorial";
            defineNumOfExperimentsDirectly = false;
            numberOfExperiments = -1;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            numberOfExperiments = parameters.Length * 2;    // only for approximate size
            List<double[]> experiments = new List<double[]>(numberOfExperiments);
            CompareDoubleArray comparer = new CompareDoubleArray();
            HashSet<double[]> hashset = new HashSet<double[]>(comparer);

            double[] experiment = new double[parameters.Length];
            double[] baseExperiment = new double[parameters.Length];
            for (int i = 0; i < parameters.Length; i++) baseExperiment[i] = 0.5;
            //experiments.Add(baseExperiment);                // first sample-1
            //hashset.Add(baseExperiment);

            double delta;
            int numOfFractions;
            for (int i = 0; i < parameters.Length; i++)
            {
                numOfFractions = parameters[i].NumOfValues;
                if (numOfFractions > 1)                     // if number of fractions is 1 it is the same as sample-1
                {
                    delta = 1d / (numOfFractions - 1);

                    for (int j = 0; j < numOfFractions; j++)
                    {
                        experiment = baseExperiment.ToArray();
                        experiment[i] = delta * j;
                        //if (!hashset.Contains(experiment))
                        {
                            experiments.Add(experiment);
                            hashset.Add(experiment);
                        }
                    }
                }
            }

            numberOfExperiments = experiments.Count;

            return experiments;
        }


    }
}
