﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LHC;


namespace OptiMaxFramework
{
    [Serializable]
    public class RandomLatinHypercubeGenerator : DOEGeneratorBase
    {
        // Variables                                                                                                                
        private int _numOfLoops = 200;


        // Constructors                                                                                                             
        public RandomLatinHypercubeGenerator()
            : this(10)
        {
        }
        public RandomLatinHypercubeGenerator(int numberOfExperiments)
        {
            Name = "Random Latin Hypercube";
            defineNumOfExperimentsDirectly = true;
            this.numberOfExperiments = numberOfExperiments;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            double phiP;
            double minDist;
            double bestPhiP = 0;
            double bestMinDist = 0;
            List<double[]> experiments;
            List<double[]> bestExperiments = null;

            for (int i = 0; i < _numOfLoops; i++)
            {
                experiments = Generate(parameters);
                EvaluateExperiments(experiments, out phiP, out minDist);
                if (phiP > bestPhiP)
                {
                    bestExperiments = experiments.DeepClone();
                    bestPhiP = phiP;
                    bestMinDist = minDist;
                }
            }

            return bestExperiments;
        }

        private List<double[]> Generate(Parameter[] parameters)
        {
            Random rnd = new Random();
            int[][] randIds = new int[parameters.Length][];
            for (int i = 0; i < parameters.Length; i++)
            {
                randIds[i] = new int[numberOfExperiments];
                for (int j = 0; j < numberOfExperiments; j++)
                {
                    randIds[i][j] = j;
                }
                ExtensionMethods.Shuffle(rnd, randIds[i]);
            }

            double[][] intervals = new double[parameters.Length][];
            double delta = 1.0 / (numberOfExperiments - 1);
            for (int i = 0; i < parameters.Length; i++)
            {
                intervals[i] = new double[numberOfExperiments];
                for (int j = 0; j < numberOfExperiments; j++)
                {
                    intervals[i][j] = delta * j;
                }
            }

            List<double[]> experiments = new List<double[]>(numberOfExperiments);
            double[] experiment = new double[parameters.Length];

            for (int i = 0; i < numberOfExperiments; i++)
            {
                for (int j = 0; j < parameters.Length; j++)
                {
                    experiment[j] = intervals[j][randIds[j][i]];
                }
                experiments.Add(experiment.ToArray());
            }

            return experiments;
        }
    }
}
