﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class SensitivityGenerator : DOEGeneratorBase
    {
        // Constructors                                                                                                             
        public SensitivityGenerator()
        {
            Name = "Sensitivity";
            defineNumOfExperimentsDirectly = false;
            numberOfExperiments = -1;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            numberOfExperiments = parameters.Length * 2;    // only for approximate size
            List<double[]> experiments = new List<double[]>(numberOfExperiments);
            
            double[] experiment = new double[parameters.Length];
            double[] baseExperiment = new double[parameters.Length];
            for (int i = 0; i < parameters.Length; i++) baseExperiment[i] = 0.5;
            experiments.Add(baseExperiment);                // first sample-1

            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i].Min = parameters[i].Value - parameters[i].Delta;  // change min and max since this is a clone
                parameters[i].Max = parameters[i].Value + parameters[i].Delta;
            }

            double delta = 1;
            int numOfFractions = 2;
            for (int i = 0; i < parameters.Length; i++)
            {
                for (int j = 0; j < numOfFractions; j++)
                {
                    experiment = baseExperiment.ToArray();
                    experiment[i] = delta * j;
                    experiments.Add(experiment);
                }
            }

            numberOfExperiments = experiments.Count;

            return experiments;
        }


    }
}
