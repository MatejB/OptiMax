﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LHC;

namespace OptiMaxFramework
{
    [Serializable]
    public class OptimalLatinHypercubeGenerator : DOEGeneratorBase
    {
        // Constructors                                                                                                             
        public OptimalLatinHypercubeGenerator()
            : this(10)
        {
        }
        public OptimalLatinHypercubeGenerator(int numberOfExperiments)
        {
            Name = "Optimal Latin Hypercube";
            defineNumOfExperimentsDirectly = true;
            this.numberOfExperiments = numberOfExperiments;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            int[][] cubeIds = FastOptimalLatinHypercube.Generate(numberOfExperiments, parameters.Length);

            double[][] intervals = new double[parameters.Length][];
            double delta = 1.0 / (numberOfExperiments - 1);
            for (int i = 0; i < parameters.Length; i++)
            {
                intervals[i] = new double[numberOfExperiments];
                for (int j = 0; j < numberOfExperiments; j++)
                {
                    intervals[i][j] = delta * j;
                }
            }

            List<double[]> experiments = new List<double[]>(numberOfExperiments);
            double[] experiment = new double[parameters.Length];

            for (int i = 0; i < cubeIds.Length; i++)
            {
                for (int j = 0; j < parameters.Length; j++)
                {
                    experiment[j] = intervals[j][cubeIds[i][j]];
                }
                experiments.Add(experiment.ToArray());
            }

            //double phiP;
            //double minDist;
            //EvaluateExperiments(experiments, out phiP, out minDist);

            return experiments;
        }
    }
}
