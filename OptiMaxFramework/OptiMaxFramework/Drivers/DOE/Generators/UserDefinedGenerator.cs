﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    [Serializable]
    public class UserDefinedGenerator : DOEGeneratorBase
    {
        // Variables                                                                                                                
        private string arrayName;
        private string[] userDefinedValuesAsString;


        // Properities                                                                                                              
        public string ArrayName
        {
            get
            {
                if (arrayName == null) return "";
                else return arrayName;
            }
            set
            {
                arrayName = value;
                if (arrayName == "") arrayName = null;
            }
        }

        public string[] UserDefinedValuesAsString
        {
            get { return userDefinedValuesAsString; }
            set { userDefinedValuesAsString = value; }
        }



        // Constructors                                                                                                             
        public UserDefinedGenerator()
        {
            Name = "User Defined";
            defineNumOfExperimentsDirectly = false;
            numberOfExperiments = -1;
            userDefinedValuesAsString = null;
        }


        // Methods                                                                                                                  
        public override List<double[]> GenerateExperiments(Parameter[] parameters)
        {
            List<double[]> experiments = new List<double[]>();

            if (userDefinedValuesAsString != null)
            {
                string[] tmp;
                string[] separator = new string[] { ", ", " " };
                double[] experiment;

                for (int i = 0; i < userDefinedValuesAsString.Length; i++)
                {
                    tmp = userDefinedValuesAsString[i].Split(separator, StringSplitOptions.RemoveEmptyEntries);

                    if (tmp.Length != parameters.Length)
                        throw new Exception("User defined values in row " + (i + i) + " defined in '" + arrayName +
                                            "' does not have exactly " + parameters.Length + " parameter(s).");

                    experiment = new double[tmp.Length];
                    for (int j = 0; j < tmp.Length; j++)
                    {
                        if (double.TryParse(tmp[j], System.Globalization.NumberStyles.Number, Globals.nfi, out experiment[j]))
                            experiment[j] = (experiment[j] - parameters[j].Min) / (parameters[j].Max - parameters[j].Min);
                        else throw new Exception("User defined values in row " + (i + i) + " defined in '" + arrayName +
                                                 "' can not be converted to numeric values.");
                    }

                    experiments.Add(experiment);
                }
            }

            return experiments;
        }
    }
}
