﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LHC
{
    public class FastOptimalLatinHypercube
    {
        // An Algorithm for Fast Optimal Latin Hypercube Design of Experiments                  
        // http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.885.9108&rep=rep1&type=pdf  
        //                                                                                      
        public static int[][] Generate(int np, int nv)
        {
            // inputs:  np – number of points of the desired Latin hypercube(LH)
            //          nv – number of variables in the LH
            //          seed – initial seed design(points within 1 and ns)
            //          ns – number of points in the seed design
            // outputs: X – Latin hypercube created using the translational propagation algorithm

            int[][] seed = new int[1][];
            seed[0] = new int[nv];
            int ns = seed.Length;
            int ndStar;
            double nd;
            double nb;

            // define the size of the TPLHD to be created first
            // number of divisions, nd
            nd = Math.Pow((double)np / ns, 1.0 / nv);
            ndStar = (int)Math.Ceiling(nd);

            if (ndStar > nd) nb = Math.Pow(ndStar, nv);     // it is necessary to create a bigger TPLHD
            else nb = np / ns;                              // it is NOT necessary to create a bigger TPLHD

            int npStar = (int)(nb * ns);                    // size of the TPLHD to be created first

            // reshape seed to properly create the first design
            seed = ReshapeSeed(seed, ns, npStar, ndStar, nv);

            // create TPLHD with npStar points
            int[][] X = CreateTPLHD(seed, ns, npStar, ndStar, nv);

            // resize TPLH if necessary: npStar > np;
            if (npStar >= np)
                X = ResizeTPLHD(X, npStar, np, nv);
            else
                nd = nd;

            return X;
        }
        public static int[][] Generate(int np, int nv, int[][] seed)
        {
            // inputs:  np – number of points of the desired Latin hypercube(LH)
            //          nv – number of variables in the LH
            //          seed – initial seed design(points within 1 and ns)
            //          ns – number of points in the seed design
            // outputs: X – Latin hypercube created using the translational propagation algorithm
            int ns = seed.Length;
            int ndStar;
            double nd;
            double nb;

            // define the size of the TPLHD to be created first
            // number of divisions, nd
            nd = Math.Pow((double)np / ns, 1.0 / nv);
            ndStar = (int)Math.Ceiling(nd);

            if (ndStar > nd) nb = Math.Pow(ndStar, nv);     // it is necessary to create a bigger TPLHD
            else nb = np / ns;                              // it is NOT necessary to create a bigger TPLHD

            int npStar = (int)(nb * ns);                    // size of the TPLHD to be created first

            // reshape seed to properly create the first design
            seed = ReshapeSeed(seed, ns, npStar, ndStar, nv);

            // create TPLHD with npStar points
            int[][] X = CreateTPLHD(seed, ns, npStar, ndStar, nv);

            // resize TPLH if necessary: npStar > np;
            if (npStar > np)
                X = ResizeTPLHD(X, npStar, np, nv);

            return X;
        }

        private static int[][] ReshapeSeed(int[][] seed, int ns, int npStar, int ndStar, int nv)
        {
            // inputs:  seed – initial seed design(points within 1 and ns)
            //          ns – number of points in the seed design
            //          npStar – number of points of the Latin hypercube(LH)
            //          nd – number of divisions of the LH
            //          nv – number of variables in the LH
            // outputs: seed – seed design properly scaled

            if (ns == 1)
            {
                seed = Ones(1, nv); // arbitrarily put at the origin
            }
            else
            {
                double[][] uf = Multipy(Ones(1, nv), ns);
                double[][] ut = Multipy(Ones(1, nv), ((double)npStar / ndStar) - ndStar * (nv - 1) + 1);
                double[][] rf = Add(uf, -1);
                double[][] rt = Add(ut, -1);
                double[][] a = DivideByElement(rt, rf);
                double[][] b = Subtract(ut, MultipyByElement(a, uf));

                for (int i = 0; i < ns; i++)
                {
                    //seed(i,:) = a.*seed(i,:) + b;
                    //seed = round(seed)
                    seed[i] = Round(Add(MultipyByElement(a, GetRowAsMatrix(seed, i)), b))[0];
                }
            }
            return seed;
        }
        private static int[][] CreateTPLHD(int[][] seed, int ns, int npStar, int ndStar, int nv)
        {
            // inputs:  seed – initial seed design(points within 1 and ns)
            //          ns – number of points in the seed design
            //          npStar – number of points of the Latin hypercube(LH)
            //          nd – number of divisions of the LH
            //          nv – number of variables in the LH
            // outputs: X – Latin hypercube design created by the translational propagation algorithm
            //
            // we warn that this function has to be properly translated to other programming languages to avoid problems with memory allocation

            int[][] X = seed.ToArray();
            double[][] d = OnesD(1, nv);        // just for memory allocation
            
            for (int c1 = 0; c1 < nv; c1++)     // shifting one direction at a time
            { 
                seed = X.ToArray();             // update seed with the latest points added

                SetMatrixRowElements(d, 0, 0, c1 - 1, Math.Pow(ndStar, (c1 + 1) - 2));
                d[0][c1] = npStar / ndStar;
                SetMatrixRowElements(d, 0, c1 + 1, nv, Math.Pow(ndStar, (c1 + 1) - 1));

                for (int c2 = 1; c2 < ndStar; c2++)
                {
                    ns = seed.Length;           // update seed size
                    for (int c3 = 0; c3 < ns; c3++)
                    {
                        seed[c3] = Round(Add(GetRowAsMatrix(seed, c3), d))[0];
                    }
                    
                    X = Vertcat(X, seed);
                }
            }

            return X;
        }
        private static int[][] ResizeTPLHD(int[][] X, int npStar, int np, int nv)
        {
            // inputs:  X – initial Latin hypercube design
            //          npStar – number of points in the initial X
            //          np – number of points in the final X
            //          nv – number of variables
            // outputs: X – final X, properly shrunk

            double[][] center = Multipy(Ones(1, nv), npStar / 2.0);     // center of the design space
            // distance between each point of X and the center of the design space
            double[] distance = new double[npStar];
            for (int c1 = 0; c1 < npStar; c1++) distance[c1] = NormOfRowVector(Subtract(GetRowAsMatrix(X, c1), center));

            int[] idx = Enumerable.Range(0, distance.Length).ToArray();

            Array.Sort(distance, idx);

            // resize X to np closest points
            int[][] resizedX = new int[np][];
            for (int i = 0; i < np; i++) resizedX[i] = X[idx[i]].ToArray();
            X = resizedX;

            // re - establish the LH conditions
            int[] Xmin = MinByColumns(X);
            int[] vecNp = new int[np];
            for (int i = 0; i < np; i++) vecNp[i] = i;

            for (int c1 = 0; c1 < nv; c1++)
            {
                // place X in the origin
                X = SortRows(X, c1);
                for (int i = 0; i < X.Length; i++) X[i][c1] += -Xmin[c1] + 1;

                // eliminate empty coordinates
                for (int i = 0; i < X.Length; i++)
                {
                    X[i][c1] = vecNp[i];
                }
            }
            return X;
        }

        private static double[][] ZerosD(int dim1, int dim2)
        {
            double[][] zeros = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                zeros[i] = new double[dim2];
            }
            return zeros;
        }
        private static int[][] Ones(int dim1, int dim2)
        {
            int[][] ones = new int[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                ones[i] = new int[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    ones[i][j] = 1;
                }
            }
            return ones;
        }
        private static double[][] OnesD(int dim1, int dim2)
        {
            double[][] ones = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                ones[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    ones[i][j] = 1;
                }
            }
            return ones;
        }
        private static double[][] GetRowAsMatrix(int[][] matrix, int row)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;
            double[][] result = new double[1][];
            result[0] = new double[dim2];
            for (int j = 0; j < dim2; j++)
            {
                result[0][j] = matrix[row][j];
            }
            return result;
        }
        private static int[][] Round(double[][] matrix)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;
            int[][] result = new int[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new int[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = (int)Math.Round(matrix[i][j]);
                }
            }
            return result;
        }
        private static double[][] Multipy(int[][] matrix, double a)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrix[i][j] * a;
                }
            }
            return result;
        }
        private static double[][] MultipyByElement(double[][] matrixA, double[][] matrixB)
        {
            int dim1 = matrixA.Length;
            int dim2 = matrixA[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrixA[i][j] * matrixB[i][j];
                }
            }
            return result;
        }
        private static double[][] DivideByElement(double[][] matrixA, double[][] matrixB)
        {
            int dim1 = matrixA.Length;
            int dim2 = matrixA[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrixA[i][j] / matrixB[i][j];
                }
            }
            return result;
        }
        private static double[][] Add(double[][] matrix, double a)
        {
            int dim1 = matrix.GetLength(0);
            int dim2 = matrix[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrix[i][j] + a;
                }
            }
            return result;
        }
        private static double[][] Add(double[][] matrixA, double[][] matrixB)
        {
            int dim1 = matrixA.Length;
            int dim2 = matrixA[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrixA[i][j] + matrixB[i][j];
                }
            }
            return result;
        }
        private static double[][] Subtract(double[][] matrixA, double[][] matrixB)
        {
            int dim1 = matrixA.Length;
            int dim2 = matrixA[0].Length;
            double[][] result = new double[dim1][];
            for (int i = 0; i < dim1; i++)
            {
                result[i] = new double[dim2];
                for (int j = 0; j < dim2; j++)
                {
                    result[i][j] = matrixA[i][j] - matrixB[i][j];
                }
            }
            return result;
        }
        private static void SetMatrixRowElements(double[][] matrix, int row, int start, int end, double value)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;
            if (start < 0) start = 0;
            if (end >= dim2) end = dim2 - 1;

            for (int j = start; j <= end; j++)
            {
                matrix[row][j] = value;
            }
        }
        private static int[][] Vertcat(int[][] matrixA, int[][] matrixB)
        {
            int dimA1 = matrixA.Length;
            int dimA2 = matrixA[0].Length;

            int dimB1 = matrixB.Length;
            int dimB2 = matrixB[0].Length;

            if (dimA2 != dimB2) throw new Exception();

            int[][] result = new int[dimA1 + dimB1][];
            for (int i = 0; i < dimA1; i++)
            {
                result[i] = matrixA[i].ToArray();
            }
            for (int i = 0; i < dimB1; i++)
            {
                result[dimA1 + i] = matrixB[i].ToArray();
            }
            return result;
        }
        private static double NormOfRowVector(double[][] matrix)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;

            if (dim1 != 1) throw new Exception();

            double result = 0;
            for (int j = 0; j < dim2; j++)
            {
                result += Math.Pow(matrix[0][j], 2);
            }
            return Math.Sqrt(result);
        }
        private static int[] MinByColumns(int[][] matrix)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;

            int[] min = new int[dim2];
            for (int i = 0; i < dim2; i++) min[i] = int.MaxValue;

            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    if (matrix[i][j] < min[j]) min[j] = matrix[i][j];
                }
            }

            return min;
        }
        private static int[][] SortRows(int[][] matrix, int col)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;

            int[] colToSort = new int[dim1];
            for (int i = 0; i < dim1; i++) colToSort[i] = matrix[i][col];

            Array.Sort(colToSort, matrix);

            return matrix;
        }
        private static int CompareColToVector(int[][] matrix, int col, int[] vector)
        {
            int dim1 = matrix.Length;
            int dim2 = matrix[0].Length;

            for (int i = 0; i < dim1; i++)
            {
                if (matrix[i][col] != vector[i])
                {
                    return 0;
                }
            }
            return 1;
        }
    }
}
