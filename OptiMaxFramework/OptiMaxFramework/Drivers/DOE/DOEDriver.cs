﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    public class DOEDriver : Driver, IDependent
    {
        // Variables                                                                                                                
        private string _generatorName;


        // Properties                                                                                                               
        public string DOEGeneratorName
        {
            get
            {
                return _generatorName;
            }
            set
            {
                _generatorName = value;

                bool missing = true;
                foreach (Type mytype in GetAllDOEGeneratorTypes())
                {
                    DOEGenerator = (DOEGeneratorBase)Activator.CreateInstance(mytype);

                    if (DOEGenerator.Name == _generatorName)
                    {
                        missing = false;
                        break;
                    }
                }
                if (missing) throw new NotSupportedException();
            }
        }
        public DOEGeneratorBase DOEGenerator { get; set; }


        // Constructors                                                                                                             
        public DOEDriver(string name, string description)
            : base(name, description)
        {
            if (DOEGeneratorName == null)
            {
                List<string> typeNames = GetAllDOEGeneratorNames();
                DOEGeneratorName = typeNames[0].ToString();
            }
        }


        // Methods                                                                                                                  
        public static List<string> GetAllDOEGeneratorNames()
        {
            var types = GetAllDOEGeneratorTypes();

            List<string> typeNames = new List<string>();
            DOEGeneratorBase generator;
            foreach (Type mytype in types)
            {
                generator = (DOEGeneratorBase)Activator.CreateInstance(mytype);
                typeNames.Add(generator.Name);
            }
            typeNames.Sort();

            return typeNames;
        }
        private static IEnumerable<Type> GetAllDOEGeneratorTypes()
        {
            var types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(
                mytype => mytype.IsSubclassOf(typeof(DOEGeneratorBase)));

            return types;
        }
        public override void RunEvaluation()
        {
            if (DOEGenerator != null)
            {
                // for user defined generator copy the values from the array to the generator
                if (DOEGenerator is UserDefinedGenerator udg)
                {
                    ArrayBase array = baseFrameworkItemCollection[udg.ArrayName] as ArrayBase;
                    string[] userDefinedValues = new string[array.Length];
                    for (int i = 0; i < array.Length; i++) userDefinedValues[i] = array.GetValueAsString(i);
                    udg.UserDefinedValuesAsString = userDefinedValues;
                }

                double[][] experiments = DOEGenerator.GenerateExperiments(parameters).ToArray();
                int numOfExperiments = experiments.GetLength(0);
                numOfPlaces = (numOfExperiments + 1).ToString().Length;
                string solutionID;

                jobStatusData = new JobStatusData[numOfExperiments];
                for (int i = 0; i < jobStatusData.Length; i++)
                {
                    solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                    jobStatusData[i] = new JobStatusData() {ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0)};
                }

                string[][] fitness = EvaluateAllSamples(experiments);
            }
            else throw new Exception("The DOEGenerator is not defined.");
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (DOEGenerator is UserDefinedGenerator udg)
            {
                if (!itemCollection.ContainsKey(udg.ArrayName))
                    throw new Exception("The array of user defined values in DOE driver is missing.");
            }
            return true;
        }


        // IDependent
        public List<string> InputVariables
        {
            get
            {
                if (DOEGenerator is UserDefinedGenerator udg && udg.ArrayName != "") return new List<string>() { udg.ArrayName };
                else return new List<string>();
            }
        }
    }








}
