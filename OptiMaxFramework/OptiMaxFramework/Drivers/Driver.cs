﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    public abstract class Driver : Item
    {
        // Variables                                                                                                                
        protected Workflow workflow;
        protected Parameter[] parameters;

        protected string workingDirectory;
        protected string[] resultItemNames;
        protected int count;
        protected int numOfPlaces;  //The number of places used to numerate the solution directories
        protected bool kill;
        protected JobStatusData[] jobStatusData;
        protected int countHistory;
        protected ConcurrentDictionary<double[], HistoryItem> history;
        protected string[] allHistoryItemsToExtract;
        protected string[] theBestHistoryItemsToExtract;
        private ConcurrentQueue<Exception> exceptions; // use ConcurrentQueue to enable safe enqueueing from multiple threads. 
        protected int numOfThreads;
        protected int numOfRetries;
        private Action<string> ReportStatusData;
        private Action<double[]> ReportCoverganceData;
        protected double bestValue;


        // Properties                                                                                                               
        [Browsable(false)]
        public Workflow Workflow 
        {
            get
            {
                return workflow;
            }
        }

        [Browsable(false)]
        public Parameter[] Parameters
        {
            get
            {
                return parameters;
            }
        }

        [CategoryAttribute("Execution"),
        DisplayName("Number of threads"),
        DescriptionAttribute("Input a value larger than 1 to use parallel workflow execution.")]
        public virtual int NumOfThreads 
        {
            get { return numOfThreads; }
            set
            {
                numOfThreads = Math.Max(1, value);
            }
        }

        [CategoryAttribute("Execution"),
        DisplayName("Delete working directory"),
        DescriptionAttribute("Select True to delete the working directory after workflow completition. All data that is not recorded will be lost.")]
        public bool DeleteWorkingDirectoryForSolution { get; set; }

        [CategoryAttribute("Execution"),
        DisplayName("Number of retries"),
        DescriptionAttribute("Set the number of times the workfolw will retry if it fails.")]
        public int NumberOfRetries 
        { 
            get {return numOfRetries;} 
            set
            {
                numOfRetries = Math.Max(0, value);
            }
        }

        [Browsable(false)]
        public JobStatusData[] JobStatusData
        {
            get
            {
                if (jobStatusData != null) return jobStatusData.DeepClone();
                else return null;
            }
        }

        [ReadOnly(false)]
        public override string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Browsable(false)]
        public string[] GetResultItemNames
        {
            get
            {
                return resultItemNames;
            }
        }

        [Browsable(false)]
        public void SetAListOfAllResultItemNames(List<string> variableNames, List<string> arrayNames)
        {
            Driver_GlobalVars.listOfVariableNames = variableNames;
            Driver_GlobalVars.listOfArrayNames = arrayNames;
        }
        


        // Constructors                                                                                                             
        public Driver(string name, string description)
            : base(name, description)
        {
            workflow = new Workflow();
            parameters = new Parameter[0];
            workingDirectory = null;
            resultItemNames = null;
            count = 0;
            numOfPlaces = 4;
            kill = false;
            jobStatusData = null;

            exceptions = new ConcurrentQueue<Exception>();
            NumOfThreads = 1;
            numOfRetries = 0;
        }


        // Methods                                                                                                                  
        public void SetReportStatusData(Action<string> reportStatusData)
        {
            ReportStatusData = reportStatusData;
        }
        public void SetReportConverganceData(Action<double[]> reportConverganceData)
        {
            ReportCoverganceData = reportConverganceData;
        }
        public void AddParameter(Parameter parameter)
        {
            List<Parameter> tmp = new List<Parameter>(parameters);
            tmp.Add(parameter);
            parameters = tmp.ToArray();
        }
        public void ClearParameters()
        {
            parameters = new Parameter[0];
        }

        public void Run(string workingDirectory)
        {
            kill = false;
            count = 0;
            numOfPlaces = 4;
            this.workingDirectory = workingDirectory;
            exceptions = new ConcurrentQueue<Exception>();
            CompareDoubleArray comparer = new CompareDoubleArray();
            history = new ConcurrentDictionary<double[], HistoryItem>(comparer);
            countHistory = 0;
            bestValue = double.MaxValue;

            // get all variables and arrays that are being recorded
            List<string> tmpAll = new List<string>();
            List<string> tmpTheBest = new List<string>();
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is Recorder)
                {
                    // for each item name in recorder
                    foreach (var itemName in ((Recorder)item).InputVariables)
                    {
                        // get the item from the item name
                        if (baseFrameworkItemCollection[itemName] is IRecordable)
                        {
                            // add the item if it is a variable or an array
                            if (!tmpAll.Contains(itemName)) tmpAll.Add(itemName);
                            if (!((Recorder)item).RecordAll && !tmpTheBest.Contains(itemName)) tmpTheBest.Add(itemName);
                        }
                    }
                }
            }
            allHistoryItemsToExtract = tmpAll.ToArray();
            theBestHistoryItemsToExtract = tmpTheBest.ToArray();

            // add parameters for the input arrays
            Item itemInput;
            ArrayInput ai;
            Parameter newParameter;
            List<Parameter> parClone = new List<Parameter>();
            foreach (var parameter in parameters)
            {
                itemInput = baseFrameworkItemCollection[parameter.Name];
                if (itemInput is VariableInput)
                    parClone.Add(parameter);
                if (itemInput is ArrayInput)
                {
                    ai = itemInput as ArrayInput;
                    for (int i = 0; i < ai.NumOfVariables; i++)
                    {
                        newParameter = new Parameter();
                        newParameter.Name = parameter.Name + Globals.InputArraySeparator + i;
                        newParameter.ValueType = ai.ValueType;
                        newParameter.Min = parameter.Min;
                        newParameter.Max = parameter.Max;
                        newParameter.SetValue(parameter.Value);
                        newParameter.Delta = parameter.Delta;
                        newParameter.NumOfValues = parameter.NumOfValues;

                        parClone.Add(newParameter);
                    }
                }
            }
            parameters = parClone.ToArray();

            RunEvaluation();
        }
        abstract public void RunEvaluation();
        public void Kill()
        {
            kill = true;
            if (workflow != null) workflow.Kill();

            if (jobStatusData != null)
            {
                for (int i = 0; i < jobStatusData.Length; i++)
                {
                    if (jobStatusData[i].jobStatus == JobStatus.Running)
                    {
                        jobStatusData[i].jobStatus = JobStatus.Killed;
                        jobStatusData[i].elapsedTime = DateTime.Now - jobStatusData[i].startTime;
                    }
                }
            }
        }

        /// <summary>
        /// The function evaluates all samples
        /// </summary>
        /// <param name="samples">The first index is the sample index and the second index is the index of the sample parameter.</param>
        /// <returns>The first index is the sample index and the second index is the index of the result.</returns>
        protected string[][] EvaluateAllSamples(double[][] samples)
        {
            countHistory = 0;
            string[][] fitness = new string[samples.GetLength(0)][];

            // clear the working directory
            Globals.ClearDirectoryContents(workingDirectory);

            // clear the connections to the framework - is rebilded later
            foreach (var entry in baseFrameworkItemCollection)
            {
                if (!(entry.Value is Recorder || entry.Value is Driver)) entry.Value.FrameworkItemCollection = null;
            }

            // single threaded solution
            if (NumOfThreads == 1)
            {
                for (int i = 0; i < samples.GetLength(0); i++)
                {
                    if (kill) break;
                    
                    fitness[i] = TryEvaluateSampleMiltipleTimes(i, samples[i], numOfRetries);

                    if (exceptions.Count > 0) throw new AggregateException(exceptions);
                }
            }

            // parallel solution
            else
            {
                Parallel.For(0, samples.GetLength(0), new ParallelOptions { MaxDegreeOfParallelism = NumOfThreads }, (i, loopState) =>
                {
                    if (kill) loopState.Break();
                    else fitness[i] = TryEvaluateSampleMiltipleTimes(i, samples[i], numOfRetries);
                });
                if (exceptions.Count > 0) throw new AggregateException(exceptions);
            }

            // report exceptions if any
            if (exceptions.Count > 0)
            {
                string data = "";
                foreach (var ex in exceptions) data += ex.Message + Environment.NewLine;
                OnReportStatusData(data);
            }

            count += samples.GetLength(0);

            return fitness;
        }
        private string[] TryEvaluateSampleMiltipleTimes(int i, double[] sample, int numOfTries)
        {
            string workingDirectoryForSolution = null;

            try
            {
                if (jobStatusData[i].numOfRetries == 0) jobStatusData[i].jobStatus = JobStatus.Running;
                else jobStatusData[i].jobStatus = JobStatus.Retrying;

                jobStatusData[i].startTime = DateTime.Now;
                
                string solutionID = (count + i + 1).ToString().PadLeft(numOfPlaces, '0');                
                jobStatusData[i].ID = solutionID;
                
                if (workingDirectory != null) workingDirectoryForSolution = System.IO.Path.Combine(workingDirectory, solutionID);
                else workingDirectoryForSolution = null;

                JobStatus status = JobStatus.OK;
                string[] result = EvaluateSample(solutionID, sample, workingDirectoryForSolution, ref status);

                jobStatusData[i].jobStatus = status;
                jobStatusData[i].elapsedTime = DateTime.Now - jobStatusData[i].startTime;

                if (jobStatusData[i].jobStatus == JobStatus.TimedOut)
                    throw new TimeZoneNotFoundException("Timedout");

                return result;
            }
            catch (Exception ex)
            {
                if (jobStatusData[i].numOfRetries < numOfTries)
                {
                    jobStatusData[i].numOfRetries++;
                    if (workingDirectoryForSolution != null) Globals.ClearDirectoryContents(workingDirectoryForSolution);
                    return TryEvaluateSampleMiltipleTimes(i, sample, numOfTries);
                }
                else
                {
                    if (jobStatusData[i].numOfRetries == numOfTries)
                        exceptions.Enqueue(new Exception("Evaluation of job ID=" + jobStatusData[i].ID + " not successful after " +
                                           numOfRetries + " retry/retries." + Environment.NewLine + Environment.NewLine + 
                                           ex.Message));
                    else exceptions.Enqueue(ex);
                    return null;
                }
            }
            finally
            {
                if (DeleteWorkingDirectoryForSolution)
                {
                    int count = 0;
                    do
                    {
                        System.Threading.Thread.Sleep(1000);
                        count++;
                    }
                    while (!DeleteAllWorkDirectories(workingDirectoryForSolution) && count <= 5);

                    if (count >= 5)
                        throw new Exception("Failed to delete the folder for the solution " + (count + i + 1) + " after 5 tries.");
                }
            }
        }
        protected string[] EvaluateSample(string solutionID, double[] sample, string workingDirectoryForSolution, ref JobStatus jobStatus)
        {
            try
            {
                // This function is called from multiple threads at the same time
                //
                // Clone parameters
                Parameter[] parClone = parameters.DeepClone();
                for (int i = 0; i < sample.Length; i++) parClone[i].SetValue(sample[i], intervalMin: 0, intervalMax: 1);
                // Check if the current solution already exists
                if (history.ContainsKey(sample))
                {
                    RecordSingleValueResult(solutionID, history[sample].ItemValues, true);
                    System.Threading.Interlocked.Increment(ref countHistory);
                    jobStatus = JobStatus.OK;
                    return history[sample].Results;
                }
                // Clone framework items
                Item item;
                Item itemClone;
                Dictionary<string, Item> itemsClone = new Dictionary<string, Item>();
                //
                foreach (var entry in baseFrameworkItemCollection)
                {
                    item = entry.Value;
                    if (!(item is Recorder || item is Driver))
                    {
                        if (item is IDeepCopy) itemClone = ((IDeepCopy)item).DeepCopy();
                        else itemClone = item.DeepClone();
                        // Job id
                        if (itemClone.Name == Globals.JobIdName) (itemClone as VariableConstant).SetValue(int.Parse(solutionID));
                        // Set the pointer of the FrameworkItemCollection to the cloned item collection
                        itemClone.FrameworkItemCollection = itemsClone; // reset the framework - connection cleared in EvaluateAllSamples
                        itemsClone.Add(item.Name, itemClone);
                    }
                }
                // Rxecute workflow
                string[] results = null;
                jobStatus = workflow.Execute(workingDirectoryForSolution, itemsClone, parClone);
                if (jobStatus == JobStatus.OK)
                {
                    // Record data
                    RecordAllResults(solutionID, itemsClone);
                    // Gather results
                    if (resultItemNames != null && resultItemNames.Length > 0)
                    {
                        results = new string[resultItemNames.Length];
                        for (int i = 0; i < resultItemNames.Length; i++)
                        {
                            item = itemsClone[resultItemNames[i]];
                            if (item is VariableBase) results[i] = (((VariableBase)item).GetValueAsString());
                            else if (item is ArrayBase) results[i] = (((ArrayBase)item).GetValuesAsString());
                            else throw new Exception("The item '" + item.Name + "' can not be used as a result variable/array.");
                        }
                    }
                    // Overwrite
                    Overwrite(sample, itemsClone);
                    // History
                    history.GetOrAdd(sample, new HistoryItem(solutionID, results, allHistoryItemsToExtract, itemsClone));
                }
                // Clear the dictionary due to the out of memory exception
                itemsClone.Clear();
                itemsClone = null;
                // Return values
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception(ExceptionTools.FormatException(this, ex));
            }
        }

        protected string GetHash(double[] sample)
        {
            // the hash code is also used to record the best results - NSGAII
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < sample.Length; i++)
            {
                sb.AppendFormat("{0}_", sample[i]);
            }
            return sb.ToString();
        }
        private bool DeleteAllWorkDirectories(string workingDirectoryForSolution)
        {
            try
            {
                Globals.DeleteDirectoryRecursive(workingDirectoryForSolution);

                string componentDirectory;
                foreach (var entry in baseFrameworkItemCollection)
                {
                    if (entry.Value is ISshComponent)
                    {
                        string solutionId = workingDirectoryForSolution.Split('\\').Last();
                        componentDirectory = System.IO.Path.Combine((entry.Value as ISshComponent).ServerPath, solutionId);
                        Globals.DeleteDirectoryRecursive(componentDirectory);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }
        //
        private void RecordAllResults(string recordID, Dictionary<string, Item> items)
        {
            Recorder rec;
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is Recorder)
                {
                    rec = (Recorder)item;
                    if (rec.RecordAll) rec.Record(recordID, items);
                }
            }
        }
        protected void RecordSingleValueResult(string recordID, Dictionary<string, Item> items, bool recordAll)
        {
            Recorder rec;
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is IValueRecorder)
                {
                    rec = (Recorder)item;
                    if (rec.RecordAll == recordAll)
                        rec.Record(recordID, items);
                }
            }
        }
        protected void RecordMergedValueResult(string recordID, Dictionary<string, List<Item>> items, bool recordAll)
        {
            // recordID - generation number
            // items[item name, merged items]
            Recorder rec;
            foreach (Item item in baseFrameworkItemCollection.Values)
            {
                if (item is IValueRecorder)
                {
                    rec = (Recorder)item;
                    if (rec.RecordAll == recordAll)
                        ((IValueRecorder)rec).Record(recordID, items);
                }
            }
        }
        //
        protected void Overwrite(double[] sample, Dictionary<string, Item> items)
        {
            string key = "_Overwrite";
            string[] tmp;
            int arrayElementId;
            string name;
            double value;
            //
            for (int i = 0; i < sample.Length; i++)
            {
                arrayElementId = -1;
                name = parameters[i].Name;
                tmp = name.Split(new string[] { Globals.InputArraySeparator }, StringSplitOptions.RemoveEmptyEntries);
                if (tmp.Length == 2)
                {
                    name = tmp[0];
                    arrayElementId = int.Parse(tmp[1]);
                }
                //
                foreach (var entry in items)
                {
                    if (entry.Value.Name == name + key)
                    {
                        if (entry.Value is VariableBase vb)
                            value = double.Parse(vb.GetValueAsString(), Globals.nfi);
                        else if (entry.Value is ArrayBase ab && arrayElementId >= 0)
                            value = double.Parse(ab.GetValueAsString(arrayElementId), Globals.nfi);
                        else throw new Exception("The use of the Overwrite statement for the item '" +
                                                 entry.Value.Name + "' is invalid.");
                        // Convert to interval 0 ... 1
                        value = (value - parameters[i].Min) / (parameters[i].Max - parameters[i].Min);
                        sample[i] = value; 
                    }
                }
            }
        }
        // Event handling
        protected void OnReportStatusData(string data)
        {
            if (ReportStatusData != null) ReportStatusData(data);
        }
        protected void OnReportConverganceData(double[] data)
        {
            if (ReportCoverganceData != null) ReportCoverganceData(data);
        }
    }

    /******************************************************************************************************************************/
    [Serializable]
    internal static class Driver_GlobalVars
    {
        internal static List<string> listOfVariableNames = new List<string>();
        internal static List<string> listOfArrayNames = new List<string>();
    }

    [Serializable]
    public class VariableNamesTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            //true means show a combobox
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            //true will limit to list. false will show the list, 
            //but allow free-form entry
            return true;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Driver_GlobalVars.listOfVariableNames);
        }
    }

    [Serializable]
    public class ArrayNamesTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            //true means show a combobox
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            //true will limit to list. false will show the list, 
            //but allow free-form entry
            return true;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Driver_GlobalVars.listOfArrayNames);
        }
    }

    [Serializable]
    public class ArrayNamesWithEmptyTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            //true means show a combobox
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            //true will limit to list. false will show the list, 
            //but allow free-form entry
            return true;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            Driver_GlobalVars.listOfArrayNames.Sort();
            List<string> arrayNames = new List<string>(Driver_GlobalVars.listOfArrayNames);
            arrayNames.Insert(0, "");
            return new StandardValuesCollection(arrayNames);
        }
    }





    [Serializable]
    public class ConstraintVariableName
    {
        private string name;

        [CategoryAttribute("Data"),
        DisplayName("Name"),
        DescriptionAttribute("Select the constraint name."),
        TypeConverter(typeof(VariableNamesTypeConverter))]
        public string Name 
        {
            get
            {
                if (name == null)
                {
                    if (Driver_GlobalVars.listOfVariableNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfVariableNames.Sort();
                        name = Driver_GlobalVars.listOfVariableNames[0];
                    }
                    else return "";
                }
                return name;
            }
            set
            {
                name = value;
            }
        }
    }

    [Serializable]
    public class ObjectiveVariableName
    {
        private string name;

        [CategoryAttribute("Data"),
        DisplayName("Name"),
        DescriptionAttribute("Select the objective function name."),
        TypeConverter(typeof(VariableNamesTypeConverter))]
        public string Name
        {
            get
            {
                if (name == null)
                {
                    if (Driver_GlobalVars.listOfVariableNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfVariableNames.Sort();
                        name = Driver_GlobalVars.listOfVariableNames[0];
                    }
                    else return "";
                }
                return name;
            }
            set
            {
                name = value;
            }
        }
    }


}
