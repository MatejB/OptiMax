﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework.GradientConstrained;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    public class AMQDriver : Driver
    {
        // Variables                                                                                                                
        private int recCount;
        private string objectiveVariableName;
        private BindingList<ConstraintVariableName> constraintNamesBl;
        private int maxNumOfSteps;

        // Properties                                                                                                                
        [CategoryAttribute("Control parameters"),
        DisplayName("Max number of increments"),
        DescriptionAttribute("The number of optimization increments after which the optimization exits.")]
        public int MaxNumSteps
        {
            get { return maxNumOfSteps; }
            set
            {
                if (value <= 0) throw new Exception("Max number of increments must be larger than 0.");
                else maxNumOfSteps = value;
            }
        }

        [CategoryAttribute("Control parameters"),
        DisplayName("Relative derivative delta"),
        DescriptionAttribute("The relative width of the interval used for the computation of the derivative.")]
        public double DerivativeDelta { get; set; }

        [CategoryAttribute("Objective variable"),
        DisplayName("Objective variable"),
        DescriptionAttribute("Select the variable for the objective function."),
        TypeConverter(typeof(VariableNamesTypeConverter))]
        public string ObjectiveVariableName 
        {
            get
            {
                if (objectiveVariableName == null)
                {
                    if (Driver_GlobalVars.listOfVariableNames.Count > 0)
                    {
                        //Sort the list before displaying it
                        Driver_GlobalVars.listOfVariableNames.Sort();
                        objectiveVariableName = Driver_GlobalVars.listOfVariableNames[0];
                        UpdateResultItemNames();
                    }
                    else return "";
                }
                return objectiveVariableName;
            }
            set
            {
                objectiveVariableName = value;
                UpdateResultItemNames();
            }
        }

        [CategoryAttribute("Constraints"),
        DisplayName("List of constraints"),
        DescriptionAttribute("Select the variables for the list of constraints.")]
        public BindingList<ConstraintVariableName> ConstraintNames
        {
            get
            {
                UpdateResultItemNames(); // the set property is not called by property grid
                return constraintNamesBl;
            }
            set
            {
                constraintNamesBl = value;
                UpdateResultItemNames();
            }
        }

        [Browsable(false)]
        public string[] ConstraintVariableNames 
        { 
            get
            {
                if (constraintNamesBl != null) return constraintNamesBl.Select(c => c.Name).ToArray();
                else return null;
            }
        }


        // Constructors                                                                                                             
        public AMQDriver(string name, string description)
            : this(name, description, null, null, 10, 0.001)
        {
        }
        public AMQDriver(string name, string description, string objectiveVariableName, string[] constraintVariableNames, int maxNumSteps, double derivativeDelta)
            : base(name, description)
        {
            this.objectiveVariableName = objectiveVariableName;
            
            constraintNamesBl = new BindingList<ConstraintVariableName>();
            if (constraintVariableNames != null)
            {
                foreach (var constrName in constraintVariableNames)
                    constraintNamesBl.Add(new ConstraintVariableName() { Name = constrName });
            }

            UpdateResultItemNames();

            MaxNumSteps = maxNumSteps;
            DerivativeDelta = derivativeDelta;

            constraintNamesBl = new BindingList<ConstraintVariableName>();
        }
        

        // Methods                                                                                                                  
        public override void RunEvaluation()
        {
            bool err = CorrectlyDefined(baseFrameworkItemCollection);

            double[] startParameters = new double[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
			{
                startParameters[i] = (parameters[i].Value - parameters[i].Min) / (parameters[i].Max - parameters[i].Min);
			}
            
            numOfPlaces = (MaxNumSteps + 1).ToString().Length;

            string solutionID;
            jobStatusData = new JobStatusData[parameters.Length * 2 + 1]; // only for the central difference derivation
            for (int i = 0; i < jobStatusData.Length; i++)
            {
                solutionID = (i + 1).ToString().PadLeft(numOfPlaces, '0');
                jobStatusData[i] = new JobStatusData() { ID = solutionID, jobStatus = JobStatus.InQueue, startTime = new DateTime(0) };
            }

            double[][] designVarLowUpVal = new double[parameters.Length][];
            for (int i = 0; i < parameters.Length; i++)
			{
                designVarLowUpVal[i] = new double[3];
                designVarLowUpVal[i][0] = 0;
                designVarLowUpVal[i][1] = 1;
                designVarLowUpVal[i][2] = startParameters[i];
			}

            int numConstr = 0;
            if (constraintNamesBl != null) numConstr = constraintNamesBl.Count;

            INumericalDerivative nd = new CentralDifference(DerivativeDelta);

            recCount = 0;
            Optimizer opt = new Optimizer(numConstr, designVarLowUpVal, ObjectiveFunction, nd, MaxNumSteps);
            opt.ReportTheBestSolutions = OnRecordTheBestSolutions;
            opt.ReportStatusData = OnReportStatusData;
            opt.ReportCoverganceData = OnReportConverganceData;
            string result = opt.Start();
            opt = null;
        }
        private double[][] ObjectiveFunction(double[][] allSamples)
        {
            // reset the job status
            for (int i = 0; i < jobStatusData.Length; i++)
                jobStatusData[i].jobStatus = JobStatus.InQueue;

            int numSam = allSamples.GetLength(0);
            int numPar = allSamples[0].Length;

            string[][] resultsData;
            double[][] results = new double[numSam][];

            // evaluate all samples
            resultsData = EvaluateAllSamples(allSamples);
            for (int i = 0; i < numSam; i++)
            {
                results[i] = new double[resultsData[i].Length];
                for (int j = 0; j < results[i].Length; j++)
                {
                    results[i][j] = double.Parse(resultsData[i][j], OptiMaxFramework.Globals.nfi);
                }
            }

            // report data
            //OnReportData("Evaluation " + (count + 1).ToString() + " completed.");

            return results;
        }
        private void OnRecordTheBestSolutions(double[] values)
        {
            RecordSingleValueResult((++recCount).ToString(), history[values].ItemValues, false);
        }
        private void UpdateResultItemNames()
        {
            int N = 1;
            if (constraintNamesBl != null) N += constraintNamesBl.Count;

            this.resultItemNames = new string[N];
            int count = 0;
           
            resultItemNames[count++] = objectiveVariableName;

            foreach (var conVar in constraintNamesBl)
            {
                resultItemNames[count++] = conVar.Name;
            }
        }

        override public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            if (objectiveVariableName == null)
                throw new Exception("The AMQ driver objective variable must be defined.");

            string[] uniqueNames = resultItemNames.Distinct().ToArray();
            if (resultItemNames.Length != uniqueNames.Length)
                throw new Exception("At least one objective function or one constraint is used twice.");

            foreach (var name in uniqueNames)
            {
                if (!itemCollection.ContainsKey(name))
                {
                    System.Windows.Forms.MessageBox.Show("The item '" + name + "' used in AMQ driver is missing.", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                    return false;
                }
            }

            return true;
        }

    }
}
