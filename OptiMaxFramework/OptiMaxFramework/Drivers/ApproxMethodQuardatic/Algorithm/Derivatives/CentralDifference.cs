﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework.GradientConstrained;

namespace OptiMaxFramework.GradientConstrained
{
    [Serializable]
    public class CentralDifference : INumericalDerivative
    {
        // Variables                                                                                          
        

        // Properties                                                                                         
        public double DerivativeDelta { get; set; }

        // Constructors                                                                                       
        public CentralDifference(double derivativeDelta)
        {
            DerivativeDelta = derivativeDelta;
        }

        // Event handling                                                                                     


        // Methods                                                                                            
        public double[][] PrepareSamples(double[] designVars)
        {
            double[][] allSamples = new double[1 + 2 * designVars.Length][];
            allSamples[0] = (double[])designVars.Clone();

            int count = 1;
            for (int i = 0; i < designVars.Length; i++)
            {
                // minus 
                allSamples[count] = (double[])designVars.Clone();
                allSamples[count][i] -= DerivativeDelta;
                count++;

                // plus
                allSamples[count] = (double[])designVars.Clone();
                allSamples[count][i] += DerivativeDelta;
                count++;
            }

            return allSamples;
        }

        public void ComputeDerrivatives(double[][] allResults, out double[] funValues, out double[][] derivatives)
        {
            int index;
            double fMinus, fPlus;

            int numSamples = allResults.Length;
            int numFunctions = allResults[0].Length;
            int numDesignVars = (numSamples - 1) / 2;
            
            // values
            funValues = new double[numFunctions];

            for (int i = 0; i < numFunctions; i++)
            {
                // first index is the sample index, second index is the function index
                // the sample index 0 is the basis sample
                index = 2 * i + 1;
                funValues[i] = allResults[0][i];
            }

            // derivatives
            derivatives = new double[numFunctions][];
            for (int i = 0; i < numFunctions; i++)
            {
                derivatives[i] = new double[numDesignVars];
                for (int j = 0; j < numDesignVars; j++)
                {
                    index = 2 * j + 1;
                    fMinus = allResults[index][i];
                    fPlus = allResults[index + 1][i];

                    derivatives[i][j] = (fPlus - fMinus) / (2 * DerivativeDelta);
                }
            }
        }
    }
}
