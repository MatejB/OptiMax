﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework.GradientConstrained
{
    public interface INumericalDerivative
    {
        // Methods                                                                                            
        double[][] PrepareSamples(double[] designVars);

        void ComputeDerrivatives(double[][] allResults, out double[] funValues, out double[][] derivatives);
    }
}
