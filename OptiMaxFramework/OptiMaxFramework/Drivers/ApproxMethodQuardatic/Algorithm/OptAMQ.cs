using System;
using System.Globalization;

namespace OptiMaxFramework.GradientConstrained
{
    #region KLASA OptAMQ
    /// <summary>
    /// Gradientni optimizer OptAMQ
    /// Marko Kegl, Maj 2006
    /// </summary>
    public class OptAMQ
    {
        #region PARAMETRI IN SPREMENLJIVKE
        private const string Logo = "[OptAMQ]";
        
        private const double PosInf = 1e30;   // + Infinity
        private const double NegInf = -1e30;  // - Infinity
        private const double Tiny = 1e-30;    // Tiny

        private const double MaxStepSize = 0.5;  // Maksimalna dol�ina koraka
        private const double MaxLagNorm = 10.0;  // Maksimalna pri�akovana norma Lagrangevih multiplikatorjev

        private double TolB = 1.0e-4;         // Konvergen�ni epsilon pomo�nega designa
        private double TolH = 1.0e-6;        // Konvergen�ni epsilon pogojev
        private double MoveL = 1.0e-1;        // Move limit
        private double RelaxL = 5.0e-1;        // Relax limit
        private double DumpAlfa = 0.0;       // Dump faktor za racunanje alfa
        private double MinAlfa  = 0.03125;   // Numeri�ne meje aproksimacijskih dodatkov
        private double IniAlfa  = 1.00;      // Za�etna vrednost aproksimacijskih dodatkov za NF
        private double CosBetaLim = 0.85;    // Kosinus mejnega kot Beta za regulacijo StepSize
        private int CurInxF = 0;              // Indeks ukrivljenosti namenske funkcije [0, +10]
        private int CurInxH = 0;              // Indeks ukrivljenosti pogojev [0, -10]

        private double QConFak = 2;           // Faktor za funkcijo mappinga pogoja, [0,10]
        private double QConShift = -0.05;      // Zamik za funkcijo mappinga pogoja, [-1,0]
        private double BckSteTol = 0.05;      // Toleranca za BackStep

        private bool Finished;          // Flag finished
        private string OptMsg;          // Sporo�ilo pri vra�anju iz rutine

        private int nH;            // �tevilo omejitvenih pogojev
        private int nB;            // �tevilo vseh projektnih spremenljivk
        private int OptItr;        // Globalni �tevec iteracij

        private DObj B;            // Spodnje meje projektnih spremenljivk
        private DObj BLow;         // Spodnje meje projektnih spremenljivk
        private DObj BUpp;         // Zgornje meje projektnih spremenljivk
        private BObj BAct;         // Aktivnost projektnih spremenljivk
        private double F0;           // Konstantna vrednost namenske funkcije
        private DObj DFDB;         // Projektni odvodi namenske funkcije
        private DObj H0;           // Konstantna vrednost omejitvenih funkcij
        private DObj DHDB;         // Projektni odvodi omejitvenih funkcij
        private DObj BSaved;       // Shranjen projekt
        private DObj BLast;        // Shranjen projekt
        private double F0Last;        // 
        private DObj DFDBSaved;    // Shranjeni odvodi
        private DObj DFDBLast;     // Shranjeni odvodi
        private DObj H0Last;        // 
        private DObj DHDBSaved;    // Shranjeni odvodi
        private DObj DHDBLast;     // Shranjeni odvodi
        private DObj Lag;          // Lagrangevi multiplikatorji
        private DObj AlfaF;        // Aproksimacijski faktorji
        private DObj AlfaH;        // Aproksimacijski faktorji

        private double LamFak;
        private double FakAlfaF;
        private double FakAlfaH;

        private CultureInfo cultureInf = new System.Globalization.CultureInfo("en-US");
        #endregion

        public OptAMQ()
        {
        }

        public void Ini(int SteF, int SteB)
        {
            // Kopiranje
            nH = SteF;
            nB = SteB;

            // Kontrola
            if (nH < 0 | nB < 1) throw new Exception(Logo + " Bad initialization data.");

            // Alokacija
            B = new DObj(1,nB);
            BLow = new DObj(1, nB);
            BUpp = new DObj(1, nB);
            BAct = new BObj(1,nB);

            DFDB = new DObj(1, nB);
            H0 = new DObj(1, nH);
            DHDB = new DObj(1, nH, 1,nB);

            BSaved = new DObj(1, nB);
            BLast = new DObj(1, nB);
            DFDBSaved = new DObj(1, nB);
            DFDBLast = new DObj(1, nB);
            H0Last = new DObj(1, nH);
            DHDBSaved = new DObj(1, nH, 1, nB);
            DHDBLast = new DObj(1, nH, 1, nB);

            Lag = new DObj(1, nH);
            AlfaF = new DObj(1, nB);
            AlfaH = new DObj(1, nH, 1, nB);

            // Default
            BLow.Set(NegInf);
            BUpp.Set(PosInf);
            BAct.Set(true);

            // Nastavitve
            OptItr = 0;
            Lag.Set(0.0);
        }

        #region VNOS IZNOS PODATKOV IN POIZVEDBE
        public double TolerB
        {
            get{ return TolB; }
            set { TolB = Math.Min(Math.Max(value, 1e-10), 0.1); }
        }
        public double TolerH
        {
            get { return TolH; }
            set { TolH = Math.Min(Math.Max(value, 1e-10), 0.1); }
        }
        public double MoveLim
        {
            get { return MoveL; }
            set { MoveL = Math.Min(Math.Max(value, 1e-10), 1e+10); }
        }
        public double RelaxLim
        {
            get { return RelaxL; }
            set { RelaxL = Math.Min(Math.Max(value, 1e-10), 1e+10); }
        }
        public double DumFakAlfa
        {
            get { return DumpAlfa; }
            set { DumpAlfa = Math.Min(Math.Max(value, 0.0), 1.0); }
        }
        public double MinimalAlfa
        {
            get { return MinAlfa; }
            set { MinAlfa = Math.Max(value, 1e-5); }
        }
        public void SetCurInx(int FCInx, int HCInx)
        {
            CurInxF = Math.Min(Math.Max(FCInx,0),10);
            CurInxH = Math.Min(Math.Max(HCInx, -10), 0); ;
        }

        public double QuaConFak
        {
            get { return QConFak; }
            set { QConFak = Math.Min(Math.Max(value, 0), 10); }
        }
        public double QuaConShift
        {
            get { return QConShift; }
            set { QConShift = Math.Max(Math.Min(value, 0), -0.2); }
        }
        public double BackStepTol
        {
            get { return BckSteTol; }
            set { BckSteTol = Math.Min(Math.Max(value, 0), 0.5); }
        }

        public void SetLowLim(int iB, double BL)
        {
            if (iB < 1 | iB > nB) throw new Exception(Logo + "Bad design variable index.");
            BLow[iB] = BL;
        }

        public void SetUppLim(int iB, double BU)
        {
            if (iB < 1 | iB > nB) throw new Exception(Logo + "Bad design variable index.");
            BUpp[iB] = BU;
        }

        public void SetAct(int iB, bool BA)
        {
            if (iB < 1 | iB > nB) throw new Exception(Logo + "Bad design variable index.");
            BAct[iB] = BA;
        }
        public void SetB(int iB, double BV)
        {
            if (iB < 1 | iB > nB) throw new Exception(Logo + "Bad design variable index.");
            B[iB] = BV;
        }

        public double GetB(int iB)
        {
            if (iB < 1 | iB > nB) throw new Exception(Logo + "Bad design variable index.");
            return B[iB];
        }

        public void ZerFun()
        {
            F0 = 0.0;
            DFDB.Set(0.0);
            H0.Set(0.0);
            DHDB.Set(0.0);
        }

        public void SetFun(int iH, int iB, double Hi)
        {
            if (iH < 0 | iH > nH+1 | iB < 0 | iB > nB) throw new Exception(Logo + "Bad function or design variable index.");
            if (iB == 0)
            {
                if (iH == 0)
                {
                    F0 = Hi;
                }
                else
                {
                    H0[iH] = Hi;
                }
            }
            else
            {
                if (iH == 0)
                {
                    DFDB[iB] = Hi;
                }
                else
                {
                    DHDB[iH, iB] = Hi;
                }
            }
        }

        public void AddFun(int iH, int iB, double Hi)
        {
            if (iH < 0 | iH > nH + 1 | iB < 0 | iB > nB) throw new Exception(Logo + "Bad function or design variable index.");
            if (iB == 0)
            {
                if (iH == 0)
                {
                    F0 += Hi;
                }
                else
                {
                    H0[iH] += Hi;
                }
            }
            else
            {
                if (iH == 0)
                {
                    DFDB[iB] += Hi;
                }
                else
                {
                    DHDB[iH, iB] += Hi;
                }
            }
        }

        public bool Fin()
        {
            return Finished;
        }

        public string Msg()
        {
            return OptMsg;
        }

        #endregion

        #region OPTIMIZER
        public void Opt(bool enableBackStep)
        {
            // Delovne spremenljivke
            DObj Y = new DObj(1,nB);
            DObj Y0 = new DObj(1,nB);
            DObj Y1 = new DObj(1,nB);
            DObj YLow = new DObj(1,nB);
            DObj YUpp = new DObj(1,nB);
            DObj G0 = new DObj(1,nH);
            DObj G1 = new DObj(1,nH);
            double Ap, An, NorF, NorH, NorLag, NorG1, CosBeta, StepSize, Buf;
            int ZunIter, Iter;
            bool PraznaDom, Konver, GradNic, KonverB, Relax, NeDopustno, MoveLimit;
            double QLast, Q, QDLast, QD, tao, b, c, d, det, t1, t2;

            // Stikalo prazne domene
            PraznaDom = !H0.LTTol(TolH);

            // Kvaliteta in eventualni backstep
            if (OptItr == 0)
            {
                QLast = 0;
                Q = Kvaliteta(F0, H0);
                QDLast = 0;
                QD = 0;
            }
            else 
            {
                QLast = Kvaliteta(F0Last, H0Last);
                Q = Kvaliteta(F0, H0);

                QDLast = KvalitetaDer(DFDBLast, H0Last, DHDBLast, BLast, B);
                QD = KvalitetaDer(DFDB, H0, DHDB, BLast, B);

                // Izracun minimuma kubicne interpolacije na (QLast,QDLast, Q,QD)
                if (enableBackStep)
                {
                    b = QDLast;
                    c = -3 * QLast + 3 * Q - 2 * QDLast - QD;
                    d = 2 * QLast - 2 * Q + QDLast + QD;
                    if (Math.Abs(d) > 1e-15)
                    {

                        det = Math.Sqrt(c * c - 3 * b * d);

                        t1 = (-c - det) / (3 * d);
                        t2 = (-c + det) / (3 * d);

                        tao = 0;
                        if ((2 * c + 6 * d * t1) > 0)
                        {
                            tao = t1;
                        }
                        else if ((2 * c + 6 * d * t2) > 0)
                        {
                            tao = t2;
                        }

                        // Test ce je lokacija minimuma zanimiva
                        if (tao > 0 & tao < (1 - BckSteTol))
                        {
                            for (int i = 1; i <= nB; i++) B[i] = tao * B[i] + (1 - tao) * BLast[i];

                            // Sporo�ila in status
                            OptMsg += "[Q=" + DStr(Q) + "] ++BackStep=" + DStr(1 - tao);
                            if (tao < 0.01) OptMsg += "; IncObjFunCur.";

                            Finished = false;
                            return;
                        }
                    }
                }
            }

            // Lambda faktor
            if (OptItr == 0) LamFak = 1;
            if (nH > 0)
            {
                NorF = DFDB.AbsSum() / nB;
                NorH = DHDB.AbsSum() / (nB*nH);
                if (NorF > Tiny & NorH > Tiny) LamFak = (LamFak + (NorF / NorH)) / 2;
            }

            // Pomozne optimizacijske spremenljivke
            YLow.CopyFromDif(BLow, B);
            YUpp.CopyFromDif(BUpp, B);
            Y.Set(0.0);
            Y0.Set(0.0);
            Y1.Set(0.0);

            // Priprava
            RacunajAlfa();
            Relax = false;
            Konver = false;
            NeDopustno = false;
            Iter = 1;

            // Za�etna relaxacija
            Buf = H0.MaxVal();
            if (Buf > RelaxL)
            {
                Buf = RelaxL / Buf;
                H0.Mul(Buf);
                Relax = true;
            }

            // Zunanja zanka
            for (ZunIter = 1; ZunIter <= 10; ZunIter++)
            {
                // Priprava na prvo iteracijo
                NorLag = Lag.MaxVal();
                if (NorLag > MaxLagNorm) Lag.Div(NorLag);
                StepSize = MaxStepSize / 10;
                G0.Set(0.0);

                // Iteracijska zanka
                int MaxIter = (nH+nB)*1000;
                for (Iter = 1; Iter <= MaxIter; Iter++)
                { 
                    // Pomozni design
                    for (int j = 1; j <= nB; j++)
                    {
                        if (BAct[j])
                        {
                            
                            Ap = 0; for (int i = 1; i <= nH; i++) Ap += Lag[i] * AlfaH[i, j];
                            Ap = AlfaF[j] + LamFak*Ap;
                            An = 0; for (int i = 1; i <= nH; i++) An += Lag[i] * DHDB[i, j];
                            An = DFDB[j] + LamFak * An;
                            Y1[j] = -An / Ap / 2;
                            Y[j] = Math.Min(Math.Max(Y1[j], YLow[j]), YUpp[j]);
                        }
                    }

                    // Gradient
                    for (int i = 1; i <= nH; i++)
                    {
                        Buf = 0; for (int j = 1; j <= nB; j++) Buf += (DHDB[i,j] + AlfaH[i,j]*Y[j]) * Y[j];
                        G1[i] = H0[i] + Buf;
                    }

                    // Stikala stanja in smer popravkov
                    NeDopustno = !G1.LTTol(TolH);
                    if (PraznaDom) PraznaDom = NeDopustno;

                    for (int i = 1; i <= nH; i++) if (Lag[i] == 0 & G1[i] < 0) G1[i] = 0;

                    Buf = TolH / 10;
                    GradNic = G1.LTTol(Buf);
                    Buf = TolB / 10;
                    KonverB = (Iter > 1) & DObj.AbsDifLTTol(Y1, Y0, Buf);

                    // Test konvergence
                    Konver = (GradNic & KonverB) | (PraznaDom & KonverB);
                    if (Konver) break;

                    // Normirana smer
                    NorG1 = G1.Nor();
                    if (NorG1 > 0) G1.Div(NorG1);

                    // Dolzina koraka
                    CosBeta = DObj.MulSum(G0, G1);
                    StepSize = Math.Min(MaxStepSize, StepSize * Math.Pow(2.0, (CosBeta - CosBetaLim)));

                    // Popravljeni multiplikatorji
                    Lag.AddPro(G1, StepSize);
                    Lag.LimitLow(0.0);

                    // Shrani stare
                    Y0.CopyFrom(Y1);
                    G0.CopyFrom(G1);             
                }
                if (Konver) break;

                // Korekcija vhodnih prekoracitev
                for (int i = 1; i <= nH; i++) if (H0[i] > 0) H0[i] /= 2;
                Relax = true;
            }

            // Move limit
            MoveLimit = false;
            Buf = Y.AbsMaxVal();
            if (Buf > MoveL)
            {
                Buf = MoveL / Buf;
                Y.Mul(Buf);
                MoveLimit = true;
            }

            // Popravljene projektne spremenljivke
            B.Add(Y);

            // Sporo�ila in status
            OptItr++;
            ZunIter--;
            OptMsg = "[";
            OptMsg += "Q=" + DStr(Q); // "QL: " + DStr(QLast) + " QDL: " + DStr(QDLast) + " Q: " + DStr(Q) + " QD: " + DStr(QD);
            OptMsg += "; Itr=" + OptItr.ToString() + "/" + ZunIter.ToString() + "/" + Iter.ToString();
            OptMsg += "]";

            Finished = Y.AbsLTTol(TolB);

            if (Finished) OptMsg += " Finished.";
            if (LamFak < 0.01 | LamFak > 100.0) OptMsg += " UnbaGrad.";
            if (Relax) OptMsg += " Relax.";
            if (PraznaDom) OptMsg += " EmptDom.";
            if (!Konver) OptMsg += " NoCon.";
            if (NeDopustno) OptMsg += " Infeas.";
            if (MoveLimit) OptMsg += " MoveLim.";
        }

        private void RacunajAlfa()
        {
            // Nove alfe
            if (OptItr == 0)
            {
                FakAlfaF = Math.Pow(4.0, Convert.ToDouble(CurInxF));
                FakAlfaH = 1.0 / Math.Pow(2.0, Convert.ToDouble(-CurInxH));

                double inianf = IniAlfa * FakAlfaF;
                AlfaF.Set(inianf);
                AlfaH.Set(0.0);

                BSaved.CopyFrom(B);
                DFDBSaved.CopyFrom(DFDB);
                DHDBSaved.CopyFrom(DHDB);
            }
            else
            {
                AlfaF.Div(FakAlfaF);
                AlfaH.Div(FakAlfaH);

                double BiDelta, TempAlfa;
                for (int j = 1; j <= nB; j++)
                {
                    if (Math.Abs(BLast[j] - B[j]) > TolB)
                    {
                        BSaved[j] = BLast[j];
                        DFDBSaved[j] = DFDBLast[j];
                        for (int i = 1; i <= nH; i++) DHDBSaved[i, j] = DHDBLast[i, j];
                    }

                    BiDelta = BSaved[j] - B[j];

                    if (Math.Abs(BiDelta) > TolB)
                    {
                        TempAlfa = (DFDBSaved[j] - DFDB[j]) / (2 * BiDelta);
                        AlfaF[j] = Math.Max(DumpAlfa * AlfaF[j] + (1 - DumpAlfa) * TempAlfa, MinAlfa);
                        for (int i = 1; i <= nH; i++)
                        {
                            TempAlfa = (DHDBSaved[i, j] - DHDB[i, j]) / (2 * BiDelta);
                            AlfaH[i, j] = Math.Max(DumpAlfa*AlfaH[i, j] + (1-DumpAlfa)*TempAlfa, 0.0);
                        }
                    }
                }

                FakAlfaF = Math.Pow(4.0, Convert.ToDouble(CurInxF));
                FakAlfaH = 1.0 / Math.Pow(2.0, Convert.ToDouble(-CurInxH));
                AlfaF.Mul(FakAlfaF);
                AlfaH.Mul(FakAlfaH);
            }

            // Shrani podatke
            BLast.CopyFrom(B);
            F0Last = F0;
            DFDBLast.CopyFrom(DFDB);
            H0Last.CopyFrom(H0);
            DHDBLast.CopyFrom(DHDB);
        }

        private double MapConVio(double a, double b, double x)
        {
            double f;

            if (x <= b)
            {
                f = 0;
            }
            else
            {
                f = a * (x - b);
            }

            return f;
        }

        private double MapConVioDer(double a, double b, double x)
        {
            double fd;

            if (x <= b)
            {
                fd = 0;
            }
            else
            {
                fd = a;
            }

            return fd;
        }

        private double Kvaliteta(double f, DObj h)
        {
            double q, g;

            q = f;
            for (int i = 1; i <= nH; i++)
            {
                g = MapConVio(QConFak, QConShift, h[i]);
                q += g * g;
            }

            return q;
        }

        private double KvalitetaDer(DObj fd, DObj h, DObj hd, DObj b0, DObj b1)
        {
            double qd, g, gd, gb;

            qd = 0;
            for (int j = 1; j <= nB; j++) qd += fd[j] * (b1[j] - b0[j]);
            
            for (int i = 1; i <= nH; i++)
            {
                g = MapConVio(QConFak, QConShift, h[i]);
                gd = MapConVioDer(QConFak, QConShift, h[i]);

                gb = 0;
                for (int j = 1; j <= nB; j++) gb += hd[i, j] * (b1[j] - b0[j]);

                qd += 2 * g * gd * gb;
            }
            return qd;
        }

        private string DStr(double x)
        {
            return x.ToString("0.000", cultureInf.NumberFormat);
        }


    #endregion
    }
    #endregion

    #region KLASA DObj
    public class DObj
    {
        private const double Huge = 1e300;

        //private int rank;
        private int low0, upp0, siz0;
        private int low1, upp1, siz1;
        private double[] a;

        #region KONSTRUKTORJI
        public DObj(int lw0, int up0)
        {
            //rank = 1;
            low0 = lw0;
            upp0 = up0;
            siz0 = up0 - lw0 + 1;
            a = new double[siz0];
        }

        public DObj(int lw0, int up0, int lw1, int up1)
        {
            //rank = 2;
            low0 = lw0;
            upp0 = up0;
            siz0 = up0 - lw0 + 1;
            low1 = lw1;
            upp1 = up1;
            siz1 = up1 - lw1 + 1;
            a = new double[siz0 * siz1];
        }
        #endregion

        #region AKSESORJI TER INDEKSERJI IN ADRESE
        public double this[int i]
        {
            get { return a[Adr(i)]; }
            set { a[Adr(i)] = value; }
        }

        public double this[int i, int j]
        {
            get { return a[Adr(i, j)]; }
            set { a[Adr(i, j)] = value; }
        }

        private int Adr(int i)
        {
            return i - low0;
        }

        private int Adr(int i, int j)
        {
            return (i - low0) * siz1 + j - low1;
        }

        private int Length
        {
            get { return a.Length; }
        }

        private double[] Buf
        {
            get { return a; }
        }
        #endregion

        #region MANIPULACIJA
        public void Set(double v)
        {
            for (int i = 0; i < a.Length; i++) a[i] = v;
        }

        public void CopyFrom(DObj o1)
        {
            for (int i = 0; i < a.Length; i++) a[i] = o1.Buf[i]; ;
        }

        public void CopyFromDif(DObj o1, DObj o2)
        {
            for (int i = 0; i < a.Length; i++) a[i] = (o1.Buf[i] - o2.Buf[i]);
        }

        public void LimitLow(double v)
        {
            for (int i = 0; i < a.Length; i++) if (a[i] < v) a[i] = v;
        }

        public void Add(DObj o1)
        {
            for (int i = 0; i < a.Length; i++) a[i] += o1.Buf[i];
        }

        public void AddPro(DObj o1, double v)
        {
            for (int i = 0; i < a.Length; i++) a[i] += o1.Buf[i] * v;
        }

        public void Mul(double v)
        {
            for (int i = 0; i < a.Length; i++) a[i] *= v;
        }

        public void Div(double v)
        {
            for (int i = 0; i < a.Length; i++) a[i] /= v;
        }

        #endregion

        #region POIZVEDBE
        public double Sum()
        {
            double s = 0;
            for (int i = 0; i < a.Length; i++) s += a[i];
            return s;
        }

        public double AbsSum()
        {
            double s = 0;
            for (int i = 0; i < a.Length; i++) s += Math.Abs(a[i]);
            return s;
        }

        public double Nor()
        {
            double s = 0;
            for (int i = 0; i < a.Length; i++) s += a[i]*a[i];
            return Math.Sqrt(s);
        }

        public double MaxVal()
        {
            double s = -Huge;
            for (int i = 0; i < a.Length; i++) if (a[i] > s) s = a[i];
            return s;
        }

        public double AbsMaxVal()
        {
            double s = 0;
            for (int i = 0; i < a.Length; i++) if (Math.Abs(a[i]) > s) s = Math.Abs(a[i]);
            return s;
        }

        public bool LTTol(double tol)
        {
            for (int i = 0; i < a.Length; i++) if (a[i] > tol) return false;
            return true;
        }

        public bool AbsLTTol(double tol)
        {
            for (int i = 0; i < a.Length; i++) if (Math.Abs(a[i]) > tol) return false;
            return true;
        }

        #endregion

        #region STATICNE PROCEDURE
        public static bool AbsDifLTTol(DObj o1, DObj o2, double tol)
        {
            for (int i = 0; i < o2.Length; i++) if (Math.Abs(o2.Buf[i] - o1.Buf[i]) > tol) return false;
            return true;
        }

        public static double MulSum(DObj o1, DObj o2)
        {
            double s = 0;
            for (int i = 0; i < o2.Length; i++) s += (o1.Buf[i] * o2.Buf[i]);
            return s;
        }
        #endregion
    }
    #endregion

    #region KLASA BObj
    public class BObj
    {
        //private int rank;
        private int low0, upp0, siz0;
        private int low1, upp1, siz1;
        private bool[] a;

        #region KONSTRUKTORJI
        public BObj(int lw0, int up0)
        {
            //rank = 1;
            low0 = lw0;
            upp0 = up0;
            siz0 = up0 - lw0 + 1;
            a = new bool[siz0];
        }

        public BObj(int lw0, int up0, int lw1, int up1)
        {
            //rank = 2;
            low0 = lw0;
            upp0 = up0;
            siz0 = up0 - lw0 + 1;
            low1 = lw1;
            upp1 = up1;
            siz1 = up1 - lw1 + 1;
            a = new bool[siz0 * siz1];
        }
        #endregion

        #region INDEKSERJI IN ADRESE
        public bool this[int i]
        {
            get { return a[Adr(i)]; }
            set { a[Adr(i)] = value; }
        }

        public bool this[int i, int j]
        {
            get { return a[Adr(i, j)]; }
            set { a[Adr(i, j)] = value; }
        }
        private int Adr(int i)
        {
            return i - low0;
        }

        private int Adr(int i, int j)
        {
            return (i - low0) * siz1 + j - low1;
        }
        private int Length
        {
            get { return a.Length; }
        }

        private bool[] Buf
        {
            get { return a; }
        }
        #endregion

        #region MANIPULACIJA
        public void Set(bool v)
        {
            for (int i = 0; i < a.Length; i++) a[i] = v;
        }
        #endregion
    }
    #endregion
}
