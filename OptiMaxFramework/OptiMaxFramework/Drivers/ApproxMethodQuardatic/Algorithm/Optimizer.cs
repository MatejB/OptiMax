﻿using System;

namespace OptiMaxFramework.GradientConstrained
{
    // Delegates                                                                                          
    public delegate void ReportDataDelegate(string data);

    public class Optimizer
    { 
        // Variables                                                                                          
        private int numConstr;
        private int numDesignVar;
        private int maxNumSteps;
        private double[] designVar;
        private double maxDB;
        private OptAMQ optAMQ;
        private Func<double[][], double[][]> ObjFunction;
        private INumericalDerivative derivative;
        private double bestValue;
        public Action<string> ReportStatusData;
        public Action<double[]> ReportCoverganceData;
        public Action<double[]> ReportTheBestSolutions;

        // Properties                                                                                         
        public double MaxDB
        {
            get { return maxDB; }
        }

        public double[] DesignVariables 
        {
            get { return designVar; } 
        }


        // Methods                                                                                            
        public Optimizer(int numConstr, double[][] designVarLowUpVal, Func<double[][], double[][]> ObjFunction, INumericalDerivative derivative, int maxNumSteps)
        {
            this.numConstr = numConstr;
            this.numDesignVar = designVarLowUpVal.Length;
            
            // Optimizer
            optAMQ = new OptAMQ();
            optAMQ.Ini(numConstr, numDesignVar);

            // Settings
            designVar = new double[numDesignVar];
            for (int i = 0; i < numDesignVar; i++)
            {
                optAMQ.SetLowLim(i + 1, designVarLowUpVal[i][0]);
                optAMQ.SetUppLim(i + 1, designVarLowUpVal[i][1]);
                optAMQ.SetAct(i + 1, true);
                designVar[i] = designVarLowUpVal[i][2];
            }

            this.ObjFunction = ObjFunction;
            this.maxNumSteps = maxNumSteps;

            this.derivative = derivative;
        }

        public string PerformStep(double[] objFunValDeriv,  double[,] constrFunValDeriv, double[,] designVarLowUpVal)
        { 
            // Zeroing
            optAMQ.ZerFun();

            // Design
            for (int i = 0; i < designVarLowUpVal.GetUpperBound(0) + 1; i++)
            {
                optAMQ.SetB(i + 1, designVarLowUpVal[i, 2]);
            }

            // Optimization functions
            int iH = 0;
            
            // Objective function                                                                           
            iH = 0;
            optAMQ.AddFun(iH, 0, objFunValDeriv[0]);

            // Derivatives
            for (int i = 1; i < objFunValDeriv.Length; i++)
            {
                optAMQ.AddFun(iH, i, objFunValDeriv[i]);
            }

            if (constrFunValDeriv != null)
            {
                // Constraint functions                                                                    
                for (int i = 0; i < constrFunValDeriv.GetUpperBound(0) + 1; i++)
                {
                    iH++;
                    // Functions
                    optAMQ.AddFun(iH, 0, constrFunValDeriv[i, 0]);

                    // Derivatives
                    for (int j = 1; j < constrFunValDeriv.GetUpperBound(1) + 1; j++)
                    {
                        optAMQ.AddFun(iH, j, constrFunValDeriv[i, j]);
                    }
                }
            }

            // Call optimizer
            optAMQ.Opt(true);

            // New design
            maxDB = 0;
            double db;
            for (int i = 0; i < designVarLowUpVal.GetUpperBound(0) + 1; i++)
            {
                db = optAMQ.GetB(i + 1) - designVarLowUpVal[i, 2];
                if (Math.Abs(db) > Math.Abs(maxDB)) maxDB = db;
                designVarLowUpVal[i, 2] = optAMQ.GetB(i + 1);
            }

            // Return
            return optAMQ.Msg();
        }

        public string Start()
        {
            int stepCount = 0;
            string result = "";
            bestValue = double.MaxValue;

            while (stepCount <= maxNumSteps)
            {
                result = PerformStep();

                if (ReportStatusData != null) ReportStatusData("Increment " + stepCount.ToString() + ", " + result);
                if (result.Contains("Finished")) break;

                ++stepCount;
            }

            return result;
        }

        private string PerformStep()
        {
            // Zeroing
            optAMQ.ZerFun();

            // Design
            for (int i = 0; i < numDesignVar; i++)
            {
                optAMQ.SetB(i + 1, designVar[i]);
            }

            // Optimization functions
            double[] funValues;
            double[][] derivatives;
            ComputeFunctions(designVar, ObjFunction, out funValues, out derivatives);

            for (int i = 0; i < funValues.Length; i++)
            {
                optAMQ.AddFun(i, 0, funValues[i]);
                for (int j = 0; j < derivatives[i].Length; j++)
                {
                    optAMQ.AddFun(i, j + 1, derivatives[i][j]);
                }
            }

            // Call optimizer
            optAMQ.Opt(true);

            // Record the best solution
            ReportTheBestSolutions(designVar);
            
            // Report convergance
            double current = funValues[0];
            if (current < bestValue) bestValue = current;
            ReportCoverganceData(new double[] { bestValue, current });

            // New design
            maxDB = 0;
            double db;
            for (int i = 0; i < numDesignVar; i++)
            {
                db = optAMQ.GetB(i + 1) - designVar[i];
                if (Math.Abs(db) > Math.Abs(maxDB)) maxDB = db;
                designVar[i] = optAMQ.GetB(i + 1);
            }

            // Return
            return optAMQ.Msg();
        }

        private void ComputeFunctions(double[] designVars, Func<double[][], double[][]> objFunction, out double[] funValues, out double[][] derivatives)
        {
            double[][] allSamples = derivative.PrepareSamples(designVars);
            double[][] allResults = objFunction(allSamples);
            derivative.ComputeDerrivatives(allResults, out funValues, out derivatives);
        }

    }
}
