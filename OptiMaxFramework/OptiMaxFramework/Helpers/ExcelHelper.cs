﻿using System;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;

namespace Helpers
{
    //http://www.codeproject.com/Articles/252962/Excel-Automation-With-Clean-Exit-Quit


    public class ExcelHelper
    {
        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        public static bool IsExcelRangeValid(string range)
        {
            string[] cells = range.Split(new char[] { ':' });

            if (cells.Length < 1 || cells.Length > 2) return false;

            for (int i = 0; i < cells.Length; i++)
            {
                if (!IsThisSingleCellRangeValid(cells[i])) return false;
            }

            return true;
        }

        public static bool IsThisArrayCellRangeValid(string range)
        {
            string[] cells = range.Split(new char[] { ':' });

            if (cells.Length != 2) return false;

            int colStart, rowStart, colEnd, rowEnd;

            if (!GetColRowIndexFromRange(cells[0], out rowStart, out colStart)) return false;

            if (!GetColRowIndexFromRange(cells[1], out rowEnd, out colEnd)) return false;

            if (colStart != colEnd && rowStart != rowEnd) return false;

            return true;
        }

        public static bool IsThisSingleCellRangeValid(string range)
        {
            int charId;
            int len = -1;            

            for (int j = 0; j < range.Length; j++)
            {
                charId = (int)range[j];
                if (charId < 65 || charId > 90)
                {
                    len = j;
                    break;
                }
            }

            if (len <= 0 || len > 3) return false;

            string row;
            int rowId;

            row = range.Substring(len);
            if (row.Length <= 0) return false;
            
            bool parse = int.TryParse(row, out rowId);
            if (!parse) return false;

            if (rowId < 1 || rowId > 1048576) return false;

            return true;
        }

        public static bool GetColRowIndexFromRange(string range, out int row, out int col)
        {
            row = col = -1;

            int charId;
            int len = -1;

            for (int j = 0; j < range.Length; j++)
            {
                charId = (int)range[j];
                if (charId < 65 || charId > 90)
                {
                    len = j;
                    break;
                }
            }

            if (len <= 0 || len > 3) return false;

            string colString = range.Substring(0, len);
            col = GetColIndexFromAlphabet(colString);

            string rowStr = range.Substring(len);
            if (rowStr.Length <= 0) return false;

            bool parse = int.TryParse(rowStr, out row);
            if (!parse) return false;

            if (row < 1 || row > 1048576) return false;

            return true;
        }

        public static int GetColIndexFromAlphabet(string colAlphabet)
        {
            int colIndex = 0;
            for (int i = 0; i < colAlphabet.Length; i++)
            {
                colIndex += ((int)colAlphabet[i] - 64) * (int)Math.Pow(26, colAlphabet.Length - 1 - i);
            }
            return colIndex;
        }


        public int ProcessId { get; set; }
        
        private Excel.Application xlAppObj = null;

        private Excel.Workbook xlWorkBookObj = null;

        private Excel.Worksheet xlWorkSheetObj = null;

        private string s_filePath = null;

        private string s_workSheetName = null;

        private int n_workSheetNo = 0;



        public ExcelHelper(string filePath)
        {
            s_filePath = filePath;
            n_workSheetNo = 1;              // "1" Default WorkSheetNo
        }

        public ExcelHelper(string filePath, int workSheetNo)
        {
            s_filePath = filePath;
            n_workSheetNo = workSheetNo;
        }

        public ExcelHelper(string filePath, string workSheetName)
        {
            s_filePath = filePath;
            s_workSheetName = workSheetName;
        }

        public void Open()
        {
            Excel.Workbooks xlWorkbooks = null;
            Excel.Sheets xlSheets = null;

            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                xlAppObj = new Excel.Application();
                xlWorkbooks = xlAppObj.Workbooks;

                int id;
                GetWindowThreadProcessId(xlAppObj.Hwnd, out id);
                ProcessId = id;

                //xlWorkBookObj = xlWorkbooks.Open(s_filePath, 0, false, 5, System.Reflection.Missing.Value, System.Reflection.Missing.Value, false, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true);
                xlWorkBookObj = xlWorkbooks.Open(s_filePath);

                if (n_workSheetNo != 0 && s_workSheetName == null)
                {
                    xlSheets = xlWorkBookObj.Worksheets;
                    xlWorkSheetObj = (Excel.Worksheet)xlSheets.get_Item(n_workSheetNo);
                }

                

                //if (s_workSheetName != null && n_workSheetNo == 0)
                //{
                //    bool b_worksheetNameExist = true;
                //    for (int n_loop = 1; n_loop <= xlWorkBookObj.Worksheets.Count; n_loop++)
                //    {
                //        xlWorkSheetObj = (Excel.Worksheet)xlWorkBookObj.Worksheets.get_Item(n_loop);
                //        if (xlWorkSheetObj.Name == s_workSheetName)
                //        {
                //            b_worksheetNameExist = true;
                //            break;
                //        }
                //        b_worksheetNameExist = false;
                //    }

                //    if (b_worksheetNameExist == true)
                //    {
                //        l_finalProcessIds = this.GetExcelProcessIdsSnapshot();
                //        n_processId = this.GetExcelProcessId(l_initialProcessIds, l_finalProcessIds);
                //    }
                //    else
                //    {
                //        // Exception is on its way........ ting
                //    }
                //}
                //watch.Stop();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
            finally
            {
                if (xlSheets != null)
                { 
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlSheets);
                    xlSheets = null;
                }

                if (xlWorkbooks != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkbooks);
                    xlWorkbooks = null;
                }
            }
        }

        public void Save()
        {
            try
            {
                xlWorkBookObj.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }

        public void Close()
        {
            try
            {
                if (xlWorkBookObj != null)
                {
                    xlWorkBookObj.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }

        // Release                                                                                                       
        private void ReleaseWorkSheetObject()
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(this.xlWorkSheetObj);
                this.xlWorkSheetObj = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Release WorkSheet Object" + ex.ToString(), "Error");
            }
        }

        private void ReleaseWorkBookObject()
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(this.xlWorkBookObj);
                this.xlWorkBookObj = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Release WorkBook Object" + ex.ToString(), "Error");
            }
        }

        private void ReleaseApplicationObject()
        {
            try
            {
                xlAppObj.Application.Quit();
                xlAppObj.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(this.xlAppObj);
                this.xlAppObj = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Release Application Object" + ex.ToString(), "Error");
            }
        }

        public void ReleaseObjectsAndKillExcel()
        {
            Process process = Process.GetProcessById(ProcessId);

            if (xlWorkSheetObj != null)
                this.ReleaseWorkSheetObject();

            if (xlWorkBookObj != null)
                this.ReleaseWorkBookObject();

            if (xlAppObj != null)
                this.ReleaseApplicationObject();

            process.Kill();
            process.WaitForExit(5000);

            // collect 2x: https://www.add-in-express.com/creating-addins-blog/2013/11/05/release-excel-com-objects/
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
       
        // Read                                                                                                         

        public double ReadFromCellDouble(object indexLoc)
        {
            double cellValue = 0;
            try
            {
                cellValue = Convert.ToDouble(this.ReadFromCell(indexLoc));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }

            return cellValue;
        }

        public string ReadFromCellString(object indexLoc)
        {
            string cellValue = "";
            try
            {
                cellValue = Convert.ToString(this.ReadFromCell(indexLoc));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }

            return cellValue;
        }

        private object ReadFromCell(object indexLoc)
        {
            Excel.Range xlRange = null;

            try
            {
                xlRange = xlWorkSheetObj.get_Range(indexLoc);
                return xlRange.Value2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
                return null;
            }
            finally
            {
                if (xlRange != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlRange);
                    xlRange = null;
                }
            }
        }

        public object[,] ReadFromCells(object startIndexLoc, object endIndexLoc)
        {
            Excel.Range xlRange = null;
            try
            {
                xlRange = xlWorkSheetObj.get_Range(startIndexLoc, endIndexLoc);
                return (object[,])xlRange.Value2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
                return null;
            }
            finally
            {
                if (xlRange != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlRange);
                    xlRange = null;
                }
            }
        }


        // Write                                                                                                         

        public void WriteToCell(string range, string cellValue)
        {
            Excel.Range xlRange = null;
            try
            {
                xlRange = xlWorkSheetObj.get_Range(range);
                xlRange.Value = cellValue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
            finally
            {
                if (xlRange != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlRange);
                    xlRange = null;
                }
            }
        }

        public void WriteToCells(object startIndexLoc, object endIndexLoc, double[,] cellValues)
        {
            Excel.Range xlRange = null;

            try
            {
                xlRange = xlWorkSheetObj.get_Range(startIndexLoc, endIndexLoc);
                xlRange.Value = cellValues;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
            finally
            {
                if (xlRange != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlRange);
                    xlRange = null;
                }
            }
        }
    }
}

