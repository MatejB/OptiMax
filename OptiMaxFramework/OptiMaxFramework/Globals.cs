﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.ComponentModel;                            // DescriptionAttribute
using System.Reflection;                                // FieldInfo

namespace OptiMaxFramework
{
    public static class Globals
    {
        public static System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };
        public static string InputArraySeparator = "__________";

        public static string baseDriverName = "Base";

        public const string JobIdName = "JobId";
       
        public static void ClearDirectoryContents(string directory)
        {
            try
            {
                if (Directory.Exists(directory))
                {
                    string[] directories = Directory.GetDirectories(directory);
                    foreach (string dir in directories)
                    {
                        DeleteDirectoryRecursive(dir);
                    }

                    string[] files = Directory.GetFiles(directory);
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void DeleteSubDirectories(string directory)
        {
            try
            {
                if (Directory.Exists(directory))
                {
                    string[] directories = Directory.GetDirectories(directory);
                    foreach (string dir in directories)
                    {
                        // using built in recursive function Directory.Delete(directory, true) does not work if subdirectory opened in explorer
                        DeleteDirectoryRecursive(dir);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in Globals method DeleteSubDirectories(" + directory + ")." + Environment.NewLine + ex.Message);
            }
        }

        public static void DeleteDirectoryRecursive(string directory)
        {
            if (Directory.Exists(directory))
            {
                string[] directories = Directory.GetDirectories(directory);
                foreach (string dir in directories)
                {
                    DeleteDirectoryRecursive(dir);
                }

                string[] files = Directory.GetFiles(directory);
                foreach (string file in files)
                {
                    File.Delete(file);
                }

                try
                {
                    Directory.Delete(directory);
                }
                catch
                {
                    System.Threading.Thread.Sleep(200);
                    Directory.Delete(directory);
                }
            }
            else
            {
            }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public static string ConvertToString(double d, System.Globalization.NumberFormatInfo nfi, int stringLength)
        {
            // no formating
            if (stringLength <= 5) return d.ToString(nfi);
            int depth = 0;
            return ConvertToStringRecursive(d, nfi, stringLength, ref depth);
        }

        
        private static string ConvertToStringRecursive(double d, System.Globalization.NumberFormatInfo nfi, int stringLength, ref int depth)
        {
            string number = d.ToString("G" + (stringLength - depth).ToString(), nfi).Replace("E+0", "E").Replace("E-0", "E-");
            depth++;

            if (number.Length <= stringLength) return number.PadLeft(stringLength, ' ');
            else return ConvertToStringRecursive(d, nfi, stringLength, ref depth);
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public static string ConvertToString_(double d, System.Globalization.NumberFormatInfo nfi, int stringLength) 
        {
            string number = d.ToString("G", nfi);

            if (number.Length <= stringLength)
            {
                return number.PadLeft(stringLength, ' ');
            }
            else
            {
                if (stringLength <= 5)
                    return d.ToString(nfi);
                else
                {
                    string[] tmp = d.ToString("E", nfi).Split('E');

                    number = tmp[0];
                    string mantisa = "";
                    string addZeros = null;

                    if (tmp.Length > 1)
                    {
                        mantisa = tmp[1];
                        int mantisaVal;

                        string sign = mantisa[0].ToString();
                        if (sign == "+") sign = "";

                        mantisaVal = int.Parse(mantisa);
                        mantisa = mantisa.Substring(1, mantisa.Length - 1);
                        mantisa = sign + mantisa.TrimStart('0');
                        if (mantisa != "")
                            mantisa = "E" + mantisa;

                        // mantisa E-1 takes 3 caharacters; instead divide the number with 10 and get rid of mantisa
                        if (mantisaVal < 0 && mantisa.Length > Math.Abs(mantisaVal))
                        {
                            if (number.StartsWith("+")) number = number.Substring(1, number.Length - 1);
                            
                            number = number.Replace(nfi.NumberDecimalSeparator, "");
                            if (number[0] != '-')
                                number = "0" + nfi.NumberDecimalSeparator + "".PadRight(Math.Abs(mantisaVal) - 1, '0') + number;
                            else
                                number = "-0" + nfi.NumberDecimalSeparator + "".PadRight(Math.Abs(mantisaVal) - 1, '0') + number.Substring(1, number.Length - 1);

                            if (number.Length > stringLength) number = number.Substring(0, stringLength);
                            else number = number.PadRight(stringLength, '0');
                            
                            return number;
                        }

                        // mantisa E3 takes 2 caharacters; instead multiply the number with 100 and get rid of mantisa
                        int len = 1 + mantisaVal;
                        if (number[0] == '-') len = 2 + mantisaVal;

                        if (mantisaVal > 0 && len <= stringLength && len <= number.Length)
                        {
                            number = number.Replace(nfi.NumberDecimalSeparator, "");
                            if (len != number.Length) number = number.Insert(len, nfi.NumberDecimalSeparator);

                            if (number.Length > stringLength) number = number.Substring(0, stringLength);
                            else number = number.PadRight(stringLength, '0');

                            return number;
                        }
                    }

                    stringLength -= mantisa.Length;
                    if (stringLength < 1) throw new Exception("The value of the string length of the variable is to low.");

                    if (number.StartsWith("+")) number = number.Substring(1, number.Length - 1);

                    if (number.Length > stringLength) number = number.Substring(0, stringLength);
                    else number = number.PadRight(stringLength, '0');
                    
                    number += mantisa;

                    return number;
                }
            }
        }

        /// <summary>
        /// stringLength : if the legth of the string is lower than 6, no string formating will take place.
        /// </summary>
        public static string ConvertToString(int i, int stringLength)
        {
            if (stringLength > 0) return i.ToString().PadLeft(stringLength, ' ');
            else return i.ToString();
        }

        public static string ConvertToString(string text, int stringLength)
        {
            if (stringLength > 0) return text.PadLeft(stringLength, ' ');
            else return text.ToString();
        }



        public static void KillAllProcessesSpawnedBy(UInt32 parentProcessId)
        {
            // NOTE: Process Ids are reused!
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                "SELECT * " +
                "FROM Win32_Process " +
                "WHERE ParentProcessId=" + parentProcessId);
            ManagementObjectCollection collection = searcher.Get();
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    UInt32 childProcessId = (UInt32)item["ProcessId"];
                    if ((int)childProcessId != Process.GetCurrentProcess().Id)
                    {
                        
                        KillAllProcessesSpawnedBy(childProcessId);

                        try
                        {
                            Process childProcess = Process.GetProcessById((int)childProcessId);
                            childProcess.Kill();
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        public static bool IsProcessRunning(System.Diagnostics.Process p)
        {
            if (p == null)
                throw new ArgumentNullException("There is no process to kill.");

            try
            {
                System.Diagnostics.Process.GetProcessById(p.Id);
            }
            catch
            {
                return false;
            }

            return true;
        }


        public static int GetNumOfCores
        {
            get
            {
                int coreCount = 0;
                foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
                {
                    coreCount += int.Parse(item["NumberOfCores"].ToString());
                }
                return coreCount;
            }
        }

        // Windows version
        public static string GetWindowsName()
        {
            var reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
            string productName = (string)reg.GetValue("ProductName");
            return productName;
        }
        /// Check if it's Windows 8.1
        public static bool IsWindows10orNewer()
        {
            try
            {
                string[] tmp = GetWindowsName().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                double version;

                if (tmp.Length > 2 && double.TryParse(tmp[1], out version) && version >= 10) return true;
                else return false;
            }
            catch
            {
                return false;
            }
        }



        //                                                                                                                                           
        //public static string GetDescriptionFromEnumValue(Enum value)
        //{
        //    DescriptionAttribute attribute = value.GetType()
        //        .GetField(value.ToString())
        //        .GetCustomAttributes(typeof(DescriptionAttribute), false)
        //        .SingleOrDefault() as DescriptionAttribute;
        //    return attribute == null ? value.ToString() : attribute.Description;
        //}

        //public static T GetEnumValueFromDescription<T>(string description)
        //{
        //    var type = typeof(T);
        //    if (!type.IsEnum)
        //        throw new ArgumentException();
        //    FieldInfo[] fields = type.GetFields();
        //    var field = fields
        //                    .SelectMany(f => f.GetCustomAttributes(
        //                        typeof(DescriptionAttribute), false), (
        //                            f, a) => new { Field = f, Att = a })
        //                    .Where(a => ((DescriptionAttribute)a.Att)
        //                        .Description == description).SingleOrDefault();
        //    return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        //}
    }
}
