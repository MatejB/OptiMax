﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathParserTK;

namespace OptiMaxFramework
{
    [Serializable]
    public class SymbolEquation
    {
        private string _equation;

        MathParser parser;

        public string Equation 
        { 
            get
            {
                return _equation;
            }
        }

        public SymbolEquation(string equation)
        {
            _equation = equation.Replace(" ", "");
            parser = new MathParser();
        }

        public double Solve(Dictionary<string, string> variableValues)
        {
            string[] sortedKeys = variableValues.Keys.OrderByDescending(key => key.Length).ToArray();
            string tmp = _equation;

            foreach (string key in sortedKeys)
            {
                tmp = tmp.Replace(key, "(" + variableValues[key] + ")");
            }
            
            return parser.Parse(tmp, true);
        }

        public double Solve(Dictionary<string, Item> itemCollection)
        {
            Item item;
            Dictionary<string, string> variableValues = new Dictionary<string, string>();

            foreach (var parameter in GetParameterNames())
            {
                item = itemCollection[parameter];
                if (item is VariableBase) variableValues.Add(item.Name, ((VariableBase)item).GetValueAsString());
            }

            return Solve(variableValues);
        }

        public List<string> GetParameterNames()
        {
            string[] oneCharOperators = parser.GetSingleCharOperators();

            string[] terms = _equation.Split(oneCharOperators, StringSplitOptions.RemoveEmptyEntries);
            
            string[] multiCharOperatorsAndConstants = parser.GetMultiCharOperatorsAndConstants();
            List<string> parameters = new List<string>();

            foreach (string term in terms)
            {
                if (!multiCharOperatorsAndConstants.Contains(term) && !IsNumeric(term) && !parameters.Contains(term))
                {
                    parameters.Add(term);
                }
            }

            return parameters;
        }

        public static System.Boolean IsNumeric(string expression)
        {
            double result;
            return double.TryParse(expression, out result);
        }
        
    }
}
