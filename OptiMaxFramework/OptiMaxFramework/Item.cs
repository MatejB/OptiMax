﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{
    [Serializable]
    [HumanReadableClassName("Item human readable ...")]
    public abstract class Item
    {
        // Variables                                                                                                                
        protected string name;
        protected string description;
        protected Dictionary<string, Item> baseFrameworkItemCollection;

        // Properties                                                                                                               
        [CategoryAttribute("Data")]
        [DescriptionAttribute("The name of the object.")]
        public virtual string Name
        {
            get { return name; }
            set 
            {
                if (value == "") throw new Exception("The name can not be an empty string.");
                if (value.ToLower() == "id") throw new Exception("The name 'id' is a reserved name.");
                if (value.Contains(' ')) throw new Exception("The name can not contain space characters.");
                
                name = value; 
            }
        }

        [CategoryAttribute("Misc")]
        [DescriptionAttribute("The decsription of the object.")]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        
       

        /// <summary>
        /// Get or set the pointer to the current FrameworkItemCollectio
        /// </summary>
        [BrowsableAttribute(false)]
        public Dictionary<string, Item> FrameworkItemCollection 
        {
            get
            {
                return baseFrameworkItemCollection;
            }
            set
            {
                baseFrameworkItemCollection = value;
            }
        }

        [BrowsableAttribute(false)]
        public string GetHumanReadableBaseTypeName
        {
            get
            {
                object[] attributes = base.GetType().GetCustomAttributes(typeof(HumanReadableClassNameAttribute), false);
                if (attributes.Length > 0)
                {
                    return ((HumanReadableClassNameAttribute)attributes[0]).Name;
                }
                else return base.GetType().Name;
            }
        }

        [BrowsableAttribute(false)]
        public Type GetBaseType
        {
            get
            {
                return base.GetType();
            }
        }

        // Constructors                                                                                                             
        public Item(string name, string description)
        {
            Name = name;
            CheckNameChars(name);

            this.name = name;
            this.description = description;
            this.baseFrameworkItemCollection = null;
        }


        // Methods                                                                                                                  
        public override string ToString()
        {
            if (Name != null && Name.Length > 0) return Name;
            else
            {
                return base.GetType().Name;
            }
        }

        virtual public bool CorrectlyDefined(Dictionary<string, Item> itemCollection)
        {
            return true;
        }

        private void CheckNameChars(string name)
        {
            char c;
            int letterCount = 0;
            int digitCount = 0;

            for (int i = 0; i < name.Length; i++)
            {
                c = (char)name[i];
                if (Char.IsLetter(c)) letterCount++;
                else if (Char.IsDigit(c)) digitCount++;
                else if (c != '_')
                    throw new Exception("The name can only contain a letter, a number or an underscore character.");
            }

            if (letterCount <= 0)
                throw new Exception("The name must contain at least one letter.");

            // check the item name
            MathParserTK.MathParser parser = new MathParserTK.MathParser();
            HashSet<string> names = new HashSet<string>(parser.GetOperatorsAndConstants());
            if (names.Contains(name))
                throw new Exception("The name '" + name + "' is reserved. Please rename the item.");
        }
    }
}
