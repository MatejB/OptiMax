﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OptiMaxFramework
{    
    public interface IDependent
    {        
        List<string> InputVariables { get; }
    }
}
