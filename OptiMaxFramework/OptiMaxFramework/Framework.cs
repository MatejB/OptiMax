﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OptiMaxFramework
{
    [Serializable]
    public class Framework : Item
    {
        // Variables                                                                                                                
        private string workingDirectory;
        private string projectDirectory;
        private string resultsDirectory;
        private string filesDirectory;
        private Dictionary<string, Item> items;
        private Driver driver;
        private List<Recorder> recorders;
        private bool isRunning;
        private JobStatus status;
        private int numOfFileResources;
        private bool hasMemRecorder;
        
        [NonSerialized]
        private Dictionary<string, SgeCommunicator> sgeCommunicators;

        [NonSerialized]
        private Dictionary<string, SlurmCommunicator> slurmCommunicators;

        public Action<string> ReportStatusData;
        public Action<object[]> ReportMemRecorderData;
        public Action<double[]> ReportConverganceData;


        // Properties                                                                                                               
        private Dictionary<string, Item> Items 
        {
            get
            {
                return items;
            }
        }
        public Driver Driver
        {
            get
            {
                return driver;
            }
        }
        public string ProjectDirectory
        {
            get
            {
                return projectDirectory;
            }
        }
        public string WorkingDirectory 
        {
            get
            {
                return workingDirectory;
            }
            set
            {
                if (projectDirectory == value.Trim('\\')) throw new Exception("The working and project directory must not be the same.");
                else workingDirectory = value.Trim('\\');
            }
        }
        public string ResultsDirectory
        {
            get
            {
                return resultsDirectory;
            }
            set
            {
                if (projectDirectory == value.Trim('\\')) throw new Exception("The results and project directory must not be the same.");
                else resultsDirectory = value.Trim('\\');
            }
        }
        public string FilesDirectory
        {
            get
            {
                return filesDirectory;
            }
        }
        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
        }
        public bool IsEventReportStatusDataSet
        {
            get
            {
                if (ReportStatusData != null) return true;
                else return false;
            }
        }
        public bool IsEventReportConverganceDataSet
        {
            get
            {
                if (ReportConverganceData != null) return true;
                else return false;
            }
        }
        public bool HasMemRecorder
        {
            get { return hasMemRecorder; }
        }
        public string[] GetMemRecorderTitleRow
        {
            get
            {
                foreach (Recorder rec in recorders)
                {
                    if (rec is MemRecorder) 
                        return ((MemRecorder)rec).TitleRow;
                }
                return null;
            }
        }
        public JobStatus Status
        {
            get { return status; }
        }
        public List<Recorder> Recorders { get { return recorders; } }



        // Constructors                                                                                                             
        public Framework(string name, string description, string projectDirectory)
            : base(name, description)
        {
            //if (Directory.Exists(projectDirectory)) Globals.ClearDirectory(projectDirectory);

            driver = null;

            workingDirectory = null;
            resultsDirectory = null;

            this.projectDirectory = projectDirectory.Trim('\\');
            this.filesDirectory = Path.Combine(projectDirectory, "Files");

            if (!Directory.Exists(projectDirectory)) Directory.CreateDirectory(projectDirectory);
            if (!Directory.Exists(filesDirectory)) Directory.CreateDirectory(filesDirectory);

            ClearDlls();

            items = new Dictionary<string, Item>();
            recorders = new List<Recorder>();
            numOfFileResources = 0;
            isRunning = false;
            status = JobStatus.InQueue;
            hasMemRecorder = false;
        }


        // Methods                                                                                                                  
        public new string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public new string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
        public void Add(Item item)
        {
            if (item == null) throw new Exception("The item added to the framework can not be null.");

            if (item is ItemContainer)
            {
                foreach (Item i in ((ItemContainer)item).GetItems)
                {
                    AddSingleItem(i);
                }
            }
            else AddSingleItem(item);
        }
        private void AddSingleItem(Item item)
        {
            // check the item name
            //MathParserTK.MathParser parser = new MathParserTK.MathParser();
            //string[] names = parser.GetOperatorsAndConstants();
            //if (names.Contains(item.Name.ToLower()))
            //    throw new Exception("The name of the item '" + item.Name + "' is reserved. Please rename the item.");

            // check the item deffinition
            //bool err = item.CorrectlyDefined()

            item.FrameworkItemCollection = items;

            if (item is Driver)
            {
                this.driver = (Driver)item;
                driver.SetReportStatusData(driver_ReportStatusData);
                driver.SetReportConverganceData(driver_ReportConverganceData);
            }
            else
            {
                if (item is FileResource)
                {
                    numOfFileResources++;
                }
                if (item is DllComponent dc)
                {
                    DllComponent.typesDictionary.TryAdd(dc.DllFile.FileNameWithPath, DllComponent.GetType(dc.DllFile.FileNameWithPath));
                }
                else if (item is Recorder)
                {
                    Recorder rec = (Recorder)item;
                    if (rec is MemRecorder)
                    {
                        ((MemRecorder)rec).ReportData = memRecorder_ReportData;
                        hasMemRecorder = true;
                    }
                    this.recorders.Add(rec);
                }
                items.Add(item.Name, item);
            }
        }
        public void ReplaceDriver(Driver newDriver)
        {
            if (driver != null)
            {
                driver.SetReportStatusData(null);
                driver.SetReportConverganceData(null);
            }

            driver = newDriver.DeepClone();
            driver.FrameworkItemCollection = items;
            driver.SetReportStatusData(driver_ReportStatusData);
            driver.SetReportConverganceData(driver_ReportConverganceData);
        }
        public void Clear()
        {
            if (driver != null)
            {
                driver.SetReportStatusData(null);
                driver.SetReportConverganceData(null);
            }
            driver = null;

            items = new Dictionary<string, Item>();
            foreach (Recorder rec in recorders)
            {
                if (rec is MemRecorder)
                ((MemRecorder)rec).ReportData -= memRecorder_ReportData;
            }
            recorders = new List<Recorder>();
            numOfFileResources = 0;

            ClearSgeCommunicators();
            ClearSlurmCommunicators();
        }
        private void ClearSgeCommunicators()
        {
            if (sgeCommunicators != null)
            {
                foreach (var entry in sgeCommunicators) entry.Value.Disconnect();
                sgeCommunicators.Clear();
            }
        }
        private void ClearSlurmCommunicators()
        {
            if (slurmCommunicators != null)
            {
                foreach (var entry in slurmCommunicators) entry.Value.Disconnect();
                slurmCommunicators.Clear();
            }
        }
        private void ClearDlls()
        {
            // Clear static dictionary of dll types
            if (DllComponent.typesDictionary == null) DllComponent.typesDictionary = new System.Collections.Concurrent.ConcurrentDictionary<string, Type>();
            else DllComponent.typesDictionary.Clear();
        }
        public void ChangeProjectDirectory(string currentDirectory, string newProjectDirectory)
        {
            if (newProjectDirectory != currentDirectory)
            {
                string newFileName;
                List<string> fileResources = new List<string>();

                FileResource file;
                foreach (string key in items.Keys)
                {
                    if (items[key] is FileResource)
                    {
                        file = (FileResource)items[key];
                        if (file.FileNameWithPath.Contains(projectDirectory))
                            fileResources.Add(key);
                    }
                }

                foreach (string fileResource in fileResources)
                {
                    file = (FileResource)items[fileResource];
                    newFileName = file.FileNameWithPath.Replace(projectDirectory, newProjectDirectory);
                    items[fileResource] = new FileResource(file.Name, file.Description, newFileName, file.IOType);
                }

                projectDirectory = newProjectDirectory;
            }
        }
        public void SetNewProjectDirectory(string newProjectDirectory)
        {
            string oldDirectory = projectDirectory;
            if (newProjectDirectory != projectDirectory)
            {
                projectDirectory = newProjectDirectory;
                this.filesDirectory = Path.Combine(projectDirectory, "Files");

                if (!Directory.Exists(projectDirectory)) Directory.CreateDirectory(projectDirectory);
                if (!Directory.Exists(filesDirectory)) Directory.CreateDirectory(filesDirectory);

                if (resultsDirectory != null && (resultsDirectory + "\\").StartsWith(oldDirectory + "\\"))
                    resultsDirectory = resultsDirectory.Replace(oldDirectory + "\\", newProjectDirectory + "\\");

                if (workingDirectory != null && (workingDirectory + "\\").StartsWith(oldDirectory + "\\"))
                    workingDirectory = workingDirectory.Replace(oldDirectory + "\\", newProjectDirectory + "\\");
            }
        }
        public void Run()
        {
            try
            {
                CheckCyclicReferences();

                foreach (var entry in items) entry.Value.CorrectlyDefined(items);

                PrepareSgeComunicators();
                PrepareSlurmComunicators();

                status = JobStatus.Running;

                if (isRunning) throw new Exception("Driver is allready running.");
                if (driver == null) throw new Exception("Driver of the framework was not set.");

                // prepare work directory - can be outside of the project directory
                if (numOfFileResources > 0)
                {
                    if (workingDirectory == null) throw new Exception("The project working directory was not set.");
                    else
                    {
                        if (Directory.Exists(workingDirectory)) Globals.ClearDirectoryContents(workingDirectory);
                        else Directory.CreateDirectory(workingDirectory);
                    }

                    // prepare SgeComponent and SlurmComponent work directory
                    ISshComponent sshComponent;
                    foreach (var entry in items)
                    {
                        if (entry.Value is ISshComponent)
                        {
                            sshComponent = entry.Value as ISshComponent;

                            if (sshComponent.ServerPath == null) throw new Exception("The server path was not set for the component '" + (sshComponent as Item).Name + "'.");
                            else
                            {
                                if (Directory.Exists(sshComponent.ServerPath)) Globals.ClearDirectoryContents(sshComponent.ServerPath);
                                else Directory.CreateDirectory(sshComponent.ServerPath);
                            }
                        }
                    }
                }

                // prepare results directory - can be outside of the project directory
                if (recorders.Count > 0)
                {
                    if (resultsDirectory == null) throw new Exception("The results directory was not set.");

                    if (Directory.Exists(resultsDirectory)) Globals.ClearDirectoryContents(resultsDirectory);
                    else Directory.CreateDirectory(resultsDirectory);
                }

                isRunning = true;
                StartRecorders();
                driver.Run(workingDirectory);
                StopRecorders();
                ClearSgeCommunicators();
                ClearSlurmCommunicators();

                status = JobStatus.OK;
            }
            catch (Exception ex)
            {
                string message = "     ";
                if (ex is AggregateException)
                {
                    AggregateException aex = (AggregateException)ex;
                    foreach (var exception in aex.InnerExceptions)
                    {
                        message += exception.Message + Environment.NewLine;
                    }

                }
                else message = ex.Message;

                if (status != JobStatus.Killed)
                {
                    status = JobStatus.Failed;
                    throw new Exception("Exception in Framework method Run()." + Environment.NewLine + message);
                }
            }
            finally
            {
                isRunning = false;
            }
        }
        public void Pause()
        {
            // the code to pause the execution
            isRunning = false;
        }
        public void Kill()
        {
            // the code to stop the execution
            status = JobStatus.Killed;
            driver.Kill();
            isRunning = false;
        }
        public void CheckCyclicReferences()
        {
            int count = 0;
            foreach (Item item in items.Values)
            {
                RecursiveCheck(ref count, item.Name, item.Name);
            }
        }
        public void RecursiveCheck(ref int count, string firstName, string name)
        {
            if (count > 1000)
                throw new Exception("It seems that there is a cyclic reference in the framework.");

            Item item = items[name];
            if (item is IDependent)
            {
                List<string> parents = ((IDependent)item).InputVariables;
                if (parents == null) return;

                foreach (string parent in parents)
                {
                    if (parent == firstName)
                        throw new Exception("The item '"+ firstName +"' is used in a cyclic reference.");

                    count++;
                    RecursiveCheck(ref count, firstName, parent);
                    count--;
                }
            }
        }
        private void PrepareSgeComunicators()
        {
            sgeCommunicators = new Dictionary<string, SgeCommunicator>();
            foreach (var entry in items)
            {
                if (entry.Value is SgeComponent)
                {
                    SgeComponent sge = (SgeComponent)entry.Value;
                    sge.SshCommunicators = sgeCommunicators;
                }
            }
        }
        private void PrepareSlurmComunicators()
        {
            slurmCommunicators = new Dictionary<string, SlurmCommunicator>();
            foreach (var entry in items)
            {
                if (entry.Value is SlurmComponent)
                {
                    SlurmComponent slr = (SlurmComponent)entry.Value;
                    slr.SlurmCommunicators = slurmCommunicators;
                }
            }
        }
        private void StartRecorders()
        {
            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].ResultsDirectory = resultsDirectory;
                recorders[i].StartRecording();
            }
        }
        private void StopRecorders()
        {
            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].StopRecording();
            }
        }


        // Event handlers
        private void driver_ReportStatusData(string data)
        {
            if (ReportStatusData != null) ReportStatusData(data);
        }
        private void driver_ReportConverganceData(double[] data)
        {
            if (ReportConverganceData != null) ReportConverganceData(data);
        }
        private void memRecorder_ReportData(object[] data)
        {
            if (ReportMemRecorderData != null) ReportMemRecorderData(data);
        }
        
    }
}
