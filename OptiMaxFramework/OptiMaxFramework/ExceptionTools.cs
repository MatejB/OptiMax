﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;

namespace OptiMaxFramework
{
    public static class ExceptionTools
    {
        public static void Show(object obj, Exception ex)
        {
            System.Windows.Forms.MessageBox.Show(ExceptionTools.FormatException(obj, ex), "Error", System.Windows.Forms.MessageBoxButtons.OK);
        }
        public static string FormatException(object o, Exception ex)
        {
            string indent = "";

            string message = "Message: " + ex.Message + Environment.NewLine + Environment.NewLine
                                + ParseStackTrace(ex.StackTrace, indent)
                                    + ObjectContentParser(o, indent + indent);
            
            return message + Environment.NewLine;
        }
        private static string ParseStackTrace(string stackTrace, string indent)
        {
            string[] rows = stackTrace.Split(new string[] {"\n","\r"}, StringSplitOptions.RemoveEmptyEntries);
            string[] parts;

            string message = "";
            for (int i = 0; i < rows.Length; i++)
            {
                parts = rows[i].Split(new string[] { " at ", " in " }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 2)
                    message += indent + parts[1] + Environment.NewLine;
            }

            return message;
        }
        private static string ObjectContentParser(object o, string indent)
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;

            // get all fields of this class
            FieldInfo[] allFields = o.GetType().GetFields(bindingFlags);

            string objectContents = "";
            string value;
            foreach (FieldInfo fieldInfo in allFields)
            {
                // get field values of this class instance
                if (fieldInfo.GetValue(o) == null) value = "null";
                else value = fieldInfo.GetValue(o).ToString();

                objectContents += indent + fieldInfo.Name + ": " + value + Environment.NewLine;
            }

            return objectContents;
        }
    }
}
