﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMaxFramework
{
    /// <summary>
    /// This is a human readable class name!
    /// </summary>
    public class HumanReadableClassNameAttribute : Attribute
    {
        private string _name = string.Empty;

        public HumanReadableClassNameAttribute(string name)
            : base()
        {
            _name = name;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
    }
}
