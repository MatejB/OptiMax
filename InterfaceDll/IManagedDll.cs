﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceDll
{
    public interface IManagedDll
    {
        // Query variable names
        string[] GetVariableNames();
        string[] GetArrayNames();
        // Set variables
        void SetWoringDirectory(string direcotryName);
        void SetVariableByName(string name, string value);
        void SetArrayByName(string name, string values, string delimiter = " ");
        // Run
        void Run();
        // Get variables
        string GetVariableByNameAsString(string name, System.Globalization.NumberFormatInfo nfi);
        string GetArrayByNameAsString(string name, System.Globalization.NumberFormatInfo nfi, string delimiter = " ");
    }
}
