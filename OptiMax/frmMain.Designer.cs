﻿namespace OptiMax
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMemRecordsFromcsvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslProjectName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslProjectDirectory = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbNew = new System.Windows.Forms.ToolStripButton();
            this.tsbOpen = new System.Windows.Forms.ToolStripButton();
            this.tsbSave = new System.Windows.Forms.ToolStripButton();
            this.tssbStart = new System.Windows.Forms.ToolStripSplitButton();
            this.tsmiTry = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbPause = new System.Windows.Forms.ToolStripButton();
            this.tsbStop = new System.Windows.Forms.ToolStripButton();
            this.timUpdateStatus = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timUpdateMemRecords = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageWorkflow = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.omDesigner = new OptiMax.OptiMaxDesigner();
            this.omtToolbar = new OptiMax.Designer.Toolbars.OptiMaxToolbar();
            this.tabPageActiveJobs = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbJobsInqueue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbJobsComplete = new System.Windows.Forms.TextBox();
            this.tbJobsRunning = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvActiveJobs = new System.Windows.Forms.DataGridView();
            this.tabPageMemRecords = new System.Windows.Forms.TabPage();
            this.dvMemRecords = new OptiMax.Controls.DataView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.hcConvergance = new OptiMax.Controls.HistoryControl();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageWorkflow.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPageActiveJobs.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActiveJobs)).BeginInit();
            this.tabPageMemRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(969, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.toolStripMenuItem1,
            this.saveProjectToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.openProjectToolStripMenuItem});
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.projectToolStripMenuItem.Text = "PROJECT";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.newProjectToolStripMenuItem.Text = "New";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(121, 6);
            // 
            // saveProjectToolStripMenuItem
            // 
            this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
            this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.saveProjectToolStripMenuItem.Text = "Save";
            this.saveProjectToolStripMenuItem.Click += new System.EventHandler(this.saveProjectToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.saveAsToolStripMenuItem.Text = "Save as ...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(121, 6);
            // 
            // openProjectToolStripMenuItem
            // 
            this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
            this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.openProjectToolStripMenuItem.Text = "Open";
            this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.openProjectToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.loadMemRecordsFromcsvToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.toolsToolStripMenuItem.Text = "TOOLS";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // loadMemRecordsFromcsvToolStripMenuItem
            // 
            this.loadMemRecordsFromcsvToolStripMenuItem.Name = "loadMemRecordsFromcsvToolStripMenuItem";
            this.loadMemRecordsFromcsvToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.loadMemRecordsFromcsvToolStripMenuItem.Text = "Load MemRecords from *.csv";
            this.loadMemRecordsFromcsvToolStripMenuItem.Click += new System.EventHandler(this.loadMemRecordsFromcsvToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.helpToolStripMenuItem.Text = "HELP";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslProjectName,
            this.toolStripStatusLabel1,
            this.tslProjectDirectory,
            this.tsProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 580);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(969, 24);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslProjectName
            // 
            this.tslProjectName.Name = "tslProjectName";
            this.tslProjectName.Size = new System.Drawing.Size(80, 19);
            this.tslProjectName.Text = "Project name:";
            this.tslProjectName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(22, 19);
            this.toolStripStatusLabel1.Text = "     ";
            // 
            // tslProjectDirectory
            // 
            this.tslProjectDirectory.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tslProjectDirectory.Name = "tslProjectDirectory";
            this.tslProjectDirectory.Size = new System.Drawing.Size(615, 19);
            this.tslProjectDirectory.Spring = true;
            this.tslProjectDirectory.Text = "Project directory: ";
            this.tslProjectDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsProgress
            // 
            this.tsProgress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsProgress.MarqueeAnimationSpeed = 30;
            this.tsProgress.Name = "tsProgress";
            this.tsProgress.Size = new System.Drawing.Size(233, 18);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNew,
            this.tsbOpen,
            this.tsbSave,
            this.toolStripSeparator1,
            this.tssbStart,
            this.tsbPause,
            this.tsbStop});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(969, 25);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbNew
            // 
            this.tsbNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNew.Image = ((System.Drawing.Image)(resources.GetObject("tsbNew.Image")));
            this.tsbNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNew.Name = "tsbNew";
            this.tsbNew.Size = new System.Drawing.Size(23, 22);
            this.tsbNew.Text = "toolStripButton2";
            this.tsbNew.ToolTipText = "New project";
            this.tsbNew.Click += new System.EventHandler(this.tsbNew_Click);
            // 
            // tsbOpen
            // 
            this.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpen.Image")));
            this.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOpen.Name = "tsbOpen";
            this.tsbOpen.Size = new System.Drawing.Size(23, 22);
            this.tsbOpen.Text = "toolStripButton3";
            this.tsbOpen.ToolTipText = "Open project";
            this.tsbOpen.Click += new System.EventHandler(this.tsbOpen_Click);
            // 
            // tsbSave
            // 
            this.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
            this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSave.Name = "tsbSave";
            this.tsbSave.Size = new System.Drawing.Size(23, 22);
            this.tsbSave.Text = "toolStripButton4";
            this.tsbSave.ToolTipText = "Save project";
            this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // tssbStart
            // 
            this.tssbStart.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTry});
            this.tssbStart.Image = global::OptiMax.Properties.Resources.Start;
            this.tssbStart.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tssbStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssbStart.Name = "tssbStart";
            this.tssbStart.Size = new System.Drawing.Size(63, 22);
            this.tssbStart.Text = "Start";
            this.tssbStart.ButtonClick += new System.EventHandler(this.tssbStart_ButtonClick);
            // 
            // tsmiTry
            // 
            this.tsmiTry.Image = global::OptiMax.Properties.Resources.Start;
            this.tsmiTry.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiTry.Name = "tsmiTry";
            this.tsmiTry.Size = new System.Drawing.Size(90, 22);
            this.tsmiTry.Text = "Try";
            this.tsmiTry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsmiTry.Click += new System.EventHandler(this.tsmiTry_Click);
            // 
            // tsbPause
            // 
            this.tsbPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPause.Enabled = false;
            this.tsbPause.Image = ((System.Drawing.Image)(resources.GetObject("tsbPause.Image")));
            this.tsbPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPause.Name = "tsbPause";
            this.tsbPause.Size = new System.Drawing.Size(23, 22);
            this.tsbPause.Text = "Pause";
            // 
            // tsbStop
            // 
            this.tsbStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStop.Enabled = false;
            this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
            this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStop.Name = "tsbStop";
            this.tsbStop.Size = new System.Drawing.Size(23, 22);
            this.tsbStop.Text = "Stop";
            this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click);
            // 
            // timUpdateStatus
            // 
            this.timUpdateStatus.Interval = 1001;
            this.timUpdateStatus.Tick += new System.EventHandler(this.timUpdateStatus_Tick);
            // 
            // timUpdateMemRecords
            // 
            this.timUpdateMemRecords.Interval = 2000;
            this.timUpdateMemRecords.Tick += new System.EventHandler(this.timUpdateMemRecords_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(6, 52);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(957, 523);
            this.splitContainer1.SplitterDistance = 316;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageWorkflow);
            this.tabControl1.Controls.Add(this.tabPageActiveJobs);
            this.tabControl1.Controls.Add(this.tabPageMemRecords);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tabControl1.Location = new System.Drawing.Point(20, 20);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(869, 240);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPageWorkflow
            // 
            this.tabPageWorkflow.Controls.Add(this.panel1);
            this.tabPageWorkflow.Controls.Add(this.omtToolbar);
            this.tabPageWorkflow.Location = new System.Drawing.Point(4, 24);
            this.tabPageWorkflow.Name = "tabPageWorkflow";
            this.tabPageWorkflow.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWorkflow.Size = new System.Drawing.Size(861, 212);
            this.tabPageWorkflow.TabIndex = 0;
            this.tabPageWorkflow.Text = "Workflow";
            this.tabPageWorkflow.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.omDesigner);
            this.panel1.Location = new System.Drawing.Point(42, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(817, 204);
            this.panel1.TabIndex = 15;
            // 
            // omDesigner
            // 
            this.omDesigner.AutoScroll = true;
            this.omDesigner.BackColor = System.Drawing.SystemColors.Window;
            this.omDesigner.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.omDesigner.Framework = null;
            this.omDesigner.Location = new System.Drawing.Point(128, 69);
            this.omDesigner.Name = "omDesigner";
            this.omDesigner.Size = new System.Drawing.Size(388, 183);
            this.omDesigner.SnapToGrid = true;
            this.omDesigner.TabIndex = 8;
            // 
            // omtToolbar
            // 
            this.omtToolbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.omtToolbar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.omtToolbar.Location = new System.Drawing.Point(3, 3);
            this.omtToolbar.Name = "omtToolbar";
            this.omtToolbar.Size = new System.Drawing.Size(30, 206);
            this.omtToolbar.TabIndex = 14;
            this.omtToolbar.Text = "variables1";
            this.omtToolbar.AddElement += new OptiMax.Designer.Toolbars.AddElementDelegate(this.omtToolbar_AddElement);
            // 
            // tabPageActiveJobs
            // 
            this.tabPageActiveJobs.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageActiveJobs.Controls.Add(this.groupBox1);
            this.tabPageActiveJobs.Controls.Add(this.dgvActiveJobs);
            this.tabPageActiveJobs.Location = new System.Drawing.Point(4, 24);
            this.tabPageActiveJobs.Name = "tabPageActiveJobs";
            this.tabPageActiveJobs.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageActiveJobs.Size = new System.Drawing.Size(861, 212);
            this.tabPageActiveJobs.TabIndex = 1;
            this.tabPageActiveJobs.Text = "Active jobs";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbJobsInqueue);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbJobsComplete);
            this.groupBox1.Controls.Add(this.tbJobsRunning);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(151, 109);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jobs";
            // 
            // tbJobsInqueue
            // 
            this.tbJobsInqueue.BackColor = System.Drawing.SystemColors.Window;
            this.tbJobsInqueue.Location = new System.Drawing.Point(74, 20);
            this.tbJobsInqueue.Name = "tbJobsInqueue";
            this.tbJobsInqueue.ReadOnly = true;
            this.tbJobsInqueue.Size = new System.Drawing.Size(71, 23);
            this.tbJobsInqueue.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Complete";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "In queue";
            // 
            // tbJobsComplete
            // 
            this.tbJobsComplete.BackColor = System.Drawing.SystemColors.Window;
            this.tbJobsComplete.Location = new System.Drawing.Point(74, 78);
            this.tbJobsComplete.Name = "tbJobsComplete";
            this.tbJobsComplete.ReadOnly = true;
            this.tbJobsComplete.Size = new System.Drawing.Size(71, 23);
            this.tbJobsComplete.TabIndex = 4;
            // 
            // tbJobsRunning
            // 
            this.tbJobsRunning.BackColor = System.Drawing.SystemColors.Window;
            this.tbJobsRunning.Location = new System.Drawing.Point(74, 49);
            this.tbJobsRunning.Name = "tbJobsRunning";
            this.tbJobsRunning.ReadOnly = true;
            this.tbJobsRunning.Size = new System.Drawing.Size(71, 23);
            this.tbJobsRunning.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Running";
            // 
            // dgvActiveJobs
            // 
            this.dgvActiveJobs.AllowUserToAddRows = false;
            this.dgvActiveJobs.AllowUserToDeleteRows = false;
            this.dgvActiveJobs.AllowUserToResizeRows = false;
            this.dgvActiveJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvActiveJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActiveJobs.Location = new System.Drawing.Point(163, 3);
            this.dgvActiveJobs.MultiSelect = false;
            this.dgvActiveJobs.Name = "dgvActiveJobs";
            this.dgvActiveJobs.ReadOnly = true;
            this.dgvActiveJobs.RowHeadersVisible = false;
            this.dgvActiveJobs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvActiveJobs.Size = new System.Drawing.Size(695, 206);
            this.dgvActiveJobs.TabIndex = 2;
            this.dgvActiveJobs.TabStop = false;
            this.dgvActiveJobs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvActiveJobs_MouseUp);
            // 
            // tabPageMemRecords
            // 
            this.tabPageMemRecords.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageMemRecords.Controls.Add(this.dvMemRecords);
            this.tabPageMemRecords.Location = new System.Drawing.Point(4, 24);
            this.tabPageMemRecords.Name = "tabPageMemRecords";
            this.tabPageMemRecords.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMemRecords.Size = new System.Drawing.Size(192, 72);
            this.tabPageMemRecords.TabIndex = 2;
            this.tabPageMemRecords.Text = "Memory records";
            // 
            // dvMemRecords
            // 
            this.dvMemRecords.GraphPane = ((ZedGraph.GraphPane)(resources.GetObject("dvMemRecords.GraphPane")));
            this.dvMemRecords.Location = new System.Drawing.Point(297, 77);
            this.dvMemRecords.Name = "dvMemRecords";
            this.dvMemRecords.SelectedCells = ((System.Collections.Generic.SortedList<int, System.Drawing.Point>)(resources.GetObject("dvMemRecords.SelectedCells")));
            this.dvMemRecords.Size = new System.Drawing.Size(2712, 1246);
            this.dvMemRecords.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(27, 33);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tbOutput);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.hcConvergance);
            this.splitContainer2.Size = new System.Drawing.Size(871, 108);
            this.splitContainer2.SplitterDistance = 429;
            this.splitContainer2.TabIndex = 16;
            // 
            // tbOutput
            // 
            this.tbOutput.BackColor = System.Drawing.SystemColors.Window;
            this.tbOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbOutput.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbOutput.Location = new System.Drawing.Point(16, 22);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.Size = new System.Drawing.Size(216, 59);
            this.tbOutput.TabIndex = 15;
            this.tbOutput.Text = "";
            // 
            // hcConvergance
            // 
            this.hcConvergance.BackColor = System.Drawing.SystemColors.Control;
            this.hcConvergance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.hcConvergance.Location = new System.Drawing.Point(29, 12);
            this.hcConvergance.Name = "hcConvergance";
            this.hcConvergance.Size = new System.Drawing.Size(350, 82);
            this.hcConvergance.TabIndex = 16;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(969, 604);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(697, 571);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = Globals.ProgramName;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageWorkflow.ResumeLayout(false);
            this.tabPageWorkflow.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabPageActiveJobs.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActiveJobs)).EndInit();
            this.tabPageMemRecords.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private OptiMaxDesigner omDesigner;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar tsProgress;
        private System.Windows.Forms.ToolStripButton tsbNew;
        private System.Windows.Forms.ToolStripButton tsbOpen;
        private System.Windows.Forms.ToolStripButton tsbSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbPause;
        private System.Windows.Forms.ToolStripButton tsbStop;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslProjectDirectory;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageWorkflow;
        private System.Windows.Forms.TabPage tabPageActiveJobs;
        private System.Windows.Forms.Timer timUpdateStatus;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage tabPageMemRecords;
        private System.Windows.Forms.Timer timUpdateMemRecords;
        private Controls.DataView dvMemRecords;
        private System.Windows.Forms.ToolStripMenuItem loadMemRecordsFromcsvToolStripMenuItem;
        private Designer.Toolbars.OptiMaxToolbar omtToolbar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvActiveJobs;
        private System.Windows.Forms.ToolStripSplitButton tssbStart;
        private System.Windows.Forms.ToolStripMenuItem tsmiTry;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel tslProjectName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.TextBox tbJobsInqueue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbJobsRunning;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbJobsComplete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox tbOutput;
        private Controls.HistoryControl hcConvergance;
        private System.Windows.Forms.SplitContainer splitContainer2;
    }
}

