﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptiMax
{
    public partial class frmTry : Form
    {
        // Variables                                                                                          
        private double[] values;


        // Properties                                                                                         
        public double[] Values
        { 
            get
            {
                return values;
            }
        }


        // Constructors                                                                                       
        public frmTry()
        {
            InitializeComponent();
        }


        // Methods                                                                                            
        public void SetParameter(string[] pNames, string[] pValues)
        {
            dgvParameters.Rows.Clear();
            for (int i = 0; i < pNames.Length; i++)
            {
                dgvParameters.Rows.Add(new object[] { pNames[i], pValues[i] });
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                string strValue;
                values = new double[dgvParameters.Rows.Count];
                int count = 0;

                foreach (DataGridViewRow row in dgvParameters.Rows)
                {
                    strValue = row.Cells["colValue"].Value.ToString();
                    values[count++] = double.Parse(strValue,  OptiMaxFramework.Globals.nfi);
                }

                dgvParameters.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
    }
}
