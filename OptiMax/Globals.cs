﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMax
{
    public static class Globals
    {
        public const string ProgramName = "Optimax v0.7.1";
        public const string ClipboardFormat = "MyListOfItems";
    }
}
