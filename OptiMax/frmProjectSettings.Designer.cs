﻿namespace OptiMax
{
    partial class frmProjectSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbProjectDirectory = new System.Windows.Forms.TextBox();
            this.btnBrowseProject = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowseResults = new System.Windows.Forms.Button();
            this.tbResultsDirectory = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBrowseWork = new System.Windows.Forms.Button();
            this.tbWorkDirectory = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project directory";
            // 
            // tbProjectDirectory
            // 
            this.tbProjectDirectory.BackColor = System.Drawing.SystemColors.Window;
            this.tbProjectDirectory.Location = new System.Drawing.Point(118, 82);
            this.tbProjectDirectory.Name = "tbProjectDirectory";
            this.tbProjectDirectory.ReadOnly = true;
            this.tbProjectDirectory.Size = new System.Drawing.Size(465, 23);
            this.tbProjectDirectory.TabIndex = 4;
            this.tbProjectDirectory.Text = "Project directory";
            // 
            // btnBrowseProject
            // 
            this.btnBrowseProject.Location = new System.Drawing.Point(590, 80);
            this.btnBrowseProject.Name = "btnBrowseProject";
            this.btnBrowseProject.Size = new System.Drawing.Size(87, 27);
            this.btnBrowseProject.TabIndex = 3;
            this.btnBrowseProject.Text = "Browse";
            this.btnBrowseProject.UseVisualStyleBackColor = true;
            this.btnBrowseProject.Click += new System.EventHandler(this.btnBrowseProject_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnBrowseResults);
            this.groupBox1.Controls.Add(this.tbResultsDirectory);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnBrowseWork);
            this.groupBox1.Controls.Add(this.tbWorkDirectory);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnBrowseProject);
            this.groupBox1.Controls.Add(this.tbProjectDirectory);
            this.groupBox1.Location = new System.Drawing.Point(14, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(687, 175);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 15);
            this.label4.TabIndex = 24;
            this.label4.Text = "Name";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(118, 22);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(465, 23);
            this.tbName.TabIndex = 1;
            this.tbName.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 26;
            this.label5.Text = "Description";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(118, 52);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(465, 23);
            this.tbDescription.TabIndex = 2;
            this.tbDescription.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Results directory";
            // 
            // btnBrowseResults
            // 
            this.btnBrowseResults.Location = new System.Drawing.Point(590, 141);
            this.btnBrowseResults.Name = "btnBrowseResults";
            this.btnBrowseResults.Size = new System.Drawing.Size(87, 27);
            this.btnBrowseResults.TabIndex = 7;
            this.btnBrowseResults.Text = "Browse";
            this.btnBrowseResults.UseVisualStyleBackColor = true;
            this.btnBrowseResults.Click += new System.EventHandler(this.btnBrowseResults_Click);
            // 
            // tbResultsDirectory
            // 
            this.tbResultsDirectory.Location = new System.Drawing.Point(118, 143);
            this.tbResultsDirectory.Name = "tbResultsDirectory";
            this.tbResultsDirectory.Size = new System.Drawing.Size(465, 23);
            this.tbResultsDirectory.TabIndex = 8;
            this.tbResultsDirectory.Text = "Results directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Work directory";
            // 
            // btnBrowseWork
            // 
            this.btnBrowseWork.Location = new System.Drawing.Point(590, 110);
            this.btnBrowseWork.Name = "btnBrowseWork";
            this.btnBrowseWork.Size = new System.Drawing.Size(87, 27);
            this.btnBrowseWork.TabIndex = 5;
            this.btnBrowseWork.Text = "Browse";
            this.btnBrowseWork.UseVisualStyleBackColor = true;
            this.btnBrowseWork.Click += new System.EventHandler(this.btnBrowseWork_Click);
            // 
            // tbWorkDirectory
            // 
            this.tbWorkDirectory.Location = new System.Drawing.Point(118, 112);
            this.tbWorkDirectory.Name = "tbWorkDirectory";
            this.tbWorkDirectory.Size = new System.Drawing.Size(465, 23);
            this.tbWorkDirectory.TabIndex = 6;
            this.tbWorkDirectory.Text = "Work directory";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(510, 196);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 27);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(604, 196);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(87, 27);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // frmProjectSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 233);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProjectSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit project";
            this.Load += new System.EventHandler(this.frmProjectSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbProjectDirectory;
        private System.Windows.Forms.Button btnBrowseProject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBrowseResults;
        private System.Windows.Forms.TextBox tbResultsDirectory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBrowseWork;
        private System.Windows.Forms.TextBox tbWorkDirectory;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbDescription;
    }
}