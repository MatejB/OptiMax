﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;

namespace OptiMax
{
    enum Mode
    {
        New,
        Edit,
        Rename
    }
    public partial class frmProjectSettings : Form
    {
        // Variables                                                                                          
        Mode _mode;

        // Properties        
        public Framework Framework {get; set;}


        // Constructors                                                                                       
        public frmProjectSettings()
        {
            InitializeComponent();
        }


        // Event handling                                                                                     
        private void frmProjectSettings_Load(object sender, EventArgs e)
        {
            if (Framework != null)
            {
                tbName.Text = Framework.Name;
                tbDescription.Text = Framework.Description;
                tbProjectDirectory.Text = Framework.ProjectDirectory;
                tbWorkDirectory.Text = Framework.WorkingDirectory;
                tbResultsDirectory.Text = Framework.ResultsDirectory;
            }
            else
            {
                tbName.Text = "";
                tbDescription.Text = "";
                tbProjectDirectory.Text = "";
                tbWorkDirectory.Text = "";
                tbResultsDirectory.Text = "";
            }
        }

        private void btnBrowseProject_Click(object sender, EventArgs e)
        {
            string prevDirectory = tbProjectDirectory.Text + "\\";

            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbProjectDirectory.Text = folderBrowserDialog1.SelectedPath;

                if (tbWorkDirectory.Text == "") tbWorkDirectory.Text = Path.Combine(tbProjectDirectory.Text, "Work");
                else if (tbWorkDirectory.Text.StartsWith(prevDirectory)) tbWorkDirectory.Text = tbWorkDirectory.Text.Replace(prevDirectory, tbProjectDirectory.Text + "\\");

                if (tbResultsDirectory.Text == "") tbResultsDirectory.Text = Path.Combine(tbProjectDirectory.Text, "Results");
                else if (tbResultsDirectory.Text.StartsWith(prevDirectory)) tbResultsDirectory.Text = tbResultsDirectory.Text.Replace(prevDirectory, tbProjectDirectory.Text + "\\");
            }
        }

        private void btnBrowseWork_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbWorkDirectory.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnBrowseResults_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbResultsDirectory.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Framework frm = GetFramework();
                if (frm == null) return;

                this.Framework = frm;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        public Framework GetFramework()
        {
            HashSet<string> directories = new HashSet<string>();
            directories.Add(tbProjectDirectory.Text.Trim('\\'));
            directories.Add(tbWorkDirectory.Text.Trim('\\'));
            directories.Add(tbResultsDirectory.Text.Trim('\\'));

            if (directories.Count < 3)
                throw new Exception("The names of project, work and results directories must be different.");

            if (_mode == Mode.New) // new project only
            {
                int numOfItems = Directory.GetDirectories(tbProjectDirectory.Text).Length + Directory.GetFiles(tbProjectDirectory.Text).Length;
                if (numOfItems > 0)
                {
                    if (MessageBox.Show("The selected project directory is not empty. If you continue all files and directories will be deleted\n\n" +
                                        "Continue?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        OptiMaxFramework.Globals.ClearDirectoryContents(tbProjectDirectory.Text);
                    }
                    else
                        return null;
                }
            }

            Framework frm;
            frm = new Framework(tbName.Text, tbDescription.Text, tbProjectDirectory.Text);

            string fileNameOld;
            if (Framework != null) fileNameOld = Path.Combine(Framework.ProjectDirectory, Framework.Name + ".om");
            else fileNameOld = "";

            string fileNameNew = Path.Combine(frm.ProjectDirectory, frm.Name + ".om");

            if (fileNameNew != fileNameOld && File.Exists(fileNameNew))
            {
                if (MessageBox.Show("The project named '" + tbName.Text + "' already exists. It will be overwritten if you continue. Continue?",
                                     "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No) return null;
            }

            frm.WorkingDirectory = tbWorkDirectory.Text;
            frm.ResultsDirectory = tbResultsDirectory.Text;
            return frm;
        }

        public void SetForCreateNewProject()
        {
            _mode = Mode.New;
            this.Framework = null;
            tbProjectDirectory.Enabled = true;
            btnBrowseProject.Enabled = true;
            tbWorkDirectory.Enabled = true;
            btnBrowseWork.Enabled = true;
            tbResultsDirectory.Enabled = true;
            btnBrowseResults.Enabled = true;
        }

        public void SetForEditProject(Framework framework)
        {
            SetForCreateNewProject();
            _mode = Mode.Edit;
            this.Framework = framework;
            tbProjectDirectory.Enabled = false;
            btnBrowseProject.Enabled = false;
        }
        

        public void SetForRenameProject(Framework framework)
        {
            SetForCreateNewProject();
            _mode = Mode.Rename;
            this.Framework = framework;
            tbProjectDirectory.Enabled = false;
            btnBrowseProject.Enabled = false;
            tbWorkDirectory.Enabled = false;
            btnBrowseWork.Enabled = false;
            tbResultsDirectory.Enabled = false;
            btnBrowseResults.Enabled = false;
        }


         
    }
}
