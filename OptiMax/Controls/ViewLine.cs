﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ZedGraph;
using OptiMax;

namespace OptiMax.Controls
{
    class ViewLine
    {
        private ZedGraph.LineItem _line;

        [CategoryAttribute("Data")]
        [DisplayName("Label")]
        [DescriptionAttribute("Label of the curve.")]
        public string Label { get { return _line.Label.Text; } set { _line.Label.Text = value; } }


        [CategoryAttribute("Format")]
        [DisplayName("Curve color")]
        [DescriptionAttribute("Color of the curve.")]
        public Color Color { get { return _line.Color; } set { _line.Color = value; } }

        [CategoryAttribute("Format")]
        [DisplayName("Line width")]
        [DescriptionAttribute("Width of the curve line.")]
        public float Width { get { return _line.Line.Width; } set { _line.Line.Width = value; } }



        public ViewLine(LineItem line)
        {
            _line = line;
        }
    }
}
