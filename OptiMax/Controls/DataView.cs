﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ZedGraph;
using OptiMax;

namespace OptiMax.Controls
{   
    public delegate void AddUserDataDelegate();

    public partial class DataView : UserControl
    {
        // Variables                                                                                                           
        private ZedGraph.GraphPane zedGraphPane;
        private List<object> selectedPaneObjects;
        private SortedList<int, Point> selectedCells;
        private Color[] colors;
        private static System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };
        private DataGridViewCellStyle selectedCellStyle;
        private DataGridViewCellStyle deaultCellStyle;
        private CurveItem curveToDelete;
        private ToolTip tooltip;

        // Properties                                                                                                          
        public DataTable DataSource
        {
            set
            {
                if (value != null)
                {
                    dgvMemRecords.DataSource = value;
                    for (int i = 0; i < dgvMemRecords.Columns.Count; i++)
                    {
                        dgvMemRecords.Columns[i].Width = 60;
                    }

                }
            }
        }
        public GraphPane GraphPane { get { return zedGraphPane; } set { zedGraphPane = value; panelGraph.Invalidate(); } }
        public SortedList<int, Point> SelectedCells { get { return selectedCells; } set { selectedCells = value; UpdateAllSelectedCells(); } }

        // Constructors                                                                                                        
        public DataView()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-us");

            InitializeComponent();

            selectedPaneObjects = new List<object>();

            //dgvMemRecords.Dock = DockStyle.Fill;
            //panelGraph.Dock = DockStyle.Fill;
            vScrMemRecords.Dock = DockStyle.Fill;

            dgvMemRecords.DefaultCellStyle.FormatProvider = nfi;
            dgvMemRecords.DefaultCellStyle.Format = "G";            // without this statement the FormatProvider does not work

            deaultCellStyle = dgvMemRecords.DefaultCellStyle;
            selectedCellStyle = deaultCellStyle.Clone();
            selectedCellStyle.BackColor = Color.LightGray;

            //graphPane = new ZedGraph.GraphPane();
            //ZedGraphControl zedGraph = new ZedGraphControl();
            //panelGraph.Controls.Add(zedGraph);
            //zedGraph.Dock = DockStyle.Fill;
            //zedGraph.GraphPane

            zedGraphPane = new GraphPane(new Rectangle(0, 0, panelGraph.Width, panelGraph.Height), " ", "Time", "Value");

            selectedCells = new SortedList<int, Point>();

            colors = new Color[5];
            colors[0] = Color.Red;
            colors[1] = Color.Blue;
            colors[2] = Color.Green;
            colors[3] = Color.Orange;
            colors[4] = Color.Violet;

            tooltip = new ToolTip();

            dgvMemRecords.SortCompare += DgvMemRecords_SortCompare;
            dgvMemRecords.ColumnHeaderMouseClick += DgvMemRecords_ColumnHeaderMouseClick;
        }

        private void DgvMemRecords_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void DgvMemRecords_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            
        }


        // Event handling                                                                                                      
        private void panelGraph_Resize(object sender, EventArgs e)
        {
            try
            {
                if (zedGraphPane != null)
                    zedGraphPane.Rect = new Rectangle(0, 0, panelGraph.Width, panelGraph.Height);
            }
            catch
            { }
        }
        private void panelGraph_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                zedGraphPane.AxisChange();
                zedGraphPane.Draw(e.Graphics);
            }
            catch
            { }
        }
        private void panelGraph_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                bool state = true;

                addUserDataToolStripMenuItem.Visible = state;
                lineToolStripMenuItem.Visible = state;

                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    state = false;
                    if (zedGraphPane.CurveList.Count > 0) state = true;

                    clearToolStripMenuItem.Enabled = state;
                    showMarkersToolStripMenuItem.Enabled = state;
                    showLinesToolStripMenuItem.Enabled = state;

                    object nearestObject;
                    int index;
                    deleteCurveToolStripMenuItem.Enabled = false;
                    zedGraphPane.FindNearestObject(new PointF(e.X, e.Y), this.CreateGraphics(), out nearestObject, out index);
                    if (nearestObject != null && nearestObject.GetType() == typeof(ZedGraph.LineItem))
                    {
                        curveToDelete = (ZedGraph.LineItem)nearestObject;
                        deleteCurveToolStripMenuItem.Enabled = true;
                    }
                }
            }
            catch
            { }
        }
        private void panelGraph_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    object nearestObject;
                    int index;
                    zedGraphPane.FindNearestObject(new PointF(e.X, e.Y), this.CreateGraphics(), out nearestObject, out index);
                    if (nearestObject != null && (nearestObject.GetType() == typeof(ZedGraph.XAxis) || nearestObject.GetType() == typeof(ZedGraph.YAxis)))
                    {
                        ZedGraph.Axis axis = (ZedGraph.Axis)nearestObject;
                        ViewAxis view = new ViewAxis(axis);
                        propertyGrid1.SelectedObject = view;
                        panelGraph.Invalidate();
                    }
                    else if (nearestObject != null && nearestObject.GetType() == typeof(ZedGraph.LineItem))
                    {
                        ZedGraph.LineItem line = (ZedGraph.LineItem)nearestObject;
                        ViewLine view = new ViewLine(line);
                        propertyGrid1.SelectedObject = view;
                        panelGraph.Invalidate();
                    }
                    else
                        propertyGrid1.SelectedObject = null;
                }
            }
            catch
            { }
        }
        private void panelGraph_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                TextObj pointLabel;
                object nearestObject;
                int index;
                zedGraphPane.FindNearestObject(new PointF(e.X, e.Y), this.CreateGraphics(), out nearestObject, out index);
                if (nearestObject != null && nearestObject.GetType() == typeof(ZedGraph.TextObj))
                {
                    string[] tmp = tooltip.GetToolTip(panelGraph).Split(new string[] { " ", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                    string toolTipText = tmp.Length > 0 ? tmp[0] : "";

                    pointLabel = (ZedGraph.TextObj)nearestObject;
                    tmp = pointLabel.Text.Split(new string[] { " ", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                    string pointLabelText = tmp.Length > 0 ? tmp[0] : "";

                    if (toolTipText != pointLabelText)
                    {
                        //tooltip.Active = true;
                        tooltip.Show(pointLabelText, panelGraph, e.X + 5, e.Y + 5, 10000);
                        //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() + " toolTipText: " + toolTipText + " pointLabelText: " + pointLabelText);
                    }
                }
                else
                {
                    //if (tooltip.Active) 
                    //    tooltip.Active = false;
                    tooltip.Hide(panelGraph);
                }
            }
            catch
            { }

            return;
            
            try
            {
                LineItem line;
                object nearestObject;
                int index;
                zedGraphPane.FindNearestObject(new PointF(e.X, e.Y), this.CreateGraphics(), out nearestObject, out index);
                if (nearestObject != null && nearestObject.GetType() == typeof(ZedGraph.TextObj))
                {
                    line = (ZedGraph.LineItem)nearestObject;
                    line.Line.Width = 2;
                    selectedPaneObjects.Add(line);
                    panelGraph.Invalidate();
                }

                bool redraw = false;
                if (nearestObject == null)
                {
                   if (selectedPaneObjects.Count > 0)
                   {

                   }
                   if (redraw) panelGraph.Invalidate();
                }
            }
            catch
            { }
        }


        private void addUserDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (AddUserDataEvent != null) AddUserDataEvent();
            frmAddUserData frm = new frmAddUserData();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                AddUserData(frm.Data);
            }
        }

        private void resetAxesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetAxes();
        }
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSelectedCells();
                ClearGraphData();
                panelGraph.Invalidate();
            }
            catch
            { }
        }
        private void deleteCurveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (curveToDelete != null)
                {
                    if (curveToDelete.Tag != null)
                    {
                        int key = (int)curveToDelete.Tag;
                        Point cell = selectedCells[key];

                        if (key >= 0 && key < dgvMemRecords.Columns.Count * dgvMemRecords.Rows.Count)
                        {
                            dgvMemRecords[cell.X, cell.Y].Style = deaultCellStyle;
                            selectedCells.Remove(key);
                        }

                        RemoveCellFromGraphData(key, cell);
                    }
                    else
                    {
                        RemoveCurveAndPointLabels(curveToDelete);
                    }

                    curveToDelete = null;
                    propertyGrid1.SelectedObject = null;
                    panelGraph.Invalidate();
                }
            }
            catch
            {

            }
        }
        
        private void showMarkersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMarkersToolStripMenuItem.Checked = !showMarkersToolStripMenuItem.Checked;
            foreach (CurveItem curve in zedGraphPane.CurveList)
            {
                (curve as LineItem).Symbol.IsVisible = showMarkersToolStripMenuItem.Checked;
            }

            if (!showMarkersToolStripMenuItem.Checked && !showLinesToolStripMenuItem.Checked) 
                showLinesToolStripMenuItem_Click(null, null);
            else panelGraph.Invalidate();
        }
        private void showLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLinesToolStripMenuItem.Checked = !showLinesToolStripMenuItem.Checked;

            foreach (CurveItem curve in zedGraphPane.CurveList)
            {
                (curve as LineItem).Line.IsVisible = showLinesToolStripMenuItem.Checked;
            }

            if (!showMarkersToolStripMenuItem.Checked && !showLinesToolStripMenuItem.Checked)
                showMarkersToolStripMenuItem_Click(null, null);
            else panelGraph.Invalidate();
        }

        private void dgvMemRecords_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                // Get the index of the item the mouse is below.
                DataGridView.HitTestInfo hti = dgvMemRecords.HitTest(e.X, e.Y);

                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    if (hti.ColumnIndex >= 0 && hti.RowIndex >= 0)
                    {
                        if (dgvMemRecords.CurrentCell != dgvMemRecords[hti.ColumnIndex, hti.RowIndex])
                        {
                            dgvMemRecords.ClearSelection();
                            dgvMemRecords.CurrentCell = dgvMemRecords[hti.ColumnIndex, hti.RowIndex];
                        }
                    }
                }

                if (dgvMemRecords.CurrentCell == null)
                {
                    addToGraphToolStripMenuItem.Enabled = false;
                    removeFromGraphToolStripMenuItem.Enabled = false;
                }
                else
                {
                    addToGraphToolStripMenuItem.Enabled = true;
                    removeFromGraphToolStripMenuItem.Enabled = true;
                }
            }
            catch
            { }
        }
        private void dgvMemRecords_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                // Get the index of the item the mouse is below.
                Point point = dgvMemRecords.PointToClient(MousePosition);
                DataGridView.HitTestInfo hti = dgvMemRecords.HitTest(point.X, point.Y);

                if (hti.ColumnIndex < 0 || hti.RowIndex < 0)
                    dgvMemRecords.CurrentCell = null;
            }
            catch
            { }
        }

        private void dgvMemRecords_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                UpdateAllSelectedCells();
            }
            catch
            { }
        }

        private void addToGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMemRecords.CurrentCell != null)
                {
                    Point current = new Point(dgvMemRecords.CurrentCell.ColumnIndex, dgvMemRecords.CurrentCell.RowIndex);
                    dgvMemRecords.CurrentCell = null;

                    int key = current.X + dgvMemRecords.Columns.Count * current.Y;
                    if (!selectedCells.Keys.Contains(key)) selectedCells.Add(key, current);

                    AddCurrentCellToSelectedCells(key, current);

                    SetVScrMemRecords();
                }
            }
            catch
            { }
        }
        private void removeFromGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMemRecords.CurrentCell != null)
                {
                    Point current = new Point(dgvMemRecords.CurrentCell.ColumnIndex, dgvMemRecords.CurrentCell.RowIndex);

                    int key = current.X + dgvMemRecords.Columns.Count * current.Y;

                    if (key >= 0 && key < dgvMemRecords.Columns.Count * dgvMemRecords.Rows.Count)
                    {
                        dgvMemRecords[current.X, current.Y].Style = deaultCellStyle;
                        selectedCells.Remove(key);
                    }

                    RemoveCellFromGraphData(key, current);

                    dgvMemRecords.CurrentCell = null;

                    propertyGrid1.SelectedObject = null;
                    panelGraph.Invalidate();
                }
            }
            catch
            { }
        }

        private void setAsXValuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvMemRecords.CurrentCell != null)
            {
                Point current = new Point(dgvMemRecords.CurrentCell.ColumnIndex, dgvMemRecords.CurrentCell.RowIndex);
                int key = current.X + dgvMemRecords.Columns.Count * current.Y;
                tbX.Text = "R" + (current.Y + 1) + "C" + (current.X + 1);
            }
        }
        private void setAsYValuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvMemRecords.CurrentCell != null)
            {
                Point current = new Point(dgvMemRecords.CurrentCell.ColumnIndex, dgvMemRecords.CurrentCell.RowIndex);
                int key = current.X + dgvMemRecords.Columns.Count * current.Y;
                tbY.Text = "R" + (current.Y + 1) + "C" + (current.X + 1);
            }
        }
        private void setAsPointLabelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvMemRecords.CurrentCell != null)
            {
                Point current = new Point(dgvMemRecords.CurrentCell.ColumnIndex, dgvMemRecords.CurrentCell.RowIndex);
                int key = current.X + dgvMemRecords.Columns.Count * current.Y;
                tbPointLabels.Text = "R" + (current.Y + 1) + "C" + (current.X + 1);
            }
        }
        
        private void btnAddUserData_Click(object sender, EventArgs e)
        {
            try
            {
                int row, col;
                string[] data;
                XYdata xyData = new XYdata();

                //         X                  
                string[] cell = tbX.Text.ToUpper().Split(new string[] { "R", "C" }, StringSplitOptions.RemoveEmptyEntries);
                if (cell.Length == 2)
                {
                    row = int.Parse(cell[0]) - 1;
                    col = int.Parse(cell[1]) - 1;

                    if (dgvMemRecords.Rows[row].Cells[col].Value.ToString().Contains(' ')) // multiple values
                    {
                        data = dgvMemRecords.Rows[row].Cells[col].Value.ToString().Split(new string[] { " " }, 
                                                                                         StringSplitOptions.RemoveEmptyEntries);
                        xyData.Data = new double[data.Length, 2];
                        xyData.PointLabels = new string[data.Length];
                        for (int i = 0; i < data.Length; i++)
                        {
                            xyData.Data[i, 0] = double.Parse(data[i], OptiMaxFramework.Globals.nfi);
                        }
                    }
                    else if (dgvMemRecords.Rows[row].Cells[col].ValueType == typeof(double)) // sigle value
                    {
                        xyData.Data = new double[1, 2];
                        xyData.PointLabels = new string[1];
                        xyData.Data[0, 0] = (double)dgvMemRecords.Rows[row].Cells[col].Value;
                    }
                }
                else if (cell.Length == 1 && tbX.Text.ToUpper().StartsWith("C"))
                {
                    col = int.Parse(cell[0]) - 1;
                    xyData.Data = new double[dgvMemRecords.Rows.Count, 2];
                    xyData.PointLabels = new string[dgvMemRecords.Rows.Count];
                    if (dgvMemRecords.Rows[0].Cells[col].ValueType == typeof(string))
                    {
                        for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                        {
                            xyData.Data[i, 0] = double.Parse(dgvMemRecords.Rows[i].Cells[col].Value.ToString(), 
                                                             OptiMaxFramework.Globals.nfi);
                        }
                    }
                    else if (dgvMemRecords.Rows[0].Cells[col].ValueType == typeof(int))
                    {
                        for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                        {
                            xyData.Data[i, 0] = (int)dgvMemRecords.Rows[i].Cells[col].Value;
                        }
                    }
                    else if (dgvMemRecords.Rows[0].Cells[col].ValueType == typeof(double))
                    {
                        for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                        {
                            xyData.Data[i, 0] = (double)dgvMemRecords.Rows[i].Cells[col].Value;
                        }
                    }
                    else throw new NotSupportedException();
                }

                // Normalize
                if (cbNormalizeX.Checked) NormalizeX(xyData);


                //         Y                  
                cell = tbY.Text.ToUpper().Split(new string[] { "R", "C" }, StringSplitOptions.RemoveEmptyEntries);
                if (cell.Length == 2)
                {
                    row = int.Parse(cell[0]) - 1;
                    col = int.Parse(cell[1]) - 1;

                    if (dgvMemRecords.Rows[row].Cells[col].Value.ToString().Contains(' ')) // multiple values
                    {
                        data = dgvMemRecords.Rows[row].Cells[col].Value.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < Math.Min(data.Length, xyData.Data.GetLength(0)); i++)
                        {
                            xyData.Data[i, 1] = double.Parse(data[i], OptiMaxFramework.Globals.nfi);
                        }
                    }
                    else if (dgvMemRecords.Rows[row].Cells[col].ValueType == typeof(double)) // sigle value
                    {
                        xyData.Data[0, 1] = (double)dgvMemRecords.Rows[row].Cells[col].Value;
                    }
                }
                else if (cell.Length == 1 && tbY.Text.ToUpper().StartsWith("C"))
                {
                    col = int.Parse(cell[0]) - 1;
                    if (dgvMemRecords.Rows[0].Cells[col].ValueType == typeof(string))
                    {
                        for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                        {
                            xyData.Data[i, 1] = double.Parse(dgvMemRecords.Rows[i].Cells[col].Value.ToString(),
                                                             OptiMaxFramework.Globals.nfi);
                        }
                    }
                    else if (dgvMemRecords.Rows[0].Cells[col].ValueType == typeof(double))
                    {
                        for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                        {
                            xyData.Data[i, 1] = (double)dgvMemRecords.Rows[i].Cells[col].Value;
                        }
                    }
                    else throw new NotSupportedException();
                }

                // Normalize
                if (cbNormalizeY.Checked) NormalizeY(xyData);


                //        Labels             
                xyData.Label = tbLabel.Text;

                //      Point Labels         
                cell = tbPointLabels.Text.ToUpper().Split(new string[] { "R", "C" }, StringSplitOptions.RemoveEmptyEntries);
                if (cell.Length == 2)
                {
                    row = int.Parse(cell[0]) - 1;
                    col = int.Parse(cell[1]) - 1;

                    data = dgvMemRecords.Rows[row].Cells[col].Value.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < Math.Min(data.Length, xyData.PointLabels.GetLength(0)); i++)
                    {
                        xyData.PointLabels[i] = data[i];
                    }
                }
                else if (cell.Length == 1 && tbPointLabels.Text.ToUpper().StartsWith("C"))
                {
                    col = int.Parse(cell[0]) - 1;
                    for (int i = 0; i < dgvMemRecords.Rows.Count; i++)
                    {
                        xyData.PointLabels[i] = dgvMemRecords.Rows[i].Cells[col].Value.ToString();
                    }
                }

                // Sort by X
                if (cbSort.Checked) xyData.SortByX();

                // Add                      
                AddUserData(xyData);
                tbX.Text = tbY.Text = tbLabel.Text = tbPointLabels.Text = "";
            }
            catch
            {
                MessageBox.Show("It is not possible to add the selected data.", "Add XY data error");
            }


        }
        private void vScrMemRecords_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (selectedCells != null && selectedCells.Count > 0 && dgvMemRecords.Columns.Count > 0)
                {
                    int min = selectedCells.Keys[0] / dgvMemRecords.Columns.Count;
                    int max = selectedCells.Keys.Last() / dgvMemRecords.Columns.Count;

                    int delta = vScrMemRecords.Value - min;
                    if (delta > 0 && delta >= dgvMemRecords.Rows.Count - max) delta = dgvMemRecords.Rows.Count - max - 1;
                    else if (delta < 0 && min + delta < 0) delta = -min;

                    if (delta != 0)
                    {
                        Point p;
                        SortedList<int, Point> copy = new SortedList<int, Point>();
                        foreach (Point cell in selectedCells.Values)
                        {
                            p = cell;
                            p.Offset(0, delta);

                            copy.Add(p.X + dgvMemRecords.Columns.Count * p.Y, p);
                        }

                        ClearSelectedCells();

                        selectedCells = copy;
                    }

                    UpdateAllSelectedCells();
                }
            }
            catch
            { }
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            panelGraph.Invalidate();
            propertyGrid1.Refresh();
        }

        // Methods                                                                                                             
        public new void Update()
        {
            UpdateAllSelectedCells();
            UpdateVScrMemRecords();
            panelGraph.Invalidate();
        }

        public void ClearGraphAndRecords()
        {
            dgvMemRecords.DataSource = null;

            ClearGraphData();
            selectedCells.Clear();
            panelGraph.Invalidate();
        }

        private void UpdateAllSelectedCells()
        {
            try
            {
                foreach (Point cell in selectedCells.Values)
                {
                    if (cell.Y < dgvMemRecords.Rows.Count)
                        dgvMemRecords[cell.X, cell.Y].Style = selectedCellStyle;
                }

                RemoveAllCurvesWithTag();
                foreach (var entry in selectedCells)
                {
                    AddCellToGraphData(entry.Key, entry.Value);
                }

                panelGraph.Invalidate();
            }
            catch
            { }
        }

        private void AddCurrentCellToSelectedCells(int key, Point cell)
        {
            try
            {
                if (cell.Y < dgvMemRecords.Rows.Count)
                    dgvMemRecords[cell.X, cell.Y].Style = selectedCellStyle;

                AddCellToGraphData(key, cell);
                panelGraph.Invalidate();
            }
            catch
            { }
        }

        private void ClearSelectedCells()
        {
            try
            {
                foreach (Point cell in selectedCells.Values)
                {
                    dgvMemRecords[cell.X, cell.Y].Style = deaultCellStyle;
                }
                selectedCells.Clear();
            }
            catch
            { }
        }

        private void AddCellToGraphData(int key, Point cell)
        {
            try
            {
                string title = "";
                object value;
                string[] tmp;
                double[] x;
                double[] y;
                
                if (dgvMemRecords.Rows.Count > cell.Y)
                {
                    title = dgvMemRecords.Columns[cell.X].Name + "_" + dgvMemRecords[0, cell.Y].Value;
                    value = dgvMemRecords[cell.X, cell.Y].Value;

                    if (value is double)
                        tmp = new string[] { ((double)value).ToString(nfi) };
                    else
                        tmp = value.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    x = new double[tmp.Length];
                    y = new double[tmp.Length];
                    for (int i = 0; i < tmp.Length; i++)
                    {
                        x[i] = i;
                        y[i] = double.Parse(tmp[i], nfi);
                    }

                    ZedGraph.LineItem curve = zedGraphPane.AddCurve(title, x, y, colors[zedGraphPane.CurveList.Count % colors.Length]);
                    curve.Tag = key;
                    curve.Line.IsAntiAlias = true;

                    if (showLinesToolStripMenuItem.CheckState == CheckState.Unchecked)
                        curve.Line.IsVisible = false;

                    if (showMarkersToolStripMenuItem.CheckState == CheckState.Unchecked)
                        curve.Symbol.IsVisible = false;
                }
            }
            catch
            { }
        }
        private void AddXYDataToGraph(XYdata xyData)
        {
            double[] x;
            double[] y;

            x = new double[xyData.Data.GetLength(0)];
            y = new double[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = xyData.Data[i, 0];
                y[i] = xyData.Data[i, 1];
            }

            HashSet<string> allNames = new HashSet<string>();
            foreach (var curve in zedGraphPane.CurveList) allNames.Add(curve.Label.Text);
            
            int count = 1;
            if (xyData.Label == null || xyData.Label.Length == 0)
            {
                while (allNames.Contains(count.ToString())) count++;
                xyData.Label = count.ToString();
            }
            else
            {
                while (allNames.Contains(xyData.Label + "_" + count.ToString())) count++;
            }

            ZedGraph.LineItem newCurve = zedGraphPane.AddCurve(xyData.Label, x, y, colors[zedGraphPane.CurveList.Count % colors.Length]);
            newCurve.Line.IsAntiAlias = true;

            if (showLinesToolStripMenuItem.CheckState == CheckState.Unchecked)
                newCurve.Line.IsVisible = false;

            if (showMarkersToolStripMenuItem.CheckState == CheckState.Unchecked)
                newCurve.Symbol.IsVisible = false;

            // Point labels
            AddPointLabels(xyData, newCurve.Line.Color);
        }
        private void AddPointLabels(XYdata xyData, Color color)
        {
            if (xyData.PointLabels != null)
            {
                double x;
                double y;

                for (int i = 0; i < xyData.PointLabels.GetLength(0); i++)
                {
                    if (xyData.PointLabels == null || xyData.PointLabels[i] == null || xyData.PointLabels[i].Length == 0) continue;

                    x = xyData.Data[i, 0];
                    y = xyData.Data[i, 1];

                    double Yinterval = 0;
                    // Create a text label from the Y data value
                    ZedGraph.TextObj text = new ZedGraph.TextObj("   " + xyData.PointLabels[i], x, y + Yinterval,
                    ZedGraph.CoordType.AxisXYScale, ZedGraph.AlignH.Left, ZedGraph.AlignV.Center);
                    text.FontSpec.FontColor = color;
                    text.ZOrder = ZedGraph.ZOrder.A_InFront;
                    // Hide the border and the fill
                    text.FontSpec.Border.IsVisible = false;
                    text.FontSpec.Fill.IsVisible = false;
                    text.FontSpec.Size = 10f;
                    text.FontSpec.Angle = 0;

                    text.Link = new Link(xyData.Label, "#", "");
                    zedGraphPane.GraphObjList.Add(text);
                }
            }
        }

        private void RemoveCellFromGraphData(int key, Point cell)
        {
            try
            {
                CurveItem curve = null;

                foreach (CurveItem curveItem in zedGraphPane.CurveList)
                {
                    if (curveItem.Tag != null && (int)curveItem.Tag == key)
                    {
                        curve = curveItem;
                        break;
                    }
                }

                if (curve != null) RemoveCurveAndPointLabels(curve);
            }
            catch
            { }
        }
        private void RemoveAllCurvesWithTag()
        {
            try
            {
                List<CurveItem> tagedCurves = new List<CurveItem>();
                foreach (CurveItem curveItem in zedGraphPane.CurveList)
                {
                    if (curveItem.Tag != null)
                    {
                        tagedCurves.Add(curveItem);
                    }
                }

                foreach (CurveItem curve in tagedCurves)
                {
                    RemoveCurveAndPointLabels(curve);
                }
            }
            catch
            { }
        }

        private void RemoveCurveAndPointLabels(CurveItem curve)
        {
            zedGraphPane.CurveList.Remove(curve);
            TextObj textObj;
            List<TextObj> itemsToRemove = new List<TextObj>();
            foreach (var graphObject in zedGraphPane.GraphObjList)
            {
                if (graphObject is TextObj)
                {
                    textObj = graphObject as TextObj;
                    if (textObj.Link.Title == curve.Label.Text) itemsToRemove.Add(textObj);
                }
            }

            foreach (var item in itemsToRemove) zedGraphPane.GraphObjList.Remove(item);
        }

        public void Clear()
        {
            try
            {
                tbX.Text = tbY.Text = tbLabel.Text = tbPointLabels.Text = "";
                cbNormalizeX.Checked = false;
                cbNormalizeY.Checked = false;
                zedGraphPane.CurveList.Clear();
                zedGraphPane.GraphObjList.Clear();
                cbSort.Checked = false;
                ResetAxes();
                zedGraphPane.XAxis.Title.Text = "Time";
                zedGraphPane.YAxis.Title.Text = "Value";
                propertyGrid1.SelectedObject = null;
            }
            catch
            { }
        }
        private void ClearGraphData()
        {
            try
            {
                zedGraphPane.CurveList.Clear();
                zedGraphPane.GraphObjList.Clear();
                ResetAxes();
                propertyGrid1.SelectedObject = null;
            }
            catch
            { }
        }
        private void ResetAxes()
        {
            try
            {
                zedGraphPane.XAxis.Scale.MinAuto = true;
                zedGraphPane.XAxis.Scale.MaxAuto = true;
                zedGraphPane.YAxis.Scale.MinAuto = true;
                zedGraphPane.YAxis.Scale.MaxAuto = true;
                panelGraph.Invalidate();

                propertyGrid1.Refresh();
            }
            catch
            { }
        }

        private void UpdateVScrMemRecords()
        {
            try
            {
                vScrMemRecords.Maximum = dgvMemRecords.Rows.Count - 1;
            }
            catch
            { }
        }

        private void SetVScrMemRecords()
        {
            try
            {
                if (selectedCells != null && selectedCells.Count > 0)
                {
                    vScrMemRecords.Value = selectedCells.Keys[0] / dgvMemRecords.Columns.Count;
                }
            }
            catch
            { }
        }

        public void AddUserData(XYdata data)
        {
            AddXYDataToGraph(data);
            panelGraph.Invalidate();
        }

        private void NormalizeX(XYdata xyData)
        {
            // normalize curves
            int len = xyData.Data.GetLength(0);
            double minValue = +double.MaxValue;
            double maxValue = -double.MaxValue;

            for (int i = 0; i < len; i++)
            {
                if (xyData.Data[i, 0] < minValue) minValue = xyData.Data[i, 0];
                if (xyData.Data[i, 0] > maxValue) maxValue = xyData.Data[i, 0];
            }
            double delta = maxValue - minValue;
            if (delta == 0) throw new Exception("Exception in DataView method NormalizeXArray. Delta = 0.");

            for (int i = 0; i < len; i++)
            {
                xyData.Data[i, 0] = (xyData.Data[i, 0] - minValue) / delta;
            }
        }

        private void NormalizeY(XYdata xyData)
        {
            // normalize curves
            int len = xyData.Data.GetLength(0);
            double minValue = +double.MaxValue;
            double maxValue = -double.MaxValue;

            for (int i = 0; i < len; i++)
            {
                if (xyData.Data[i, 1] < minValue) minValue = xyData.Data[i, 1];
                if (xyData.Data[i, 1] > maxValue) maxValue = xyData.Data[i, 1];
            }
            double delta = maxValue - minValue;
            if (delta == 0) throw new Exception("Exception in DataView method NormalizeYArray. Delta = 0.");

            for (int i = 0; i < len; i++)
            {
                xyData.Data[i, 1] = (xyData.Data[i, 1] - minValue) / delta;
            }
        }
























    }


}
