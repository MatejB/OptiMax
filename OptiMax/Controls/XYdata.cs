﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMax
{
    public class XYdata
    {
        public string Label;
        public string[] PointLabels;
        public double[,] Data;

        public XYdata()
        {

        }

        public void SortByX()
        {
            int[] indices = new int[Data.GetLength(0)];
            double[] x = new double[indices.Length];
            double[] y = new double[indices.Length];

            for (int i = 0; i < indices.Length; i++)
            {
                indices[i] = i;
                x[i] = Data[i, 0];
                y[i] = Data[i, 1];
            }
            string[] pointLabelsCopy = PointLabels.ToArray();

            Array.Sort(x, indices);

            for (int i = 0; i < indices.Length; i++)
            {
                Data[i, 0] = x[i]; // this is already sorted
                Data[i, 1] = y[indices[i]];
                PointLabels[i] = pointLabelsCopy[indices[i]];
            }
        }
    }
}
