﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptiMax.Controls
{
    public partial class DBPanel : Panel
    {
        public DBPanel(): base()
        {
            InitializeComponent();

            DoubleBuffered = true;
        }
    }
}
