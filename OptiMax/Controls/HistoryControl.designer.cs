namespace OptiMax.Controls
{
    partial class HistoryControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labObj = new System.Windows.Forms.Label();
            this.tbOverallBest = new System.Windows.Forms.Label();
            this.labCon = new System.Windows.Forms.Label();
            this.tbCurrentBest = new System.Windows.Forms.Label();
            this.labDB = new System.Windows.Forms.Label();
            this.txtDB = new System.Windows.Forms.Label();
            this.cmsConvergance = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlGr = new OptiMax.Controls.DBPanel();
            this.cmsConvergance.SuspendLayout();
            this.SuspendLayout();
            // 
            // labObj
            // 
            this.labObj.Location = new System.Drawing.Point(3, 3);
            this.labObj.Name = "labObj";
            this.labObj.Size = new System.Drawing.Size(87, 23);
            this.labObj.TabIndex = 1;
            this.labObj.Text = "Best value";
            this.labObj.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbOverallBest
            // 
            this.tbOverallBest.BackColor = System.Drawing.SystemColors.Control;
            this.tbOverallBest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOverallBest.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbOverallBest.ForeColor = System.Drawing.Color.Lime;
            this.tbOverallBest.Location = new System.Drawing.Point(96, 3);
            this.tbOverallBest.Name = "tbOverallBest";
            this.tbOverallBest.Size = new System.Drawing.Size(75, 23);
            this.tbOverallBest.TabIndex = 2;
            this.tbOverallBest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labCon
            // 
            this.labCon.Location = new System.Drawing.Point(3, 30);
            this.labCon.Name = "labCon";
            this.labCon.Size = new System.Drawing.Size(87, 23);
            this.labCon.TabIndex = 1;
            this.labCon.Text = "Current value";
            this.labCon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCurrentBest
            // 
            this.tbCurrentBest.BackColor = System.Drawing.SystemColors.Control;
            this.tbCurrentBest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrentBest.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbCurrentBest.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCurrentBest.ForeColor = System.Drawing.Color.Red;
            this.tbCurrentBest.Location = new System.Drawing.Point(96, 30);
            this.tbCurrentBest.Name = "tbCurrentBest";
            this.tbCurrentBest.Size = new System.Drawing.Size(75, 23);
            this.tbCurrentBest.TabIndex = 2;
            this.tbCurrentBest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labDB
            // 
            this.labDB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labDB.Location = new System.Drawing.Point(3, 56);
            this.labDB.Name = "labDB";
            this.labDB.Size = new System.Drawing.Size(87, 23);
            this.labDB.TabIndex = 1;
            this.labDB.Text = "/";
            this.labDB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labDB.Visible = false;
            // 
            // txtDB
            // 
            this.txtDB.BackColor = System.Drawing.SystemColors.Control;
            this.txtDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDB.ForeColor = System.Drawing.Color.Tan;
            this.txtDB.Location = new System.Drawing.Point(96, 56);
            this.txtDB.Name = "txtDB";
            this.txtDB.Size = new System.Drawing.Size(75, 23);
            this.txtDB.TabIndex = 2;
            this.txtDB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtDB.Visible = false;
            // 
            // cmsConvergance
            // 
            this.cmsConvergance.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyDataToolStripMenuItem});
            this.cmsConvergance.Name = "cmsConvergance";
            this.cmsConvergance.Size = new System.Drawing.Size(170, 26);
            // 
            // copyDataToolStripMenuItem
            // 
            this.copyDataToolStripMenuItem.Name = "copyDataToolStripMenuItem";
            this.copyDataToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.copyDataToolStripMenuItem.Text = "Copy to clipboard";
            this.copyDataToolStripMenuItem.Click += new System.EventHandler(this.copyDataToolStripMenuItem_Click);
            // 
            // pnlGr
            // 
            this.pnlGr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGr.BackColor = System.Drawing.SystemColors.Control;
            this.pnlGr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGr.ContextMenuStrip = this.cmsConvergance;
            this.pnlGr.Cursor = System.Windows.Forms.Cursors.Default;
            this.pnlGr.Location = new System.Drawing.Point(177, 3);
            this.pnlGr.Name = "pnlGr";
            this.pnlGr.Size = new System.Drawing.Size(169, 76);
            this.pnlGr.TabIndex = 0;
            this.pnlGr.SizeChanged += new System.EventHandler(this.pnlGr_SizeChanged);
            this.pnlGr.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlGr_Paint);
            // 
            // HistoryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDB);
            this.Controls.Add(this.tbOverallBest);
            this.Controls.Add(this.labDB);
            this.Controls.Add(this.tbCurrentBest);
            this.Controls.Add(this.labCon);
            this.Controls.Add(this.labObj);
            this.Controls.Add(this.pnlGr);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "HistoryControl";
            this.Size = new System.Drawing.Size(350, 82);
            this.cmsConvergance.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.DBPanel pnlGr;
        private System.Windows.Forms.Label labObj;
        private System.Windows.Forms.Label tbOverallBest;
        private System.Windows.Forms.Label labCon;
        private System.Windows.Forms.Label tbCurrentBest;
        private System.Windows.Forms.Label labDB;
        private System.Windows.Forms.Label txtDB;
        private System.Windows.Forms.ContextMenuStrip cmsConvergance;
        private System.Windows.Forms.ToolStripMenuItem copyDataToolStripMenuItem;
    }
}
