﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ZedGraph;
using OptiMax;

namespace OptiMax.Controls
{
    class ViewAxis
    {
        private ZedGraph.Axis _axis;

        [CategoryAttribute("Appearance")]
        [DisplayName("Axis tilte")]
        [DescriptionAttribute("Title of the axis.")]
        public string Title { get { return _axis.Title.Text; } set { _axis.Title.Text = value;} }

        [CategoryAttribute("Appearance")]
        [DisplayName("Omit scale")]
        [DescriptionAttribute("Title of the axis.")]
        public bool Scale { get { return _axis.Title.IsOmitMag; } set { _axis.Title.IsOmitMag = value; } }
      


        [CategoryAttribute("Bounds")]
        [DisplayName("\t\t\tMin value")]
        [DescriptionAttribute("Min axis value.")]
        public double Min { get { return _axis.Scale.Min; } set { _axis.Scale.Min = value; } }

        [CategoryAttribute("Bounds")]
        [DisplayName("\t\tMax value")]
        [DescriptionAttribute("Max axis value.")]
        public double Max { get { return _axis.Scale.Max; } set { _axis.Scale.Max = value; } }

        [CategoryAttribute("Bounds")]
        [DisplayName("\tAuto min")]
        [DescriptionAttribute("Automatically compute min axis value.")]
        public bool MinAuto { get { return _axis.Scale.MinAuto; } set { _axis.Scale.MinAuto = value; } }

        [CategoryAttribute("Bounds")]
        [DisplayName("Auto max")]
        [DescriptionAttribute("Automatically compute max axis value.")]
        public bool MaxAuto { get { return _axis.Scale.MaxAuto; } set { _axis.Scale.MaxAuto = value; } }

        [CategoryAttribute("Scale")]
        [DisplayName("Scale")]
        [DescriptionAttribute("Set the axis scale in powers of 10.")]
        public int Mag { get { return _axis.Scale.Mag; } set { _axis.Scale.Mag = value; } }

       

        //[CategoryAttribute("Mag")]
        //[DisplayName("Mag value")]
        //[DescriptionAttribute("Mag axis value.")]
        //public int Mag { get { return _axis.Scale.Mag; } set { _axis.Scale.Mag = value; } }

        //[CategoryAttribute("Exponent")]
        //[DisplayName("Exponent value")]
        //[DescriptionAttribute("Exponent axis value.")]
        //public double Exponent { get { return _axis.Scale.Exponent; } set { _axis.Scale.Exponent = value; } }

        public ViewAxis(Axis axis)
        {
            _axis = axis;
        }
    }
}
