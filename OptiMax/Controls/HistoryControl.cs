using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace OptiMax.Controls
{
    public partial class HistoryControl : UserControl
    {
        // Variables                                                                                                                
        private List<double> overallBest = new List<double>();
        private List<double> currentBest = new List<double>();
        private double min, max;
        private int hor, ver;


        // Constructors                                                                                                             
        public HistoryControl()
        {
            InitializeComponent();

            // Panel za risanje
            hor = pnlGr.ClientRectangle.Width;
            ver = pnlGr.ClientRectangle.Height - 1;
        }


        // Event handlers                                                                                                           
        private void pnlGr_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Draw(e.Graphics);
        }
        private void pnlGr_SizeChanged(object sender, System.EventArgs e)
        {
            hor = pnlGr.ClientRectangle.Width;
            ver = pnlGr.ClientRectangle.Height - 1;
        }
        private void copyDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //int i = 1;
                StringBuilder sb = new StringBuilder();

                //foreach (var value in overallBest)
                //{
                //    if (i > 1) sb.AppendLine();
                //    sb.AppendFormat("{0} {1}", i++, value.ToString(OptiMaxFramework.Globals.nfi));
                //}

                for (int i = 0; i < overallBest.Count; i++)
                {
                    if (i > 0) sb.AppendLine();
                    sb.AppendFormat("{0} {1} {2}", i+1, currentBest[i].ToString(OptiMaxFramework.Globals.nfi),
                                                      overallBest[i].ToString(OptiMaxFramework.Globals.nfi));
                }
                Clipboard.SetText(sb.ToString());
            }
            catch
            {
            }
        }


        // Methods                                                                                                                  
        public void Clear()
        {
            min = -1;
            max = +1;
            overallBest.Clear();
            currentBest.Clear();

            pnlGr.Invalidate();
        }
        public void Add(double bestValue, double currentValue)
        {
            overallBest.Add(bestValue);
            currentBest.Add(currentValue);

            if (this.overallBest.Count == 1)
            {
                min = bestValue;
                max = bestValue;
            }
            else
            {
                if (bestValue < min) min = bestValue;
                if (bestValue > max) max = bestValue;

                if (currentValue < min) min = currentValue;
                if (currentValue > max) max = currentValue;
            }

            pnlGr.Invalidate();
        }

        private void Draw(System.Drawing.Graphics gr)
        {
            try
            {
                // Clear
                //gr.Clear(SystemColors.Control);

                // Mesh
                Pen p = new Pen(Color.Gray);

                gr.DrawLine(p, 0, ver / 4, hor, ver / 4);
                gr.DrawLine(p, 0, ver / 2, hor, ver / 2);
                gr.DrawLine(p, 0, 3 * ver / 4, hor, 3 * ver / 4);

                gr.DrawLine(p, hor / 4, 0, hor / 4, ver);
                gr.DrawLine(p, hor / 2, 0, hor / 2, ver);
                gr.DrawLine(p, 3 * hor / 4, 0, 3 * hor / 4, ver);

                // Podatki
                tbOverallBest.Text = "";
                tbCurrentBest.Text = "";
                txtDB.Text = "";

                // Prvi izhod
                if (overallBest.Count < 1) return;

                // Podatki
                tbOverallBest.Text = overallBest[overallBest.Count - 1].ToString("G4");
                tbCurrentBest.Text = currentBest[currentBest.Count - 1].ToString("G4");

                // Drugi izhod
                if (overallBest.Count < 2) return;

                // current
                p = new Pen(Color.Red, 2);
                p.Width = 2;
                Line(gr, currentBest, min, max, p);

                // overall
                p = new Pen(Color.Green, 2);
                Line(gr, overallBest, min, max, p);
            }
            catch
            {
            }
        }
        private void Line(System.Drawing.Graphics gr, List<double> val, double vmin, double vmax, System.Drawing.Pen linePen)
        {
            int x0, y0, x1, y1;

            double a = (vmax + vmin) / 2;
            double b = (vmax - vmin) / 2 * 1.04;  // Korekcija , da ni risano do robov
            if (b == 0) b = 1;

            int num = Math.Max(20, val.Count);
            for (int j = 1; j < val.Count; j++)
            {
                x0 = ((j - 1) * hor) / num;
                x1 = (j * hor) / num;
                y0 = Convert.ToInt32((ver * (1 - (val[j - 1] - a) / b)) / 2);
                y1 = Convert.ToInt32((ver * (1 - (val[j] - a) / b)) / 2);

                gr.DrawLine(linePen, x0, y0, x1, y1);
            }
        }

        
    }
}
