﻿namespace OptiMax.Controls
{
    partial class DataView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.vScrMemRecords = new System.Windows.Forms.VScrollBar();
            this.dgvMemRecords = new OptiMax.Controls.GridViewDandD();
            this.cmMemRecords = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAsXValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAsYValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAsPointLabelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.removeFromGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbSort = new System.Windows.Forms.CheckBox();
            this.tbPointLabels = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbNormalizeX = new System.Windows.Forms.CheckBox();
            this.tbLabel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddUserData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbY = new System.Windows.Forms.TextBox();
            this.tbX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panelGraph = new OptiMax.Controls.DBPanel();
            this.cmGraph = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addUserDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.showMarkersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.resetAxesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteCurveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbNormalizeY = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMemRecords)).BeginInit();
            this.cmMemRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbEdit.SuspendLayout();
            this.cmGraph.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.dgvMemRecords);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(815, 575);
            this.splitContainer1.SplitterDistance = 180;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.vScrMemRecords);
            this.panel1.Location = new System.Drawing.Point(794, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(18, 177);
            this.panel1.TabIndex = 2;
            // 
            // vScrMemRecords
            // 
            this.vScrMemRecords.LargeChange = 1;
            this.vScrMemRecords.Location = new System.Drawing.Point(0, 0);
            this.vScrMemRecords.Name = "vScrMemRecords";
            this.vScrMemRecords.Size = new System.Drawing.Size(13, 83);
            this.vScrMemRecords.TabIndex = 1;
            this.vScrMemRecords.Value = 1;
            this.vScrMemRecords.ValueChanged += new System.EventHandler(this.vScrMemRecords_ValueChanged);
            // 
            // dgvMemRecords
            // 
            this.dgvMemRecords.AllowDrop = true;
            this.dgvMemRecords.AllowUserToAddRows = false;
            this.dgvMemRecords.AllowUserToDeleteRows = false;
            this.dgvMemRecords.AllowUserToOrderColumns = true;
            this.dgvMemRecords.AllowUserToOrderRows = false;
            this.dgvMemRecords.AllowUserToResizeRows = false;
            this.dgvMemRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMemRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMemRecords.ContextMenuStrip = this.cmMemRecords;
            this.dgvMemRecords.Location = new System.Drawing.Point(0, 0);
            this.dgvMemRecords.MultiSelect = false;
            this.dgvMemRecords.Name = "dgvMemRecords";
            this.dgvMemRecords.ReadOnly = true;
            this.dgvMemRecords.RowHeadersVisible = false;
            this.dgvMemRecords.Size = new System.Drawing.Size(788, 177);
            this.dgvMemRecords.TabIndex = 0;
            this.dgvMemRecords.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMemRecords_ColumnHeaderMouseClick);
            this.dgvMemRecords.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvMemRecords_MouseDown);
            this.dgvMemRecords.MouseLeave += new System.EventHandler(this.dgvMemRecords_MouseLeave);
            // 
            // cmMemRecords
            // 
            this.cmMemRecords.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToGraphToolStripMenuItem,
            this.setAsXValuesToolStripMenuItem,
            this.setAsYValuesToolStripMenuItem,
            this.setAsPointLabelsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.removeFromGraphToolStripMenuItem});
            this.cmMemRecords.Name = "contextMenuStrip1";
            this.cmMemRecords.Size = new System.Drawing.Size(181, 120);
            // 
            // addToGraphToolStripMenuItem
            // 
            this.addToGraphToolStripMenuItem.Name = "addToGraphToolStripMenuItem";
            this.addToGraphToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addToGraphToolStripMenuItem.Text = "Add to graph";
            this.addToGraphToolStripMenuItem.Click += new System.EventHandler(this.addToGraphToolStripMenuItem_Click);
            // 
            // setAsXValuesToolStripMenuItem
            // 
            this.setAsXValuesToolStripMenuItem.Name = "setAsXValuesToolStripMenuItem";
            this.setAsXValuesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.setAsXValuesToolStripMenuItem.Text = "Set as X values";
            this.setAsXValuesToolStripMenuItem.Click += new System.EventHandler(this.setAsXValuesToolStripMenuItem_Click);
            // 
            // setAsYValuesToolStripMenuItem
            // 
            this.setAsYValuesToolStripMenuItem.Name = "setAsYValuesToolStripMenuItem";
            this.setAsYValuesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.setAsYValuesToolStripMenuItem.Text = "Set as Y values";
            this.setAsYValuesToolStripMenuItem.Click += new System.EventHandler(this.setAsYValuesToolStripMenuItem_Click);
            // 
            // setAsPointLabelsToolStripMenuItem
            // 
            this.setAsPointLabelsToolStripMenuItem.Name = "setAsPointLabelsToolStripMenuItem";
            this.setAsPointLabelsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.setAsPointLabelsToolStripMenuItem.Text = "Set as Point Labels";
            this.setAsPointLabelsToolStripMenuItem.Click += new System.EventHandler(this.setAsPointLabelsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // removeFromGraphToolStripMenuItem
            // 
            this.removeFromGraphToolStripMenuItem.Name = "removeFromGraphToolStripMenuItem";
            this.removeFromGraphToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removeFromGraphToolStripMenuItem.Text = "Remove from graph";
            this.removeFromGraphToolStripMenuItem.Click += new System.EventHandler(this.removeFromGraphToolStripMenuItem_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer2.Panel1.Controls.Add(this.gbEdit);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panelGraph);
            this.splitContainer2.Size = new System.Drawing.Size(815, 391);
            this.splitContainer2.SplitterDistance = 314;
            this.splitContainer2.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbNormalizeY);
            this.groupBox1.Controls.Add(this.cbSort);
            this.groupBox1.Controls.Add(this.tbPointLabels);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbNormalizeX);
            this.groupBox1.Controls.Add(this.tbLabel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnAddUserData);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbY);
            this.groupBox1.Controls.Add(this.tbX);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox1.Location = new System.Drawing.Point(0, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 147);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "XY data";
            // 
            // cbSort
            // 
            this.cbSort.AutoSize = true;
            this.cbSort.Location = new System.Drawing.Point(80, 123);
            this.cbSort.Name = "cbSort";
            this.cbSort.Size = new System.Drawing.Size(99, 19);
            this.cbSort.TabIndex = 7;
            this.cbSort.Text = "Sort data by X";
            this.cbSort.UseVisualStyleBackColor = true;
            // 
            // tbPointLabels
            // 
            this.tbPointLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPointLabels.Location = new System.Drawing.Point(80, 95);
            this.tbPointLabels.MinimumSize = new System.Drawing.Size(100, 23);
            this.tbPointLabels.Name = "tbPointLabels";
            this.tbPointLabels.Size = new System.Drawing.Size(226, 23);
            this.tbPointLabels.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Point labels";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbNormalizeX
            // 
            this.cbNormalizeX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNormalizeX.AutoSize = true;
            this.cbNormalizeX.Location = new System.Drawing.Point(226, 19);
            this.cbNormalizeX.Name = "cbNormalizeX";
            this.cbNormalizeX.Size = new System.Drawing.Size(80, 19);
            this.cbNormalizeX.TabIndex = 2;
            this.cbNormalizeX.Text = "Normalize";
            this.cbNormalizeX.UseVisualStyleBackColor = true;
            // 
            // tbLabel
            // 
            this.tbLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLabel.Location = new System.Drawing.Point(80, 69);
            this.tbLabel.MinimumSize = new System.Drawing.Size(100, 23);
            this.tbLabel.Name = "tbLabel";
            this.tbLabel.Size = new System.Drawing.Size(226, 23);
            this.tbLabel.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Label";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAddUserData
            // 
            this.btnAddUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddUserData.Location = new System.Drawing.Point(231, 120);
            this.btnAddUserData.Name = "btnAddUserData";
            this.btnAddUserData.Size = new System.Drawing.Size(75, 23);
            this.btnAddUserData.TabIndex = 8;
            this.btnAddUserData.Text = "Add";
            this.btnAddUserData.UseVisualStyleBackColor = true;
            this.btnAddUserData.Click += new System.EventHandler(this.btnAddUserData_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Y data";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbY
            // 
            this.tbY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbY.Location = new System.Drawing.Point(80, 43);
            this.tbY.MinimumSize = new System.Drawing.Size(100, 23);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(140, 23);
            this.tbY.TabIndex = 3;
            // 
            // tbX
            // 
            this.tbX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbX.Location = new System.Drawing.Point(80, 17);
            this.tbX.MinimumSize = new System.Drawing.Size(100, 23);
            this.tbX.Name = "tbX";
            this.tbX.Size = new System.Drawing.Size(140, 23);
            this.tbX.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "X data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbEdit
            // 
            this.gbEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEdit.Controls.Add(this.propertyGrid1);
            this.gbEdit.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gbEdit.Location = new System.Drawing.Point(0, 154);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Size = new System.Drawing.Size(312, 234);
            this.gbEdit.TabIndex = 3;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "Properties";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 19);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(306, 212);
            this.propertyGrid1.TabIndex = 2;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            // 
            // panelGraph
            // 
            this.panelGraph.ContextMenuStrip = this.cmGraph;
            this.panelGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGraph.Location = new System.Drawing.Point(0, 0);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(497, 391);
            this.panelGraph.TabIndex = 0;
            this.panelGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraph_Paint);
            this.panelGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseDown);
            this.panelGraph.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseMove);
            this.panelGraph.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseUp);
            this.panelGraph.Resize += new System.EventHandler(this.panelGraph_Resize);
            // 
            // cmGraph
            // 
            this.cmGraph.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUserDataToolStripMenuItem,
            this.lineToolStripMenuItem,
            this.showMarkersToolStripMenuItem,
            this.showLinesToolStripMenuItem,
            this.toolStripMenuItem2,
            this.resetAxesToolStripMenuItem,
            this.toolStripMenuItem3,
            this.deleteCurveToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.cmGraph.Name = "cmGraph";
            this.cmGraph.Size = new System.Drawing.Size(149, 154);
            // 
            // addUserDataToolStripMenuItem
            // 
            this.addUserDataToolStripMenuItem.Name = "addUserDataToolStripMenuItem";
            this.addUserDataToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addUserDataToolStripMenuItem.Text = "Add user data";
            this.addUserDataToolStripMenuItem.Click += new System.EventHandler(this.addUserDataToolStripMenuItem_Click);
            // 
            // lineToolStripMenuItem
            // 
            this.lineToolStripMenuItem.Name = "lineToolStripMenuItem";
            this.lineToolStripMenuItem.Size = new System.Drawing.Size(145, 6);
            // 
            // showMarkersToolStripMenuItem
            // 
            this.showMarkersToolStripMenuItem.Checked = true;
            this.showMarkersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showMarkersToolStripMenuItem.Name = "showMarkersToolStripMenuItem";
            this.showMarkersToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.showMarkersToolStripMenuItem.Text = "Show markers";
            this.showMarkersToolStripMenuItem.Click += new System.EventHandler(this.showMarkersToolStripMenuItem_Click);
            // 
            // showLinesToolStripMenuItem
            // 
            this.showLinesToolStripMenuItem.Checked = true;
            this.showLinesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showLinesToolStripMenuItem.Name = "showLinesToolStripMenuItem";
            this.showLinesToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.showLinesToolStripMenuItem.Text = "Show lines";
            this.showLinesToolStripMenuItem.Click += new System.EventHandler(this.showLinesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(145, 6);
            // 
            // resetAxesToolStripMenuItem
            // 
            this.resetAxesToolStripMenuItem.Name = "resetAxesToolStripMenuItem";
            this.resetAxesToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.resetAxesToolStripMenuItem.Text = "Reset Axes";
            this.resetAxesToolStripMenuItem.Click += new System.EventHandler(this.resetAxesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(145, 6);
            // 
            // deleteCurveToolStripMenuItem
            // 
            this.deleteCurveToolStripMenuItem.Name = "deleteCurveToolStripMenuItem";
            this.deleteCurveToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.deleteCurveToolStripMenuItem.Text = "Delete curve";
            this.deleteCurveToolStripMenuItem.Click += new System.EventHandler(this.deleteCurveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // cbNormalizeY
            // 
            this.cbNormalizeY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNormalizeY.AutoSize = true;
            this.cbNormalizeY.Location = new System.Drawing.Point(226, 45);
            this.cbNormalizeY.Name = "cbNormalizeY";
            this.cbNormalizeY.Size = new System.Drawing.Size(80, 19);
            this.cbNormalizeY.TabIndex = 4;
            this.cbNormalizeY.Text = "Normalize";
            this.cbNormalizeY.UseVisualStyleBackColor = true;
            // 
            // DataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "DataView";
            this.Size = new System.Drawing.Size(815, 575);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMemRecords)).EndInit();
            this.cmMemRecords.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbEdit.ResumeLayout(false);
            this.cmGraph.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private GridViewDandD dgvMemRecords;
        private DBPanel panelGraph;
        private System.Windows.Forms.ContextMenuStrip cmMemRecords;
        private System.Windows.Forms.ToolStripMenuItem addToGraphToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmGraph;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeFromGraphToolStripMenuItem;
        private System.Windows.Forms.VScrollBar vScrMemRecords;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem showMarkersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator lineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addUserDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.TextBox tbX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem setAsXValuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAsYValuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.Button btnAddUserData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbLabel;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.ToolStripMenuItem resetAxesToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem deleteCurveToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbNormalizeX;
        private System.Windows.Forms.ToolStripMenuItem setAsPointLabelsToolStripMenuItem;
        private System.Windows.Forms.TextBox tbPointLabels;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbSort;
        private System.Windows.Forms.CheckBox cbNormalizeY;
    }
}
