﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptiMax.Controls
{
    public partial class GridViewDandD : DataGridView
    {
        // Variables                                                                                          
        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;


        // Variables                                                                                          
        public bool AllowUserToOrderRows { get; set; }


        // Constructors                                                                                       
        public GridViewDandD()
        {
            InitializeComponent();

            this.AllowDrop = true;
            this.AllowUserToOrderRows = true;
        }


        // Event handling                                                                                     
        void GridViewDandD_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            try
            {
                if (AllowUserToOrderRows)
                {
                    e.Effect = DragDropEffects.Move;
                }
            }
            catch
            { }
        }

        void GridViewDandD_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            try
            {
                if (AllowUserToOrderRows)
                {
                    // The mouse locations are relative to the screen, so they must be 
                    // converted to client coordinates.
                    Point clientPoint = this.PointToClient(new Point(e.X, e.Y));

                    // Get the row index of the item the mouse is below. 
                    rowIndexOfItemUnderMouseToDrop = this.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

                    if (rowIndexOfItemUnderMouseToDrop >= 0)
                    {
                        // If the drag operation was a move then remove and insert the row.
                        if (e.Effect == DragDropEffects.Move)
                        {
                            DataGridViewRow rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
                            this.Rows.RemoveAt(rowIndexFromMouseDown);
                            this.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                        }
                    }
                }
            }
            catch
            { }
        }

        void GridViewDandD_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            try
            {
                if (AllowUserToOrderRows)
                {
                    // Get the index of the item the mouse is below.
                    rowIndexFromMouseDown = this.HitTest(e.X, e.Y).RowIndex;
                    if (rowIndexFromMouseDown != -1)
                    {
                        // Remember the point where the mouse down occurred. 
                        // The DragSize indicates the size that the mouse can move 
                        // before a drag event should be started.                
                        Size dragSize = SystemInformation.DragSize;

                        // Create a rectangle using the DragSize, with the mouse position being
                        // at the center of the rectangle.
                        dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                                       e.Y - (dragSize.Height / 2)),
                                                                       dragSize);
                    }
                    else
                        // Reset the rectangle if the mouse is not over an item in the ListBox.
                        dragBoxFromMouseDown = Rectangle.Empty;
                }
            }
            catch
            { }
        }

        void GridViewDandD_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            try
            {
                if (AllowUserToOrderRows)
                {
                    if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                    {
                        // If the mouse moves outside the rectangle, start the drag.
                        if (dragBoxFromMouseDown != Rectangle.Empty &&
                            !dragBoxFromMouseDown.Contains(e.X, e.Y))
                        {
                            // Proceed with the drag and drop, passing in the list item.                    
                            DragDropEffects dropEffect = this.DoDragDrop(this.Rows[rowIndexFromMouseDown], DragDropEffects.Move);
                        }
                    }
                }
            }
            catch
            { }
        }
    }
}
