﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmAddUserData : Form
    {
        public XYdata Data { get; set; }

        public frmAddUserData()
        {
            InitializeComponent();
        }

        private void frmAddUserData_Load(object sender, EventArgs e)
        {
            //tbData.Text = "0, 100\r\n25, 100";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Data = GetData(tbData.Text);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private XYdata GetData(string stringData)
        {
            string[] rows = stringData.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            XYdata xyData = new XYdata();

            xyData.Label = tbLabel.Text;
            xyData.Data = new double[rows.Length, 2];

            string[] xy;
            for (int i = 0; i < rows.Length; i++)
            {
                xy = rows[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                xyData.Data[i, 0] = double.Parse(xy[0], OptiMaxFramework.Globals.nfi);
                xyData.Data[i, 1] = double.Parse(xy[1], OptiMaxFramework.Globals.nfi);
            }

            return xyData;
        }
    }
}
