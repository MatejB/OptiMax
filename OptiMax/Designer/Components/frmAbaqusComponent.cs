﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmAbaqusComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;
        bool updateArgument;

        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(AbaqusComponent);
        }
        public AbaqusComponent Component
        {
            get
            {
                return (AbaqusComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmAbaqusComponent()
        {
            InitializeComponent();

            allFileResources = null;
            updateArgument = true;
            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmAbaqusComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            updateArgument = false;
           
            // fill in AbaqusComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                for (int i = 0; i < comboInputFile.Items.Count; i++)
                {
                    if ((string)comboInputFile.Items[i] == Component.InputFileResourceName)
                    {
                        comboInputFile.SelectedIndex = i;
                        break;
                    }
                }
                tbJobName.Text = Component.JobName;
                numMemoryGB.Value = Component.MemorySizeGB;
                numNumOfCpus.Value = Component.NumOfCpus;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000f)).ToString(OptiMaxFramework.Globals.nfi);
                tbArgumentToAdd.Text = Component.ArgumentsToAdd;

                // argument
                Component.FrameworkItemCollection = Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers();
                tbArgument.Text = Component.Argument;
                Component.FrameworkItemCollection = null;

                for (int i = 0; i < comboReportFile.Items.Count; i++)
                {
                    if ((string)comboReportFile.Items[i] == Component.ReportFileResourceName)
                    {
                        comboReportFile.SelectedIndex = i;
                        break;
                    }
                }
                if (Component.OdbReportType == ReportType.Field) comboOdbReportType.SelectedItem = "Field output";
                else comboOdbReportType.SelectedItem = "History output";

                // fill DataGridView
                Item item;
                List<string> itemNames = Component.InputVariables;
                FileResource fileResource;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
            
            updateArgument = true;
            argument_Changed(null, null);
        }
        private void comboInputFile_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ResetFileResources();
            argument_Changed(null, null);
        }
        private void comboReportFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFileResources();
        }
        private void comboInputFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboReportFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboOdbReportType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void argument_Changed(object sender, EventArgs e)
        {
            try
            {
                if (updateArgument)
                {
                    AbaqusComponent ac = GetAbaqusComponent(false);
                    ac.FrameworkItemCollection = Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers();
                    tbArgument.Text = ac.Argument;
                }
            }
            catch
            {
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetAbaqusComponent(true);

                allFileResources = null;
                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        private AbaqusComponent GetAbaqusComponent(bool renameReportFile)
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            if (tbJobName.Text == "") throw new Exception("Please enter the job name.");

            if (tbTimeout.Text.Contains(',')) throw new Exception("Please use a dot as a decimal separator.");

            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            string reportFileResourceName = (string)comboReportFile.SelectedItem;
            int timeoutMilliSeconds = (int)(float.Parse(tbTimeout.Text, OptiMaxFramework.Globals.nfi) * 60 * 1000);

            if (reportFileResourceName == null) throw new Exception("The output file resource is not set.");

            if (renameReportFile)
            {
                FileResource fr = (FileResource)Pairs.GetItemByNameIncludingFromContainers(reportFileResourceName);
                if (fr.FileNameWithoutPath != tbJobName.Text + ".rep")
                {
                    FileResource newFr = new FileResource(fr.Name, fr.Description, tbJobName.Text + ".rep", fr.IOType);
                    Pairs.UpdateItem(fr.Name, newFr);
                }
            }

            AbaqusComponent component = new AbaqusComponent(tbName.Text, tbDescription.Text, inputFileResourceName, tbJobName.Text, reportFileResourceName, timeoutMilliSeconds);

            component.MemorySizeGB = (int)numMemoryGB.Value;
            component.NumOfCpus = (int)numNumOfCpus.Value;
            component.ArgumentsToAdd = tbArgumentToAdd.Text;
            if ((string)comboOdbReportType.SelectedItem == "Field output") component.OdbReportType = ReportType.Field;
            else component.OdbReportType = ReportType.History;

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    component.Add((string)row.Cells["colName"].Value);
                }
            }

            return component;
        }
        public bool DoAllItemsExist(PairsCollection pairs)
        {
            FileResource[] fileResources = pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            int numOfInputFiles = 0;
            int numOfOutputFiles = 0;
            foreach (FileResource fileResource in fileResources)
            {
                if (fileResource.IOType == FileIOType.Input)
                {
                    extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".inp") numOfInputFiles++;
                }
                else if (fileResource.IOType == FileIOType.Output)
                {
                    numOfOutputFiles++;
                }
            }
            if (numOfInputFiles <= 0 || numOfOutputFiles <= 0) return false;
            else return true;
        }
        override protected void ResetForm()
        {
            updateArgument = false;
            // clear all control resources
            comboInputFile.Items.Clear();
            comboReportFile.Items.Clear();
            dgvFiles.Rows.Clear();

            // fill combo box with *.inp file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.IOType == FileIOType.Input || fileResource.IOType == FileIOType.Constant || fileResource.IOType == FileIOType.Copy)
                {
                    extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".inp") comboInputFile.Items.Add(fileResource.Name);
                }
                else if (fileResource.IOType == FileIOType.Output)
                {
                    comboReportFile.Items.Add(fileResource.Name);
                }
            }
            if (comboInputFile.Items.Count > 0) comboInputFile.SelectedIndex = 0;
            if (comboReportFile.Items.Count > 0) comboReportFile.SelectedIndex = 0;

            // fill combo box and dataGridView with all file resources
            ResetFileResources();

            tbName.Text = "";
            tbDescription.Text = "";
            tbJobName.Text = "";
            numMemoryGB.Value = 1;
            numNumOfCpus.Value = 1;
            tbTimeout.Text = "5";
            tbArgumentToAdd.Text = "";
            tbArgument.Text = "";
            comboOdbReportType.SelectedItem = "Field output";

            updateArgument = true;
            argument_Changed(null, null);
        }
        private void ResetFileResources()
        {
            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            string reportFileResourceName = (string)comboReportFile.SelectedItem;

            Dictionary<string, bool> prevStates = new Dictionary<string, bool>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                prevStates.Add((string)row.Cells["colName"].Value, (bool)row.Cells["colInclude"].Value);
            }

            // hide input and report file resources
            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.Name != inputFileResourceName && fileResource.Name != reportFileResourceName)
                    dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }

            // reset previous states
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (prevStates.ContainsKey((string)row.Cells["colName"].Value))
                {
                   row.Cells["colInclude"].Value =  prevStates[(string)row.Cells["colName"].Value];
                }
            }
        }

       

       

      



    }
}
