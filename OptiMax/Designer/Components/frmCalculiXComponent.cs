﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmCalculiXComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(CalculiXComponent);
        }
        public override string GetItemHelp()
        {
            return "Use 'CalculiX Component' to run a CalculiX analysis during the job run.";
        }
        public CalculiXComponent Component
        {
            get
            {
                return (CalculiXComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmCalculiXComponent()
        {
            InitializeComponent();

            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmDosComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in DosComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                tbExeName.Text = Component.Executable;

                for (int i = 0; i < comboInputFile.Items.Count; i++)
                {
                    if ((string)comboInputFile.Items[i] == Component.InputFileResourceName)
                    {
                        comboInputFile.SelectedIndex = i;
                        break;
                    }
                }

                numNumOfCpus.Value = Component.NumOfCpus;

                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();

                // fill DataGridView
                FileResource fileResource;
                Item item;
                List<string> itemNames = Component.InputVariables;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        private void comboInputFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFileResources();
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Executable | *.exe";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbExeName.Text = openFileDialog1.FileName;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetCalculiXComponent();

                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        private CalculiXComponent GetCalculiXComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            CalculiXComponent calculixComponent = new CalculiXComponent(tbName.Text, tbDescription.Text, tbExeName.Text, inputFileResourceName, timeoutMilliSeconds);
            calculixComponent.NumOfCpus = (int)numNumOfCpus.Value;

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    calculixComponent.Add((string)row.Cells["colName"].Value);
                }
            }
            
            return calculixComponent;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            comboInputFile.Items.Clear();
            dgvFiles.Rows.Clear();

            // fill combo box and dataGridView with all file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.IOType != FileIOType.Output)
                {
                    extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".inp") comboInputFile.Items.Add(fileResource.Name);
                }
            }
            if (comboInputFile.Items.Count > 0) comboInputFile.SelectedIndex = 0;

            ResetFileResources();

            tbName.Text = "";
            tbDescription.Text = "";
            tbExeName.Text = "";
            numNumOfCpus.Value = 1;
            tbTimeout.Text = "5";
        }
        private void ResetFileResources()
        {
            string inputFileResourceName = (string)comboInputFile.SelectedItem;

            Dictionary<string, bool> prevStates = new Dictionary<string, bool>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                prevStates.Add((string)row.Cells["colName"].Value, (bool)row.Cells["colInclude"].Value);
            }

            // hide input and report file resources
            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.Name != inputFileResourceName)
                    dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }

            // reset previous states
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (prevStates.ContainsKey((string)row.Cells["colName"].Value))
                {
                    row.Cells["colInclude"].Value = prevStates[(string)row.Cells["colName"].Value];
                }
            }
        }

     

       

        
    }
}
