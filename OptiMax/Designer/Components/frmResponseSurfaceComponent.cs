﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.Diagnostics;
using System.IO;

namespace OptiMax
{
    public partial class frmResponseSurfaceComponent : FormWithItem, IFilesDirectory
    {
        // Variables                                                                                          


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ResponseSurfaceComponent);
        }
        public ResponseSurfaceComponent Component
        {
            get
            {
                return (ResponseSurfaceComponent)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }


        // Constructors                                                                                       
        public frmResponseSurfaceComponent()
        {
            InitializeComponent();

            toolStripMenuItems = null;
            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmResponseSurfaceComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in DllComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();

                comboType.SelectedItem = Component.ResponseSurfaceType.ToString();

                if (Component.InputVariableArrayPairs != null)
                {
                    foreach (VariableArrayPair pair in Component.InputVariableArrayPairs)
                    {
                        if (colInpVarName.Items.Contains(pair.Variable) && colInpArrName.Items.Contains(pair.Array))
                            dgvInput.Rows.Add(new object[] { pair.Variable, pair.Array });
                        else MessageBox.Show("The variable '" + pair.Variable + "' or array '" + pair.Array + "' does not exist anymore.", "Error");
                    }
                }

                if (Component.OutputArrayNames != null)
                {
                    foreach (string name in Component.OutputArrayNames)
                    {
                        if (colOutArrName.Items.Contains(name))
                            dgvOutput.Rows.Add(new object[] { name });
                        else MessageBox.Show("The array '" + name + "' does not exist anymore.", "Error");
                    }
                }
            }
        }
        private void btnAddInputArray_Click(object sender, EventArgs e)
        {
            dgvInput.Rows.Add(new object[] { colInpVarName.Items[0], colInpArrName.Items[0] });
        }
        private void btnAddOutputArray_Click(object sender, EventArgs e)
        {
            dgvOutput.Rows.Add(new object[] { colOutArrName.Items[0] });
        }
        private void btnFit_Click(object sender, EventArgs e)
        {
            try
            {
                ResponseSurfaceComponent rsc = GetResponseSurfaceComponent();
                rsc.FrameworkItemCollection = Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers();
                rsc.Execute("");
                tbFit.Text = rsc.GetStatusData();
            }
            catch (Exception ex)
            {
                tbFit.Text = "Error: " + ex.Message;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetResponseSurfaceComponent();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private ResponseSurfaceComponent GetResponseSurfaceComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            HashSet<string> allNames = new HashSet<string>();
            VariableArrayPair[] inputVariableArrayNames = new VariableArrayPair[dgvInput.Rows.Count];
            string[] outputArrayNames = new string[dgvOutput.Rows.Count];

            string variable;
            string array;
            
            // Input arrays
            for (int i = 0; i < dgvInput.Rows.Count; i++)
            {
                variable = dgvInput[0, i].Value.ToString(); // [col][row]
                array = dgvInput[1, i].Value.ToString(); // [col][row]

                if (allNames.Contains(variable)) throw new Exception("The variable '" + variable + "' is defined more than once.");
                if (allNames.Contains(array)) throw new Exception("The array '" + array + "' is defined more than once.");

                allNames.Add(variable);
                allNames.Add(array);
                inputVariableArrayNames[i] = new VariableArrayPair(variable, array);
            }
            if (inputVariableArrayNames.Length <= 0)
                throw new Exception("Add at least one input variable array pair.");

            // Output arrays
            for (int i = 0; i < dgvOutput.Rows.Count; i++)
            {
                array = dgvOutput[0, i].Value.ToString(); // [col][row]

                if (allNames.Contains(array)) throw new Exception("The array '" + array + "' is defined more than once.");

                allNames.Add(array);
                outputArrayNames[i] = array;
            }
            if (outputArrayNames.Length <= 0)
                throw new Exception("Add at least one output variable array pair.");

            ResponseSurfaceType type;
            if (comboType.SelectedItem.ToString() == ResponseSurfaceType.Linear.ToString())
                type = ResponseSurfaceType.Linear;
            else if (comboType.SelectedItem.ToString() == ResponseSurfaceType.Parabolic.ToString())
                type = ResponseSurfaceType.Parabolic;
            else throw new NotSupportedException();

            ResponseSurfaceComponent component = new ResponseSurfaceComponent(tbName.Text, 
                                                                              tbDescription.Text,
                                                                              timeoutMilliSeconds,
                                                                              inputVariableArrayNames,
                                                                              outputArrayNames,
                                                                              type);
            return component;
        }
        override protected void ResetForm()
        {
            tbName.Text = "";
            tbDescription.Text = "";
            tbTimeout.Text = "5";

            comboType.Items.Clear();
            comboType.Items.Add(ResponseSurfaceType.Linear.ToString());
            comboType.Items.Add(ResponseSurfaceType.Parabolic.ToString());
            comboType.SelectedIndex = 0;

            // add variable names
            Item[] items = Pairs.GetAllVariablesIncludingFromContainers();
            colInpVarName.Items.Clear();
            foreach (var item in items)
            {
                colInpVarName.Items.Add(item.Name);
            }

            // add array names
            items = Pairs.GetAllArraysIncludingFromContainers();
            colInpArrName.Items.Clear();
            colOutArrName.Items.Clear();
            foreach (var item in items)
            {
                colInpArrName.Items.Add(item.Name);
                colOutArrName.Items.Add(item.Name);
            }
        }

        
    }
}
