﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmMatlabComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(MatlabComponent);
        }
        public override string GetItemHelp()
        {
            return "Use 'Matlab Component' to run a Matlab script during the job run.";
        }
        public MatlabComponent Component
        {
            get
            {
                return (MatlabComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmMatlabComponent()
        {
            InitializeComponent();

            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmDosComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in DosComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;

                for (int i = 0; i < comboInputFile.Items.Count; i++)
                {
                    if ((string)comboInputFile.Items[i] == Component.InputFileResourceName)
                    {
                        comboInputFile.SelectedIndex = i;
                        break;
                    }
                }

                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();

                // fill DataGridView
                FileResource fileResource;
                Item item;
                List<string> itemNames = Component.InputVariables;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        private void comboInputFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFileResources();
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetMatlabComponent();

                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        private MatlabComponent GetMatlabComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            MatlabComponent matlabComponent = new MatlabComponent(tbName.Text, tbDescription.Text, inputFileResourceName, timeoutMilliSeconds);

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    matlabComponent.Add((string)row.Cells["colName"].Value);
                }
            }
            
            return matlabComponent;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            comboInputFile.Items.Clear();
            dgvFiles.Rows.Clear();

            // fill combo box and dataGridView with all file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.IOType != FileIOType.Output)
                {
                    extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".m") comboInputFile.Items.Add(fileResource.Name);
                }
            }
            if (comboInputFile.Items.Count > 0) comboInputFile.SelectedIndex = 0;

            ResetFileResources();

            tbName.Text = "";
            tbDescription.Text = "";
            tbTimeout.Text = "5";
        }
        private void ResetFileResources()
        {
            string inputFileResourceName = (string)comboInputFile.SelectedItem;

            Dictionary<string, bool> prevStates = new Dictionary<string, bool>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                prevStates.Add((string)row.Cells["colName"].Value, (bool)row.Cells["colInclude"].Value);
            }

            // hide input and report file resources
            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.Name != inputFileResourceName)
                    dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }

            // reset previous states
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (prevStates.ContainsKey((string)row.Cells["colName"].Value))
                {
                    row.Cells["colInclude"].Value = prevStates[(string)row.Cells["colName"].Value];
                }
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                WindowsDesktop.Desktop matlabDesktop;
                matlabDesktop = new WindowsDesktop.Desktop("MATLAB");
                try
                {
                    matlabDesktop.CreateProcessInIt("explorer.exe");
                    matlabDesktop.Switch();
                    System.Threading.Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace, "Error");
                }
                finally
                {
                    if (matlabDesktop != null) matlabDesktop.SwitchToOrginal();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace, "Error");
            }
        }
    }
}
