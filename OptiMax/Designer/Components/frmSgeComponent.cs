﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmSgeComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;

        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(SgeComponent);
        }

        public SgeComponent Component
        {
            get
            {
                return (SgeComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmSgeComponent()
        {
            InitializeComponent();

            allFileResources = null;
            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmSgeComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in AbaqusComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                for (int i = 0; i < comboScriptFile.Items.Count; i++)
                {
                    if ((string)comboScriptFile.Items[i] == Component.ScriptFileResourceName)
                    {
                        comboScriptFile.SelectedIndex = i;
                        break;
                    }
                }
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();
                tbArgumentToAdd.Text = Component.Argument;

                tbServerName.Text = Component.ServerName;
                tbUserName.Text = Component.ServerUserName;
                mtbUserPassword.Text = Component.ServerUserPass;
                tbServerPath.Text = Component.ServerPath;

                // fill DataGridView
                Item item;
                FileResource fileResource;
                List<string> itemNames = Component.InputVariables;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void comboInputFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbServerPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetSgeComponent();

                allFileResources = null;
                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private SgeComponent GetSgeComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            if (tbServerPath.Text == null || tbServerPath.Text.Length == 0) throw new Exception("The server path is not selected.");

            string scriptFileResourceName = (string)comboScriptFile.SelectedItem;
            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            SgeComponent component = new SgeComponent(tbName.Text, tbDescription.Text, tbServerName.Text, tbUserName.Text, mtbUserPassword.Text,
                                                        tbServerPath.Text, scriptFileResourceName, timeoutMilliSeconds);

            if (tbArgumentToAdd.Text.Length > 0) component.Argument = tbArgumentToAdd.Text;

            string name;

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    name = (string)row.Cells["colName"].Value;
                    if (!component.InputVariables.Contains(name)) component.Add(name);
                }
            }

            return component;
        }

        public bool DoAllItemsExist(PairsCollection pairs)
        {
            FileResource[] fileResources = pairs.GetAllFileResourcesIncludingFromContainers();
            int numOfInputFiles = 0;
            foreach (FileResource fileResource in fileResources)
            {
                if (fileResource.IOType == FileIOType.Input)
                {
                    numOfInputFiles++;
                }
            }
            if (numOfInputFiles <= 0) return false;
            else return true;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            comboScriptFile.Items.Clear();
            dgvFiles.Rows.Clear();

            // fill combo box with *.inp file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            foreach (FileResource allFileResource in allFileResources)
            {
                if (allFileResource.IOType == FileIOType.Input || allFileResource.IOType == FileIOType.Constant)
                {
                    comboScriptFile.Items.Add(allFileResource.Name);
                }

                dgvFiles.Rows.Add(new object[] { false, allFileResource.Name, allFileResource.Description, allFileResource.FileNameWithPath });
            }
            if (comboScriptFile.Items.Count > 0) comboScriptFile.SelectedIndex = 0;

            tbName.Text = "";
            tbDescription.Text = "";
            if (comboScriptFile.Items.Count > 0) comboScriptFile.SelectedIndex = 0;
            tbTimeout.Text = "5";
            tbArgumentToAdd.Text = "";
            tbServerName.Text = "";
            tbUserName.Text = "";
            mtbUserPassword.Text = "";
        }

      


       

      

      

       



       

        
    }
}
