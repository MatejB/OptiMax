﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.Diagnostics;
using System.IO;

namespace OptiMax
{
    public partial class frmDllComponent : FormWithItem, IFilesDirectory
    {
        // Variables                                                                                          
        FileResource[] allFileResources;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(DllComponent);
        }
        public DllComponent Component
        {
            get
            {
                return (DllComponent)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }


        // Constructors                                                                                       
        public frmDllComponent()
        {
            InitializeComponent();

            toolStripMenuItems = null;
            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmDllComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in DllComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                
                tbDllFileName.Text = Component.DllFile.FileNameWithPath;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();
               
                if (Component.InpVariableDllNamePairs != null)
                {
                    foreach (VariableDllNamePair pair in Component.InpVariableDllNamePairs)
                    {
                        if (colVarArrName.Items.Contains(pair.Variable) && colDllName.Items.Contains(pair.DllName))
                            dgvInpVars.Rows.Add(new object[] { pair.Variable, pair.DllName });
                        else MessageBox.Show("The variable '" + pair.Variable + "' does not exist anymore.", "Error");
                    }
                }

                // fill DataGridView
                FileResource fileResource;
                Item item;
                List<string> itemNames = Component.InputVariables;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                string path = null;
                if (Directory.Exists(tbDllFileName.Text)) path = Path.GetDirectoryName(tbDllFileName.Text);
                else path = FilesDirectory;

                if (path != null && Directory.Exists(path)) openFileDialog1.InitialDirectory = path;

                openFileDialog1.Filter = "Dll files (*.dll) | *.dll";
                openFileDialog1.FileName = tbDllFileName.Text;
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    tbDllFileName.Text = openFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        private void tbDllFileName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                colDllName.Items.Clear();

                if (File.Exists(tbDllFileName.Text))
                {
                    var dll = DllComponent.GetDllObject(tbDllFileName.Text);
                    foreach (var name in dll.GetVariableNames()) colDllName.Items.Add(name);
                    foreach (var name in dll.GetArrayNames()) colDllName.Items.Add(name);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            dgvInpVars.Rows.Add(new object[] { colVarArrName.Items[0], ""});
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetDllComponent();

                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private DllComponent GetDllComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            if (tbDllFileName.Text == "") throw new Exception("Please enter the managed dll file name.");
            if (!System.IO.File.Exists(tbDllFileName.Text)) throw new Exception("The managed dll file '" + tbDllFileName.Text + "' does not exist.");

            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            Dictionary<string, VariableDllNamePair> valueDllNamePairs = new Dictionary<string, VariableDllNamePair>();
            string variable;
            string dllName;

            for (int i = 0; i < dgvInpVars.Rows.Count; i++)
            {
                variable = dgvInpVars[0, i].Value.ToString();
                dllName = dgvInpVars[1, i].Value.ToString();

                if (valueDllNamePairs.ContainsKey(variable))
                    throw new Exception("The variable '" + variable + "' is defined more than once.");

                valueDllNamePairs.Add(variable, new VariableDllNamePair(variable, dllName));
            }

            DllComponent component = new DllComponent(tbName.Text, tbDescription.Text, timeoutMilliSeconds, tbDllFileName.Text,
                                                      valueDllNamePairs.Values.ToArray(), FilesDirectory);

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    component.Add((string)row.Cells["colName"].Value);
                }
            }

            component.Check();

            return component;
        }
        override protected void ResetForm()
        {
            tbName.Text = "";
            tbDescription.Text = "";

            tbDllFileName.Text = "";
            tbTimeout.Text = "5";

            // clear all control resources
            dgvInpVars.Rows.Clear();

            Item[] items = Pairs.GetAllValueItemsIncludingFromContainers();
            colVarArrName.Items.Clear();
            foreach (var item in items)
            {
                colVarArrName.Items.Add(item.Name);
            }

            // file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();

            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }
        }

        
    }
}
