﻿namespace OptiMax
{
    partial class frmResponseSurfaceComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAddInputArray = new System.Windows.Forms.Button();
            this.dgvInput = new OptiMax.Controls.GridViewDandD();
            this.colInpVarName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colInpArrName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTimeout = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAddOutputArray = new System.Windows.Forms.Button();
            this.dgvOutput = new OptiMax.Controls.GridViewDandD();
            this.colOutArrName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnFit = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbFit = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInput)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutput)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAddInputArray);
            this.groupBox2.Controls.Add(this.dgvInput);
            this.groupBox2.Location = new System.Drawing.Point(12, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(418, 200);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Independent variable array pairs";
            // 
            // btnAddInputArray
            // 
            this.btnAddInputArray.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddInputArray.Location = new System.Drawing.Point(337, 171);
            this.btnAddInputArray.Name = "btnAddInputArray";
            this.btnAddInputArray.Size = new System.Drawing.Size(75, 23);
            this.btnAddInputArray.TabIndex = 12;
            this.btnAddInputArray.Text = "Add item";
            this.btnAddInputArray.UseVisualStyleBackColor = true;
            this.btnAddInputArray.Click += new System.EventHandler(this.btnAddInputArray_Click);
            // 
            // dgvInput
            // 
            this.dgvInput.AllowDrop = true;
            this.dgvInput.AllowUserToAddRows = false;
            this.dgvInput.AllowUserToOrderRows = true;
            this.dgvInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInput.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInpVarName,
            this.colInpArrName});
            this.dgvInput.Location = new System.Drawing.Point(6, 19);
            this.dgvInput.Name = "dgvInput";
            this.dgvInput.Size = new System.Drawing.Size(405, 147);
            this.dgvInput.TabIndex = 0;
            // 
            // colInpVarName
            // 
            this.colInpVarName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colInpVarName.DisplayStyleForCurrentCellOnly = true;
            this.colInpVarName.HeaderText = "Variable name";
            this.colInpVarName.Name = "colInpVarName";
            this.colInpVarName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInpVarName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colInpVarName.Width = 150;
            // 
            // colInpArrName
            // 
            this.colInpArrName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colInpArrName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colInpArrName.DisplayStyleForCurrentCellOnly = true;
            this.colInpArrName.HeaderText = "Array name";
            this.colInpArrName.Name = "colInpArrName";
            this.colInpArrName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInpArrName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(268, 589);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboType);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbTimeout);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(418, 127);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Response surface type";
            // 
            // comboType
            // 
            this.comboType.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboType.FormattingEnabled = true;
            this.comboType.Items.AddRange(new object[] {
            "Average",
            "Min",
            "Max",
            "Sum"});
            this.comboType.Location = new System.Drawing.Point(128, 97);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(171, 21);
            this.comboType.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Timeout [min]";
            // 
            // tbTimeout
            // 
            this.tbTimeout.Location = new System.Drawing.Point(128, 71);
            this.tbTimeout.Name = "tbTimeout";
            this.tbTimeout.Size = new System.Drawing.Size(120, 20);
            this.tbTimeout.TabIndex = 44;
            this.tbTimeout.Text = "5";
            this.tbTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(128, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(120, 20);
            this.tbName.TabIndex = 1;
            this.tbName.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(128, 45);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(120, 20);
            this.tbDescription.TabIndex = 2;
            this.tbDescription.Text = "Description";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(349, 589);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnAddOutputArray);
            this.groupBox3.Controls.Add(this.dgvOutput);
            this.groupBox3.Location = new System.Drawing.Point(12, 351);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(417, 150);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dependent arrays";
            // 
            // btnAddOutputArray
            // 
            this.btnAddOutputArray.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddOutputArray.Location = new System.Drawing.Point(337, 121);
            this.btnAddOutputArray.Name = "btnAddOutputArray";
            this.btnAddOutputArray.Size = new System.Drawing.Size(75, 23);
            this.btnAddOutputArray.TabIndex = 12;
            this.btnAddOutputArray.Text = "Add item";
            this.btnAddOutputArray.UseVisualStyleBackColor = true;
            this.btnAddOutputArray.Click += new System.EventHandler(this.btnAddOutputArray_Click);
            // 
            // dgvOutput
            // 
            this.dgvOutput.AllowDrop = true;
            this.dgvOutput.AllowUserToAddRows = false;
            this.dgvOutput.AllowUserToOrderRows = true;
            this.dgvOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOutput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOutput.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colOutArrName});
            this.dgvOutput.Location = new System.Drawing.Point(6, 19);
            this.dgvOutput.Name = "dgvOutput";
            this.dgvOutput.Size = new System.Drawing.Size(406, 97);
            this.dgvOutput.TabIndex = 0;
            // 
            // colOutArrName
            // 
            this.colOutArrName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colOutArrName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colOutArrName.DisplayStyleForCurrentCellOnly = true;
            this.colOutArrName.HeaderText = "Array name";
            this.colOutArrName.Name = "colOutArrName";
            this.colOutArrName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colOutArrName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnFit
            // 
            this.btnFit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFit.Location = new System.Drawing.Point(337, 49);
            this.btnFit.Name = "btnFit";
            this.btnFit.Size = new System.Drawing.Size(75, 23);
            this.btnFit.TabIndex = 57;
            this.btnFit.Text = "Fit data";
            this.btnFit.UseVisualStyleBackColor = true;
            this.btnFit.Click += new System.EventHandler(this.btnFit_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbFit);
            this.groupBox4.Controls.Add(this.btnFit);
            this.groupBox4.Location = new System.Drawing.Point(12, 507);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(417, 78);
            this.groupBox4.TabIndex = 58;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Fit";
            // 
            // tbFit
            // 
            this.tbFit.Location = new System.Drawing.Point(6, 23);
            this.tbFit.Name = "tbFit";
            this.tbFit.Size = new System.Drawing.Size(406, 20);
            this.tbFit.TabIndex = 58;
            // 
            // frmResponseSurfaceComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 622);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmResponseSurfaceComponent";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Response Surface Component";
            this.Load += new System.EventHandler(this.frmResponseSurfaceComponent_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInput)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutput)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Controls.GridViewDandD dgvInput;
        private System.Windows.Forms.Button btnAddInputArray;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTimeout;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAddOutputArray;
        private Controls.GridViewDandD dgvOutput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboType;
        private System.Windows.Forms.DataGridViewComboBoxColumn colInpVarName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colInpArrName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colOutArrName;
        private System.Windows.Forms.Button btnFit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbFit;
    }
}