﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmLsDynaComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;
        bool updateArgument;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(LsDynaComponent);
        }
        public LsDynaComponent Component
        {
            get
            {
                return (LsDynaComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmLsDynaComponent()
        {
            InitializeComponent();

            allFileResources = null;
            updateArgument = true;
            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmLsDynaComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            updateArgument = false;

            // fill in LsDynaComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                for (int i = 0; i < comboInputFile.Items.Count; i++)
                {
                    if ((string)comboInputFile.Items[i] == Component.InputFileResourceName)
                    {
                        comboInputFile.SelectedIndex = i;
                        break;
                    }
                }
                tbSolverName.Text = Component.Executable;
                numMemoryWords.Value = Component.MemorySizeWords;
                numNumOfCpus.Value = Component.NumOfCpus;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();
                tbArgumentToAdd.Text = Component.ArgumentsToAdd;

                // argument
                Component.FrameworkItemCollection = Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers();
                tbArgument.Text = Component.Argument;
                Component.FrameworkItemCollection = null;

                for (int i = 0; i < comboOutputFile.Items.Count; i++)
                {
                    if ((string)comboOutputFile.Items[i] == Component.OutputFileResourceName)
                    {
                        comboOutputFile.SelectedIndex = i;
                        break;
                    }
                }

                // fill DataGridView
                Item item;
                List<string> itemNames = Component.InputVariables;
                FileResource fileResource;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
           
            updateArgument = true;
            argument_Changed(null, null);
        }
        private void comboInputFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFileResources();
            argument_Changed(null, null);
        }
        private void comboOutputFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFileResources();
        }

        private void comboInputFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboReportFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboOdbReportType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void argument_Changed(object sender, EventArgs e)
        {
            try
            {
                if (updateArgument)
                {
                    LsDynaComponent ac = GetLsDynaComponent();
                    ac.FrameworkItemCollection = Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers();
                    tbArgument.Text = ac.Argument;
                }
            }
            catch
            {
            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Ls-Dyna solver (*.exe) | *.exe";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbSolverName.Text = openFileDialog1.FileName;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetLsDynaComponent();

                allFileResources = null;
                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private LsDynaComponent GetLsDynaComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            if (tbSolverName.Text == "") throw new Exception("Please enter the solver name.");
            if (!System.IO.File.Exists(tbSolverName.Text)) throw new Exception("The solver with given file name does not exist.");

            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            string outputFileResourceName = (string)comboOutputFile.SelectedItem;
            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            LsDynaComponent component = new LsDynaComponent(tbName.Text, tbDescription.Text, tbSolverName.Text, inputFileResourceName, outputFileResourceName, timeoutMilliSeconds);

            component.MemorySizeWords = (int)numMemoryWords.Value;
            component.NumOfCpus = (int)numNumOfCpus.Value;
            component.ArgumentsToAdd = tbArgumentToAdd.Text;

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    component.Add((string)row.Cells["colName"].Value);
                }
            }

            return component;
        }
        public bool DoAllItemsExist(PairsCollection pairs)
        {
            FileResource[] fileResources = pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            int numOfInputFiles = 0;
            int numOfOutputFiles = 0;
            foreach (FileResource fileResource in fileResources)
            {
                if (fileResource.IOType == FileIOType.Input)
                {
                    extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".dyn" || extension == ".k") numOfInputFiles++;
                }
                else if (fileResource.IOType == FileIOType.Output)
                {
                    numOfOutputFiles++;
                }
            }
            if (numOfInputFiles <= 0 || numOfOutputFiles <= 0) return false;
            else return true;
        }
        override protected void ResetForm()
        {
            updateArgument = false;
            // clear all control resources
            comboInputFile.Items.Clear();
            comboOutputFile.Items.Clear();
            dgvFiles.Rows.Clear();

            // fill combo box with *.dyn and *.k file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            foreach (FileResource allFileResource in allFileResources)
            {
                if (allFileResource.IOType == FileIOType.Input || allFileResource.IOType == FileIOType.Constant || allFileResource.IOType == FileIOType.Copy)
                {
                    extension = System.IO.Path.GetExtension(allFileResource.FileNameWithoutPath).ToLower();
                    if (extension == ".dyn" || extension == ".k") comboInputFile.Items.Add(allFileResource.Name);
                }
                else if (allFileResource.IOType == FileIOType.Output)
                {
                    comboOutputFile.Items.Add(allFileResource.Name);
                }
            }
            if (comboInputFile.Items.Count > 0) comboInputFile.SelectedIndex = 0;
            if (comboOutputFile.Items.Count > 0) comboOutputFile.SelectedIndex = 0;

            // fill combo box and dataGridView with all file resources
            ResetFileResources();

            tbName.Text = "";
            tbDescription.Text = "";
            if (comboInputFile.Items.Count > 0) comboInputFile.SelectedIndex = 0;
            tbSolverName.Text = "";
            numMemoryWords.Value = 250000;
            numNumOfCpus.Value = 1;
            tbTimeout.Text = "5";
            tbArgumentToAdd.Text = "";
            tbArgument.Text = "";

            updateArgument = true;
            argument_Changed(null, null);
        }
        private void ResetFileResources()
        {
            string inputFileResourceName = (string)comboInputFile.SelectedItem;
            string outputFileResourceName = (string)comboOutputFile.SelectedItem;

            Dictionary<string, bool> prevStates = new Dictionary<string, bool>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                prevStates.Add((string)row.Cells["colName"].Value, (bool)row.Cells["colInclude"].Value);
            }

            // hide input and report file resources
            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.Name != inputFileResourceName && fileResource.Name != outputFileResourceName)
                    dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }

            // reset previous states
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (prevStates.ContainsKey((string)row.Cells["colName"].Value))
                {
                    row.Cells["colInclude"].Value = prevStates[(string)row.Cells["colName"].Value];
                }
            }
        }

       

        

      

      

       



       

        
    }
}
