﻿namespace OptiMax
{
    partial class frmDosComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.comboLocalExeFile = new System.Windows.Forms.ComboBox();
            this.rbUseNonLocalExe = new System.Windows.Forms.RadioButton();
            this.rbUseLocalExe = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTimeout = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbArgument = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tbExeName = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvFiles = new System.Windows.Forms.DataGridView();
            this.colInclude = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvEnvVariables = new System.Windows.Forms.DataGridView();
            this.colIncludeEnvVar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colNameEnvVar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValueEnvVar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnvVariables)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(420, 551);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(573, 75);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(147, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(120, 20);
            this.tbName.TabIndex = 1;
            this.tbName.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(147, 45);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(120, 20);
            this.tbDescription.TabIndex = 2;
            this.tbDescription.Text = "Description";
            // 
            // comboLocalExeFile
            // 
            this.comboLocalExeFile.Enabled = false;
            this.comboLocalExeFile.FormattingEnabled = true;
            this.comboLocalExeFile.Location = new System.Drawing.Point(147, 19);
            this.comboLocalExeFile.Name = "comboLocalExeFile";
            this.comboLocalExeFile.Size = new System.Drawing.Size(120, 21);
            this.comboLocalExeFile.TabIndex = 4;
            this.comboLocalExeFile.SelectedIndexChanged += new System.EventHandler(this.comboLocalExeFile_SelectedIndexChanged);
            this.comboLocalExeFile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboLocalExeFile_KeyPress);
            // 
            // rbUseNonLocalExe
            // 
            this.rbUseNonLocalExe.AutoSize = true;
            this.rbUseNonLocalExe.Checked = true;
            this.rbUseNonLocalExe.Location = new System.Drawing.Point(15, 47);
            this.rbUseNonLocalExe.Name = "rbUseNonLocalExe";
            this.rbUseNonLocalExe.Size = new System.Drawing.Size(126, 17);
            this.rbUseNonLocalExe.TabIndex = 5;
            this.rbUseNonLocalExe.TabStop = true;
            this.rbUseNonLocalExe.Text = "Use non-local exe file";
            this.rbUseNonLocalExe.UseVisualStyleBackColor = true;
            // 
            // rbUseLocalExe
            // 
            this.rbUseLocalExe.AutoSize = true;
            this.rbUseLocalExe.Location = new System.Drawing.Point(15, 20);
            this.rbUseLocalExe.Name = "rbUseLocalExe";
            this.rbUseLocalExe.Size = new System.Drawing.Size(105, 17);
            this.rbUseLocalExe.TabIndex = 3;
            this.rbUseLocalExe.Text = "Use local exe file";
            this.rbUseLocalExe.UseVisualStyleBackColor = true;
            this.rbUseLocalExe.CheckedChanged += new System.EventHandler(this.rbUseLocalExe_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(273, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "[min]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(96, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Timeout";
            // 
            // tbTimeout
            // 
            this.tbTimeout.Location = new System.Drawing.Point(147, 98);
            this.tbTimeout.Name = "tbTimeout";
            this.tbTimeout.Size = new System.Drawing.Size(120, 20);
            this.tbTimeout.TabIndex = 9;
            this.tbTimeout.Text = "5";
            this.tbTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Argument";
            // 
            // tbArgument
            // 
            this.tbArgument.Location = new System.Drawing.Point(147, 72);
            this.tbArgument.Name = "tbArgument";
            this.tbArgument.Size = new System.Drawing.Size(336, 20);
            this.tbArgument.TabIndex = 8;
            this.tbArgument.Text = "Argument";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(489, 44);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 6;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // tbExeName
            // 
            this.tbExeName.Location = new System.Drawing.Point(147, 46);
            this.tbExeName.Name = "tbExeName";
            this.tbExeName.Size = new System.Drawing.Size(336, 20);
            this.tbExeName.TabIndex = 7;
            this.tbExeName.Text = "File name";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOK.Location = new System.Drawing.Point(501, 551);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvFiles);
            this.groupBox2.Location = new System.Drawing.Point(12, 363);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 181);
            this.groupBox2.TabIndex = 48;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "File resources";
            // 
            // dgvFiles
            // 
            this.dgvFiles.AllowUserToAddRows = false;
            this.dgvFiles.AllowUserToDeleteRows = false;
            this.dgvFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInclude,
            this.colName,
            this.colDescription,
            this.colFileName});
            this.dgvFiles.Location = new System.Drawing.Point(6, 19);
            this.dgvFiles.MultiSelect = false;
            this.dgvFiles.Name = "dgvFiles";
            this.dgvFiles.RowHeadersVisible = false;
            this.dgvFiles.Size = new System.Drawing.Size(558, 156);
            this.dgvFiles.TabIndex = 48;
            // 
            // colInclude
            // 
            this.colInclude.HeaderText = "Include";
            this.colInclude.Name = "colInclude";
            this.colInclude.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colInclude.Width = 50;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.HeaderText = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            // 
            // colFileName
            // 
            this.colFileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colFileName.HeaderText = "File name";
            this.colFileName.Name = "colFileName";
            this.colFileName.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboLocalExeFile);
            this.groupBox3.Controls.Add(this.rbUseLocalExe);
            this.groupBox3.Controls.Add(this.rbUseNonLocalExe);
            this.groupBox3.Controls.Add(this.tbExeName);
            this.groupBox3.Controls.Add(this.btnBrowse);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.tbArgument);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbTimeout);
            this.groupBox3.Location = new System.Drawing.Point(12, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(573, 131);
            this.groupBox3.TabIndex = 46;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Executable";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgvEnvVariables);
            this.groupBox4.Location = new System.Drawing.Point(12, 230);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(573, 127);
            this.groupBox4.TabIndex = 49;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Environment variables (select entire row to delete it)";
            // 
            // dgvEnvVariables
            // 
            this.dgvEnvVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEnvVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEnvVariables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIncludeEnvVar,
            this.colNameEnvVar,
            this.colValueEnvVar});
            this.dgvEnvVariables.Location = new System.Drawing.Point(6, 19);
            this.dgvEnvVariables.MultiSelect = false;
            this.dgvEnvVariables.Name = "dgvEnvVariables";
            this.dgvEnvVariables.Size = new System.Drawing.Size(558, 102);
            this.dgvEnvVariables.TabIndex = 48;
            // 
            // colIncludeEnvVar
            // 
            this.colIncludeEnvVar.HeaderText = "Include";
            this.colIncludeEnvVar.Name = "colIncludeEnvVar";
            this.colIncludeEnvVar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIncludeEnvVar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIncludeEnvVar.Width = 50;
            // 
            // colNameEnvVar
            // 
            this.colNameEnvVar.HeaderText = "Name";
            this.colNameEnvVar.Name = "colNameEnvVar";
            this.colNameEnvVar.Width = 200;
            // 
            // colValueEnvVar
            // 
            this.colValueEnvVar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colValueEnvVar.HeaderText = "Value";
            this.colValueEnvVar.Name = "colValueEnvVar";
            // 
            // frmDosComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 586);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDosComponent";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Dos Component";
            this.Load += new System.EventHandler(this.frmDosComponent_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnvVariables)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbExeName;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbArgument;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTimeout;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboLocalExeFile;
        private System.Windows.Forms.RadioButton rbUseNonLocalExe;
        private System.Windows.Forms.RadioButton rbUseLocalExe;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colInclude;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFileName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvEnvVariables;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIncludeEnvVar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNameEnvVar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValueEnvVar;
    }
}