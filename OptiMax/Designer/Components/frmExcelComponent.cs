﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.Diagnostics;
using System.IO;

namespace OptiMax
{
    public partial class frmExcelComponent : FormWithItem, IFilesDirectory
    {
        // Variables                                                                                          


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ExcelComponent);
        }
        public ExcelComponent Component
        {
            get
            {
                return (ExcelComponent)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }


        // Constructors                                                                                       
        public frmExcelComponent()
        {
            InitializeComponent();

            toolStripMenuItems = new ToolStripMenuItem[2];
            toolStripMenuItems[0] = new ToolStripMenuItem();
            toolStripMenuItems[0].Name = "tsmiEditExcelFile";
            toolStripMenuItems[0].Size = new System.Drawing.Size(152, 22);
            toolStripMenuItems[0].Text = "Edit Excel file";
            toolStripMenuItems[0].Click += editExcelFileToolStripMenuItem_Click;

            toolStripMenuItems[1] = new ToolStripMenuItem();
            toolStripMenuItems[1].Name = "tsmiKillAllExcels";
            toolStripMenuItems[1].Size = new System.Drawing.Size(152, 22);
            toolStripMenuItems[1].Text = "Kill all Excel processes";
            toolStripMenuItems[1].Click += killAllExcelsToolStripMenuItem_Click;

            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmExcelComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in ExcelComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;
                
                tbExcelFileName.Text = Component.ExcelFile.FileNameWithPath;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();
                if (Component.NumOfConcurrentApps > 0)
                {
                    cbLimitNumOfConcurrentApps.Checked = true;
                    numNumOfConcurrent.Value = Component.NumOfConcurrentApps;
                }

                if (Component.InpVariableRangePairs != null)
                {
                    foreach (VariableExcelRangePair pair in Component.InpVariableRangePairs)
                    {
                        if (colName.Items.Contains(pair.Variable))
                            dgvInpVars.Rows.Add(new object[] { pair.Variable, pair.Range });
                        else MessageBox.Show("The variable '" + pair.Variable + "' does not exist anymore.", "Error");
                    }
                }
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            string path = null;
            if (Directory.Exists(tbExcelFileName.Text)) path = Path.GetDirectoryName(tbExcelFileName.Text);
            else path = FilesDirectory;

            if (path != null && Directory.Exists(path)) openFileDialog1.InitialDirectory = path;

            openFileDialog1.Filter = "Excel files (*.xls; *.xlsx; *.xlsm) | *.xls;*.xlsx;*.xlsm";
            openFileDialog1.FileName = tbExcelFileName.Text;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbExcelFileName.Text = openFileDialog1.FileName;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(tbExcelFileName.Text);
            }
            catch
            {
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            dgvInpVars.Rows.Add(new object[] {colName.Items[0], ""});
        }

        private void cbLimitNumOfConcurrentApps_CheckedChanged(object sender, EventArgs e)
        {
            numNumOfConcurrent.Enabled = cbLimitNumOfConcurrentApps.Checked;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetExcelComponent();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnKillAllExcels_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to kill all running Excel processes?", "Warning", MessageBoxButtons.YesNo) ==
                                System.Windows.Forms.DialogResult.Yes)
            {
                int count = 0;
                Process[] processes = Process.GetProcessesByName("EXCEL");
                foreach (Process p in processes)
                {
                    p.Kill();
                    count++;
                }

                if (count > 0) System.Threading.Thread.Sleep(1000);

                int countRemain = 0;
                processes = Process.GetProcessesByName("EXCEL");
                foreach (Process p in processes)
                {
                    countRemain++;
                }

                if (countRemain > 0)
                    MessageBox.Show("Attempted to killed " + count.ToString() + " Excel processes. " + countRemain.ToString() + " Excel processes still remainin",
                                    "Information", MessageBoxButtons.OK);
                else if (count > 0)
                    MessageBox.Show("Successfuly killed all (" + count.ToString() + ") Excel processes.", "Information", MessageBoxButtons.OK);
                else
                    MessageBox.Show("There are no running Excel processes to kill.", "Information", MessageBoxButtons.OK);
            }
        }

        void editExcelFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Component != null)
            {
                tbExcelFileName.Text = Component.ExcelFile.FileNameWithPath;
                btnEdit_Click(null, null);
            }
        }

        void killAllExcelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnKillAllExcels_Click(null, null);
        }
        

        // Methods                                                                                            
        private ExcelComponent GetExcelComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            if (tbExcelFileName.Text == "") throw new Exception("Please enter the excel file name.");
            if (!System.IO.File.Exists(tbExcelFileName.Text)) throw new Exception("The excel file '" + tbExcelFileName.Text + "' does not exist.");

            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            Dictionary<string, VariableExcelRangePair> valRangePairs = new Dictionary<string, VariableExcelRangePair>();
            string variable;
            string range;

            for (int i = 0; i < dgvInpVars.Rows.Count; i++)
            {
                variable = dgvInpVars[0, i].Value.ToString();
                range = dgvInpVars[1, i].Value.ToString();

                item = Pairs.GetItemByNameIncludingFromContainers(variable);
                if (item is VariableBase)
                {
                    if (!Helpers.ExcelHelper.IsThisSingleCellRangeValid(range))
                        throw new Exception("The cell range '" + range + "' is not a valid single cell range.");
                }
                else if (item is ArrayBase)
                {
                    if (!Helpers.ExcelHelper.IsThisArrayCellRangeValid(range))
                        throw new Exception("The cell range '" + range + "' is not a valid array cell range.");
                }

                if (valRangePairs.ContainsKey(variable))
                    throw new Exception("The variable '" + variable + "' is defined more than once.");

                valRangePairs.Add(variable, new VariableExcelRangePair(variable, range));
            }

            ExcelComponent component = new ExcelComponent(tbName.Text, tbDescription.Text, timeoutMilliSeconds, tbExcelFileName.Text, valRangePairs.Values.ToArray(), FilesDirectory);
            if (cbLimitNumOfConcurrentApps.Checked)
                component.NumOfConcurrentApps = (int)numNumOfConcurrent.Value;
            
            return component;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            dgvInpVars.Rows.Clear();

            Item[] items = Pairs.GetAllValueItemsIncludingFromContainers();
            colName.Items.Clear();
            foreach (var item in items)
            {
                colName.Items.Add(item.Name);
            }

            tbName.Text = "";
            tbDescription.Text = "";

            tbExcelFileName.Text = "";
            tbTimeout.Text = "5";
            cbLimitNumOfConcurrentApps.Checked = false;
            numNumOfConcurrent.Value = 4;
        }

      

       

       


        

      

      

       



       

        
    }
}
