﻿using System; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmDosComponent : FormWithItem
    {
        // Variables                                                                                          
        FileResource[] allFileResources;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(DosComponent);
        }
        public override string GetItemHelp()
        {
            return "Use 'Dos Component' to run any executable file during the job run.";
        }
        public DosComponent Component
        {
            get
            {
                return (DosComponent)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmDosComponent()
        {
            InitializeComponent();

            Component = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmDosComponent_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill in DosComponent values
            if (Component != null)
            {
                tbName.Text = Component.Name;
                tbDescription.Text = Component.Description;                
                tbArgument.Text = Component.Argument;
                tbTimeout.Text = (Component.TimeoutMilliSeconds / (60 * 1000)).ToString();

                // Environment variables
                if (Component.EnvironmentVariables != null)
                {
                    foreach (var envVariable in Component.EnvironmentVariables)
                    {
                        dgvEnvVariables.Rows.Add(new object[] { envVariable.Include, envVariable.Name, envVariable.Value });
                    }
                }

                FileResource fileResource;

                // select the appropriate item in comboLocalExeFile
                if (Component.UseLocalExe)
                {
                    rbUseLocalExe.Checked = true;
                    int index = 0;
                    foreach (string name in comboLocalExeFile.Items)
                    {
                        fileResource = (FileResource)Pairs.GetItemByNameIncludingFromContainers(name);

                        if (fileResource != null)
                        {
                            if (Component.Executable == fileResource.FileNameWithPath) break;
                            index++;
                        }
                    }
                    if (index >= comboLocalExeFile.Items.Count) index = 0;
                    comboLocalExeFile.SelectedIndex = index;
                    tbExeName.Text = "";

                    // comboLocalExeFile.SelectedIndex = index results in the selection of the index component in the dataGridView
                    foreach (DataGridViewRow row in dgvFiles.Rows)
                    {
                        row.Cells["colInclude"].Value = false;
                    }
                }
                else
                {
                    rbUseNonLocalExe.Checked = true;
                    if (comboLocalExeFile.Items.Count > 0) comboLocalExeFile.SelectedIndex = 0;
                    tbExeName.Text = Component.Executable;
                }

                // fill DataGridView
                Item item;
                List<string> itemNames = Component.InputVariables;
                foreach (var itemName in itemNames)
                {
                    item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                    if (item is FileResource)
                    {
                        fileResource = (FileResource)item;

                        if (fileResource != null)
                        {
                            foreach (DataGridViewRow row in dgvFiles.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == fileResource.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        private void comboLocalExeFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void rbUseLocalExe_CheckedChanged(object sender, EventArgs e)
        {
            comboLocalExeFile.Enabled = rbUseLocalExe.Checked;

            tbExeName.Enabled = rbUseNonLocalExe.Checked;
            btnBrowse.Enabled = rbUseNonLocalExe.Checked;

            comboLocalExeFile_SelectedIndexChanged(null, null);
        }
        private void comboLocalExeFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = (string)comboLocalExeFile.SelectedItem;
            ResetFileResources();
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Executable | *.exe;*.bat";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbExeName.Text = openFileDialog1.FileName;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Component = GetDosComponent();

                //allFileResources = null;
                Pairs = null;
                dgvFiles.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private DosComponent GetDosComponent()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Component) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            int timeoutMilliSeconds = int.Parse(tbTimeout.Text) * 60 * 1000;

            //timeoutMilliSeconds = 1;
            
            string exeFileName;
            if (rbUseLocalExe.Checked)
            {
                item = Pairs.GetItemByNameIncludingFromContainers((string)comboLocalExeFile.SelectedItem);
                if (item == null) throw new Exception("The local exe file name must be selected.");

                exeFileName = ((FileResource)item).FileNameWithPath;
            }
            else
            {
                exeFileName = tbExeName.Text;
            }

            List<EnvironmentVariable> environmentVariables = new List<EnvironmentVariable>();
            if (dgvEnvVariables.Rows.Count > 1)
            {
                EnvironmentVariable envVariable;
                foreach (DataGridViewRow row in dgvEnvVariables.Rows)
                {
                    if (row.Cells["colIncludeEnvVar"].Value != null)
                    {
                        envVariable = new EnvironmentVariable();
                        envVariable.Include = (bool)row.Cells["colIncludeEnvVar"].Value;
                        envVariable.Name = (string)row.Cells["colNameEnvVar"].Value;
                        envVariable.Value = (string)row.Cells["colValueEnvVar"].Value;

                        if (envVariable.Name == null || envVariable.Value == null || envVariable.Name == "" || envVariable.Value == "")
                            throw new Exception("The name or value of an environment variable are empty.");

                        environmentVariables.Add(envVariable);
                    }
                }
            }

            DosComponent dosComponent = new DosComponent(tbName.Text, tbDescription.Text, exeFileName, rbUseLocalExe.Checked, timeoutMilliSeconds, environmentVariables.ToArray());
            dosComponent.Argument = tbArgument.Text;
            
            string[] argumentParameters = dosComponent.ArgumentParameters;
            Item parItem;
            if (argumentParameters != null)
            {
                foreach (var parameter in argumentParameters)
                {
                    if (parameter == "workDirectory") continue;

                    parItem = Pairs.GetItemByNameIncludingFromContainers(parameter);
                    if (parItem != null) dosComponent.Add(parameter);
                    else throw new Exception("The parameter '" + parameter + "' used in argument does not exist.");
                }
            }

            if (rbUseLocalExe.Checked) dosComponent.Add((string)comboLocalExeFile.SelectedItem);

            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    dosComponent.Add((string)row.Cells["colName"].Value);
                }
            }
            
            return dosComponent;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            dgvEnvVariables.Rows.Clear();
            dgvFiles.Rows.Clear();

            comboLocalExeFile.Items.Clear();

            // fill combo box and dataGridView with all file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            string extension;
            foreach (FileResource fileResource in allFileResources)
            {
                extension = System.IO.Path.GetExtension(fileResource.FileNameWithoutPath).ToLower();
                if (extension == ".exe" || extension == ".bat") comboLocalExeFile.Items.Add(fileResource.Name);
            }

            ResetFileResources();

            if (comboLocalExeFile.Items.Count <= 0)
            {
                //MessageBox.Show("The workflow does not contain any *.exe or *.bat file resources to use as an component. First create appropriate file resource.", "Error", MessageBoxButtons.OK);
                //this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                //return;

                rbUseLocalExe.Enabled = false;
                rbUseLocalExe.Checked = false;
            }
            else
            {
                rbUseLocalExe.Enabled = true;
                rbUseLocalExe.Checked = true;
            }
            
            //if (dgvFiles.Rows.Count <= 0)
            //{
            //    MessageBox.Show("The workflow does not contain any file resources to use as an component. First create a file resource.", "Error", MessageBoxButtons.OK);
            //    this.DialogResult = System.Windows.Forms.DialogResult.Abort;
            //    return;
            //}


            tbName.Text = "";
            tbDescription.Text = "";
            if (comboLocalExeFile.Items.Count > 0) comboLocalExeFile.SelectedIndex = 0;
            tbExeName.Text = "";
            tbArgument.Text = "";
            tbTimeout.Text = "5";
        }
        private void ResetFileResources()
        {
            string localExeFileName = null;
            if (rbUseLocalExe.Checked) localExeFileName = (string)comboLocalExeFile.SelectedItem;

            Dictionary<string, bool> prevStates = new Dictionary<string, bool>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                prevStates.Add((string)row.Cells["colName"].Value, (bool)row.Cells["colInclude"].Value);
            }

            // hide input and report file resources
            dgvFiles.Rows.Clear();
            foreach (FileResource fileResource in allFileResources)
            {
                if (fileResource.Name != localExeFileName)
                    dgvFiles.Rows.Add(new object[] { false, fileResource.Name, fileResource.Description, fileResource.FileNameWithPath });
            }

            // reset previous states
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (prevStates.ContainsKey((string)row.Cells["colName"].Value))
                {
                    row.Cells["colInclude"].Value = prevStates[(string)row.Cells["colName"].Value];
                }
            }
        }
     

       

        
    }
}
