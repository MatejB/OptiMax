﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework;
using Dalssoft.DiagramNet;
using System.IO;

namespace OptiMax
{
    [Serializable]
    public class PairsCollection
    {
        // Variables                                                                                          
        private Dictionary<BaseElement, Item> element_item;
        private Dictionary<Item, BaseElement> item_element;

        // Constructors                                                                                       
        public PairsCollection()
        {
            element_item = new Dictionary<BaseElement, Item>();
            item_element = new Dictionary<Item, BaseElement>();

            HiddenElement element = new HiddenElement();
            VariableConstant variableConstant = new VariableConstant(OptiMaxFramework.Globals.JobIdName, "", VariableValueType.Integer);
            variableConstant.SetValue(-1);
            AddPair(element, variableConstant);
        }


        // Methods                                                                                            
        public void AddPair(BaseElement element, Item item)
        {
            element_item.Add(element, item);
            item_element.Add(item, element);
        }

        public void Clear()
        {
            element_item.Clear();
            item_element.Clear();
        }

        public Item GetItem(BaseElement element)
        {
            if (element_item.ContainsKey(element))
                return element_item[element];
            else return null;
        }

        public Item[] GetAllItems()
        {
            return item_element.Keys.ToArray();
        }

        public string[] GetAllItemNamesIncludingFromContainers()
        {
            List<string> itemNames = new List<string>();
            foreach (var item in item_element.Keys)
            {                
                if (item is ItemContainer)
                {
                    foreach (Item itemInContainer in ((ItemContainer)item).GetItems)
                    {
                        itemNames.Add(itemInContainer.Name);
                    }
                }
                else itemNames.Add(item.Name);
            }
            return itemNames.ToArray();
        }

        public Item[] GetAllValueItemsIncludingFromContainers()
        {
            List<Item> items = new List<Item>();
            foreach (var item in item_element.Keys)
            {
                if (item is VariableBase || item is OptiMaxFramework.ArrayBase) items.Add(item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is VariableBase || i is OptiMaxFramework.ArrayBase) items.Add(i);
                    }
                    
                }
            }
            return items.ToArray();
        }

        public Item[] GetAllRecordableItemsIncludingFromContainers()
        {
            List<Item> items = new List<Item>();
            foreach (var item in item_element.Keys)
            {
                if (item is IRecordable) items.Add(item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is IRecordable) items.Add(i);
                    }

                }
            }
            return items.ToArray();
        }

        public VariableBase[] GetAllVariablesIncludingFromContainers()
        {
            List<VariableBase> variables = new List<VariableBase>();
            foreach (var item in item_element.Keys)
            {
                if (item is VariableBase) variables.Add((VariableBase)item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is VariableBase) variables.Add((VariableBase)i);
                    }

                }
            }
            return variables.ToArray();
        }

        public Item[] GetAllInputItemsIncludingFromContainers()
        {
            List<Item> items = new List<Item>();
            foreach (var item in item_element.Keys)
            {
                if (item is VariableInput) items.Add(item);
                else if (item is ArrayInput) items.Add(item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is VariableInput) items.Add(i);
                        else if (i is ArrayInput) items.Add(i);
                    }
                }
            }
            return items.ToArray();
        }
        public ArrayInput[] GetAllInputArraysIncludingFromContainers()
        {
            List<ArrayInput> arrays = new List<ArrayInput>();
            foreach (var item in item_element.Keys)
            {
                if (item is ArrayInput) arrays.Add((ArrayInput)item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is ArrayInput) arrays.Add((ArrayInput)i);
                    }
                }
            }
            return arrays.ToArray();
        }

        public FileResource[] GetAllFileResourcesIncludingFromContainers()
        {
            List<FileResource> fileResources = new List<FileResource>();
            foreach (var item in item_element.Keys)
            {
                if (item is FileResource) fileResources.Add((FileResource)item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is FileResource) fileResources.Add((FileResource)i);
                    }
                }
            }
            return fileResources.ToArray();
        }

        public FileResource[] GetAllFileResourcesIncludingFromContainersExcept(string thisContainerName)
        {
            List<FileResource> fileResources = new List<FileResource>();
            foreach (var item in item_element.Keys)
            {
                if (item is FileResource) fileResources.Add((FileResource)item);
                else if (item is ItemContainer)
                {
                    if (item.Name != thisContainerName)
                    {
                        foreach (Item i in ((ItemContainer)item).GetItems)
                        {
                            if (i is FileResource) fileResources.Add((FileResource)i);
                        }
                    }
                }
            }
            return fileResources.ToArray();
        }

        public Component[] GetAllComponents()
        {
            List<Component> components = new List<Component>();
            foreach (var item in item_element.Keys)
            {
                if (item is Component) components.Add((Component)item);
            }
            return components.ToArray();
        }

        public Recorder[] GetAllRecorders()
        {
            List<Recorder> recorders = new List<Recorder>();
            foreach (var item in item_element.Keys)
            {
                if (item is Recorder) recorders.Add((Recorder)item);
            }
            return recorders.ToArray();
        }

        public ArrayBase[] GetAllArraysIncludingFromContainers()
        {
            List<ArrayBase> arrays = new List<ArrayBase>();
            foreach (var item in item_element.Keys)
            {
                if (item is ArrayBase) arrays.Add((ArrayBase)item);
                else if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i is ArrayBase) arrays.Add((ArrayBase)i);
                    }
                }
            }
            return arrays.ToArray();
        }

        public Driver GetDriver()
        {
            foreach (Item item in item_element.Keys)
            {
                if (item is Driver && !(item is BaseDriver)) return (Driver)item;
            }
            return null;
        }

        public Item GetItemByNameIncludingFromContainers(string name)
        {
            if (name == null) return null;

            name = name.Split(new string[] { OptiMaxFramework.Globals.InputArraySeparator }, StringSplitOptions.None)[0];
            foreach (Item item in item_element.Keys)
            {
                if (item.Name == name) return item;
                else if (item is ItemContainer ic)
                {
                    foreach (Item itemEntry in ic.GetItems)
                    {
                        if (itemEntry.Name == name) return itemEntry;
                    }
                }
            }
            return null;
        }

        public Item GetItemByNameIncludingFromContainersExcept(string name, string thisContainerName)
        {
            foreach (Item item in item_element.Keys)
            {
                if (item.Name == name) return item;
                else if (item is ItemContainer && item.Name != thisContainerName)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        if (i.Name == name) return i;
                    }
                }
            }
            return null;
        }

        public BaseElement GetElementByItemName(string name)
        {
            foreach (Item item in item_element.Keys)
            {
                if (item.Name == name) return item_element[item];

                if (item is ItemContainer)
                {
                    foreach (var v in ((ItemContainer)item).GetItems)
                    {
                        if (v.Name == name) return item_element[item];
                    }
                }
            }
            return null;
        }

        public BaseElement GetElement(Item item)
        {
            BaseElement be = null;


            List<ItemContainer> containers = new List<ItemContainer>();

            foreach (var entry in item_element)
            {
                if (entry.Key is ItemContainer ic)
                {
                    containers.Add(ic);
                }
            }

            if (item_element.ContainsKey(item)) be = item_element[item];
            else
            {
                foreach (var entry in item_element)
                {
                    if (entry.Key is ItemContainer ic)
                    {
                        if (ic.GetItems.Contains(item))
                        {
                            be = entry.Value;
                            break;
                        }
                    }
                }


                //foreach (Item i in item_element.Keys)
                //{
                //    if (i is ItemContainer)
                //    {
                //        if (((ItemContainer)i).GetItems.Contains(item))
                //            be = item_element[i];
                //    }
                //}
            }
            return be;
        }

        private void UpdateItem(BaseElement element, Item newItem)
        {
            Item oldItem = element_item[element];
            element_item[element] = newItem;

            item_element.Remove(oldItem);
            item_element.Add(newItem, element);
        }

        public void UpdateItem(string oldItemName, Item newItem)
        {
            Item oldItem = null;
            BaseElement element = null;
            ItemContainer itemContainer = null;
            foreach (var entry in item_element)
            {
                if (entry.Key.Name == oldItemName)
                {
                    oldItem = entry.Key;
                    element = entry.Value;
                }
                else if (entry.Key is ItemContainer ic)
                {
                    foreach (var itemEntry in ic.GetItems)
                    {
                        if (itemEntry.Name == oldItemName)
                        {
                            itemContainer = ic;
                            oldItem = itemEntry;
                            element = entry.Value;
                            break;
                        }
                    }
                }
            }

            if (itemContainer != null)
            {
                itemContainer.Replace(oldItem, newItem);
            }
            else
            {
                UpdateItem(element, newItem);
            }
        }

        public void Remove(BaseElement element)
        {
            if (element_item.ContainsKey(element))
            {
                Item itemToDelete = element_item[element];
                element_item.Remove(element);
                item_element.Remove(itemToDelete);
            }
        }

        public Dictionary<string, Item> GetItemNameItemPairsDictionary()
        {
            Dictionary<string, Item> frameworkItems = new Dictionary<string, Item>();

            foreach (Item item in item_element.Keys)
            {
                frameworkItems.Add(item.Name, item);
            }

            return frameworkItems;
        }

        public Dictionary<string, Item> GetItemNameItemPairsDictionaryIncludingFromContainers()
        {
            Dictionary<string, Item> frameworkItems = new Dictionary<string, Item>();

            foreach (Item item in item_element.Keys)
            {
                if (item is ItemContainer)
                {
                    foreach (Item i in ((ItemContainer)item).GetItems)
                    {
                        frameworkItems.Add(i.Name, i);
                    }
                }
                else frameworkItems.Add(item.Name, item);
            }

            return frameworkItems;
        }

        //                                                                                           
        public void SetNewFilesDirectory(string oldFilesDirectory, string newFilesDirectory)
        {
            // file resources are allways in /Files subdirectory - create Combine
            string directory;
            BaseElement el;
            FileResource[] fileResources = GetAllFileResourcesIncludingFromContainers();
            for (int i = 0; i < fileResources.Length; i++)
            {
                if (fileResources[i].IOType == FileIOType.Input || fileResources[i].IOType == FileIOType.Constant)
                {
                    directory = Path.GetDirectoryName(fileResources[i].FileNameWithPath) + "\\";

                    if (directory.StartsWith(oldFilesDirectory + "\\"))
                        fileResources[i].FileNameWithPath = fileResources[i].FileNameWithPath.Replace(oldFilesDirectory + "\\", newFilesDirectory + "\\");
                }

                el = GetElement(fileResources[i]);  // this must be done before the FileNameWith path is changed
                if (el != null)
                    UpdateItem(fileResources[i].Name, fileResources[i]);
                else
                    System.Windows.Forms.MessageBox.Show("The base element with the file resource named '" + fileResources[i].Name + "' can not be found.", "Error");
            }

            ExcelComponent excel;
            DosComponent dos;
            Component[] components = GetAllComponents();
            foreach (Component component in components)
            {
                if (component is ExcelComponent)
                {
                    excel = component as ExcelComponent;
                    directory = Path.GetDirectoryName(excel.ExcelFile.FileNameWithPath) + "\\";
                    if (directory.StartsWith(oldFilesDirectory + "\\"))
                        excel.ExcelFile.FileNameWithPath = excel.ExcelFile.FileNameWithPath.Replace(oldFilesDirectory + "\\", newFilesDirectory + "\\");
                }
                else if (component is DosComponent) // Dos, Abaqus, LS-Dyna
                {
                    dos = component as DosComponent;
                    directory = Path.GetDirectoryName(dos.Executable) + "\\";
                    if (directory.StartsWith(oldFilesDirectory + "\\"))
                        dos.Executable = dos.Executable.Replace(oldFilesDirectory + "\\", newFilesDirectory + "\\");
                }
            }
            
        }
    }
}
