﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dalssoft.DiagramNet;
using OptiMaxFramework;
using System.Reflection;
using System.IO;

namespace OptiMax
{
    public partial class OptiMaxDesigner : Dalssoft.DiagramNet.Designer
    {
        // Variables                                                                                          
        private PairsCollection pairsCollection;
        private Point newElementPosition;
        private Framework framework;
        private BaseElement rightClickedElement;
        private bool deletingAllElements;
        private Point mousePositionAtRightClick;

        // Properties                                                                                         
        public Framework Framework 
        {
            get
            {
                return framework;
            }
            set
            {
                framework = value;
            }
        }

        public Framework AssembledFramework
        {
            get
            {
                if (framework != null)
                {
                    framework.Clear();

                    foreach (Item item in pairsCollection.GetAllItems())
                    {
                        if (item.Name != OptiMaxFramework.Globals.baseDriverName)
                            framework.Add(item);
                    }

                    return framework;
                }
                return null;
            }
        }


        // Constructors                                                                                       
        public OptiMaxDesigner()
        {
            LockEditMode = true;
            LockLabel = true;
            LockMove = false;
            LockDeleteLinks = true;
            SnapToGrid = true;
            deletingAllElements = false;

            InitializeComponent();

            InitializeVariablesAndForms();
        }


        // Events                                                                                             
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Document.SelectedElements.Count <= 0) return;

                bool delete = true;
                foreach (BaseElement el in Document.SelectedElements)
	            {
                    if (el is RectangleNode)
                    {
                        if (((RectangleNode)el).Label.Text.StartsWith(OptiMaxFramework.Globals.baseDriverName))
                        {
                            delete = false;
                            break;
                        }
                    }
	            }

                if (delete)
                {
                    if (MessageBox.Show("Are you sure you want to delete the selected item/items?", "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        base.OnKeyDown(e);
                }
                else MessageBox.Show("The selected element '" + OptiMaxFramework.Globals.baseDriverName + "' can not be deleted.", "Error", MessageBoxButtons.OK);
            }
            else if (e.KeyCode == Keys.C && e.Modifiers == Keys.Control)
            {
                tsmiCopyItem_Click(null, null);
            }
            else if (e.KeyCode == Keys.V && e.Modifiers == Keys.Control)
            {
                // reset to 0
                mousePositionAtRightClick = this.PointToClient(MousePosition);
                tsmiPaste_Click(null, null);
            }
        }


        // Event handling                                                                                     
        private void OptiMaxDesigner_ElementMouseDrag(object sender, ElementMouseEventArgs e)
        {
            RegenerateLinks();
        }
        void element_RightClicked(object sender, EventArgs e)
        {
            FormWithItem form = null;
            rightClickedElement = null;

            try
            {
                rightClickedElement = (BaseElement)sender;
                if (rightClickedElement != null)
                {
                    form = GetFormFromElement(rightClickedElement, pairsCollection.GetItem(rightClickedElement));
                    if (form != null)
                    {
                        this.ContextMenuStrip = new ContextMenuStrip();
                        this.ContextMenuStrip.Items.Add(tsmiEditItem);

                        if (form.GetContextMenuItems != null)
                        {
                            foreach (ToolStripMenuItem conMenStrip in form.GetContextMenuItems)
                            {
                                this.ContextMenuStrip.Items.Add(conMenStrip);
                            }

                        }
                        // Copy
                        if (!(pairsCollection.GetItem(rightClickedElement) is Driver))
                        {
                            this.ContextMenuStrip.Items.Add(tsmiLine1);
                            this.ContextMenuStrip.Items.Add(tsmiCopyItem);
                        }
                        // Delete
                        this.ContextMenuStrip.Items.Add(tsmiLine2);
                        this.ContextMenuStrip.Items.Add(tsmiDeleteElement);
                    }
                    else throw new Exception("The edit form of the seleced element is not found.");

                    if (!Document.SelectedElements.Contains(rightClickedElement))
                    {
                        Document.ClearSelection();
                        Document.SelectElement(rightClickedElement);
                    }
                }
            }
            catch
            {

            }
            finally
            {
            }
        }
        void element_DoubleClicked(object sender, EventArgs e)
        {
            BaseElement element = (BaseElement)sender;
            LabelElement le = ((ILabelElement)element).Label;
            Item item = pairsCollection.GetItem(element);

            if (EditElement(item, element, le) == true)
            {
                element.BorderColor = Color.Black;
                element.BorderWidth = 1;
                Invalidate();
            }

            RegenerateLinks();
        }
        void element_Deleted(object sender, EventArgs e)
        {
            BaseElement be = (BaseElement)sender;
        
            pairsCollection.Remove(be);

            if (!deletingAllElements) RegenerateLinks();
        }
        private void OptiMaxDesigner_MouseUp(object sender, MouseEventArgs e)
        {
            ContextMenuStrip = contextMenuMain;
            tsmiPaste.Enabled = Clipboard.ContainsData(Globals.ClipboardFormat);

            if (e.Button == MouseButtons.Right)
            {
                mousePositionAtRightClick = e.Location;
            }
            else
            {
                // reset to 0
                mousePositionAtRightClick = new Point(0, 0);
            }
        }
        private bool EditElement(Item item, BaseElement element, LabelElement le)
        {
            try
            {
                string oldItemName = item.Name;
                FormWithItem form = GetFormFromElement(element, item);
                
                if (form != null && form.ShowDialog() == DialogResult.OK)
                {
                    pairsCollection.UpdateItem(oldItemName, form.Item);
                    le.Text = form.Item.Name;
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                // this should be properly coded
                throw new Exception(ex.Message);
            }
            finally
            {
                Document.ClearSelection();
                Application.DoEvents();
                this.Invalidate();
            }
        }

        
        // Methods                                                                                            
        private void InitializeVariablesAndForms()
        {
            pairsCollection = new PairsCollection();
            newElementPosition = new Point(10, 10);
            framework = null;

            //frm_VariableInput = new frmVariableInput();
            //frm_VariableConstant = new frmVariableConstant();
            //frm_VariableOutput = new frmVariableOutput();
            //frm_VariableFunction = new frmVariableFunction();
            //frm_VariableFromArray = new frmVariableFromArray();
            //frm_VariablesContainer = new frmVariablesContainer();
            ////
            //frm_ArrayConstant = new frmArrayConstant();
            //frm_ArrayOutput = new frmArrayOutput();
            //frm_ArrayFunction = new frmArrayFunction();
            //frm_ArrayOperation = new frmArrayOperation();
            ////
            //frm_FileResource = new frmFileResource();
            ////
            //frm_DosComponent = new frmDosComponent();
            //frm_AbaqusComponent = new frmAbaqusComponent();
            //frm_LsDynaComponent = new frmLsDynaComponent();
            //frm_QsubComponent = new frmQsubComponent();
            //frm_ExcelComponent = new frmExcelComponent();
            ////
            //frm_BaseDriver = new frmBaseDriver();
            //frm_GADriver = new frmGADriver();
            //frm_DOEDriver = new frmDOEDriver();
            //frm_NMDriver = new frmNMDriver();
            //frm_LMDriver = new frmLMDriver();
            //frm_AMQDriver = new frmAMQDriver();
            ////
            //frm_FileRecorder = new frmFileRecorder();
            //frm_CsvRecorder = new frmCSVRecorder();
            //frm_MemRecorder = new frmMemRecorder();
        }
        private void AddBaseDriver()
        {
            pairsCollection.AddPair(new RectangleElement(), new BaseDriver());
        }
        public void AddElement(BaseElement element, Item item, bool showForm = true)
        {
            try
            {
                LabelElement le = ((ILabelElement)element).Label;
                FormWithItem form = GetFormFromElement(element, item);
                
                if (item is Driver && pairsCollection.GetDriver() != null)
                {
                    MessageBox.Show("The workflow already has one driver. First delete it to create a new one.");
                    item = null;    // this prevents the execution of the code after finally
                    return;
                }

                if (showForm)
                {
                    if (form != null && form.ShowDialog() == DialogResult.OK)
                        item = form.Item;
                    else
                        item = null;
                }

                if (item != null)
                {
                    le.Text = item.Name;
                    le.Font = this.Font;    // font is set int the frmMain ob thi scontrol

                    if (element.Location.X == 0 && element.Location.Y == 0)
                    {
                        newElementPosition.X = 32 + 16;
                        newElementPosition.Y = 32 + 16;
                    }
                    else
                    {
                        newElementPosition.X = element.Location.X / 16 * 16;
                        newElementPosition.Y = element.Location.Y / 16 * 16;

                        if (newElementPosition.X < 0) newElementPosition.X = 0;
                        else if (newElementPosition.X > Width) newElementPosition.X = Width - 32;

                        if (newElementPosition.Y < 0) newElementPosition.Y = 0;
                        else if (newElementPosition.Y > Height) newElementPosition.Y = Height - 32;
                    }
                    element.Location = newElementPosition;

                    element.RightClicked += element_RightClicked;
                    element.DoubleClicked += element_DoubleClicked;
                    element.Deleted += element_Deleted;
                    this.Document.AddElement(element);
                    
                    pairsCollection.AddPair(element, item);
                    RegenerateLinks();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in 'OptiMaxDesigner' method AddElement()." + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK);
            }
            finally
            {
                if (item != null && element != null)
                {
                    Document.ClearSelection();
                    Document.SelectElement(element);
                    this.InvalidateBase();
                }
            }
        }
        public void Clear()
        {
            Document.SelectAllElements();
            deletingAllElements = true;
            Document.DeleteSelectedElements(true);
            deletingAllElements = false;

            InitializeVariablesAndForms();

            AddBaseDriver();
        }
        public void SaveToFile(string fileName, Size frmSize, DataTable memRecords, ZedGraph.GraphPane graph, SortedList<int, Point> selectedCells)
        {
            List<object> objects = new List<object>();
            objects.Add(this.Document);
            objects.Add(pairsCollection);
            objects.Add(newElementPosition);
            objects.Add(framework);

            objects.Add(frmSize);
            objects.Add(memRecords);
            objects.Add(graph);
            objects.Add(selectedCells);
            
            objects.DumpToFile(fileName);

            Document.ClearSelection();
            this.Invalidate();

            Bitmap b = new Bitmap(this.Width, this.Height);
            this.DrawToBitmap(b, new Rectangle(0, 0, b.Width, b.Height));
            b.Save(Path.Combine(Path.GetDirectoryName(fileName),  Path.GetFileNameWithoutExtension(fileName) + ".png"), System.Drawing.Imaging.ImageFormat.Png);
        }
        public bool LoadFromFile(string fileName, ref Size frmSize, ref DataTable memRecords, ref ZedGraph.GraphPane graph, ref SortedList<int, Point> selctedCells)
        {
            try
            {
                List<object> objects = null;
                objects = objects.LoadDumpFromFile(fileName);

                this.SetDocument((Document)objects[0]);
                this.pairsCollection = (PairsCollection)objects[1];
                this.newElementPosition = (Point)objects[2];
                this.framework = (Framework)objects[3];
                if (framework != null && framework.IsRunning) framework.Kill();

                if (objects.Count > 4) frmSize = (Size)objects[4];
                if (objects.Count > 5) memRecords = (DataTable)objects[5];
                if (objects.Count > 6) graph = (ZedGraph.GraphPane)objects[6];
                if (objects.Count > 7) selctedCells = (SortedList<int, Point>)objects[7];

                //if (changeDirectory)
                //{
                //    string newProjectDirectory = System.IO.Path.GetDirectoryName(fileName);
                //    pairsCollection.ChangeProjectDirectory(framework.ProjectDirectory, newProjectDirectory);
                //    framework.ChangeProjectDirectory(newProjectDirectory);
                //}

                LabelElement le;
                ElementCollection ec = this.Document.Elements;
                foreach (BaseElement element in ec)
                {
                    element.RightClicked += element_RightClicked;
                    element.DoubleClicked += element_DoubleClicked;
                    element.Deleted += element_Deleted;

                    le = ((ILabelElement)element).Label;
                    le.Font = this.Font;    // font is set in the frmMain ob thi scontrol
                }

                RegenerateLinks();

                this.Invalidate();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in OptiMaxDesigner method LoadFormFile(" + fileName + ")." + Environment.NewLine + ex.Message);
            }
        }
        public bool CheckFrameworkValidity()
        {
            try
            {
                bool valid = true;
                List<string> parents;
                Item parentItem;
                BaseElement parentBe;

                // Dataflow
                Dictionary<string, Item> items = pairsCollection.GetItemNameItemPairsDictionaryIncludingFromContainers();
                foreach (var item in items.Values)
                {
                    if (item is IDependent)
                    {
                        parents = ((IDependent)item).InputVariables;

                        if (parents != null)
                        {
                            foreach (var itemName in parents)
                            {
                                parentItem = pairsCollection.GetItemByNameIncludingFromContainers(itemName);

                                if (parentItem == null)
                                {
                                    valid = false;
                                    parentBe = pairsCollection.GetElementByItemName(item.Name);

                                    if (parentBe.BorderColor != Color.Red)
                                    {
                                        parentBe.BorderColor = Color.Red;
                                        parentBe.BorderWidth = 4;
                                        Invalidate();

                                        MessageBox.Show("The item '" + itemName + "' referenced in item '" + item.Name + "' does not exist.\n" +
                                                        "Please update the item '" + item.Name + "'.", "Error");
                                    }
                                    break;
                                }
                            }
                            
                        }
                    }
                }

                // Workflow
                Item childItem;
                Driver driver = pairsCollection.GetDriver();
                if (driver != null)
                {
                    // Parameters
                    var children = from p in driver.Parameters 
                                   select p.Name;

                    parentBe = pairsCollection.GetElement(driver);

                    foreach (string childName in children)
                    {
                        childItem = pairsCollection.GetItemByNameIncludingFromContainers(childName);
                        if (childItem == null)
                        {
                            valid = false;

                            if (parentBe.BorderColor != Color.Red)
                            {
                                parentBe.BorderColor = Color.Red;
                                parentBe.BorderWidth = 4;
                                Invalidate();

                                MessageBox.Show("The parameter '" + childName + "' referenced in driver does not exist.\n" +
                                                "Please update the driver.", "Error");
                            }
                            break;
                        }
                    }

                    // Driver
                    children = driver.Workflow.InputVariables;
                    parentBe = pairsCollection.GetElement(driver);
                    
                    // check components which are defined inside workflow
                    if (parentBe.BorderColor != Color.Red)
                    {
                        foreach (string childName in children)
                        {
                            childItem = pairsCollection.GetItemByNameIncludingFromContainers(childName);
                            if (childItem == null)
                            {
                                valid = false;
                                parentBe.BorderColor = Color.Red;
                                parentBe.BorderWidth = 4;
                                Invalidate();

                                MessageBox.Show("The item '" + childName + "' referenced in driver does not exist.\n" +
                                                "Please update the driver.", "Error");
                                break;
                            }
                        }
                    }

                    // Check objectives and constraint (they are not IDependent)
                    if (parentBe.BorderColor != Color.Red)
                    {
                        bool correctlyDefined = true;
                        try { correctlyDefined = driver.CorrectlyDefined(pairsCollection.GetItemNameItemPairsDictionaryIncludingFromContainers()); }
                        catch { correctlyDefined = false; } // silently catch thrown exceptions

                        if (!correctlyDefined)
                        {
                            valid = false;
                            parentBe.BorderColor = Color.Red;
                            parentBe.BorderWidth = 4;
                            Invalidate();
                            //MessageBox.Show("The driver is not correctly defined.\nPlease update the driver.", "Error");
                        }
                    }
                }

                return valid;
            }
            catch (Exception ex)
            {
                throw new Exception(OptiMaxFramework.ExceptionTools.FormatException(this, ex));
            }
        }
        private void RegenerateLinks()
        {
            try
            {
                if (!CheckFrameworkValidity()) return;

                Document.DeleteAllLinks();
                Document.LinkType = LinkType.RightAngle;

                bool switchElements;
                List<string> parents;
                Item parentItem;
                BaseElement parentEl;
                BaseElement childEl;
                
                Dictionary<string, bool> links = new Dictionary<string, bool>();
                string key;

                // Dataflow
                Dictionary<string, Item> items = pairsCollection.GetItemNameItemPairsDictionary();
                foreach (var item in items.Values)
                {
                    if (item is IDependent && !(item is Recorder))
                    { 
                        childEl = pairsCollection.GetElement(item);
                        parents = ((IDependent)item).InputVariables;

                        if (parents != null)
                        {
                            foreach (var itemName in parents)
                            {
                                switchElements = false;
                                parentItem = pairsCollection.GetItemByNameIncludingFromContainers(itemName);

                                if (parentItem == null)
                                {
                                    MessageBox.Show("The item named '" + itemName + "' does not exist anymore.", "Error");
                                }
                                else
                                {
                                    parentEl = pairsCollection.GetElement(parentItem);
                                    // in case of an container the container can point to it self
                                    if (childEl == parentEl) continue;

                                    // switch the arrow direction for the output file resource
                                    if (item is Component && parentItem is FileResource)
                                    {
                                        FileResource fr = (FileResource)parentItem;
                                        if (fr.IOType == FileIOType.Output) switchElements = true;
                                    }

                                    key = childEl.GetHashCode().ToString() + "_" + parentEl.GetHashCode().ToString();
                                    if (links.ContainsKey(key)) continue;
                                    key = parentEl.GetHashCode().ToString() + "_" + childEl.GetHashCode().ToString();
                                    if (links.ContainsKey(key)) continue;

                                    if (switchElements) AddDirectedLink(childEl, parentEl, Color.Black);
                                    else AddDirectedLink(parentEl, childEl, Color.Black);

                                    key = childEl.GetHashCode().ToString() + "_" + parentEl.GetHashCode().ToString();
                                    links.Add(key, true);
                                    key = parentEl.GetHashCode().ToString() + "_" + childEl.GetHashCode().ToString();
                                    links.Add(key, true);
                                }
                            }
                        }
                    }
                }

                // Workflow
                Driver driver = pairsCollection.GetDriver();
                Recorder[] recorders = pairsCollection.GetAllRecorders();

                if (driver != null)
                {
                    List<string> children = driver.Workflow.InputVariables;
                    parentEl = pairsCollection.GetElement(driver);

                    foreach (string childName in children)
                    {
                        childEl = pairsCollection.GetElementByItemName(childName);
                        AddDirectedLink(parentEl, childEl, Color.Red, 2);

                        parentEl = childEl;
                    }
                    
                    foreach (Recorder rec in recorders)
                    {
                        childEl = pairsCollection.GetElement(rec);
                        AddDirectedLink(parentEl, childEl, Color.Red, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in OptiMaxDesigner method RegenerateLinks()." + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK);
            }
            finally
            {
                Invalidate();
            }
        }
        private void AddDirectedLink(BaseElement parent, BaseElement child, Color color, int lineWidth = 1)
        {
            // Connector locations  
            //  ---------0----------
            //  |                  |
            //  2                  3
            //  |                  |
            //  ---------1----------

            if (parent is HiddenElement || child is HiddenElement) return;

            NodeElement parentNode = (NodeElement)parent;
            NodeElement childNode = (NodeElement)child;

            if (parentNode != null && childNode != null)
            {
                int childConnID;
                int parentConnID;

                double dx = child.Location.X - parent.Location.X;
                double dy = child.Location.Y - parent.Location.Y;
                if (dx == 0) dx = 0.00001;

                double k = (dy) / (dx);

                if (k < -1 || k > 1)
                {
                    // vertical
                    if (dy > 0)
                    {
                        // child is higher
                        childConnID = 0;
                        parentConnID = 1;
                    }
                    else
                    {
                        // parent is higher
                        childConnID = 1;
                        parentConnID = 0;
                    }
                }
                else
                {
                    // horizontal
                    if (dx > 0)
                    {
                        // child is more right
                        childConnID = 2;
                        parentConnID = 3;
                    }
                    else
                    {
                        // parent is more right
                        childConnID = 3;
                        parentConnID = 2;
                    }
                }
                Document.AddLink(parentNode.Connectors[parentConnID], childNode.Connectors[childConnID], color, lineWidth);
            }
        }
        public FormWithItem GetFormFromElement(BaseElement element, Item item)
        {
            FormWithItem form;
            foreach (Type mytype in System.Reflection.Assembly.GetExecutingAssembly().GetTypes())
            {
                if (mytype.ToString() == element.PropertieFormTypeName)
                {
                    form = (FormWithItem)Activator.CreateInstance(mytype);
                    form.Item = item;
                    form.Pairs = pairsCollection;

                    // Exceptions
                    if (form is IFilesDirectory) ((IFilesDirectory)form).FilesDirectory = framework.FilesDirectory;
                    if (form.Item is CsvRecorder) ((CsvRecorder)form.Item).ResultsDirectory = framework.ResultsDirectory;

                    return form;
                }
            }
            return null;
        }


        // Context menus
        private void tsmiPaste_Click(object sender, EventArgs e)
        {
            try
            {
                tsmiPaste.Enabled = false;                

                if (Clipboard.ContainsData(Globals.ClipboardFormat))
                {
                    tsmiPaste.Enabled = true;

                    object tmp = null;
                    for (int i = 0; i < 5; ++i)
                    {
                        tmp = Clipboard.GetData(Globals.ClipboardFormat);
                        if (!(tmp is ClipboardData[])) System.Threading.Thread.Sleep(100);
                    }

                    ClipboardData[] data = (ClipboardData[])tmp;
                    BaseElement[] elementToSelect = new BaseElement[data.Length];
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (data[i].Item == null) continue;

                        // try for each item
                        try
                        {
                            string[] itemNames = pairsCollection.GetAllItemNamesIncludingFromContainers();

                            string itemName = data[i].Item.Name + "_Copy";
                            int count = 1;
                            while (itemNames.Contains(itemName + count)) count++;
                            data[i].Item.Name = itemName + count;

                            //
                            RectangleIconNode el = new RectangleIconNode(data[i].Icon, 0, 0, data[i].Size.Width, data[i].Size.Height);
                            el.Label.AutoSize = true;
                            el.Label.Text = "";
                            el.Label.Font = new Font(FontFamily.GenericSansSerif, 10);
                            el.Label.DoAutoSize();
                            el.Location = new Point(mousePositionAtRightClick.X + data[i].Location.X, mousePositionAtRightClick.Y + data[i].Location.Y);
                            el.PropertieFormTypeName = data[i].PropertyFormTypeName;

                            elementToSelect[i] = el;
                            AddElement(el, data[i].Item, false);
                        }
                        catch
                        { }
                    }

                    Document.ClearSelection();
                    Document.SelectElements(elementToSelect);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("It is not possible to paste." + Environment.NewLine + ex.Message);
            }
            
        }
        private void openFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CsvRecorder csv = (CsvRecorder)rightClicked;
            //string path = Path.Combine(framework.ResultsDirectory, csv.Name);
            //if (path != null && Directory.Exists(path))
            //{
            //    System.Diagnostics.Process.Start(path);
            //}
            //else MessageBox.Show("The results directory does not exists.", "Error", MessageBoxButtons.OK);
        }
        private void opencsvFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CsvRecorder csv = (CsvRecorder)rightClicked;
            //string path = Path.Combine(framework.ResultsDirectory, csv.Name);
            //string file = Path.Combine(path, csv.Name + ".csv");
            //if (file != null && System.IO.File.Exists(file))
            //    System.Diagnostics.Process.Start(file);
            //else MessageBox.Show("The results file does not exists.", "Error", MessageBoxButtons.OK);
        }
        private void tsmiEditItem_Click(object sender, EventArgs e)
        {
            element_DoubleClicked(rightClickedElement, null);
        }
        private void tsmiCopyItem_Click(object sender, EventArgs e)
        {
            try
            {
                var allElements = this.Document.SelectedElements;
                List<RectangleIconNode> nodeElements = new List<RectangleIconNode>();
                Point min = new Point(Width, Height);
                Point max = new Point(0, 0);
                Point center;
                foreach (var element in allElements)
                {
                    if (element is RectangleIconNode rin && !(pairsCollection.GetItem(rin) is Driver))
                    {
                        nodeElements.Add(rin);
                        if (rin.Location.X + 32 > max.X) max.X = rin.Location.X + 32;
                        if (rin.Location.X < min.X) min.X = rin.Location.X;
                        if (rin.Location.Y + 32 > max.Y) max.Y = rin.Location.Y + 32;
                        if (rin.Location.Y < min.Y) min.Y = rin.Location.Y;
                    }
                }
                center = new Point((min.X + max.X) / 2, (min.Y + max.Y) / 2);

                ClipboardData[] data = new ClipboardData[nodeElements.Count];
                
                int count = 0;
                foreach (RectangleIconNode nodeElement in nodeElements)
                {
                    data[count].Icon = nodeElement.Icon;
                    data[count].Location = new Point(nodeElement.Location.X - center.X, nodeElement.Location.Y - center.Y);
                    data[count].Size = nodeElement.Size;
                    data[count].PropertyFormTypeName = nodeElement.PropertieFormTypeName;
                    data[count].Item = pairsCollection.GetItem(nodeElement);
                    count++;
                }

                Clipboard.SetData(Globals.ClipboardFormat, data);
            }
            catch (Exception ex)
            {
                MessageBox.Show("The copy command failed." + Environment.NewLine + ex.Message, "Error");
            }
        }
        private void tsmiDeleteElement_Click(object sender, EventArgs e)
        {
            KeyEventArgs ke = new KeyEventArgs(Keys.Delete);
            OnKeyDown(ke);
        }
        //private void 


        // Change project directory
        public void SetNewProjectDirectory(string newProjectDirectory)
        {
            if (framework.ProjectDirectory != newProjectDirectory)
            {
                string oldFilesDirectory = framework.FilesDirectory;

                framework.SetNewProjectDirectory(newProjectDirectory);
                pairsCollection.SetNewFilesDirectory(oldFilesDirectory, framework.FilesDirectory);
            }
        }

        // Test

        public void Test()
        {
            frmDriverEditor frmTest = new frmDriverEditor();
            frmTest.Item = new AMQDriver("Driver", "");
            frmTest.Item = new DOEDriver("Driver", "");
            frmTest.Item = new GADriver("Driver", "");
            frmTest.Item = new NMDriver("Driver", "");
            frmTest.Item = new LMDriver("Driver", "");

            frmTest.Pairs = pairsCollection;
            frmTest.ShowDialog();
            //Application.Exit();
        }

       
    }
}
