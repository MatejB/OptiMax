﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmFileRecorder : FormWithItem
    {
        // Variables                                                                                          
        private FileResource[] allFileResources;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(FileRecorder);
        }

        public FileRecorder Recorder
        {
            get
            {
                return (FileRecorder)Item;
            }
            set
            {
                Item = value;
            }
        }

        
        // Constructors                                                                                       
        public frmFileRecorder()
        {
            InitializeComponent();

            allFileResources = null;
            Recorder = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmFileRecorder_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill recorder data
            if (Recorder != null)
            {
                tbName.Text = Recorder.Name;
                tbDescription.Text = Recorder.Description;

                // fill DataGridView
                FileResource fileResource = null;
                List<string> files = Recorder.InputVariables;
                foreach (var file in files)
                {
                    Item item = Pairs.GetItemByNameIncludingFromContainers(file);
                    if (item is FileResource) fileResource = (FileResource)item;
                    else if (item is ExcelComponent) fileResource = ((ExcelComponent)item).ExcelFile;
                    
                    if (fileResource != null)
                    {
                        foreach (DataGridViewRow row in dgvFiles.Rows)
                        {
                            if ((string)row.Cells["colName"].Value == fileResource.Name)
                            {
                                row.Cells["colInclude"].Value = true;
                            }
                        }
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Recorder = GetFileRecorder();

                allFileResources = null;
                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        private FileRecorder GetFileRecorder()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Recorder) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            //if (recorderFileResources.Count == 0) throw new Exception("Add at least one file resource to the recorder.");

            FileRecorder fr = new FileRecorder(tbName.Text, tbDescription.Text);
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    fr.Add((string)row.Cells["colName"].Value);
                }
            }
            return fr;
        }
        override protected void ResetForm()
        {
            // clear all control resources
            dgvFiles.Rows.Clear();

            // fill dataGridView with all file resources
            allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            foreach (FileResource allFileResource in allFileResources)
            {
                dgvFiles.Rows.Add(new object[] { false, allFileResource.Name, allFileResource.Description, allFileResource.FileNameWithoutPath });
            }

            foreach (OptiMaxFramework.Component component in Pairs.GetAllComponents())
            {
                if (component is ExcelComponent)
                {
                    dgvFiles.Rows.Add(new object[] { false, component.Name, component.Description, (component as ExcelComponent).ExcelFile.FileNameWithoutPath });
                }
            }

            tbName.Text = "";
            tbDescription.Text = "";
        }

    }
}
