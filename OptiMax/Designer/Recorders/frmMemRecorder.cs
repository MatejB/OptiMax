﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmMemRecorder : FormWithItem
    {
        // Variables                                                                                          
        private Item[] allRecordableItems;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(MemRecorder);
        }

        public MemRecorder Recorder
        {
            get
            {
                return (MemRecorder)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmMemRecorder()
        {
            InitializeComponent();

            allRecordableItems = null;
            Recorder = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmCSVRecorder_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill recorder data
            if (Recorder != null)
            {
                tbName.Text = Recorder.Name;
                tbDescription.Text = Recorder.Description;

                rbRecordAll.Checked = Recorder.RecordAll;
                rbRecordOnlyTheBest.Checked = !Recorder.RecordAll;

                // fill DataGridView
                string[] variables = Recorder.InputVariables.ToArray();
                for (int i = variables.Length - 1; i >= 0; i--)
                {
                    Item item = Pairs.GetItemByNameIncludingFromContainers(variables[i]);
                    if (item is IRecordable)
                    {
                        if (item != null)
                        {
                            foreach (DataGridViewRow row in dgvVariables.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == item.Name)
                                {
                                    // sort selected items first
                                    row.Cells["colInclude"].Value = true;
                                    dgvVariables.Rows.Remove(row);
                                    dgvVariables.Rows.Insert(0, row);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Recorder = GetMemRecorder();

                allRecordableItems = null;
                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private MemRecorder GetMemRecorder()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Recorder) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            MemRecorder mem = new MemRecorder(tbName.Text, tbDescription.Text);
            foreach (DataGridViewRow row in dgvVariables.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    mem.Add((string)row.Cells["colName"].Value);
                }
            }
            mem.RecordAll = rbRecordAll.Checked;
            return mem;
        }

        override protected void ResetForm()
        {
            rbRecordAll.Checked = true;
            rbRecordOnlyTheBest.Checked = false;

            // clear all control resources
            dgvVariables.Rows.Clear();

            // fill dataGridView with all file resources
            allRecordableItems = Pairs.GetAllRecordableItemsIncludingFromContainers();
            string type = "";

            foreach (Item recordableItem in allRecordableItems)
            {
                if (recordableItem is VariableInput) type = "Variable input";
                else if (recordableItem is VariableConstant) type = "Variable constant";
                else if (recordableItem is VariableOutput) type = "Variable output";
                else if (recordableItem is VariableFunction) type = "Variable function";
                else if (recordableItem is VariableFromArray) type = "Variable from array";
                else if (recordableItem is VariableFromCompareCurves) type = "Variable from compare curves";

                else if (recordableItem is ArrayInput) type = "Array input";
                else if (recordableItem is ArrayConstant) type = "Array constant";
                else if (recordableItem is ArrayOutput) type = "Array output";
                else if (recordableItem is ArrayFunction) type = "Array function";
                else if (recordableItem is ArrayOperation) type = "Array filter";

                else if (recordableItem is ResponseSurfaceComponent) type = "Response surface";

                else type = "unknown";
                
                if (recordableItem is ResponseSurfaceComponent)
                {
                    dgvVariables.Rows.Add(new object[] { false, recordableItem.Name, "Coefficient of determination R; Equation", type });
                }
                else
                {
                    dgvVariables.Rows.Add(new object[] { false, recordableItem.Name, recordableItem.Description, type });
                }
            }

            tbName.Text = "memRec";
            tbDescription.Text = "";
        }
    }
}
