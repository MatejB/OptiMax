﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmCSVRecorder : FormWithItem
    {
        // Variables                                                                                          
        private Item[] allRecordableItems;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(CsvRecorder);
        }

        public CsvRecorder Recorder
        {
            get
            {
                return (CsvRecorder)Item;
            }
            set
            {
                Item = value;
            }
        }

        
        // Constructors                                                                                       
        public frmCSVRecorder()
        {
            InitializeComponent();

            toolStripMenuItems = new ToolStripMenuItem[2];

            toolStripMenuItems[0] = new ToolStripMenuItem();
            toolStripMenuItems[0].Name = "tsmiOpenFolder";
            toolStripMenuItems[0].Size = new System.Drawing.Size(152, 22);
            toolStripMenuItems[0].Text = "Open folder";
            toolStripMenuItems[0].Click += tsmiOpenFolder_Click;

            toolStripMenuItems[1] = new ToolStripMenuItem();
            toolStripMenuItems[1].Name = "tsmiOpenCsvFile";
            toolStripMenuItems[1].Size = new System.Drawing.Size(152, 22);
            toolStripMenuItems[1].Text = "Open *.csv file";
            toolStripMenuItems[1].Click += tsmiOpenCsvFile_Click;

            allRecordableItems = null;
            Recorder = null;
            Pairs = null;
        }

       


        // Event handling                                                                                     
        private void frmCSVRecorder_Load(object sender, EventArgs e)
        {
            ResetForm();

            // fill recorder data
            if (Recorder != null)
            {
                tbName.Text = Recorder.Name;
                tbDescription.Text = Recorder.Description;

                rbRecordAll.Checked = Recorder.RecordAll;
                rbRecordOnlyTheBest.Checked = !Recorder.RecordAll;

                if (Recorder.DecimalSeparator == ".") comboDecimalSeparator.SelectedIndex = 0;
                else comboDecimalSeparator.SelectedIndex = 1;

                // fill DataGridView
                string[] variables = Recorder.InputVariables.ToArray();
                for (int i = variables.Length - 1; i >= 0; i--)
                {
                    Item item = Pairs.GetItemByNameIncludingFromContainers(variables[i]);
                    if (item is IRecordable)
                    {
                        if (item != null)
                        {
                            foreach (DataGridViewRow row in dgvVariables.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == item.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                    dgvVariables.Rows.Remove(row);
                                    dgvVariables.Rows.Insert(0, row);
                                    break;
                                }
                            }
                        }
                    }
                }


                foreach (var variableName in variables)
                {
                    Item item = Pairs.GetItemByNameIncludingFromContainers(variableName);
                    if (item is VariableBase || item is OptiMaxFramework.ArrayBase)
                    {
                        if (item != null)
                        {
                            foreach (DataGridViewRow row in dgvVariables.Rows)
                            {
                                if ((string)row.Cells["colName"].Value == item.Name)
                                {
                                    row.Cells["colInclude"].Value = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Recorder = GetCsvRecorder();

                allRecordableItems = null;
                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        void tsmiOpenFolder_Click(object sender, EventArgs e)
        {
            string path = Recorder.ResultsDirectory;
            if (path != null && Directory.Exists(path))
            {
                System.Diagnostics.Process.Start(path);
            }
            else MessageBox.Show("The results directory does not exists.", "Error", MessageBoxButtons.OK);
        }

        void tsmiOpenCsvFile_Click(object sender, EventArgs e)
        {
            string file = Path.Combine(Recorder.ResultsDirectory, Recorder.Name + ".csv");
            if (file != null && System.IO.File.Exists(file))
                System.Diagnostics.Process.Start(file);
            else MessageBox.Show("The results file does not exists.", "Error", MessageBoxButtons.OK);
        }


        // Methods                                                                                            
        private CsvRecorder GetCsvRecorder()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Recorder) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            string[] tmp = ((string)comboDecimalSeparator.SelectedItem).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            CsvRecorder csv = new CsvRecorder(tbName.Text, tbDescription.Text, tmp[0]);

            foreach (DataGridViewRow row in dgvVariables.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value)
                {
                    csv.Add((string)row.Cells["colName"].Value);
                }
            }
            csv.RecordAll = rbRecordAll.Checked;
            return csv;
        }

        override protected void ResetForm()
        {
            rbRecordAll.Checked = true;
            rbRecordOnlyTheBest.Checked = false;

            // clear all control resources
            dgvVariables.Rows.Clear();

            // fill dataGridView with all file resources
            allRecordableItems = Pairs.GetAllRecordableItemsIncludingFromContainers();
            string type = "";

            foreach (Item recordableItem in allRecordableItems)
            {
                if (recordableItem is VariableInput) type = "Variable input";
                else if (recordableItem is VariableConstant) type = "Variable constant";
                else if (recordableItem is VariableOutput) type = "Variable output";
                else if (recordableItem is VariableFunction) type = "Variable function";
                else if (recordableItem is VariableFromArray) type = "Variable from array";
                else if (recordableItem is VariableFromCompareCurves) type = "Variable from compare curves";

                else if (recordableItem is ArrayInput) type = "Array input";
                else if (recordableItem is ArrayConstant) type = "Array constant";
                else if (recordableItem is ArrayOutput) type = "Array output";
                else if (recordableItem is ArrayFunction) type = "Array function";
                else if (recordableItem is ArrayOperation) type = "Array filter";

                else if (recordableItem is ResponseSurfaceComponent) type = "Response surface";

                else type = "unknown";

                if (recordableItem is ResponseSurfaceComponent)
                {
                    dgvVariables.Rows.Add(new object[] { false, recordableItem.Name, "Coefficient of determination R; Equation", type });
                }
                else
                {
                    dgvVariables.Rows.Add(new object[] { false, recordableItem.Name, recordableItem.Description, type });
                }
            }

            tbName.Text = "";
            tbDescription.Text = "";
            comboDecimalSeparator.SelectedIndex = 0;
        }


    }
}
