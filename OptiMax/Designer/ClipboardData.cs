﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMax
{
    [Serializable]
    struct ClipboardData
    {
        public System.Drawing.Image Icon;
        public System.Drawing.Point Location;
        public System.Drawing.Size Size;
        public string PropertyFormTypeName;
        public OptiMaxFramework.Item Item;
    }
}
