﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptiMax
{
    public interface IFilesDirectory
    {
        string FilesDirectory { get; set; }
    }
}
