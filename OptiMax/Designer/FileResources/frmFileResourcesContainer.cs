﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace OptiMax
{
    public partial class frmFileResourcesContainer : FormWithItem, IFilesDirectory
    {
        // Variables
        private List<FileResource> fileResources;

        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ItemContainer);
        }
        public override string GetItemHelp()
        {
            return "";
        }
        public ItemContainer ItemContainer
        {
            get
            {
                return (ItemContainer)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }


        // Constructors                                                                                       
        public frmFileResourcesContainer()
        {
            InitializeComponent();

            ItemContainer = null;

            fileResources = new List<FileResource>();
        }
        

        // Event handling                                                                                     
        private void frmFileResourcesContainer_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (ItemContainer != null)
            {
                tbName.Text = ItemContainer.Name;
                tbDescription.Text = ItemContainer.Description;

                foreach (Item item in ItemContainer.GetItems)
                {
                    InsertNewItem(item as FileResource);
                }
                lvFileResources.Items[0].Selected = true;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                ItemContainer = GetContainer();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if ((string)cbFileResourceType.SelectedItem == "Input file resource")
                InsertNewItem(new FileResource("input", "", FileResource.EmptyFileName, FileIOType.Input));
            else if ((string)cbFileResourceType.SelectedItem == "Constant file resource")
                InsertNewItem(new FileResource("constant", "", FileResource.EmptyFileName, FileIOType.Constant));
            else if ((string)cbFileResourceType.SelectedItem == "Output file resource")
                InsertNewItem(new FileResource("output", "", FileResource.EmptyFileName, FileIOType.Output));
            else if ((string)cbFileResourceType.SelectedItem == "Copy file resource")
                InsertNewItem(new FileResource("copy", "", FileResource.EmptyFileName, FileIOType.Copy, ""));

            lvFileResources.Items[fileResources.Count - 1].Selected = true;
        }
        private void lvFileResources_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvFileResources.SelectedIndices.Count > 0)
            {
                int index = lvFileResources.SelectedIndices[0];
                FileResource fr = fileResources[index];
                if (fr.IOType == FileIOType.Copy)
                {
                    // get all names except the names of files in this container if it exists
                    FileResource[] allFileResources;
                    if (ItemContainer != null) allFileResources = Pairs.GetAllFileResourcesIncludingFromContainersExcept(ItemContainer.Name);
                    else allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();

                    List<string> fileResourceNames = new List<string>();
                    for (int i = 0; i < allFileResources.Length; i++) fileResourceNames.Add(allFileResources[i].Name);
                    foreach (var fileResource in fileResources)
                    {
                        if (fileResource.Name != fr.Name) fileResourceNames.Add(fileResource.Name);
                    }
                    ViewCopyFileResource vfr = new ViewCopyFileResource(fr, fileResourceNames.ToArray());
                    pgProperties.SelectedObject = vfr;
                }
                else pgProperties.SelectedObject = fr;

            }
            else pgProperties.SelectedObject = null;
        }
        private void pgProperties_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (lvFileResources.SelectedIndices.Count > 0)
            {
                int i = lvFileResources.SelectedIndices[0];

                object item = pgProperties.SelectedObject;  // first copy it

                fileResources.RemoveAt(i);
                lvFileResources.Items.RemoveAt(i);

                if (item is FileResource fr) InsertNewItem(fr, i);
                else if (item is ViewCopyFileResource vfr) InsertNewItem(vfr.GetBase(), i);

                lvFileResources.Items[i].Selected = true;
            }
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            if (lvFileResources.SelectedIndices.Count > 0)
            {
                int index = lvFileResources.SelectedIndices[0];

                if (index > 0)
                {
                    FileResource fileResource = fileResources[index];

                    fileResources.RemoveAt(index);
                    lvFileResources.Items.RemoveAt(index);

                    InsertNewItem(fileResource, index - 1);

                    lvFileResources.Items[index - 1].Selected = true;
                }
            }
        }
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (lvFileResources.SelectedIndices.Count > 0)
            {
                int index = lvFileResources.SelectedIndices[0];

                if (index < fileResources.Count - 1)
                {
                    FileResource fileResource = fileResources[index];

                    fileResources.RemoveAt(index);
                    lvFileResources.Items.RemoveAt(index);

                    InsertNewItem(fileResource, index + 1);

                    lvFileResources.Items[index + 1].Selected = true;
                }
            }
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lvFileResources.SelectedIndices.Count > 0)
            {
                int i = lvFileResources.SelectedIndices[0];
                fileResources.RemoveAt(i);
                lvFileResources.Items.RemoveAt(i);
            }
        }


        // Methods                                                                                            
        public ItemContainer GetContainer()
        {
            Item item;
            FileResource fr;
            ItemContainer ic;

            item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.ItemContainer) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            ic = new ItemContainer(tbName.Text, tbDescription.Text);

            if (fileResources.Count == 0)
                throw new Exception("At least one file resource must be defined.");

            List<string> fileVariables;
            bool containsAll;
            string fileVariable;
            foreach (Item fileResItem in fileResources)
            {
                item = Pairs.GetItemByNameIncludingFromContainersExcept(fileResItem.Name, ic.Name);
                if (item != null && item != fileResItem)
                    throw new Exception("The name '" + fileResItem.Name + "' is allready in use.");

                fr = fileResItem as FileResource;

                if (fr.FileNameWithPath == FileResource.EmptyFileName)
                    throw new Exception("The file name of the file resource '" + fileResItem.Name + "' is not defined.");

                if ((fr.IOType == FileIOType.Input || fr.IOType == FileIOType.Constant) && !System.IO.File.Exists(fr.FileNameWithPath))
                    throw new Exception("The file '" + fr.FileNameWithPath + "' does not exist.");

                // check if all variables exist
                fr.FileNameWithPath = fr.FileNameWithPath;  //this rechecks for the variables in the file
                fileVariables = fr.InputVariables;
                containsAll = true;
                fileVariable = "";
                if (fileVariables != null)
                {
                    foreach (var variable in fileVariables)
                    {
                        fileVariable = variable;
                        item = Pairs.GetItemByNameIncludingFromContainers(variable);
                        if (item == null) containsAll = false;
                        break;
                    }
                    if (!containsAll) throw new Exception("The variable '" + fileVariable + "' from the file resource '" + fr.Name + "' does not exist.");
                }

                if (!fr.SetToFilesDirectory(FilesDirectory))
                    throw new Exception("The file '" + fr.FileNameWithPath  + "' could not be copied to the files directory.");

                ic.Add(fileResItem);
            }

            return ic;
        }
        override protected void ResetForm()
        {
            fileResources.Clear();

            cbFileResourceType.SelectedIndex = 0;
            lvFileResources.Items.Clear();
            pgProperties.SelectedObject = null;
            tbName.Text = "";
            tbDescription.Text = "";
        }
        private void InsertNewItem(FileResource fileResource, int index = -1)
        {
            string data = fileResource.IOType.ToString();
                
            if (index == -1)
            {
                lvFileResources.Items.Add(new ListViewItem(new string[] { fileResource.Name, data }));
                fileResources.Add(fileResource);
            }
            else
            {
                lvFileResources.Items.Insert(index, new ListViewItem(new string[] { fileResource.Name, data }));
                fileResources.Insert(index, fileResource);
            }
        }

       




    }
   

}
