﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DynamicTypeDescriptor;
using OptiMaxFramework;

namespace OptiMax
{
    [Serializable]
    public class ViewCopyFileResource
    {
        // Variables                                                                                                                
        private DynamicCustomTypeDescriptor _dctd = null;
        private FileResource _fileResource;


        // Properties                                                                                                               
        [Category("Data")]
        [OrderedDisplayName(0, 10, "File resource name")]
        [Description("The name of the object.")]
        public string Name { get { return _fileResource.Name; } set { _fileResource.Name = value; } }

        [Category("Data")]
        [OrderedDisplayName(1, 10, "File resource to copy from")]
        [Description("Select the file resource to copy from.")]
        public string FileResourceToCopyFrom { get { return _fileResource.FileResourceToCopyFrom; } set { _fileResource.FileResourceToCopyFrom = value; } }

        [Category("Data")]
        [OrderedDisplayName(2, 10, "File name")]
        [Description("The file resource file name.")]
        public string FileNameWithPath { get { return _fileResource.FileNameWithPath; } set { _fileResource.FileNameWithPath = value; } }

        [DescriptionAttribute("The decsription of the object.")]
        public string Description { get { return _fileResource.Description; } set { _fileResource.Description = value; } }


        // Constructors                                                                                                             
        public ViewCopyFileResource(FileResource fileResource, string[] sources)
        {
            // the order is important
            _fileResource = fileResource;
            _dctd = ProviderInstaller.Install(this);

            PopululateDropDownList(sources);
        }



        // Methods                                                                                                                  
        public FileResource GetBase()
        {
            return _fileResource;
        }
        public void PopululateDropDownList(string[] sources)
        {
            if (sources != null && sources.Length > 0)
            {
                _dctd.PopulateProperty(() => this.FileResourceToCopyFrom, sources);
                if (FileResourceToCopyFrom == null || FileResourceToCopyFrom == "") FileResourceToCopyFrom = sources[0];
            }
        }
    }

}
