﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace OptiMax
{
    [Flags]
    public enum AssocF
    {
        None = 0x0,
        Init_NoRemapCLSID = 0x1,
        Init_ByExeName = 0x2,
        Open_ByExeName = 0x2,
        Init_DefaultToStar = 0x4,
        Init_DefaultToFolder = 0x8,
        NoUserSettings = 0x10,
        NoTruncate = 0x20,
        Verify = 0x40,
        RemapRunDll = 0x80,
        NoFixUps = 0x100,
        IgnoreBaseClass = 0x200
    }
    public enum AssocStr
    {
        Command = 1,
        Executable,
        FriendlyDocName,
        FriendlyAppName,
        NoOpen,
        ShellNewValue,
        DDECommand,
        DDEIfExec,
        DDEApplication,
        DDETopic
    }


    public partial class frmFileResource : FormWithItem, IFilesDirectory
    {
        //-----------------------------------------------------------------------------
        [DllImport("Shlwapi.dll", CharSet = CharSet.Unicode)]
        public static extern uint AssocQueryString(AssocF flags, AssocStr str, string pszAssoc, string pszExtra, StringBuilder pszOut, ref uint pcchOut);
        static string AssocQueryString(AssocStr association, string extension)
        {
            const int S_OK = 0;
            const int S_FALSE = 1;

            uint length = 0;
            uint ret = AssocQueryString(AssocF.None, association, extension, null, null, ref length);
            if (ret != S_FALSE)
            {
                throw new InvalidOperationException("Could not determine associated string");
            }

            var sb = new StringBuilder((int)length); // (length-1) will probably work too as the marshaller adds null termination
            ret = AssocQueryString(AssocF.None, association, extension, null, sb, ref length);
            if (ret != S_OK)
            {
                throw new InvalidOperationException("Could not determine associated string");
            }

            return sb.ToString();
        }
        //-----------------------------------------------------------------------------



        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(FileResource);
        }
        public override string GetItemHelp()
        {
            return "Use 'File Resource' for all files which must be copied to\n"
                 + "or will be created in the work directory during the job run.";
        }
        public FileResource FileResource
        {
            get
            {
                return (FileResource)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }

        
        // Constructors                                                                                       
        public frmFileResource()
        {
            InitializeComponent();

            toolStripMenuItems = new ToolStripMenuItem[1];
            toolStripMenuItems[0] = new ToolStripMenuItem();
            toolStripMenuItems[0].Name = "tsmiEditFile";
            toolStripMenuItems[0].Size = new System.Drawing.Size(152, 22);
            toolStripMenuItems[0].Text = "Edit file resource";
            toolStripMenuItems[0].Click += editFileToolStripMenuItem_Click;

            FileResource = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmFileResource_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (FileResource != null)
            {
                if (FileResource.IOType == FileIOType.Input) rbInputFile.Checked = true;
                else if (FileResource.IOType == FileIOType.Output) rbOutputFile.Checked = true;
                else if (FileResource.IOType == FileIOType.Constant) rbConstantFile.Checked = true;
                else if (FileResource.IOType == FileIOType.Copy)
                {
                    rbCopy.Checked = true;
                    comboFileResources.SelectedItem = FileResource.FileResourceToCopyFrom;
                }

                tbName.Text = FileResource.Name;
                tbDescription.Text = FileResource.Description;
                tbFileName.Text = FileResource.FileNameWithPath;
            }
        }
        private void rbCopy_CheckedChanged(object sender, EventArgs e)
        {
            comboFileResources.Enabled = rbCopy.Checked;
        }
        private void tbFileName_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(tbFileName.Text)) btnEdit.Enabled = true;
            else btnEdit.Enabled = false; ;
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            string path = null;
            if (Directory.Exists(tbFileName.Text))
                path = Path.GetDirectoryName(tbFileName.Text);
            else path = FilesDirectory;

            if (path != null && Directory.Exists(path)) openFileDialog1.InitialDirectory = path;
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbFileName.Text = openFileDialog1.FileName;
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                string application = AssocQueryString(AssocStr.Executable, ".txt");

                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = application;
                psi.Arguments = "\"" + tbFileName.Text + "\"";
                Process exe = new Process();
                exe.StartInfo = psi;
                exe.Start();
            }
            catch
            {
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                FileResource tmp = GetFileResource();   // this has to be done over tmp in order to keep FileResource since GetFileResource can return null
                if (tmp != null)
                {
                    FileResource = tmp;
                    Pairs = null;

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        void editFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileResource != null)
            {
                tbFileName.Text = FileResource.FileNameWithPath;
                btnEdit_Click(null, null);
            }
        }


        // Methods                                                                                            
        private FileResource GetFileResource()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.FileResource) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            FileIOType type = FileIOType.Input;

            if (rbInputFile.Checked) type = FileIOType.Input;
            else if (rbOutputFile.Checked) type = FileIOType.Output;
            else if (rbConstantFile.Checked) type = FileIOType.Constant;
            else if (rbCopy.Checked) type = FileIOType.Copy;

            string copyFromfileResourceName = null;
            if (type == FileIOType.Copy) 
                copyFromfileResourceName = (string)comboFileResources.SelectedItem;

            FileResource fileResource = new FileResource(tbName.Text, tbDescription.Text, tbFileName.Text, type, copyFromfileResourceName);

            List<string> fileVariables = fileResource.InputVariables;

            bool containsAll = true;
            string fileVariable = "";
            if (fileVariables != null)
            {
                foreach (var variable in fileVariables)
                {
                    fileVariable = variable;
                    item = Pairs.GetItemByNameIncludingFromContainers(variable);
                    if (item == null) containsAll = false;
                    break;
                }
                if (!containsAll) throw new Exception("The variable '" + fileVariable + "' from the file resource '" + fileResource.Name + "' does not exist.");
            }

            if (fileResource.SetToFilesDirectory(FilesDirectory))
                return fileResource;
            else
                return null;
        }

        override protected void ResetForm()
        {
            // clear all control resources
            comboFileResources.Enabled = false;
            comboFileResources.Items.Clear();

            // fill combo box and dataGridView with all file resources
            FileResource[] allFileResources = Pairs.GetAllFileResourcesIncludingFromContainers();
            foreach (FileResource allFileResource in allFileResources)
            {
                if (allFileResource != FileResource)
                    comboFileResources.Items.Add(allFileResource.Name);
            }

            if (comboFileResources.Items.Count > 0) comboFileResources.SelectedIndex = 0;
            rbInputFile.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            tbFileName.Text = "";
        }

       
        
    }
}
