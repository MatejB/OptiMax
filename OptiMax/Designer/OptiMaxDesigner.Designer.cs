﻿namespace OptiMax
{
    partial class OptiMaxDesigner
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuCsvRecorder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opencsvFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEditItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCopyItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteElement = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLine1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiLine2 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuCsvRecorder.SuspendLayout();
            this.contextMenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuCsvRecorder
            // 
            this.contextMenuCsvRecorder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFolderToolStripMenuItem,
            this.opencsvFileToolStripMenuItem,
            this.tsmiEditItem,
            this.tsmiCopyItem,
            this.tsmiDeleteElement,
            this.tsmiLine1,
            this.tsmiLine2});
            this.contextMenuCsvRecorder.Name = "contextMenuCsvRecorder";
            this.contextMenuCsvRecorder.Size = new System.Drawing.Size(151, 126);
            // 
            // openFolderToolStripMenuItem
            // 
            this.openFolderToolStripMenuItem.Name = "openFolderToolStripMenuItem";
            this.openFolderToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.openFolderToolStripMenuItem.Text = "Open folder";
            this.openFolderToolStripMenuItem.Click += new System.EventHandler(this.openFolderToolStripMenuItem_Click);
            // 
            // opencsvFileToolStripMenuItem
            // 
            this.opencsvFileToolStripMenuItem.Name = "opencsvFileToolStripMenuItem";
            this.opencsvFileToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.opencsvFileToolStripMenuItem.Text = "Open *.csv file";
            this.opencsvFileToolStripMenuItem.Click += new System.EventHandler(this.opencsvFileToolStripMenuItem_Click);
            // 
            // tsmiEditItem
            // 
            this.tsmiEditItem.Name = "tsmiEditItem";
            this.tsmiEditItem.Size = new System.Drawing.Size(150, 22);
            this.tsmiEditItem.Text = "Edit";
            this.tsmiEditItem.Click += new System.EventHandler(this.tsmiEditItem_Click);
            // 
            // tsmiCopyItem
            // 
            this.tsmiCopyItem.Name = "tsmiCopyItem";
            this.tsmiCopyItem.Size = new System.Drawing.Size(150, 22);
            this.tsmiCopyItem.Text = "Copy";
            this.tsmiCopyItem.Click += new System.EventHandler(this.tsmiCopyItem_Click);
            // 
            // tsmiDeleteElement
            // 
            this.tsmiDeleteElement.Name = "tsmiDeleteElement";
            this.tsmiDeleteElement.Size = new System.Drawing.Size(150, 22);
            this.tsmiDeleteElement.Text = "Delete";
            this.tsmiDeleteElement.Click += new System.EventHandler(this.tsmiDeleteElement_Click);
            // 
            // tsmiLine1
            // 
            this.tsmiLine1.Name = "tsmiLine1";
            this.tsmiLine1.Size = new System.Drawing.Size(147, 6);
            // 
            // tsmiLine2
            // 
            this.tsmiLine2.Name = "tsmiLine2";
            this.tsmiLine2.Size = new System.Drawing.Size(147, 6);
            // 
            // contextMenuMain
            // 
            this.contextMenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPaste});
            this.contextMenuMain.Name = "contextMenuMain";
            this.contextMenuMain.Size = new System.Drawing.Size(181, 48);
            // 
            // tsmiPaste
            // 
            this.tsmiPaste.Name = "tsmiPaste";
            this.tsmiPaste.Size = new System.Drawing.Size(180, 22);
            this.tsmiPaste.Text = "Paste";
            this.tsmiPaste.Click += new System.EventHandler(this.tsmiPaste_Click);
            // 
            // OptiMaxDesigner
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ContextMenuStrip = this.contextMenuMain;
            this.Name = "OptiMaxDesigner";
            this.Size = new System.Drawing.Size(331, 301);
            this.ElementMouseDrag += new Dalssoft.DiagramNet.Designer.ElementMouseEventHandler(this.OptiMaxDesigner_ElementMouseDrag);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptiMaxDesigner_MouseUp);
            this.contextMenuCsvRecorder.ResumeLayout(false);
            this.contextMenuMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuCsvRecorder;
        private System.Windows.Forms.ToolStripMenuItem openFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opencsvFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiEditItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiCopyItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteElement;
        private System.Windows.Forms.ToolStripSeparator tsmiLine1;
        private System.Windows.Forms.ToolStripSeparator tsmiLine2;
        private System.Windows.Forms.ContextMenuStrip contextMenuMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiPaste;
    }
}
