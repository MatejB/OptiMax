﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmVariableInput : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableInput);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variable Input' for scalar parameter which will be changed\n"
                 + "by the driver during the job run.";
        }

        public VariableInput Variable
        {
            get
            {
                return (VariableInput)Item;
            }
            set
            {
                Item = value;
            }
        }

        // Constructors                                                                                       
        public frmVariableInput()
        {
            InitializeComponent();

            Variable = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                if (Variable.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Variable.ValueType == VariableValueType.Integer) rbInteger.Checked = true;

                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }

        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetVariable();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public VariableInput GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;

            VariableInput vi = new VariableInput(tbName.Text, tbDescription.Text, type);

            if (cbStringLength.Checked) vi.StringLength = (int)numStringLength.Value;
            else vi.StringLength = -1;

            return vi;
        }

        override protected void ResetForm()
        {
            numStringLength.Value = 10;
            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            cbStringLength.Checked = false;
        }
      


    }
}
