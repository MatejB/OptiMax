﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmVariableFromArray : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableFromArray);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variable From Array' for a scalar parameter which will be computed\n"
                 + "from other vector parameters in the workflow.";
        }

        public VariableFromArray Variable
        {
            get
            {
                return (VariableFromArray)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmVariableFromArray()
        {
            InitializeComponent();

            Variable = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                if (Variable.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Variable.ValueType == VariableValueType.Integer) rbInteger.Checked = true;
                else if (Variable.ValueType == VariableValueType.String) rbString.Checked = true;

                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;
                
                comboArrays.SelectedItem = Variable.ArrayName;

                string operation = "Average";
                if (Variable.Operation == ArrayToVariableOperation.Average) operation = "Average";
                else if (Variable.Operation == ArrayToVariableOperation.Min) operation = "Min";
                else if (Variable.Operation == ArrayToVariableOperation.MinID) operation = "MinID";
                else if (Variable.Operation == ArrayToVariableOperation.Max) operation = "Max";
                else if (Variable.Operation == ArrayToVariableOperation.MaxID) operation = "MaxID";
                else if (Variable.Operation == ArrayToVariableOperation.Sum) operation = "Sum";
                else if (Variable.Operation == ArrayToVariableOperation.GetElementByNumber) operation = "Get element by number";
                comboOperations.SelectedItem = operation;

                if (operation == "Get element by number") tbElementNumberEq.Text = Variable.Equation;

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }

        private void comboFileResources_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }
        
        private void comboOperations_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)comboOperations.SelectedItem == "Get element by number") tbElementNumberEq.Enabled = true;
            else tbElementNumberEq.Enabled = false;
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetVariable();

                Pairs = null;
                comboArrays.Items.Clear();
                comboOperations.Items.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public VariableFromArray GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;
            else if (rbString.Checked) type = VariableValueType.String;

            ArrayToVariableOperation operation = ArrayToVariableOperation.Average;
            string elementNumber = "";
            if ((string)comboOperations.SelectedItem == "Average") operation = ArrayToVariableOperation.Average;
            else if ((string)comboOperations.SelectedItem == "Min") operation = ArrayToVariableOperation.Min;
            else if ((string)comboOperations.SelectedItem == "MinID") operation = ArrayToVariableOperation.MinID;
            else if ((string)comboOperations.SelectedItem == "Max") operation = ArrayToVariableOperation.Max;
            else if ((string)comboOperations.SelectedItem == "MaxID") operation = ArrayToVariableOperation.MaxID;
            else if ((string)comboOperations.SelectedItem == "Sum") operation = ArrayToVariableOperation.Sum;
            else if ((string)comboOperations.SelectedItem == "Get element by number")
            {
                operation = ArrayToVariableOperation.GetElementByNumber;
                elementNumber = tbElementNumberEq.Text;
            }

            VariableFromArray vfa = new VariableFromArray(tbName.Text, tbDescription.Text, type, (string)comboArrays.SelectedItem, operation, elementNumber);
            List<string> eqVariables = vfa.InputVariables;

            bool containsAll = true;
            string eqVariable = "";
            foreach (var variable in eqVariables)
            {
                eqVariable = variable;
                item = Pairs.GetItemByNameIncludingFromContainers(variable);
                if (item == null)
                {
                    containsAll = false;
                    break;
                }
            }
            if (!containsAll) throw new Exception("The variable '" + eqVariable + "' does not exist.");


            if (cbStringLength.Checked) vfa.StringLength = (int)numStringLength.Value;
            else vfa.StringLength = -1;

            return vfa;
        }

        override protected void ResetForm()
        {
            comboArrays.Items.Clear();
            foreach (ArrayBase array in Pairs.GetAllArraysIncludingFromContainers())
            {
                comboArrays.Items.Add(array.Name);
            }
            if (comboArrays.Items.Count == 0)
            {
                MessageBox.Show("The workflow does not contain any arrays. First create an array.", "Error", MessageBoxButtons.OK);
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                return;
            }

            comboOperations.Items.Clear();
            comboOperations.Items.Add("Average");
            comboOperations.Items.Add("Min");
            comboOperations.Items.Add("MinID");
            comboOperations.Items.Add("Max");
            comboOperations.Items.Add("MaxID");
            comboOperations.Items.Add("Sum");
            comboOperations.Items.Add("Get element by number");

            numStringLength.Value = 10;

            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            comboArrays.SelectedIndex = 0;
            comboOperations.SelectedIndex = 0;
            tbElementNumberEq.Text = "";
        }

        

       

        

    }
}
