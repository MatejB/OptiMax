﻿namespace OptiMax
{
    partial class frmVariableOutput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tcMiners = new System.Windows.Forms.TabControl();
            this.tpTxt = new System.Windows.Forms.TabPage();
            this.gbTextFile = new System.Windows.Forms.GroupBox();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.tpExcel = new System.Windows.Forms.TabPage();
            this.gbExcelFile = new System.Windows.Forms.GroupBox();
            this.tbCellRange = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tpDll = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboDllItemNames = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tpResponseSurface = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboOutputArrayName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rbString = new System.Windows.Forms.RadioButton();
            this.rbInteger = new System.Windows.Forms.RadioButton();
            this.rbDouble = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.comboFileResourcesComponents = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numStringLength = new System.Windows.Forms.NumericUpDown();
            this.cbStringLength = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.tcMiners.SuspendLayout();
            this.tpTxt.SuspendLayout();
            this.gbTextFile.SuspendLayout();
            this.tpExcel.SuspendLayout();
            this.gbExcelFile.SuspendLayout();
            this.tpDll.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpResponseSurface.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStringLength)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(312, 290);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(138, 72);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(237, 20);
            this.tbDescription.TabIndex = 3;
            this.tbDescription.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(138, 46);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(237, 20);
            this.tbName.TabIndex = 2;
            this.tbName.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(97, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Variable type";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tcMiners);
            this.groupBox1.Controls.Add(this.rbString);
            this.groupBox1.Controls.Add(this.rbInteger);
            this.groupBox1.Controls.Add(this.rbDouble);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboFileResourcesComponents);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 215);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // tcMiners
            // 
            this.tcMiners.Controls.Add(this.tpTxt);
            this.tcMiners.Controls.Add(this.tpExcel);
            this.tcMiners.Controls.Add(this.tpDll);
            this.tcMiners.Controls.Add(this.tpResponseSurface);
            this.tcMiners.Location = new System.Drawing.Point(6, 125);
            this.tcMiners.Name = "tcMiners";
            this.tcMiners.SelectedIndex = 0;
            this.tcMiners.Size = new System.Drawing.Size(369, 86);
            this.tcMiners.TabIndex = 34;
            // 
            // tpTxt
            // 
            this.tpTxt.Controls.Add(this.gbTextFile);
            this.tpTxt.Location = new System.Drawing.Point(4, 22);
            this.tpTxt.Name = "tpTxt";
            this.tpTxt.Padding = new System.Windows.Forms.Padding(3);
            this.tpTxt.Size = new System.Drawing.Size(361, 60);
            this.tpTxt.TabIndex = 0;
            this.tpTxt.Text = "Txt";
            this.tpTxt.UseVisualStyleBackColor = true;
            // 
            // gbTextFile
            // 
            this.gbTextFile.Controls.Add(this.btnTemplate);
            this.gbTextFile.Location = new System.Drawing.Point(3, 6);
            this.gbTextFile.Name = "gbTextFile";
            this.gbTextFile.Size = new System.Drawing.Size(355, 52);
            this.gbTextFile.TabIndex = 41;
            this.gbTextFile.TabStop = false;
            this.gbTextFile.Text = "Text file";
            // 
            // btnTemplate
            // 
            this.btnTemplate.Location = new System.Drawing.Point(3, 19);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(349, 23);
            this.btnTemplate.TabIndex = 33;
            this.btnTemplate.Text = "Create/edit template";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // tpExcel
            // 
            this.tpExcel.Controls.Add(this.gbExcelFile);
            this.tpExcel.Location = new System.Drawing.Point(4, 22);
            this.tpExcel.Name = "tpExcel";
            this.tpExcel.Padding = new System.Windows.Forms.Padding(3);
            this.tpExcel.Size = new System.Drawing.Size(361, 60);
            this.tpExcel.TabIndex = 1;
            this.tpExcel.Text = "Excel";
            this.tpExcel.UseVisualStyleBackColor = true;
            // 
            // gbExcelFile
            // 
            this.gbExcelFile.Controls.Add(this.tbCellRange);
            this.gbExcelFile.Controls.Add(this.label5);
            this.gbExcelFile.Location = new System.Drawing.Point(3, 6);
            this.gbExcelFile.Name = "gbExcelFile";
            this.gbExcelFile.Size = new System.Drawing.Size(355, 52);
            this.gbExcelFile.TabIndex = 42;
            this.gbExcelFile.TabStop = false;
            this.gbExcelFile.Text = "Excel file (Range: A2)";
            // 
            // tbCellRange
            // 
            this.tbCellRange.Location = new System.Drawing.Point(75, 21);
            this.tbCellRange.Name = "tbCellRange";
            this.tbCellRange.Size = new System.Drawing.Size(274, 20);
            this.tbCellRange.TabIndex = 39;
            this.tbCellRange.Text = "Cell Range";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Cell Range";
            // 
            // tpDll
            // 
            this.tpDll.Controls.Add(this.groupBox3);
            this.tpDll.Location = new System.Drawing.Point(4, 22);
            this.tpDll.Name = "tpDll";
            this.tpDll.Size = new System.Drawing.Size(361, 60);
            this.tpDll.TabIndex = 2;
            this.tpDll.Text = "Dll";
            this.tpDll.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboDllItemNames);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(3, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(355, 52);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dll file";
            // 
            // comboDllItemNames
            // 
            this.comboDllItemNames.FormattingEnabled = true;
            this.comboDllItemNames.Location = new System.Drawing.Point(132, 21);
            this.comboDllItemNames.Name = "comboDllItemNames";
            this.comboDllItemNames.Size = new System.Drawing.Size(217, 21);
            this.comboDllItemNames.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Dll variable/array name";
            // 
            // tpResponseSurface
            // 
            this.tpResponseSurface.Controls.Add(this.groupBox4);
            this.tpResponseSurface.Location = new System.Drawing.Point(4, 22);
            this.tpResponseSurface.Name = "tpResponseSurface";
            this.tpResponseSurface.Padding = new System.Windows.Forms.Padding(3);
            this.tpResponseSurface.Size = new System.Drawing.Size(361, 60);
            this.tpResponseSurface.TabIndex = 3;
            this.tpResponseSurface.Text = "Response surface";
            this.tpResponseSurface.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboOutputArrayName);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(355, 52);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Array name";
            // 
            // comboOutputArrayName
            // 
            this.comboOutputArrayName.FormattingEnabled = true;
            this.comboOutputArrayName.Location = new System.Drawing.Point(159, 21);
            this.comboOutputArrayName.Name = "comboOutputArrayName";
            this.comboOutputArrayName.Size = new System.Drawing.Size(190, 21);
            this.comboOutputArrayName.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Link with output array named";
            // 
            // rbString
            // 
            this.rbString.AutoSize = true;
            this.rbString.Location = new System.Drawing.Point(267, 19);
            this.rbString.Name = "rbString";
            this.rbString.Size = new System.Drawing.Size(52, 17);
            this.rbString.TabIndex = 43;
            this.rbString.Text = "String";
            this.rbString.UseVisualStyleBackColor = true;
            // 
            // rbInteger
            // 
            this.rbInteger.AutoSize = true;
            this.rbInteger.Location = new System.Drawing.Point(203, 19);
            this.rbInteger.Name = "rbInteger";
            this.rbInteger.Size = new System.Drawing.Size(58, 17);
            this.rbInteger.TabIndex = 36;
            this.rbInteger.Text = "Integer";
            this.rbInteger.UseVisualStyleBackColor = true;
            // 
            // rbDouble
            // 
            this.rbDouble.AutoSize = true;
            this.rbDouble.Checked = true;
            this.rbDouble.Location = new System.Drawing.Point(138, 19);
            this.rbDouble.Name = "rbDouble";
            this.rbDouble.Size = new System.Drawing.Size(59, 17);
            this.rbDouble.TabIndex = 35;
            this.rbDouble.TabStop = true;
            this.rbDouble.Text = "Double";
            this.rbDouble.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "File resource/component";
            // 
            // comboFileResourcesComponents
            // 
            this.comboFileResourcesComponents.FormattingEnabled = true;
            this.comboFileResourcesComponents.Items.AddRange(new object[] {
            "Double",
            "Integer"});
            this.comboFileResourcesComponents.Location = new System.Drawing.Point(138, 98);
            this.comboFileResourcesComponents.Name = "comboFileResourcesComponents";
            this.comboFileResourcesComponents.Size = new System.Drawing.Size(237, 21);
            this.comboFileResourcesComponents.TabIndex = 31;
            this.comboFileResourcesComponents.SelectedIndexChanged += new System.EventHandler(this.comboFileResourcesComponents_SelectedIndexChanged);
            this.comboFileResourcesComponents.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboFileResourcesComponents_KeyPress);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(231, 290);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numStringLength);
            this.groupBox2.Controls.Add(this.cbStringLength);
            this.groupBox2.Location = new System.Drawing.Point(12, 234);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 52);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output format";
            // 
            // numStringLength
            // 
            this.numStringLength.Enabled = false;
            this.numStringLength.Location = new System.Drawing.Point(208, 22);
            this.numStringLength.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numStringLength.Name = "numStringLength";
            this.numStringLength.Size = new System.Drawing.Size(61, 20);
            this.numStringLength.TabIndex = 34;
            this.numStringLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numStringLength.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbStringLength
            // 
            this.cbStringLength.AutoSize = true;
            this.cbStringLength.Location = new System.Drawing.Point(9, 23);
            this.cbStringLength.Name = "cbStringLength";
            this.cbStringLength.Size = new System.Drawing.Size(193, 17);
            this.cbStringLength.TabIndex = 33;
            this.cbStringLength.Text = "Use fixed length of the output string";
            this.cbStringLength.UseVisualStyleBackColor = true;
            this.cbStringLength.CheckedChanged += new System.EventHandler(this.cbStringLength_CheckedChanged);
            // 
            // frmVariableOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 319);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVariableOutput";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Variable Output Editor";
            this.Load += new System.EventHandler(this.frmVariable_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcMiners.ResumeLayout(false);
            this.tpTxt.ResumeLayout(false);
            this.gbTextFile.ResumeLayout(false);
            this.tpExcel.ResumeLayout(false);
            this.gbExcelFile.ResumeLayout(false);
            this.gbExcelFile.PerformLayout();
            this.tpDll.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpResponseSurface.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStringLength)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboFileResourcesComponents;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.RadioButton rbInteger;
        private System.Windows.Forms.RadioButton rbDouble;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numStringLength;
        private System.Windows.Forms.CheckBox cbStringLength;
        private System.Windows.Forms.GroupBox gbExcelFile;
        private System.Windows.Forms.TextBox tbCellRange;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox gbTextFile;
        private System.Windows.Forms.RadioButton rbString;
        private System.Windows.Forms.TabControl tcMiners;
        private System.Windows.Forms.TabPage tpTxt;
        private System.Windows.Forms.TabPage tpExcel;
        private System.Windows.Forms.TabPage tpDll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboDllItemNames;
        private System.Windows.Forms.TabPage tpResponseSurface;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboOutputArrayName;
        private System.Windows.Forms.Label label7;
    }
}