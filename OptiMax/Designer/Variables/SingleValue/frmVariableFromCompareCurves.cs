﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmVariableFromCompareCurves : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableFromCompareCurves);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variable from Compare Curves' for a scalar parameter which will be computed\n"
                 + "as the difference between two curves.";
        }

        public VariableFromCompareCurves Variable
        {
            get
            {
                return (VariableFromCompareCurves)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmVariableFromCompareCurves()
        {
            InitializeComponent();

            Variable = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;
                if (Variable.ExtrapolateWith0) rbExtrapolate.Checked = true;
                else rbTrim.Checked = true;

                comboDataX1.SelectedItem = Variable.DataXtargetName;
                comboDataY1.SelectedItem = Variable.DataYtargetName;
                cbNormalizeX1.Checked = Variable.NormalizeXtarget;
                comboDataX2.SelectedItem = Variable.DataXcomputedName;
                comboDataY2.SelectedItem = Variable.DataYcomputedName;
                cbNormalizeX2.Checked = Variable.NormalizeXcomputed;

                string method = "Mean square error";
                if (Variable.Method == CurveCompareMethod.MeanSquareError) method = "Mean square error";
                comboMethod.SelectedItem = method;

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }
        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetVariable();

                Pairs = null;
                comboMethod.Items.Clear();
                comboDataX1.Items.Clear();
                comboDataY1.Items.Clear();
                comboDataX2.Items.Clear();
                comboDataY2.Items.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public VariableFromCompareCurves GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            CurveCompareMethod method = CurveCompareMethod.MeanSquareError;
            if (comboMethod.SelectedText == "Mean square error") method = CurveCompareMethod.MeanSquareError;

            VariableFromCompareCurves vfcc = new VariableFromCompareCurves(tbName.Text, tbDescription.Text, type,
                                                                           (string)comboDataX1.SelectedItem,
                                                                           (string)comboDataY1.SelectedItem,
                                                                           cbNormalizeX1.Checked,
                                                                           (string)comboDataX2.SelectedItem,
                                                                           (string)comboDataY2.SelectedItem, 
                                                                           cbNormalizeX2.Checked,
                                                                           method,
                                                                           rbExtrapolate.Checked);
            List<string> allVariables = vfcc.InputVariables;

            bool containsAll = true;
            string variable = "";
            foreach (var var in allVariables)
            {
                variable = var;
                item = Pairs.GetItemByNameIncludingFromContainers(var);
                if (item == null)
                {
                    containsAll = false;
                    break;
                }
            }
            if (!containsAll) throw new Exception("The variable '" + variable + "' does not exist.");


            if (cbStringLength.Checked) vfcc.StringLength = (int)numStringLength.Value;
            else vfcc.StringLength = -1;

            return vfcc;
        }

        override protected void ResetForm()
        {
            comboDataX1.Items.Clear();
            comboDataY1.Items.Clear();
            cbNormalizeX1.Checked = false;
            comboDataX2.Items.Clear();
            comboDataY2.Items.Clear();
            cbNormalizeX2.Checked = false;

            foreach (ArrayBase array in Pairs.GetAllArraysIncludingFromContainers())
            {
                comboDataX1.Items.Add(array.Name);
                comboDataY1.Items.Add(array.Name);
                comboDataX2.Items.Add(array.Name);
                comboDataY2.Items.Add(array.Name);
            }
            if (comboDataX1.Items.Count == 0)
            {
                MessageBox.Show("The workflow does not contain any arrays. First create an array.", "Error", MessageBoxButtons.OK);
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                return;
            }

            comboMethod.Items.Clear();
            comboMethod.Items.Add("Mean square error");

            numStringLength.Value = 10;

            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            comboDataX1.SelectedIndex = 0;
            comboDataY1.SelectedIndex = 0;
            comboDataX2.SelectedIndex = 0;
            comboDataY2.SelectedIndex = 0;

            comboMethod.SelectedIndex = 0;
        }

       

        

       

        

    }
}
