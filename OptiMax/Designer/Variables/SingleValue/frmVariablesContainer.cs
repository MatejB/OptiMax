﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace OptiMax
{
    public partial class frmVariablesContainer : FormWithItem
    {
        // Variables
        private List<Item> variables;

        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ItemContainer);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variables Input' for multiple scalar parameters\n"
                 + "which will be changed by the driver during the job run.";
        }

        public ItemContainer ItemContainer
        {
            get
            {
                return (ItemContainer)Item;
            }
            set
            {
                Item = value;
            }
        }

        // Constructors                                                                                       
        public frmVariablesContainer()
        {
            InitializeComponent();

            ItemContainer = null;

            variables = new List<Item>();
        }
        

        // Event handling                                                                                     
        private void frmVariablesContainer_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (ItemContainer != null)
            {
                tbName.Text = ItemContainer.Name;
                tbDescription.Text = ItemContainer.Description;

                foreach (Item item in ItemContainer.GetItems)
                {
                    InsertNewItem(item);
                }
                lvVariables.Items[0].Selected = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                ItemContainer = GetContainer();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if ((string)cbVariableType.SelectedItem == VariableInput.HumanReadableTypeName)
                InsertNewItem(new VariableInput("variable", "", VariableValueType.Double));
            else if ((string)cbVariableType.SelectedItem == VariableConstant.HumanReadableTypeName)
                InsertNewItem(new VariableConstant("constant", "", VariableValueType.Double));
            else if ((string)cbVariableType.SelectedItem == VariableFunction.HumanReadableTypeName)
                InsertNewItem(new VariableFunction("function", "", ""));

            lvVariables.Items[variables.Count - 1].Selected = true;
        }

        private void lvVariables_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvVariables.SelectedIndices.Count > 0)
            {
                int index = lvVariables.SelectedIndices[0];
                pgProperties.SelectedObject = variables[index];
            }
            else pgProperties.SelectedObject = null;
        }

        private void pgProperties_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (lvVariables.SelectedIndices.Count > 0)
            {
                int i = lvVariables.SelectedIndices[0];

                Item item = (Item)pgProperties.SelectedObject;

                variables.RemoveAt(i);
                lvVariables.Items.RemoveAt(i);

                InsertNewItem(item, i);

                lvVariables.Items[i].Selected = true;
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (lvVariables.SelectedIndices.Count > 0)
            {
                int index = lvVariables.SelectedIndices[0];

                if (index > 0)
                {
                    Item item = variables[index];

                    variables.RemoveAt(index);
                    lvVariables.Items.RemoveAt(index);

                    InsertNewItem(item, index - 1);

                    lvVariables.Items[index - 1].Selected = true;
                }
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (lvVariables.SelectedIndices.Count > 0)
            {
                int index = lvVariables.SelectedIndices[0];

                if (index < variables.Count - 1)
                {
                    Item item = variables[index];

                    variables.RemoveAt(index);
                    lvVariables.Items.RemoveAt(index);

                    InsertNewItem(item, index + 1);

                    lvVariables.Items[index + 1].Selected = true;
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lvVariables.SelectedIndices.Count > 0)
            {
                int i = lvVariables.SelectedIndices[0];
                variables.RemoveAt(i);
                lvVariables.Items.RemoveAt(i);
            }
        }


        // Methods                                                                                            
        public ItemContainer GetContainer()
        {
            Item item;
            ItemContainer ic;

            item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.ItemContainer) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            ic = new ItemContainer(tbName.Text, tbDescription.Text);

            if (variables.Count == 0)
                throw new Exception("At least one input variable must be defined.");

            foreach (Item var in variables)
            {
                item = Pairs.GetItemByNameIncludingFromContainersExcept(var.Name, ic.Name);
                if (item != null && item != var) throw new Exception("The name '" + var.Name + "' is allready in use.");

                if (var is VariableFunction)
                    CheckTheVariableFunction((VariableFunction)var);
               
                ic.Add(var);
            }

            return ic;
        }

        override protected void ResetForm()
        {
            variables.Clear();

            cbVariableType.SelectedIndex = 0;
            lvVariables.Items.Clear();
            pgProperties.SelectedObject = null;
            tbName.Text = "";
            tbDescription.Text = "";
        }

        private void CheckTheVariableFunction(VariableFunction vf)
        {
            Item item;

            List<string> eqVariables = vf.InputVariables;
            //if (eqVariables == null || eqVariables.Count == 0) throw new Exception("The variable functions '" + vf.Name + "' equation does not contain any variables.");

            foreach (string variable in eqVariables)
            {
                item = Pairs.GetItemByNameIncludingFromContainers(variable);
                if (item == null && variables.Find(v => v.Name == variable) == null)
                    throw new Exception("The variable '" + variable + "' referenced in variable function '" + vf.Name + "' does not exist.");
            }
        }

        private void InsertNewItem(Item item, int index = -1)
        {
            string data = "";
            
            if (item is VariableInput) data = "Input variable";
            else if (item is VariableConstant) data = ((VariableConstant)item).Value;
            else if (item is VariableFunction) data = ((VariableFunction)item).Equation;

            if (index == -1)
            {
                lvVariables.Items.Add(new ListViewItem(new string[] { item.Name, data }));
                variables.Add(item);
            }
            else
            {
                lvVariables.Items.Insert(index, new ListViewItem(new string[] { item.Name, data }));
                variables.Insert(index, item);
            }
        }

       




    }
   

}
