﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using Dalssoft.DiagramNet;

namespace OptiMax
{
    public partial class frmVariableFunction : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableFunction);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variable Function' for scalar parameter which will be computed\n"
                 + "from other scalar parameters in the workflow.";
        }

        public VariableFunction Variable
        {
            get
            {
                return (VariableFunction)Item;
            }
            set
            {
                Item = value;
            }
        }
        

        // Constructors                                                                                       
        public frmVariableFunction()
        {
            InitializeComponent();

            Variable = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmFunction_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;
                tbEquation.Text = Variable.Equation;

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }

        private void cbVariableType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetFunction();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private VariableFunction GetFunction()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableFunction vf = new VariableFunction(tbName.Text, tbDescription.Text, tbEquation.Text);
            
            List<string> eqVariables = vf.InputVariables;
            //if (eqVariables == null || eqVariables.Count == 0) throw new Exception("The function equation must have at least one variable.");

            foreach (string variable in eqVariables)
            {
                item = Pairs.GetItemByNameIncludingFromContainers(variable);
                if (item == null) throw new Exception("The variable '" + variable + "' does not exist.");
                if (item.Name == vf.Name) throw new Exception("The function equation may not contain a self-reference.");
            }

            if (cbStringLength.Checked) vf.StringLength = (int)numStringLength.Value;
            else vf.StringLength = -1;

            return vf;
        }

        override protected void ResetForm()
        {
            numStringLength.Value = 10;
            tbName.Text = "";
            tbDescription.Text = "";
            tbEquation.Text = "";
        }


        
    }
}
