﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmVariableConstant : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableConstant);
        }

        public override string GetItemHelp()
        {
            return "Use 'Variable Constant' for scalar parameter which will remain\n"
                 + "constant during the job run.";
        }

        public VariableConstant Variable
        {
            get
            {
                return (VariableConstant)Item;
            }
            set
            {
                Item = value;
            }
        }
        

        // Constructors                                                                                       
        public frmVariableConstant()
        {
            InitializeComponent();

            Variable = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                if (Variable.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Variable.ValueType == VariableValueType.Integer) rbInteger.Checked = true;
                else if (Variable.ValueType == VariableValueType.String) rbString.Checked = true;

                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;
                tbValue.Text = Variable.GetValueAsString();

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }

        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
            numStringLength_ValueChanged(null, null);
        }

        private void numStringLength_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (numStringLength.Enabled)
                {
                    if (rbDouble.Checked)
                    {
                        double d = double.Parse(tbValue.Text, OptiMaxFramework.Globals.nfi);
                        tbValue.Text = OptiMaxFramework.Globals.ConvertToString(d, OptiMaxFramework.Globals.nfi, (int)numStringLength.Value);
                    }
                    else if (rbInteger.Checked)
                    {
                        int i = int.Parse(tbValue.Text);
                        tbValue.Text = OptiMaxFramework.Globals.ConvertToString(i, (int)numStringLength.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetVariable();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public VariableConstant GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;
            else if (rbString.Checked) type = VariableValueType.String;

            VariableConstant vc = new VariableConstant(tbName.Text, tbDescription.Text, type);

            if (rbDouble.Checked) vc.SetValue(double.Parse(tbValue.Text, OptiMaxFramework.Globals.nfi));
            else if (rbInteger.Checked) vc.SetValue(int.Parse(tbValue.Text));
            else if (rbString.Checked) vc.SetValue(tbValue.Text);

            if (cbStringLength.Checked) vc.StringLength = (int)numStringLength.Value;
            else vc.StringLength = -1;

            return vc;
        }
        override protected void ResetForm()
        {
            numStringLength.Value = 10;
            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            tbValue.Text = "";
        }


    }
}
