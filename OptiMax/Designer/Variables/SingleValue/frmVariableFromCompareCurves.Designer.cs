﻿namespace OptiMax
{
    partial class frmVariableFromCompareCurves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboMethod = new System.Windows.Forms.ComboBox();
            this.rbDouble = new System.Windows.Forms.RadioButton();
            this.rbTrim = new System.Windows.Forms.RadioButton();
            this.rbExtrapolate = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numStringLength = new System.Windows.Forms.NumericUpDown();
            this.cbStringLength = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbNormalizeX1 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboDataY1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboDataX1 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbNormalizeX2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboDataY2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboDataX2 = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStringLength)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(449, 386);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(79, 72);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(433, 20);
            this.tbDescription.TabIndex = 3;
            this.tbDescription.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(79, 46);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(433, 20);
            this.tbName.TabIndex = 2;
            this.tbName.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Variable type";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboMethod);
            this.groupBox1.Controls.Add(this.rbDouble);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(518, 129);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Method";
            // 
            // comboMethod
            // 
            this.comboMethod.FormattingEnabled = true;
            this.comboMethod.Items.AddRange(new object[] {
            "Average",
            "Min",
            "Max",
            "Sum"});
            this.comboMethod.Location = new System.Drawing.Point(79, 98);
            this.comboMethod.Name = "comboMethod";
            this.comboMethod.Size = new System.Drawing.Size(171, 21);
            this.comboMethod.TabIndex = 37;
            // 
            // rbDouble
            // 
            this.rbDouble.AutoSize = true;
            this.rbDouble.Location = new System.Drawing.Point(80, 20);
            this.rbDouble.Name = "rbDouble";
            this.rbDouble.Size = new System.Drawing.Size(59, 17);
            this.rbDouble.TabIndex = 35;
            this.rbDouble.Text = "Double";
            this.rbDouble.UseVisualStyleBackColor = true;
            // 
            // rbTrim
            // 
            this.rbTrim.AutoSize = true;
            this.rbTrim.Checked = true;
            this.rbTrim.Location = new System.Drawing.Point(12, 19);
            this.rbTrim.Name = "rbTrim";
            this.rbTrim.Size = new System.Drawing.Size(303, 17);
            this.rbTrim.TabIndex = 40;
            this.rbTrim.TabStop = true;
            this.rbTrim.Text = "Trim longer curve to the length of the shorter (using X units)";
            this.rbTrim.UseVisualStyleBackColor = true;
            // 
            // rbExtrapolate
            // 
            this.rbExtrapolate.AutoSize = true;
            this.rbExtrapolate.Location = new System.Drawing.Point(12, 42);
            this.rbExtrapolate.Name = "rbExtrapolate";
            this.rbExtrapolate.Size = new System.Drawing.Size(233, 17);
            this.rbExtrapolate.TabIndex = 39;
            this.rbExtrapolate.Text = "Extrapolate shorter curve using 0 as Y value";
            this.rbExtrapolate.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(368, 386);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numStringLength);
            this.groupBox2.Controls.Add(this.cbStringLength);
            this.groupBox2.Location = new System.Drawing.Point(12, 328);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(518, 52);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output format";
            // 
            // numStringLength
            // 
            this.numStringLength.Enabled = false;
            this.numStringLength.Location = new System.Drawing.Point(208, 22);
            this.numStringLength.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numStringLength.Name = "numStringLength";
            this.numStringLength.Size = new System.Drawing.Size(61, 20);
            this.numStringLength.TabIndex = 34;
            this.numStringLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numStringLength.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbStringLength
            // 
            this.cbStringLength.AutoSize = true;
            this.cbStringLength.Location = new System.Drawing.Point(9, 23);
            this.cbStringLength.Name = "cbStringLength";
            this.cbStringLength.Size = new System.Drawing.Size(193, 17);
            this.cbStringLength.TabIndex = 33;
            this.cbStringLength.Text = "Use fixed length of the output string";
            this.cbStringLength.UseVisualStyleBackColor = true;
            this.cbStringLength.CheckedChanged += new System.EventHandler(this.cbStringLength_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbNormalizeX1);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.comboDataY1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.comboDataX1);
            this.groupBox3.Location = new System.Drawing.Point(12, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 100);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target curve";
            // 
            // cbNormalizeX1
            // 
            this.cbNormalizeX1.AutoSize = true;
            this.cbNormalizeX1.Location = new System.Drawing.Point(79, 73);
            this.cbNormalizeX1.Name = "cbNormalizeX1";
            this.cbNormalizeX1.Size = new System.Drawing.Size(147, 17);
            this.cbNormalizeX1.TabIndex = 37;
            this.cbNormalizeX1.Text = "Normalize X array to [0, 1]";
            this.cbNormalizeX1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Y data array";
            // 
            // comboDataY1
            // 
            this.comboDataY1.FormattingEnabled = true;
            this.comboDataY1.Location = new System.Drawing.Point(79, 46);
            this.comboDataY1.Name = "comboDataY1";
            this.comboDataY1.Size = new System.Drawing.Size(171, 21);
            this.comboDataY1.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "X data array";
            // 
            // comboDataX1
            // 
            this.comboDataX1.FormattingEnabled = true;
            this.comboDataX1.Location = new System.Drawing.Point(79, 19);
            this.comboDataX1.Name = "comboDataX1";
            this.comboDataX1.Size = new System.Drawing.Size(171, 21);
            this.comboDataX1.TabIndex = 33;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbNormalizeX2);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.comboDataY2);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.comboDataX2);
            this.groupBox4.Location = new System.Drawing.Point(274, 148);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(256, 100);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Computed curve";
            // 
            // cbNormalizeX2
            // 
            this.cbNormalizeX2.AutoSize = true;
            this.cbNormalizeX2.Location = new System.Drawing.Point(79, 73);
            this.cbNormalizeX2.Name = "cbNormalizeX2";
            this.cbNormalizeX2.Size = new System.Drawing.Size(147, 17);
            this.cbNormalizeX2.TabIndex = 38;
            this.cbNormalizeX2.Text = "Normalize X array to [0, 1]";
            this.cbNormalizeX2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Y data array";
            // 
            // comboDataY2
            // 
            this.comboDataY2.FormattingEnabled = true;
            this.comboDataY2.Location = new System.Drawing.Point(79, 46);
            this.comboDataY2.Name = "comboDataY2";
            this.comboDataY2.Size = new System.Drawing.Size(171, 21);
            this.comboDataY2.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "X data array";
            // 
            // comboDataX2
            // 
            this.comboDataX2.FormattingEnabled = true;
            this.comboDataX2.Location = new System.Drawing.Point(79, 19);
            this.comboDataX2.Name = "comboDataX2";
            this.comboDataX2.Size = new System.Drawing.Size(171, 21);
            this.comboDataX2.TabIndex = 33;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbTrim);
            this.groupBox5.Controls.Add(this.rbExtrapolate);
            this.groupBox5.Location = new System.Drawing.Point(12, 254);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(518, 68);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Trim/Extrapolate";
            // 
            // frmVariableFromCompareCurves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 421);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVariableFromCompareCurves";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Variable From Compare Curves Editor";
            this.Load += new System.EventHandler(this.frmVariable_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStringLength)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton rbDouble;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboMethod;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numStringLength;
        private System.Windows.Forms.CheckBox cbStringLength;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboDataY1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboDataX1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboDataY2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboDataX2;
        private System.Windows.Forms.CheckBox cbNormalizeX1;
        private System.Windows.Forms.CheckBox cbNormalizeX2;
        private System.Windows.Forms.RadioButton rbTrim;
        private System.Windows.Forms.RadioButton rbExtrapolate;
        private System.Windows.Forms.GroupBox groupBox5;
    }
}