﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmVariableOutput : FormWithItem, IFilesDirectory
    {
        // Variables                                                                                          
        private DataMiningTemplate template;
        private Dictionary<Type, TabPage> _pages;

        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(VariableOutput);
        }
        
        public override string GetItemHelp()
        {
            return "Use 'Variable Ouput' for scalar parameter which will be read\n"
                 + "from the 'File resource'.";
        }

        public VariableOutput Variable
        {
            get
            {
                return (VariableOutput)Item;
            }
            set
            {
                Item = value;
            }
        }

        public string FilesDirectory { get; set; }

        // Constructors                                                                                       
        public frmVariableOutput()
        {
            InitializeComponent();

            Variable = null;
            template = null;

            _pages = new Dictionary<Type, TabPage>();
            _pages.Add(typeof(DataMiningTemplateTxt), tcMiners.TabPages[0]);
            _pages.Add(typeof(DataMiningTemplateExcel), tcMiners.TabPages[1]);
            _pages.Add(typeof(DataMiningTemplateDll), tcMiners.TabPages[2]);
            _pages.Add(typeof(DataMiningTemplateResponseSurface), tcMiners.TabPages[3]);
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Variable != null)
            {
                if (Variable.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Variable.ValueType == VariableValueType.Integer) rbInteger.Checked = true;
                else if (Variable.ValueType == VariableValueType.String) rbString.Checked = true;

                tbName.Text = Variable.Name;
                tbDescription.Text = Variable.Description;
                
                comboFileResourcesComponents.SelectedItem = Variable.OutputItemName;
                comboFileResourcesComponents_SelectedIndexChanged(null, null);    // turn tabs on/off

                // set template and activate appropriate tab page
                tcMiners.SelectedTab = _pages[Variable.Template.GetType()];
                template = Variable.Template;  // previous lines reset the template

                // fill in mining data
                if (template is DataMiningTemplateExcel dmte) tbCellRange.Text = dmte.CellRange;
                else if (template is DataMiningTemplateDll dmtd) comboDllItemNames.SelectedItem = dmtd.DllName;
                else if (template is DataMiningTemplateResponseSurface dmtrs) comboOutputArrayName.SelectedItem = dmtrs.OutputArrayName;

                if (Variable.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Variable.StringLength >= numStringLength.Minimum) numStringLength.Value = Variable.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
            else
                comboFileResourcesComponents_SelectedIndexChanged(null, null);
        }
        private void comboFileResourcesComponents_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboFileResourcesComponents_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string itemName = (string)comboFileResourcesComponents.SelectedItem;
                Item item = Pairs.GetItemByNameIncludingFromContainers(itemName);
                if (item != null)
                {
                    tcMiners.TabPages.Clear();

                    if (item is ExcelComponent)
                    {
                        tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateExcel)]);
                    }
                    else if (item is DllComponent dc)
                    {
                        tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateDll)]);
                        var dll = DllComponent.GetDllObject(dc.DllFile.FileNameWithPath);
                        comboDllItemNames.Items.Clear();

                        foreach (var name in dll.GetVariableNames()) comboDllItemNames.Items.Add(name);
                    }
                    else if (item is ResponseSurfaceComponent rs)
                    {
                        tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateResponseSurface)]);
                        comboOutputArrayName.Items.Clear();
                        foreach (var name in rs.OutputArrayNames) comboOutputArrayName.Items.Add(name);
                    }
                    else
                    {
                        tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateTxt)]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }
        private void btnTemplate_Click(object sender, EventArgs e)
        {
            frmDataMining frm_dataMining = new frmDataMining();
            frm_dataMining.Template = template as DataMiningTemplateTxt;
            frm_dataMining.FilesDirectory = this.FilesDirectory;
            
            if (frm_dataMining.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                template = frm_dataMining.Template;
                if (frm_dataMining.Template.RowNumbers.Length > 1)
                    MessageBox.Show("Only the first row of the selected data will be mined.", "Warning", MessageBoxButtons.OK);
                btnOK.Enabled = true;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Variable = GetVariable();

                Pairs = null;
                comboFileResourcesComponents.Items.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        

        // Methods                                                                                            
        public VariableOutput GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Variable) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;
            else if (rbString.Checked) type = VariableValueType.String;

            string outputItemName = (string)comboFileResourcesComponents.SelectedItem;
            if (outputItemName == null || outputItemName == "") throw new Exception("Select one file resource or component.");

            if (tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateExcel)])                // Excel
            {
                template = new DataMiningTemplateExcel(tbCellRange.Text);

                if (!Helpers.ExcelHelper.IsThisSingleCellRangeValid((template as DataMiningTemplateExcel).CellRange))
                    throw new Exception("The cell range '" + (template as DataMiningTemplateExcel).CellRange + "' is not a valid single cell range.");
            }
            else if(tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateDll)])              // Dll
            {
                template = new DataMiningTemplateDll((string)comboDllItemNames.SelectedItem);
            }
            else if (tcMiners.SelectedTab== _pages[typeof(DataMiningTemplateResponseSurface)])  // RS
            {
                template = new DataMiningTemplateResponseSurface((string)comboOutputArrayName.SelectedItem);
            }
            else                                                                                // Txt
            {
                if (template == null || !(template is DataMiningTemplateTxt))
                {
                    throw new Exception("The txt mining template is missing.");
                }
            }

            VariableOutput vo = new VariableOutput(tbName.Text, tbDescription.Text, type, outputItemName, template);

            if (cbStringLength.Checked) vo.StringLength = (int)numStringLength.Value;
            else vo.StringLength = -1;

            return vo;
        }
        override protected void ResetForm()
        {
            comboFileResourcesComponents.Items.Clear();
            foreach (FileResource fileResource in Pairs.GetAllFileResourcesIncludingFromContainers())
            {
                comboFileResourcesComponents.Items.Add(fileResource.Name);
            }

            foreach (Component component in Pairs.GetAllComponents())
            {
                if (component is ExcelComponent) comboFileResourcesComponents.Items.Add(component.Name);
                else if (component is DllComponent) comboFileResourcesComponents.Items.Add(component.Name);
                else if (component is ResponseSurfaceComponent) comboFileResourcesComponents.Items.Add(component.Name);
            }

            numStringLength.Value = 10;

            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            if (comboFileResourcesComponents.Items.Count > 0) comboFileResourcesComponents.SelectedIndex = 0;
            tbCellRange.Text = "";
            comboDllItemNames.Items.Clear();
        }

        

    }
}
