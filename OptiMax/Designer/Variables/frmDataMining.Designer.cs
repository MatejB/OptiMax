﻿namespace OptiMax
{
    partial class frmDataMining
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbLineNumbers = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numSkipLines = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numNumOfRows = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClearWord = new System.Windows.Forms.Button();
            this.tbStartWord = new System.Windows.Forms.TextBox();
            this.btnClearStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetDataWord = new System.Windows.Forms.Button();
            this.numDataWordColumn = new System.Windows.Forms.NumericUpDown();
            this.btnSetStartWord = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboDecimalSeparator = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbOtherDelimiter = new System.Windows.Forms.TextBox();
            this.cbOther = new System.Windows.Forms.CheckBox();
            this.cbSemicolon = new System.Windows.Forms.CheckBox();
            this.cbComma = new System.Windows.Forms.CheckBox();
            this.cbTab = new System.Windows.Forms.CheckBox();
            this.cbSpace = new System.Windows.Forms.CheckBox();
            this.rtbFileData = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setStartWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDataWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLineNumbers)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSkipLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNumOfRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDataWordColumn)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pbLineNumbers);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.rtbFileData);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(797, 461);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // pbLineNumbers
            // 
            this.pbLineNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pbLineNumbers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbLineNumbers.Location = new System.Drawing.Point(224, 25);
            this.pbLineNumbers.Name = "pbLineNumbers";
            this.pbLineNumbers.Size = new System.Drawing.Size(47, 430);
            this.pbLineNumbers.TabIndex = 46;
            this.pbLineNumbers.TabStop = false;
            this.pbLineNumbers.Paint += new System.Windows.Forms.PaintEventHandler(this.pbLineNumbers_Paint);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numSkipLines);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.numNumOfRows);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.btnClearWord);
            this.groupBox4.Controls.Add(this.tbStartWord);
            this.groupBox4.Controls.Add(this.btnClearStart);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnSetDataWord);
            this.groupBox4.Controls.Add(this.numDataWordColumn);
            this.groupBox4.Controls.Add(this.btnSetStartWord);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(6, 192);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(212, 227);
            this.groupBox4.TabIndex = 45;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Mining";
            // 
            // numSkipLines
            // 
            this.numSkipLines.BackColor = System.Drawing.SystemColors.Window;
            this.numSkipLines.Location = new System.Drawing.Point(117, 139);
            this.numSkipLines.Name = "numSkipLines";
            this.numSkipLines.Size = new System.Drawing.Size(85, 20);
            this.numSkipLines.TabIndex = 56;
            this.numSkipLines.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSkipLines.ValueChanged += new System.EventHandler(this.numSkipLines_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Skip lines";
            // 
            // numNumOfRows
            // 
            this.numNumOfRows.BackColor = System.Drawing.SystemColors.Window;
            this.numNumOfRows.Enabled = false;
            this.numNumOfRows.Location = new System.Drawing.Point(117, 87);
            this.numNumOfRows.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numNumOfRows.Name = "numNumOfRows";
            this.numNumOfRows.ReadOnly = true;
            this.numNumOfRows.Size = new System.Drawing.Size(85, 20);
            this.numNumOfRows.TabIndex = 54;
            this.numNumOfRows.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 53;
            this.label3.Text = "Num. of rows";
            // 
            // btnClearWord
            // 
            this.btnClearWord.Location = new System.Drawing.Point(36, 172);
            this.btnClearWord.Name = "btnClearWord";
            this.btnClearWord.Size = new System.Drawing.Size(75, 23);
            this.btnClearWord.TabIndex = 51;
            this.btnClearWord.Text = "Clear data";
            this.btnClearWord.UseVisualStyleBackColor = true;
            this.btnClearWord.Click += new System.EventHandler(this.btnClearDataWord_Click);
            // 
            // tbStartWord
            // 
            this.tbStartWord.BackColor = System.Drawing.SystemColors.Window;
            this.tbStartWord.Enabled = false;
            this.tbStartWord.Location = new System.Drawing.Point(117, 19);
            this.tbStartWord.Name = "tbStartWord";
            this.tbStartWord.ReadOnly = true;
            this.tbStartWord.Size = new System.Drawing.Size(85, 20);
            this.tbStartWord.TabIndex = 33;
            // 
            // btnClearStart
            // 
            this.btnClearStart.Location = new System.Drawing.Point(36, 45);
            this.btnClearStart.Name = "btnClearStart";
            this.btnClearStart.Size = new System.Drawing.Size(75, 23);
            this.btnClearStart.TabIndex = 50;
            this.btnClearStart.Text = "Clear start";
            this.btnClearStart.UseVisualStyleBackColor = true;
            this.btnClearStart.Click += new System.EventHandler(this.btnClearStartWord_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Start text";
            // 
            // btnSetDataWord
            // 
            this.btnSetDataWord.Location = new System.Drawing.Point(117, 172);
            this.btnSetDataWord.Name = "btnSetDataWord";
            this.btnSetDataWord.Size = new System.Drawing.Size(85, 23);
            this.btnSetDataWord.TabIndex = 49;
            this.btnSetDataWord.Text = "Set data";
            this.btnSetDataWord.UseVisualStyleBackColor = true;
            this.btnSetDataWord.Click += new System.EventHandler(this.btnSetDataWord_Click);
            // 
            // numDataWordColumn
            // 
            this.numDataWordColumn.BackColor = System.Drawing.SystemColors.Window;
            this.numDataWordColumn.Location = new System.Drawing.Point(117, 113);
            this.numDataWordColumn.Name = "numDataWordColumn";
            this.numDataWordColumn.Size = new System.Drawing.Size(85, 20);
            this.numDataWordColumn.TabIndex = 39;
            this.numDataWordColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numDataWordColumn.ValueChanged += new System.EventHandler(this.numDataWordColumn_ValueChanged);
            // 
            // btnSetStartWord
            // 
            this.btnSetStartWord.Location = new System.Drawing.Point(117, 45);
            this.btnSetStartWord.Name = "btnSetStartWord";
            this.btnSetStartWord.Size = new System.Drawing.Size(85, 23);
            this.btnSetStartWord.TabIndex = 48;
            this.btnSetStartWord.Text = "Set start";
            this.btnSetStartWord.UseVisualStyleBackColor = true;
            this.btnSetStartWord.Click += new System.EventHandler(this.btnSetStartWord_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Data word col";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.comboDecimalSeparator);
            this.groupBox3.Location = new System.Drawing.Point(6, 131);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(212, 55);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Numbers";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Decimal separator";
            // 
            // comboDecimalSeparator
            // 
            this.comboDecimalSeparator.FormattingEnabled = true;
            this.comboDecimalSeparator.Items.AddRange(new object[] {
            ". (dot)",
            ", (comma)"});
            this.comboDecimalSeparator.Location = new System.Drawing.Point(117, 21);
            this.comboDecimalSeparator.Name = "comboDecimalSeparator";
            this.comboDecimalSeparator.Size = new System.Drawing.Size(85, 21);
            this.comboDecimalSeparator.TabIndex = 41;
            this.comboDecimalSeparator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboDecimalSeparator_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbOtherDelimiter);
            this.groupBox2.Controls.Add(this.cbOther);
            this.groupBox2.Controls.Add(this.cbSemicolon);
            this.groupBox2.Controls.Add(this.cbComma);
            this.groupBox2.Controls.Add(this.cbTab);
            this.groupBox2.Controls.Add(this.cbSpace);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 106);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Word delimiters";
            // 
            // tbOtherDelimiter
            // 
            this.tbOtherDelimiter.Enabled = false;
            this.tbOtherDelimiter.Location = new System.Drawing.Point(117, 74);
            this.tbOtherDelimiter.Name = "tbOtherDelimiter";
            this.tbOtherDelimiter.Size = new System.Drawing.Size(85, 20);
            this.tbOtherDelimiter.TabIndex = 34;
            this.tbOtherDelimiter.TextChanged += new System.EventHandler(this.tbOtherDelimiter_TextChanged);
            // 
            // cbOther
            // 
            this.cbOther.AutoSize = true;
            this.cbOther.Location = new System.Drawing.Point(22, 75);
            this.cbOther.Name = "cbOther";
            this.cbOther.Size = new System.Drawing.Size(74, 17);
            this.cbOther.TabIndex = 4;
            this.cbOther.Text = "Other (-,*):";
            this.cbOther.UseVisualStyleBackColor = true;
            this.cbOther.CheckedChanged += new System.EventHandler(this.cbOther_CheckedChanged);
            // 
            // cbSemicolon
            // 
            this.cbSemicolon.AutoSize = true;
            this.cbSemicolon.Location = new System.Drawing.Point(117, 49);
            this.cbSemicolon.Name = "cbSemicolon";
            this.cbSemicolon.Size = new System.Drawing.Size(75, 17);
            this.cbSemicolon.TabIndex = 3;
            this.cbSemicolon.Text = "Semicolon";
            this.cbSemicolon.UseVisualStyleBackColor = true;
            this.cbSemicolon.CheckedChanged += new System.EventHandler(this.cbDelimiters_CheckedChanged);
            // 
            // cbComma
            // 
            this.cbComma.AutoSize = true;
            this.cbComma.Location = new System.Drawing.Point(22, 49);
            this.cbComma.Name = "cbComma";
            this.cbComma.Size = new System.Drawing.Size(61, 17);
            this.cbComma.TabIndex = 2;
            this.cbComma.Text = "Comma";
            this.cbComma.UseVisualStyleBackColor = true;
            this.cbComma.CheckedChanged += new System.EventHandler(this.cbDelimiters_CheckedChanged);
            // 
            // cbTab
            // 
            this.cbTab.AutoSize = true;
            this.cbTab.Checked = true;
            this.cbTab.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTab.Location = new System.Drawing.Point(117, 23);
            this.cbTab.Name = "cbTab";
            this.cbTab.Size = new System.Drawing.Size(45, 17);
            this.cbTab.TabIndex = 1;
            this.cbTab.Text = "Tab";
            this.cbTab.UseVisualStyleBackColor = true;
            this.cbTab.CheckedChanged += new System.EventHandler(this.cbDelimiters_CheckedChanged);
            // 
            // cbSpace
            // 
            this.cbSpace.AutoSize = true;
            this.cbSpace.Checked = true;
            this.cbSpace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSpace.Location = new System.Drawing.Point(22, 23);
            this.cbSpace.Name = "cbSpace";
            this.cbSpace.Size = new System.Drawing.Size(57, 17);
            this.cbSpace.TabIndex = 0;
            this.cbSpace.Text = "Space";
            this.cbSpace.UseVisualStyleBackColor = true;
            this.cbSpace.CheckedChanged += new System.EventHandler(this.cbDelimiters_CheckedChanged);
            // 
            // rtbFileData
            // 
            this.rtbFileData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbFileData.BackColor = System.Drawing.SystemColors.Window;
            this.rtbFileData.ContextMenuStrip = this.contextMenuStrip1;
            this.rtbFileData.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbFileData.Location = new System.Drawing.Point(271, 25);
            this.rtbFileData.Name = "rtbFileData";
            this.rtbFileData.ReadOnly = true;
            this.rtbFileData.Size = new System.Drawing.Size(520, 430);
            this.rtbFileData.TabIndex = 27;
            this.rtbFileData.Text = "";
            this.rtbFileData.WordWrap = false;
            this.rtbFileData.VScroll += new System.EventHandler(this.rtbFileData_VScroll);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.setStartWordToolStripMenuItem,
            this.setDataWordToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(147, 70);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // setStartWordToolStripMenuItem
            // 
            this.setStartWordToolStripMenuItem.Name = "setStartWordToolStripMenuItem";
            this.setStartWordToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.setStartWordToolStripMenuItem.Text = "Set start word";
            this.setStartWordToolStripMenuItem.Click += new System.EventHandler(this.setStartWordToolStripMenuItem_Click);
            // 
            // setDataWordToolStripMenuItem
            // 
            this.setDataWordToolStripMenuItem.Name = "setDataWordToolStripMenuItem";
            this.setDataWordToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.setDataWordToolStripMenuItem.Text = "Set data word";
            this.setDataWordToolStripMenuItem.Click += new System.EventHandler(this.setDataWordToolStripMenuItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(647, 479);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(728, 479);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 29;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmDataMining
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 509);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDataMining";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Data Mining";
            this.Load += new System.EventHandler(this.frmDataMining_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLineNumbers)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSkipLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNumOfRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDataWordColumn)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtbFileData;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.NumericUpDown numDataWordColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboDecimalSeparator;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbOtherDelimiter;
        private System.Windows.Forms.CheckBox cbOther;
        private System.Windows.Forms.CheckBox cbSemicolon;
        private System.Windows.Forms.CheckBox cbComma;
        private System.Windows.Forms.CheckBox cbTab;
        private System.Windows.Forms.CheckBox cbSpace;
        private System.Windows.Forms.Button btnClearWord;
        private System.Windows.Forms.Button btnClearStart;
        private System.Windows.Forms.Button btnSetDataWord;
        private System.Windows.Forms.Button btnSetStartWord;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setStartWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDataWordToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbLineNumbers;
        private System.Windows.Forms.TextBox tbStartWord;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numNumOfRows;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numSkipLines;
        private System.Windows.Forms.Label label2;
    }
}