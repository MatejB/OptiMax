﻿namespace OptiMax
{
    partial class frmArrayOutput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tcMiners = new System.Windows.Forms.TabControl();
            this.tpAbaqus = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbAbaqusHistoryOutputs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboAbaqusHistoryOutputs = new System.Windows.Forms.ComboBox();
            this.btnLoadAbaqusOutputs = new System.Windows.Forms.Button();
            this.tpDynaNodout = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbLsDynaNodout = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboLsDynaNodout = new System.Windows.Forms.ComboBox();
            this.btnLoadLsDyna = new System.Windows.Forms.Button();
            this.tpExcel = new System.Windows.Forms.TabPage();
            this.gbExcelFile = new System.Windows.Forms.GroupBox();
            this.tbCellRange = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tpTxt = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.tpDll = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.rbInteger = new System.Windows.Forms.RadioButton();
            this.rbDouble = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.comboFileResources = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.cbDllName = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.tcMiners.SuspendLayout();
            this.tpAbaqus.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpDynaNodout.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpExcel.SuspendLayout();
            this.gbExcelFile.SuspendLayout();
            this.tpTxt.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tpDll.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(227, 262);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tcMiners);
            this.groupBox1.Controls.Add(this.rbInteger);
            this.groupBox1.Controls.Add(this.rbDouble);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboFileResources);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(416, 243);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // tcMiners
            // 
            this.tcMiners.Controls.Add(this.tpAbaqus);
            this.tcMiners.Controls.Add(this.tpDynaNodout);
            this.tcMiners.Controls.Add(this.tpExcel);
            this.tcMiners.Controls.Add(this.tpTxt);
            this.tcMiners.Controls.Add(this.tpDll);
            this.tcMiners.Location = new System.Drawing.Point(6, 125);
            this.tcMiners.Name = "tcMiners";
            this.tcMiners.SelectedIndex = 0;
            this.tcMiners.Size = new System.Drawing.Size(404, 112);
            this.tcMiners.TabIndex = 32;
            this.tcMiners.SelectedIndexChanged += new System.EventHandler(this.tcMiners_SelectedIndexChanged);
            // 
            // tpAbaqus
            // 
            this.tpAbaqus.Controls.Add(this.groupBox2);
            this.tpAbaqus.Location = new System.Drawing.Point(4, 22);
            this.tpAbaqus.Name = "tpAbaqus";
            this.tpAbaqus.Padding = new System.Windows.Forms.Padding(3);
            this.tpAbaqus.Size = new System.Drawing.Size(396, 86);
            this.tpAbaqus.TabIndex = 0;
            this.tpAbaqus.Text = "Abaqus";
            this.tpAbaqus.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbAbaqusHistoryOutputs);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.comboAbaqusHistoryOutputs);
            this.groupBox2.Controls.Add(this.btnLoadAbaqusOutputs);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(390, 80);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Abaqus file";
            // 
            // tbAbaqusHistoryOutputs
            // 
            this.tbAbaqusHistoryOutputs.Location = new System.Drawing.Point(90, 48);
            this.tbAbaqusHistoryOutputs.Name = "tbAbaqusHistoryOutputs";
            this.tbAbaqusHistoryOutputs.Size = new System.Drawing.Size(294, 20);
            this.tbAbaqusHistoryOutputs.TabIndex = 36;
            this.tbAbaqusHistoryOutputs.Text = "Outputs";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Variable name";
            // 
            // comboAbaqusHistoryOutputs
            // 
            this.comboAbaqusHistoryOutputs.FormattingEnabled = true;
            this.comboAbaqusHistoryOutputs.Location = new System.Drawing.Point(90, 21);
            this.comboAbaqusHistoryOutputs.Name = "comboAbaqusHistoryOutputs";
            this.comboAbaqusHistoryOutputs.Size = new System.Drawing.Size(294, 21);
            this.comboAbaqusHistoryOutputs.TabIndex = 34;
            this.comboAbaqusHistoryOutputs.SelectedIndexChanged += new System.EventHandler(this.comboAbaqusHistoryOutputs_SelectedIndexChanged);
            // 
            // btnLoadAbaqusOutputs
            // 
            this.btnLoadAbaqusOutputs.Location = new System.Drawing.Point(6, 19);
            this.btnLoadAbaqusOutputs.Name = "btnLoadAbaqusOutputs";
            this.btnLoadAbaqusOutputs.Size = new System.Drawing.Size(78, 23);
            this.btnLoadAbaqusOutputs.TabIndex = 33;
            this.btnLoadAbaqusOutputs.Text = "Load outputs";
            this.btnLoadAbaqusOutputs.UseVisualStyleBackColor = true;
            this.btnLoadAbaqusOutputs.Click += new System.EventHandler(this.btnLoadAbaqusOutputs_Click);
            // 
            // tpDynaNodout
            // 
            this.tpDynaNodout.Controls.Add(this.groupBox3);
            this.tpDynaNodout.Location = new System.Drawing.Point(4, 22);
            this.tpDynaNodout.Name = "tpDynaNodout";
            this.tpDynaNodout.Size = new System.Drawing.Size(396, 86);
            this.tpDynaNodout.TabIndex = 3;
            this.tpDynaNodout.Text = "Ls-Dyna";
            this.tpDynaNodout.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbLsDynaNodout);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.comboLsDynaNodout);
            this.groupBox3.Controls.Add(this.btnLoadLsDyna);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(390, 80);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ls-Dyna result file";
            // 
            // tbLsDynaNodout
            // 
            this.tbLsDynaNodout.Location = new System.Drawing.Point(90, 48);
            this.tbLsDynaNodout.Name = "tbLsDynaNodout";
            this.tbLsDynaNodout.Size = new System.Drawing.Size(294, 20);
            this.tbLsDynaNodout.TabIndex = 36;
            this.tbLsDynaNodout.Text = "Outputs";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Variable name";
            // 
            // comboLsDynaNodout
            // 
            this.comboLsDynaNodout.FormattingEnabled = true;
            this.comboLsDynaNodout.Location = new System.Drawing.Point(90, 21);
            this.comboLsDynaNodout.Name = "comboLsDynaNodout";
            this.comboLsDynaNodout.Size = new System.Drawing.Size(294, 21);
            this.comboLsDynaNodout.TabIndex = 34;
            this.comboLsDynaNodout.SelectedIndexChanged += new System.EventHandler(this.comboLsDynaNodout_SelectedIndexChanged);
            // 
            // btnLoadLsDyna
            // 
            this.btnLoadLsDyna.Location = new System.Drawing.Point(6, 19);
            this.btnLoadLsDyna.Name = "btnLoadLsDyna";
            this.btnLoadLsDyna.Size = new System.Drawing.Size(78, 23);
            this.btnLoadLsDyna.TabIndex = 33;
            this.btnLoadLsDyna.Text = "Load outputs";
            this.btnLoadLsDyna.UseVisualStyleBackColor = true;
            this.btnLoadLsDyna.Click += new System.EventHandler(this.btnLoadLsDyna_Click);
            // 
            // tpExcel
            // 
            this.tpExcel.Controls.Add(this.gbExcelFile);
            this.tpExcel.Location = new System.Drawing.Point(4, 22);
            this.tpExcel.Name = "tpExcel";
            this.tpExcel.Padding = new System.Windows.Forms.Padding(3);
            this.tpExcel.Size = new System.Drawing.Size(396, 86);
            this.tpExcel.TabIndex = 1;
            this.tpExcel.Text = "Excel";
            this.tpExcel.UseVisualStyleBackColor = true;
            // 
            // gbExcelFile
            // 
            this.gbExcelFile.Controls.Add(this.tbCellRange);
            this.gbExcelFile.Controls.Add(this.label5);
            this.gbExcelFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbExcelFile.Location = new System.Drawing.Point(3, 3);
            this.gbExcelFile.Name = "gbExcelFile";
            this.gbExcelFile.Size = new System.Drawing.Size(390, 80);
            this.gbExcelFile.TabIndex = 43;
            this.gbExcelFile.TabStop = false;
            this.gbExcelFile.Text = "Excel file (Range: A2:A11)";
            // 
            // tbCellRange
            // 
            this.tbCellRange.Location = new System.Drawing.Point(75, 21);
            this.tbCellRange.Name = "tbCellRange";
            this.tbCellRange.Size = new System.Drawing.Size(309, 20);
            this.tbCellRange.TabIndex = 39;
            this.tbCellRange.Text = "Cell Range";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Cell Range";
            // 
            // tpTxt
            // 
            this.tpTxt.Controls.Add(this.groupBox4);
            this.tpTxt.Location = new System.Drawing.Point(4, 22);
            this.tpTxt.Name = "tpTxt";
            this.tpTxt.Size = new System.Drawing.Size(396, 86);
            this.tpTxt.TabIndex = 2;
            this.tpTxt.Text = "Txt file";
            this.tpTxt.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnTemplate);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(390, 80);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Text file";
            // 
            // btnTemplate
            // 
            this.btnTemplate.Location = new System.Drawing.Point(6, 19);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(378, 23);
            this.btnTemplate.TabIndex = 33;
            this.btnTemplate.Text = "Create/edit template";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // tpDll
            // 
            this.tpDll.Controls.Add(this.groupBox5);
            this.tpDll.Location = new System.Drawing.Point(4, 22);
            this.tpDll.Name = "tpDll";
            this.tpDll.Size = new System.Drawing.Size(396, 86);
            this.tpDll.TabIndex = 4;
            this.tpDll.Text = "Dll";
            this.tpDll.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbDllName);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(390, 80);
            this.groupBox5.TabIndex = 44;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dll file";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Dll variable/array name";
            // 
            // rbInteger
            // 
            this.rbInteger.AutoSize = true;
            this.rbInteger.Location = new System.Drawing.Point(145, 20);
            this.rbInteger.Name = "rbInteger";
            this.rbInteger.Size = new System.Drawing.Size(58, 17);
            this.rbInteger.TabIndex = 36;
            this.rbInteger.Text = "Integer";
            this.rbInteger.UseVisualStyleBackColor = true;
            // 
            // rbDouble
            // 
            this.rbDouble.AutoSize = true;
            this.rbDouble.Checked = true;
            this.rbDouble.Location = new System.Drawing.Point(80, 20);
            this.rbDouble.Name = "rbDouble";
            this.rbDouble.Size = new System.Drawing.Size(59, 17);
            this.rbDouble.TabIndex = 35;
            this.rbDouble.TabStop = true;
            this.rbDouble.Text = "Double";
            this.rbDouble.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "File resource";
            // 
            // comboFileResources
            // 
            this.comboFileResources.FormattingEnabled = true;
            this.comboFileResources.Items.AddRange(new object[] {
            "Double",
            "Integer"});
            this.comboFileResources.Location = new System.Drawing.Point(79, 98);
            this.comboFileResources.Name = "comboFileResources";
            this.comboFileResources.Size = new System.Drawing.Size(331, 21);
            this.comboFileResources.TabIndex = 31;
            this.comboFileResources.SelectedIndexChanged += new System.EventHandler(this.comboFileResources_SelectedIndexChanged);
            this.comboFileResources.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboFileResources_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Variable type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Name";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(79, 46);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(331, 20);
            this.tbName.TabIndex = 2;
            this.tbName.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(79, 72);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(331, 20);
            this.tbDescription.TabIndex = 3;
            this.tbDescription.Text = "Description";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(308, 262);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cbDllName
            // 
            this.cbDllName.FormattingEnabled = true;
            this.cbDllName.Location = new System.Drawing.Point(132, 21);
            this.cbDllName.Name = "cbDllName";
            this.cbDllName.Size = new System.Drawing.Size(252, 21);
            this.cbDllName.TabIndex = 45;
            // 
            // frmArrayOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 292);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmArrayOutput";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Output Array Editor";
            this.Load += new System.EventHandler(this.frmVariable_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcMiners.ResumeLayout(false);
            this.tpAbaqus.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tpDynaNodout.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpExcel.ResumeLayout(false);
            this.gbExcelFile.ResumeLayout(false);
            this.gbExcelFile.PerformLayout();
            this.tpTxt.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tpDll.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboFileResources;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.RadioButton rbInteger;
        private System.Windows.Forms.RadioButton rbDouble;
        private System.Windows.Forms.GroupBox gbExcelFile;
        private System.Windows.Forms.TextBox tbCellRange;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tcMiners;
        private System.Windows.Forms.TabPage tpAbaqus;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnLoadAbaqusOutputs;
        private System.Windows.Forms.TabPage tpExcel;
        private System.Windows.Forms.TabPage tpTxt;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox comboAbaqusHistoryOutputs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbAbaqusHistoryOutputs;
        private System.Windows.Forms.TabPage tpDynaNodout;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbLsDynaNodout;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboLsDynaNodout;
        private System.Windows.Forms.Button btnLoadLsDyna;
        private System.Windows.Forms.TabPage tpDll;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbDllName;
    }
}