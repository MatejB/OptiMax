﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using Dalssoft.DiagramNet;

namespace OptiMax
{
    public partial class frmArrayFunction : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ArrayFunction);
        }

        public override string GetItemHelp()
        {
            return "Use 'Array Function' for vector parameter which will be computed\n"
                 + "from other vector parameters (of the same length) in the workflow.";
        }

        public ArrayFunction Array
        {
            get
            {
                return (ArrayFunction)Item;
            }
            set
            {
                Item = value;
            }
        }
        

        // Constructors                                                                                       
        public frmArrayFunction()
        {
            InitializeComponent();

            Array = null;
            Pairs = null;
        }


        // Event handling                                                                                     
        private void frmFunction_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Array != null)
            {
                tbName.Text = Array.Name;
                tbDescription.Text = Array.Description;
                tbEquation.Text = Array.Equation;
            }
        }

        private void cbVariableType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Array = GetFunction();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        private ArrayFunction GetFunction()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Array) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            ArrayFunction arrFunction = new ArrayFunction(tbName.Text, tbDescription.Text, tbEquation.Text);
            
            List<string> eqVariables = arrFunction.InputVariables;
            if (eqVariables == null || eqVariables.Count == 0) throw new Exception("The function equation must have at least one variable.");

            int numArrVars = 0;
            foreach (string variable in eqVariables)
            {
                item = Pairs.GetItemByNameIncludingFromContainers(variable);
                if (item == null) throw new Exception("The variable '" + variable + "' does not exist.");
                if (item.Name == arrFunction.Name) throw new Exception("The function equation may not contain a self-reference.");

                if (item is ArrayBase) numArrVars++;
            }
            
            if (numArrVars <= 0) throw new Exception("The function equation must have at least one array variable.");
            

            return arrFunction;
        }

        override protected void ResetForm()
        {
            int numArrVars = Pairs.GetAllArraysIncludingFromContainers().Length;
            if (numArrVars <= 0)
            {
                MessageBox.Show("The workflow does not contain any arrays. First create an array.", "Error", MessageBoxButtons.OK);
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                return;
            }

            tbName.Text = "";
            tbDescription.Text = "";
            tbEquation.Text = "";
        }

       

        


        
    }
}
