﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmArrayConstant : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ArrayConstant);
        }

        public override string GetItemHelp()
        {
            return "Use 'Array Constant' for vector parameter which will remain\n"
                 + "constant during the job run.";
        }

        public ArrayConstant Array
        {
            get
            {
                return (ArrayConstant)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmArrayConstant()
        {
            InitializeComponent();

            Array = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Array != null)
            {
                if (Array.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Array.ValueType == VariableValueType.Integer) rbInteger.Checked = true;
                else if (Array.ValueType == VariableValueType.String) rbString.Checked = true;

                tbName.Text = Array.Name;
                tbDescription.Text = Array.Description;
                tbValue.Text = Array.GetValuesAsString(OptiMaxFramework.Globals.nfi, Environment.NewLine);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Array = GetVariable();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public ArrayConstant GetVariable()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Array) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;
            else if (rbString.Checked) type = VariableValueType.String;

            ArrayConstant ac = new ArrayConstant(tbName.Text, tbDescription.Text, type);

            string[] array = tbValue.Text.Split(new string[] {"\r", "\n"}, StringSplitOptions.RemoveEmptyEntries);
            if (array.Length <= 0) throw new Exception("The array must contain at least one row of data.");

            if (rbDouble.Checked)
            {
                double[] values = new double[array.Length];
                for (int i = 0; i < array.Length; i++)
			    {
                    values[i] = double.Parse(array[i], OptiMaxFramework.Globals.nfi);
			    }
                ac.SetValues(values);
            }
            else if (rbInteger.Checked)
            {
                int[] values = new int[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    values[i] = int.Parse(array[i], OptiMaxFramework.Globals.nfi);
                }
                ac.SetValues(values);
            }
            else if (rbString.Checked) ac.SetValues(array);

            return ac;
        }

        override protected void ResetForm()
        {
            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            tbValue.Text = "";
        }

        private void tbValue_TextChanged(object sender, EventArgs e)
        {
            labNumOfValues.Text = "Number of values: " + tbValue.Lines.Length;
        }
    }
}
