﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;
using System.IO;

namespace OptiMax
{
    enum TemplateNames
    {
        Abaqus = 0,
        LsDyna = 1,
        Excel = 2,
        Txt = 3,
        Dll = 4
    }
    public partial class frmArrayOutput : FormWithItem, IFilesDirectory
    {
        // Variables                                                                                          
        private DataMiningTemplate template;
        private Dictionary<Type, TabPage> _pages;

        
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ArrayOutput);
        }
        public override string GetItemHelp()
        {
            return "Use 'Array Ouput' for vector parameter which will be read\n"
                 + "from the 'File resource'.";
        }
        public ArrayOutput Array
        {
            get
            {
                return (ArrayOutput)Item;
            }
            set
            {
                Item = value;
            }
        }
        public string FilesDirectory { get; set; }


        // Constructors                                                                                       
        public frmArrayOutput()
        {
            InitializeComponent();

            Array = null;
            template = null;

            _pages = new Dictionary<Type, TabPage>();
            _pages.Add(typeof(DataMiningTemplateAbaqusReport), tcMiners.TabPages[0]);
            _pages.Add(typeof(DataMiningTemplateLsDyna), tcMiners.TabPages[1]);
            _pages.Add(typeof(DataMiningTemplateExcel), tcMiners.TabPages[2]);
            _pages.Add(typeof(DataMiningTemplateTxt), tcMiners.TabPages[3]);
            _pages.Add(typeof(DataMiningTemplateDll), tcMiners.TabPages[4]);
        }


        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Array != null)
            {
                if (Array.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Array.ValueType == VariableValueType.Integer) rbInteger.Checked = true;

                tbName.Text = Array.Name;
                tbDescription.Text = Array.Description;

                comboFileResources.SelectedItem = Array.OutputItemName;
                comboFileResources_SelectedIndexChanged(null, null);    // turn tabs on/off

                // set template and activate appropriate tab page
                tcMiners.SelectedTab = _pages[Array.Template.GetType()];
                template = Array.Template;  // previous lines reset the template

                // fill in mining data
                if (template is DataMiningTemplateAbaqusReport dmta) tbAbaqusHistoryOutputs.Text = dmta.HistoryVariableName;
                else if (template is DataMiningTemplateLsDyna dmtl) tbLsDynaNodout.Text = dmtl.VariableName;
                else if (template is DataMiningTemplateExcel dmte) tbCellRange.Text = dmte.CellRange;
                else if (template is DataMiningTemplateDll dmtd) cbDllName.SelectedItem = dmtd.DllName;
            }
            else
                comboFileResources_SelectedIndexChanged(null, null);
        }
        private void comboFileResources_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void comboFileResources_SelectedIndexChanged(object sender, EventArgs e)
        {
            string resource = (string)comboFileResources.SelectedItem;
            Item item = Pairs.GetItemByNameIncludingFromContainers(resource);
            if (item != null)
            {
                tcMiners.TabPages.Clear();

                if (item is ExcelComponent)
                {
                    tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateExcel)]);
                }
                else if (item is DllComponent dc)
                {
                    tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateDll)]);
                    var dll = DllComponent.GetDllObject(dc.DllFile.FileNameWithPath);
                    cbDllName.Items.Clear();

                    foreach (var name in dll.GetArrayNames()) cbDllName.Items.Add(name);
                }
                else
                {
                    tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateAbaqusReport)]);
                    tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateLsDyna)]);
                    tcMiners.TabPages.Add(_pages[typeof(DataMiningTemplateTxt)]);
                }
            }
        }
        private void tcMiners_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void btnLoadAbaqusOutputs_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fileName = openFileDialog1.FileName;
                    if (!(Path.GetDirectoryName(fileName) == FilesDirectory && Path.GetExtension(fileName) == ".template"))
                    {
                        string fileNameTemplate = Path.Combine(FilesDirectory, Path.GetFileName(fileName) + ".template");

                        if (File.Exists(fileNameTemplate) && MessageBox.Show("Overwrite existing template file?",
                                                                             "Warning", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                        {
                            return;         // to stop continuation of this method
                        }
                        File.Copy(fileName, fileNameTemplate, true);
                        fileName = fileNameTemplate;
                    }

                    comboAbaqusHistoryOutputs.Items.Clear();
                    AbaqusHistoryReader.AbaqusHistoryReader abaqusReader = new AbaqusHistoryReader.AbaqusHistoryReader(fileName);
                    string[] existingNames = abaqusReader.GetExistingHistoryOutputNames();
                    if (existingNames.Length <= 0)
                        MessageBox.Show("The selected template file contains no usable data.", "Error", MessageBoxButtons.OK);
                    else
                    {
                        comboAbaqusHistoryOutputs.Items.AddRange(existingNames);
                        comboAbaqusHistoryOutputs.SelectedIndex = 0;
                    }
                }
            }
            catch
            { }
        }
        private void comboAbaqusHistoryOutputs_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbAbaqusHistoryOutputs.Text = (string)comboAbaqusHistoryOutputs.SelectedItem;
        }

        private void btnLoadLsDyna_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fileName = openFileDialog1.FileName;
                    if (!(Path.GetDirectoryName(fileName) == FilesDirectory && Path.GetExtension(fileName) == ".template"))
                    {
                        string fileNameTemplate = Path.Combine(FilesDirectory, Path.GetFileName(fileName) + ".template");

                        if (File.Exists(fileNameTemplate) && MessageBox.Show("Overwrite existing template file?",
                                                                             "Warning", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                        {
                            return;         // to stop continuation of this method
                        }
                        File.Copy(fileName, fileNameTemplate, true);
                        fileName = fileNameTemplate;
                    }

                    string[] allNames = null;
                    if (Path.GetFileNameWithoutExtension(fileName) == LsDynaResultFile.nodout.ToString())
                    {
                        LsDynaNodoutReader.LsDynaNodoutReader dynaReader = new LsDynaNodoutReader.LsDynaNodoutReader(fileName);
                        allNames = dynaReader.GetAllVariableNames();
                    }
                    else if (Path.GetFileNameWithoutExtension(fileName) == LsDynaResultFile.spcforc.ToString())
                    {
                        LsDynaSpcForcReader.SpcForcReader dynaReader = new LsDynaSpcForcReader.SpcForcReader(fileName);
                        allNames = dynaReader.GetAllVariableNames();
                    }
                    else if (Path.GetFileNameWithoutExtension(fileName) == LsDynaResultFile.matsum.ToString())
                    {
                        LsDynaMatsumReader.LsDynaMatsumReader dynaReader = new LsDynaMatsumReader.LsDynaMatsumReader(fileName);
                        allNames = dynaReader.GetAllVariableNames();
                    }
                    else if (Path.GetFileNameWithoutExtension(fileName) == LsDynaResultFile.elout.ToString())
                    {
                        LsDynaEloutReader.LsDynaEloutReader dynaReader = new LsDynaEloutReader.LsDynaEloutReader(fileName);
                        allNames = dynaReader.GetAllVariableNames();
                    }

                    comboLsDynaNodout.Items.Clear();
                    comboLsDynaNodout.Items.AddRange(allNames);
                    comboLsDynaNodout.SelectedIndex = 0;
                }
            }
            catch
            { }
        }
        private void comboLsDynaNodout_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbLsDynaNodout.Text = (string)comboLsDynaNodout.SelectedItem;
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            frmDataMining frm_dataMining = new frmDataMining();
            frm_dataMining.Template = template as DataMiningTemplateTxt;
            frm_dataMining.FilesDirectory = this.FilesDirectory;

            if (frm_dataMining.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                template = frm_dataMining.Template;
                btnOK.Enabled = true;
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Array = GetArray();

                comboFileResources.Items.Clear();
                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
       

        // Methods                                                                                            
        public ArrayOutput GetArray()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Array) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;

            if (tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateAbaqusReport)]) // Abaqus
            {
                template = new DataMiningTemplateAbaqusReport(tbAbaqusHistoryOutputs.Text);
            }
            else if (tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateLsDyna)])  // Ls-Dyna
            {
                string resource = (string)comboFileResources.SelectedItem;
                FileResource fileResource = (FileResource)Pairs.GetItemByNameIncludingFromContainers(resource);
                LsDynaResultFile resultFile;
                if (fileResource.FileNameWithoutPath.ToLower() == LsDynaResultFile.nodout.ToString()) resultFile = LsDynaResultFile.nodout;
                else if (fileResource.FileNameWithoutPath.ToLower() == LsDynaResultFile.spcforc.ToString()) resultFile = LsDynaResultFile.spcforc;
                else if (fileResource.FileNameWithoutPath.ToLower() == LsDynaResultFile.matsum.ToString()) resultFile = LsDynaResultFile.matsum;
                else if (fileResource.FileNameWithoutPath.ToLower() == LsDynaResultFile.elout.ToString()) resultFile = LsDynaResultFile.elout;
                else throw new Exception("The selected Ls-Dyna result file is not supported.");

                template = new DataMiningTemplateLsDyna(resultFile, tbLsDynaNodout.Text);
            }
            else if (tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateExcel)])   // Excel
            {
                template = new DataMiningTemplateExcel(tbCellRange.Text);

                if (!Helpers.ExcelHelper.IsThisArrayCellRangeValid((template as DataMiningTemplateExcel).CellRange))
                    throw new Exception("The cell range '" + (template as DataMiningTemplateExcel).CellRange + "' is not a valid array cell range.");
            }
            else if (tcMiners.SelectedTab == _pages[typeof(DataMiningTemplateDll)])     // Dll
            {
                template = new DataMiningTemplateDll((string)cbDllName.SelectedItem);
            }
            else                                                                        // Txt
            {
                if (template == null || !(template is DataMiningTemplateTxt))
                {
                    throw new Exception("The txt mining template is missing.");
                }
            }

            return new ArrayOutput(tbName.Text, tbDescription.Text, type, (string)comboFileResources.SelectedItem, template);
        }
        override protected void ResetForm()
        {
            comboFileResources.Items.Clear();
            foreach (FileResource fileResource in Pairs.GetAllFileResourcesIncludingFromContainers())
            {
                comboFileResources.Items.Add(fileResource.Name);
            }

            foreach (Component component in Pairs.GetAllComponents())
            {
                if (component is ExcelComponent || component is DllComponent)
                    comboFileResources.Items.Add(component.Name);
            }

            if (comboFileResources.Items.Count <= 0)
            {
                MessageBox.Show("The workflow does not contain any file recources. First create a file resource.", "Error", MessageBoxButtons.OK);
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                return;
            }

            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            comboFileResources.SelectedIndex = 0;
            tbAbaqusHistoryOutputs.Text = "";
            tbLsDynaNodout.Text = "";
            tbCellRange.Text = "";

            tcMiners.TabPages.Clear();
        }

      
       

       

       

      




    }
}
