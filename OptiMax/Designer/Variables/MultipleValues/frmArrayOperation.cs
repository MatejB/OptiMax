﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmArrayOperation : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ArrayOperation);
        }

        public override string GetItemHelp()
        {
            return "Use 'Array Operation' to apply an operation to one\n"
                 + "of the arrays in the workflow.";
        }

        public ArrayOperation Array
        {
            get
            {
                return (ArrayOperation)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmArrayOperation()
        {
            InitializeComponent();

            Array = null;
            Pairs = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Array != null)
            {
                tbName.Text = Array.Name;
                tbDescription.Text = Array.Description;
                
                comboArray.SelectedItem = Array.ArrayToOperateOn;

                comboOperation.SelectedItem = ArrayOperationTypeHelper.Description[Array.OperationType];

                tbOperationParameters.Text = "";
                double[] parameters = Array.OperationParameters;
                foreach (double par in parameters)
                {
                    if (tbOperationParameters.Text.Length > 0) tbOperationParameters.Text += ", ";
                    tbOperationParameters.Text += par.ToString(OptiMaxFramework.Globals.nfi);
                }
            }
        }

        private void comboArrays_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void comboOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            // do a lookup in the description dictionarry
            ArrayOperationType operation = ArrayOperationTypeHelper.Description.First(x => x.Value == (string)comboOperation.SelectedItem).Key;

            tbParHelp.Text = ArrayOperationTypeHelper.ParameterHelp[operation];
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Array = GetArray();

                Pairs = null;
                comboArray.Items.Clear();
                comboOperation.Items.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public ArrayOperation GetArray()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Array) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            // do a lookup in the description dictionarry
            ArrayOperationType operation = ArrayOperationTypeHelper.Description.First(x => x.Value == (string)comboOperation.SelectedItem).Key;

            string[] tmp = tbOperationParameters.Text.Split(new string[] {",", " ", "\t"}, StringSplitOptions.RemoveEmptyEntries);
            double[] parameters = new double[tmp.Length];
            for (int i = 0; i < tmp.Length; i++)
			{
                parameters[i] = double.Parse(tmp[i], OptiMaxFramework.Globals.nfi);
			}

            ArrayOperation af = new ArrayOperation(tbName.Text, tbDescription.Text, (string)comboArray.SelectedItem, operation, parameters);

            return af;
        }

        override protected void ResetForm()
        {
            comboArray.Items.Clear();
            foreach (ArrayBase array in Pairs.GetAllArraysIncludingFromContainers())
            {
                if (!(Array != null && array.Name == Array.Name))
                    comboArray.Items.Add(array.Name);
            }

            if (comboArray.Items.Count <= 0)
            {
                MessageBox.Show("The workflow does not contain any arrays. First create an array.", "Error", MessageBoxButtons.OK);
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                return;
            }

            comboOperation.Items.Clear();
            foreach (ArrayOperationType operType in (ArrayOperationType[])Enum.GetValues(typeof(ArrayOperationType)))
            {
                comboOperation.Items.Add(ArrayOperationTypeHelper.Description[operType]);
            }

            tbName.Text = "";
            tbDescription.Text = "";
            comboArray.SelectedIndex = 0;
            comboOperation.SelectedIndex = 0;
            tbOperationParameters.Text = "";
        }

       

        

       

        

    }
}
