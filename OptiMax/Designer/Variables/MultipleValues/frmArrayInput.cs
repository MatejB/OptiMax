﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmArrayInput : FormWithItem
    {
        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(ArrayInput);
        }
        public override string GetItemHelp()
        {
            return "Use 'Array Input' for array parameter which will be changed\n"
                 + "by the driver during the job run.";
        }
        public ArrayInput Array
        {
            get
            {
                return (ArrayInput)Item;
            }
            set
            {
                Item = value;
            }
        }


        // Constructors                                                                                       
        public frmArrayInput()
        {
            InitializeComponent();

            Array = null;
        }
        

        // Event handling                                                                                     
        private void frmVariable_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Array != null)
            {
                if (Array.ValueType == VariableValueType.Double) rbDouble.Checked = true;
                else if (Array.ValueType == VariableValueType.Integer) rbInteger.Checked = true;

                tbName.Text = Array.Name;
                tbDescription.Text = Array.Description;
                numNumOfValues.Value = Math.Max(Array.NumOfVariables, 1);

                if (Array.StringLength > -1)
                {
                    cbStringLength.Checked = true;
                    if (Array.StringLength >= numStringLength.Minimum) numStringLength.Value = Array.StringLength;
                    else numStringLength.Value = numStringLength.Minimum;
                }
            }
        }
        private void cbStringLength_CheckedChanged(object sender, EventArgs e)
        {
            numStringLength.Enabled = cbStringLength.Checked;
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Array = GetArray();

                Pairs = null;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Value: " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        // Methods                                                                                            
        public ArrayInput GetArray()
        {
            Item item = Pairs.GetItemByNameIncludingFromContainers(tbName.Text);
            if (item != null && item != this.Array) throw new Exception("The name '" + tbName.Text + "' is allready in use.");

            VariableValueType type = VariableValueType.Double;

            if (rbDouble.Checked) type = VariableValueType.Double;
            else if (rbInteger.Checked) type = VariableValueType.Integer;

            ArrayInput ai = new ArrayInput(tbName.Text, tbDescription.Text, type, (int)numNumOfValues.Value);

            if (cbStringLength.Checked) ai.StringLength = (int)numStringLength.Value;
            else ai.StringLength = -1;

            return ai;
        }
        override protected void ResetForm()
        {
            rbDouble.Checked = true;
            tbName.Text = "";
            tbDescription.Text = "";
            numNumOfValues.Value = 10;
            cbStringLength.Checked = false;
            numStringLength.Value = 10;
        }
      


    }
}
