﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OptiMaxFramework;

namespace OptiMax
{
    struct Selection
    {
        public int Start;
        public int Length;
        public int RowIndex;
        public int ColIndex;

        public Selection(int start, int length, int rowIndex, int wordIndex)
        {
            Start = start;
            Length = length;
            RowIndex = rowIndex;
            ColIndex = wordIndex;
        }
    }

    struct SelectionStyle
    {
        public Color ForegroundColor;
        public Color BackgroungColor;
        public FontStyle FontStyle;

        public SelectionStyle(Color foregroundColor, Color backgroungColor, FontStyle fontStyle)
        {
            ForegroundColor = foregroundColor;
            BackgroungColor = backgroungColor;
            FontStyle = fontStyle;
        }
    }

    public partial class frmDataMining : Form, IFilesDirectory 
    {
        // Variables                                                                                                                
        private string data;
        private List<Selection> startWord;
        private List<Selection> dataWords;
        private SelectionStyle startWordStyle;
        private SelectionStyle dataWordStyle;
        private SelectionStyle defaultStyle;
        private float rtbFontHeight;
        private int firstRowOfSelection;
        private int lastRowOfSelection;
        private DataTable records;

        // Properties                                                                                                               
        public DataMiningTemplateTxt Template {get; set;}

        public string FilesDirectory { get; set; }


        // Constructors                                                                                                             
        public frmDataMining()
        {
            InitializeComponent();

            startWord = new List<Selection>();
            dataWords = new List<Selection>();

            startWordStyle = new SelectionStyle(Color.Black, Color.FromArgb(249, 201, 255), FontStyle.Regular);
            dataWordStyle = new SelectionStyle(Color.White, Color.Gray, FontStyle.Regular);
            defaultStyle = new SelectionStyle(Color.Black, Color.White, FontStyle.Regular);

            Template = new DataMiningTemplateTxt(null, null, 0, null, null);
        }


        // Event handling                                                                                                           
        private void frmDataMining_Load(object sender, EventArgs e)
        {
            data = null;

            rtbFileData.Lines = new string[] { " ", " "};
            rtbFontHeight = rtbFileData.GetPositionFromCharIndex(rtbFileData.GetFirstCharIndexFromLine(1)).Y -
                            rtbFileData.GetPositionFromCharIndex(rtbFileData.GetFirstCharIndexFromLine(0)).Y;

            rtbFileData.Lines = new string[] { " " };

            records = new DataTable("Records");

            OpenFile();
        }
        
        private void comboDecimalSeparator_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void comboOperation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void OpenFile()
        {
            openFileDialog1.InitialDirectory = FilesDirectory;
            if (Template != null && Template.FileName != null) openFileDialog1.FileName = Template.FileName;
            else openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ClearAll();

                string file = openFileDialog1.FileName;

                if (!(Path.GetDirectoryName(openFileDialog1.FileName) == FilesDirectory && Path.GetExtension(file) == ".template"))
                {
                    file = Path.Combine(FilesDirectory, Path.GetFileName(openFileDialog1.FileName) + ".template");

                    if (File.Exists(file) && MessageBox.Show("Overwrite existing template file?", "Warning", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    {
                        this.DialogResult = DialogResult.Cancel;
                        this.Close();   // to close this form
                        return;         // to stop continuation of this method
                    }
                    File.Copy(openFileDialog1.FileName, file, true);
                }
                data = File.ReadAllText(file, Encoding.Default);
                data = data.Replace("\r\n", "\n");
                data = data.Replace("\r", "\n");
                rtbFileData.Text = data;

                PrepareDGV();

                SetTemplate();
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        private void PrepareDGV() /*             FOR DATA GRID VIEW           */
        {
            records.Clear();
            string[] lines = data.Split('\n');
            string[] tmp;
            foreach (string line in lines)
            {
                tmp = line.Split(GetDelimitersString(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = records.Columns.Count; i < tmp.Length; i++)
                {
                    records.Columns.Add(i.ToString());
                }
                records.Rows.Add(tmp);
            }
            //dgvWords.DataSource = records;

            //foreach (DataGridViewRow row in dgvWords.Rows)
            //{
            //    row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            //    row.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            //}

        }

        private void cbDelimiters_CheckedChanged(object sender, EventArgs e)
        {
            ClearAll();
            PrepareDGV();
        }

        private void cbOther_CheckedChanged(object sender, EventArgs e)
        {
            tbOtherDelimiter.Enabled = cbOther.Checked;
            cbDelimiters_CheckedChanged(null, null);
        }

        private void tbOtherDelimiter_TextChanged(object sender, EventArgs e)
        {
            //if (tbOtherDelimiter.Text.Length > 1) tbOtherDelimiter.Text = tbOtherDelimiter.Text[0].ToString();
        }

        private void numNumRows_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numDataWordColumn_ValueChanged(object sender, EventArgs e)
        {
            int newColIndex = (int)numDataWordColumn.Value;

            if (dataWords.Count > 0 && newColIndex != dataWords[0].ColIndex)
            {
                string[] delimiters = GetDelimitersString();
                string row;
                string[] tmp;
                Selection word;

                for (int i = 0; i < dataWords.Count; i++)
                {
                    word = dataWords[i];

                    row = rtbFileData.Lines[word.RowIndex];
                    tmp = row.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                    if (tmp.Length < newColIndex) newColIndex = tmp.Length;
                }

                for (int i = 0; i < dataWords.Count; i++)
                {
                    word = dataWords[i];

                    row = rtbFileData.Lines[word.RowIndex];
                    tmp = row.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                    if (newColIndex >= 0 && newColIndex < tmp.Length)
                    {
                        ClearFormat(word);
                        SelectWordInRow(word.RowIndex, newColIndex);
                        SetDataWordToSelection(ref word);

                        dataWords[i] = word;
                    }
                }
            }
        }

        private void numSkipLines_ValueChanged(object sender, EventArgs e)
        {

        }

        private void rtbFileData_VScroll(object sender, EventArgs e)
        {
            pbLineNumbers.Invalidate();
        }

        private void pbLineNumbers_Paint(object sender, PaintEventArgs e)
        {
            if (rtbFontHeight == 0) return;

            //Get the first visible line index and location
            int firstIndex  = rtbFileData.GetCharIndexFromPosition(new Point(0, (int)(e.Graphics.VisibleClipBounds.Y + rtbFontHeight / 3)));
            int firstLine = rtbFileData.GetLineFromCharIndex(firstIndex);
            int firstLineY = rtbFileData.GetPositionFromCharIndex(firstIndex).Y;

            //Print on the PictureBox the visible line numbers of the RichTextBox
            e.Graphics.Clear(Control.DefaultBackColor);
            int i  = firstLine;
            string lineNum;
            float y = 0;
            while( y < e.Graphics.VisibleClipBounds.Y + e.Graphics.VisibleClipBounds.Height && i <= rtbFileData.Lines.Length)
            {
                y = firstLineY + rtbFontHeight * (i - firstLine - 1) - 1;
                lineNum = i.ToString();
                e.Graphics.DrawString(lineNum, rtbFileData.Font, Brushes.Gray,
                                      pbLineNumbers.Width - e.Graphics.MeasureString(lineNum, rtbFileData.Font).Width - 5, y);
                i++;
            }
        }

        // Buttons
        private void btnSetStartWord_Click(object sender, EventArgs e)
        {
            try
            {
                if (rtbFileData.SelectionLength > 0 || SelectWordAtCurrentPosition())
                {
                    if (dataWords.Count > 0 && dataWords[0].Start < rtbFileData.SelectionStart + rtbFileData.SelectionLength)
                        MessageBox.Show("The start word and data word should not overlap and the start word should end before the data words begin.");
                    else
                    {
                        if (startWord.Count > 0) ClearFormat(startWord[0]);
                        else startWord.Add(new Selection());

                        Selection newSelection = startWord[0];
                        SetWordAndFormat(ref newSelection, startWordStyle);
                        newSelection.RowIndex = rtbFileData.GetLineFromCharIndex(newSelection.Start + newSelection.Length);
                        startWord[0] = newSelection;

                        // set controls
                        tbStartWord.Text = rtbFileData.SelectedText;

                        rtbFileData.SelectionLength = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnSetDataWord_Click(object sender, EventArgs e)
        {
            try
            {
                string selectedText = rtbFileData.SelectedText;
                string[] delimiters = GetDelimitersString();
                string[] tmp = selectedText.Split(delimiters, StringSplitOptions.None);

                firstRowOfSelection = rtbFileData.GetLineFromCharIndex(rtbFileData.SelectionStart);
                lastRowOfSelection = rtbFileData.GetLineFromCharIndex(rtbFileData.SelectionStart + rtbFileData.SelectionLength);

                bool moreThanOneWordSelected = false;
                if (tmp.Length > 1) moreThanOneWordSelected = true;

                if (moreThanOneWordSelected || SelectWordAtCurrentPosition())
                {
                    if (startWord.Count > 0 && startWord[0].Start + startWord[0].Length > rtbFileData.SelectionStart)
                        MessageBox.Show("The start word and data word should not overlap and the start word should end before the data words begin.");
                    else
                    {
                        foreach (var selection in dataWords)
                        {
                            ClearFormat(selection);
                        }
                        dataWords.Clear();

                        Selection newDataWord;

                        // one word selected
                        if (!moreThanOneWordSelected)
                        {
                            newDataWord = new Selection();
                            SetDataWordToSelection(ref newDataWord);
                            dataWords.Add(newDataWord);

                            // set controls
                            numDataWordColumn.Value = dataWords.Last().ColIndex;

                            numNumOfRows.Value = 1;
                            rtbFileData.SelectionLength = 0;
                        }

                        // more than one word selected
                        else
                        {
                            
                            int rowStep = (int)numSkipLines.Value + 1;

                            for (int i = firstRowOfSelection; i <= lastRowOfSelection; i += rowStep)
                            {
                                newDataWord = new Selection();
                                SelectWordInRow(i, 0);
                                SetDataWordToSelection(ref newDataWord);
                                dataWords.Add(newDataWord);

                                if (i == firstRowOfSelection)
                                {
                                    numNumOfRows.Value = lastRowOfSelection - firstRowOfSelection + 1;
                                    numDataWordColumn.Value = dataWords.Last().ColIndex;
                                }
                            }
                        }
                        rtbFileData.SelectionLength = 0;
                    }
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnClearStartWord_Click(object sender, EventArgs e)
        {
            if (startWord.Count > 0) ClearFormat(startWord[0]);
            startWord.Clear();

            tbStartWord.Text = "";
        }

        private void btnClearDataWord_Click(object sender, EventArgs e)
        {
            if (dataWords.Count > 0)
            {
                foreach (Selection word in dataWords)
                {
                    ClearFormat(word);
                }
                dataWords.Clear();
            }

            numDataWordColumn.Value = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Template = GetTemplate();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Mouse right button
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void setStartWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnSetStartWord_Click(null, null);
        }

        private void setDataWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnSetDataWord_Click(null, null);
        }

        // Methods                                                                                            
        private DataMiningTemplateTxt GetTemplate()
        {
            if (dataWords.Count == 0) throw new Exception("Data mining word is not set.");

            DataMiningTemplateTxt tmp = new DataMiningTemplateTxt();

            tmp.StartText = null;
            if (startWord.Count > 0 && startWord[0].Length > 0) tmp.StartText = data.Substring(startWord[0].Start, startWord[0].Length);

            int startRowIndex = 0;
            if (startWord.Count > 0) startRowIndex = startWord[0].RowIndex;

            int firstRow = dataWords.First().RowIndex - startRowIndex;
            int lastRow = dataWords.Last().RowIndex - startRowIndex;
            
            List<int> rowNumbers = new List<int>();
            int rowStep = (int)numSkipLines.Value + 1;
            for (int i = firstRow; i <= lastRow; i += rowStep)
            {
                rowNumbers.Add(i);
            }
            tmp.RowNumbers = rowNumbers.ToArray();

            tmp.WordColIndex = dataWords[0].ColIndex;

            tmp.WordDelimiters = GetDelimitersString();

            if (((string)comboDecimalSeparator.SelectedItem).StartsWith(".")) tmp.DecimalSeparator = ".";
            else tmp.DecimalSeparator = ",";

            tmp = new DataMiningTemplateTxt(tmp.StartText, tmp.RowNumbers, tmp.WordColIndex, tmp.WordDelimiters, tmp.DecimalSeparator);

            tmp.FileName = openFileDialog1.FileName;

            return tmp;
        }

        private void SetTemplate()
        {
            if (Template == null) return;

            if (data == null || (Template.RowNumbers != null && rtbFileData.Lines.Length < Template.RowNumbers.Last())
                             || (Template.StartText != null && data.IndexOf(Template.StartText) == -1))
            {
                MessageBox.Show("The template can not be set on the text data displayed");
            }
            else
            {
                if (Template.DecimalSeparator != null && Template.RowNumbers != null && Template.WordDelimiters != null)
                {
                    // each checkBox change runs a ClearAll() function          
                    tbOtherDelimiter.Text = "";
                    cbTab.Checked = false;
                    cbSpace.Checked = false;
                    cbComma.Checked = false;
                    cbSemicolon.Checked = false;
                    cbOther.Checked = false;
                    foreach (var delimiter in Template.WordDelimiters)
                    {
                        if (delimiter == "\t") cbTab.Checked = true;
                        else if (delimiter == " ") cbSpace.Checked = true;
                        else if (delimiter == ",") cbComma.Checked = true;
                        else if (delimiter == ";") cbSemicolon.Checked = true;
                        else if (delimiter != "\n")
                        {
                            cbOther.Checked = true;
                            if (tbOtherDelimiter.Text.Length > 0) tbOtherDelimiter.Text += ",";
                            tbOtherDelimiter.Text += delimiter;
                        }
                    }
                    //                                                          

                    int startWordLastRow = 0;
                    if (Template.StartText != null)
                    {
                        tbStartWord.Text = Template.StartText;
                        startWordLastRow = rtbFileData.GetLineFromCharIndex(data.IndexOf(Template.StartText) + Template.StartText.Length);
                    }
                    else tbStartWord.Text = "";

                    firstRowOfSelection = Template.RowNumbers.First() + startWordLastRow;
                    lastRowOfSelection = Template.RowNumbers.Last() + startWordLastRow;
                    
                    // set visual control values
                    numNumOfRows.Value = lastRowOfSelection - firstRowOfSelection + 1;
                    numDataWordColumn.Value = Template.WordColIndex;
                    if (Template.RowNumbers.Length > 1) numSkipLines.Value = Template.RowNumbers[1] - Template.RowNumbers[0] - 1;
                    else numSkipLines.Value = 0;

                    if (Template.DecimalSeparator == ".") comboDecimalSeparator.SelectedIndex = 0;
                    else comboDecimalSeparator.SelectedIndex = 1;

                    // set formatings                                            
                    int prevStart = rtbFileData.SelectionStart;
                    int prevLength = rtbFileData.SelectionLength;

                    int startChar = 0;
                    if (Template.StartText != null) startChar = data.IndexOf(Template.StartText);
                    int length = 0;
                    if (Template.StartText != null) length = Template.StartText.Length;

                    startWord.Add(new Selection(startChar, length, startWordLastRow, -1));
                    SetFormatFromWord(startWord[0], startWordStyle);

                    int firstRow = startWordLastRow;
                    int colIndex = Template.WordColIndex;
                    Selection newSelection = new Selection();

                    foreach (int i in Template.RowNumbers)
                    {
                        SelectWordInRow(firstRow + i, colIndex);
                        SetDataWordToSelection(ref newSelection);
                        dataWords.Add(newSelection);
                    }

                    //rtbFileData.SelectionStart = prevStart;
                    //rtbFileData.SelectionLength = prevLength;
                }
            }
        }

        private void ClearAll()
        {
            if (startWord.Count > 0) ClearFormat(startWord[0]);
            startWord.Clear();

            if (dataWords.Count > 0)
            {
                foreach (Selection word in dataWords)
                {
                    ClearFormat(word);
                }
                dataWords.Clear();
            }

            tbStartWord.Text = "";
            numDataWordColumn.Value = 0;

            comboDecimalSeparator.SelectedIndex = 0;
        }

        private void SetDataWordToSelection(ref Selection dataWord)
        {
            dataWord.RowIndex = rtbFileData.GetLineFromCharIndex(rtbFileData.SelectionStart);

            // get word number
            string word = rtbFileData.SelectedText;
            int wordStartIndexInLine = rtbFileData.SelectionStart - rtbFileData.GetFirstCharIndexFromLine(dataWord.RowIndex);
            dataWord.ColIndex = GetWordNumber(wordStartIndexInLine, word, dataWord.RowIndex);

            SetWordAndFormat(ref dataWord, dataWordStyle);
        }

        private void SelectWordInRow(int rowIndex, int wordIndex)
        {
            string[] delimiters = GetDelimitersString();
            string row = rtbFileData.Lines[rowIndex];
            string[] tmp = row.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            if (wordIndex >= 0 && wordIndex < tmp.Length)
            {
                string newWord = tmp[wordIndex];

                int index = 0;
                int indexFromStart = 0;
                for (int i = 0; i <= wordIndex; i++)
                {
                    index = row.IndexOf(tmp[i]);
                    indexFromStart += (index + tmp[i].Length);

                    row = row.Substring(index + tmp[i].Length);
                }

                indexFromStart -= newWord.Length;
                rtbFileData.SelectionStart = indexFromStart + rtbFileData.GetFirstCharIndexFromLine(rowIndex);
                rtbFileData.SelectionLength = newWord.Length;
            }
        }

        private bool SelectWordAtCurrentPosition()
        {
            int selectionStart = rtbFileData.SelectionStart;
            if (selectionStart >= data.Length) selectionStart--;

            string[] delimiters = GetDelimitersString();

            while (!IsDelimiter(Convert.ToString(data[selectionStart]), delimiters))
            {
                if (selectionStart == 0) break;
                selectionStart--;
            }
            if (selectionStart > 0) selectionStart++;

            
            int selectionLength = 0;
            while (!IsDelimiter(Convert.ToString(data[selectionStart + selectionLength]), delimiters))
            {
                selectionLength++;
                if (selectionStart + selectionLength >= data.Length) break;
            }

            rtbFileData.SelectionStart = selectionStart;
            rtbFileData.SelectionLength = selectionLength;

            if (selectionLength > 0) return true;
            else return false;
        }

        private int GetWordNumber(int startCharIndexLine, string word, int rowNumber)
        {
            string[] delimiters = GetDelimitersString();

            string row = rtbFileData.Lines[rowNumber];
            string[] tmp = row.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            int index = 0;
            int indexFromStart = 0;
            for (int i = 0; i < tmp.Length; i++)
            {
                index = row.IndexOf(tmp[i]);
                indexFromStart += index;

                if (tmp[i] == word && indexFromStart == startCharIndexLine)
                {
                    return i;
                }

                row = row.Substring(index + tmp[i].Length);
                indexFromStart += tmp[i].Length;
            }

            return 0;
            //throw new Exception("The word '" + word + "' does not exist in the line " + rowNumber.ToString());
        }

        private void SetWordAndFormat(ref Selection selection, SelectionStyle style)
        {
            Font font;
            
            selection.Start = rtbFileData.SelectionStart;
            selection.Length = rtbFileData.SelectionLength;
            rtbFileData.SelectionColor = style.ForegroundColor;
            rtbFileData.SelectionBackColor = style.BackgroungColor;

            font = new Font(rtbFileData.Font, style.FontStyle);
            rtbFileData.SelectionFont = font;
        }

        private void SetFormatFromWord(Selection selection, SelectionStyle style)
        {

            int prevStart = rtbFileData.SelectionStart;
            int prevLength = rtbFileData.SelectionLength;

            rtbFileData.SelectionStart = selection.Start;
            rtbFileData.SelectionLength = selection.Length;
            rtbFileData.SelectionColor = style.ForegroundColor;
            rtbFileData.SelectionBackColor = style.BackgroungColor;

            Font font;
            font = new Font(rtbFileData.Font, style.FontStyle);
            rtbFileData.SelectionFont = font;

            rtbFileData.SelectionStart = prevStart;
            rtbFileData.SelectionLength = prevLength;
        }

        private void ClearFormat(Selection selection)
        {
            Font font;

            int prevStart = rtbFileData.SelectionStart;
            int prevLength = rtbFileData.SelectionLength;

            rtbFileData.SelectionStart = selection.Start;
            rtbFileData.SelectionLength = selection.Length;
            rtbFileData.SelectionColor = defaultStyle.ForegroundColor;
            rtbFileData.SelectionBackColor = defaultStyle.BackgroungColor;

            font = new Font(rtbFileData.Font, defaultStyle.FontStyle);
            rtbFileData.SelectionFont = font;

            rtbFileData.SelectionStart = prevStart;
            rtbFileData.SelectionLength = prevLength;
        }

        private bool IsDelimiter(string input, string[] delimiters)
        {
            foreach (var delimiter in delimiters)
            {
                if (input == delimiter) return true;
            }
            return false;
        }

        private string[] GetDelimitersString()
        {
            List<string> delimiters = new List<string>();
            if (cbSpace.Checked) delimiters.Add(" ");
            if (cbTab.Checked) delimiters.Add("\t");
            if (cbComma.Checked) delimiters.Add(",");
            if (cbSemicolon.Checked) delimiters.Add(";");
            if (cbOther.Checked) delimiters.AddRange(tbOtherDelimiter.Text.Split(','));
            delimiters.Add("\n");
            return delimiters.ToArray();
        }

        private void dgvWords_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString() + " ";

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

       
      

       

        

        
       


 
       
 
    }
}
