﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptiMax.Designer.Toolbars
{
    public delegate void AddElementDelegate(Image image, string propertiesFormTypeName, OptiMaxFramework.Item item = null);

    public partial class OptiMaxToolbar : ToolStrip
    {
        public event AddElementDelegate AddElement;
       
        public OptiMaxToolbar()
        {
            InitializeComponent();
            tsmiVariableInput.Click += MenuItem_Click;
            tsmiVariablesContainer.Click += MenuItem_Click;
            tsmiVariableConstant.Click += MenuItem_Click;
            tsmiVariableOutput.Click += MenuItem_Click;
            tsmiVariableFunction.Click += MenuItem_Click;
            tsmiVariableFromArray.Click += MenuItem_Click;
            tsmiVariableFromCompareCurves.Click += MenuItem_Click;

            tsmiArrayInput.Click += MenuItem_Click;
            tsmiArrayConstant.Click += MenuItem_Click;
            tsmiArrayOutput.Click += MenuItem_Click;
            tsmiArrayFunction.Click += MenuItem_Click;
            tsmiArrayOperation.Click += MenuItem_Click;

            tsmiFileResource.Click += MenuItem_Click;
            tsmiFileResourceContainer.Click += MenuItem_Click;

            tsmiComponentDos.Click += MenuItem_Click;
            tsmiComponentAbaqus.Click += MenuItem_Click;
            tsmiComponentCalculiX.Click += MenuItem_Click;
            tsmiComponentLSDyna.Click += MenuItem_Click;
            tsmiComponentSge.Click += MenuItem_Click;
            tsmiComponentSlurm.Click += MenuItem_Click;
            tsmiComponentExcel.Click += MenuItem_Click;
            tsmiComponentDll.Click += MenuItem_Click;
            tsmiComponentResponseSurface.Click += MenuItem_Click;
            tsmiComponentMatlab.Click += MenuItem_Click;

            tsmiDriverDOE.Click += MenuItem_Click;
            tsmiDriverNM.Click += MenuItem_Click;
            tsmiDriverGA.Click += MenuItem_Click;
            tsmiDriverAMQ.Click += MenuItem_Click;
            tsmiDriverLM.Click += MenuItem_Click;
            tsmiDriverNSGAII.Click += MenuItem_Click;

            tsmiRecorderFile.Click += MenuItem_Click;
            tsmiRecorderCsv.Click += MenuItem_Click;
            tsmiRecorderMem.Click += MenuItem_Click;

        }

        void MenuItem_Click(object sender, System.EventArgs e)
        {
            ToolStripMenuItem menuItem;
            ToolStripButton menuButton;

            if (sender is ToolStripMenuItem)
            {
                menuItem = (ToolStripMenuItem)sender;
                if (menuItem.Name.StartsWith("tsmiVariable"))
                {
                    VariableMenuItem(menuItem);
                }
                else if (menuItem.Name.StartsWith("tsmiArray"))
                {
                    ArrayMenuItem(menuItem);
                }
                else if (menuItem.Name.StartsWith("tsmiFileResource"))
                {
                    FileResourceMenuItem(menuItem);
                }
                else if (menuItem.Name.StartsWith("tsmiComponent"))
                {
                    ComponentMenuItem(menuItem);
                }
                else if (menuItem.Name.StartsWith("tsmiDriver"))
                {
                    DriverMenuItem(menuItem);
                }
                else if (menuItem.Name.StartsWith("tsmiRecorder"))
                {
                    RecorderMenuItem(menuItem);
                }
                else 
                    throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
            }
            //else if (sender is ToolStripButton)
            //{
            //    menuButton = (ToolStripButton)sender;
            //    if (menuButton == tsbFileResourceOld)
            //    {
            //        if (AddElement != null)
            //            AddElement(menuButton.Image, typeof(frmFileResourcesContainer).ToString());
            //    }
            //    else
            //        throw new Exception("The menu button named '" + menuButton.Name + "' does not have an action assigned.");
            //}
        }

        private void VariableMenuItem(ToolStripMenuItem menuItem)
        {
            if (menuItem == tsmiVariableInput && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableInput).ToString());
            else if (menuItem == tsmiVariableConstant && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableConstant).ToString());
            else if (menuItem == tsmiVariableOutput && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableOutput).ToString());
            else if (menuItem == tsmiVariableFunction && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableFunction).ToString());
            else if (menuItem == tsmiVariableFromArray && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableFromArray).ToString());
            else if (menuItem == tsmiVariableFromCompareCurves && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariableFromCompareCurves).ToString());
            else if (menuItem == tsmiVariablesContainer && AddElement != null)
                AddElement(menuItem.Image, typeof(frmVariablesContainer).ToString());
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

        private void FileResourceMenuItem(ToolStripMenuItem menuItem)
        {
            if (menuItem == tsmiFileResource && AddElement != null)
                AddElement(menuItem.Image, typeof(frmFileResource).ToString());
            else if (menuItem == tsmiFileResourceContainer && AddElement != null)
                AddElement(menuItem.Image, typeof(frmFileResourcesContainer).ToString());
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

        private void ArrayMenuItem(ToolStripMenuItem menuItem)
        {
            Color c = Color.FromArgb(170, 170, 170);

            if (menuItem == tsmiArrayInput && AddElement != null)
                AddElement(menuItem.Image, typeof(frmArrayInput).ToString());
            else if (menuItem == tsmiArrayConstant && AddElement != null)
                AddElement(menuItem.Image, typeof(frmArrayConstant).ToString());
            else if (menuItem == tsmiArrayOutput && AddElement != null)
                AddElement(menuItem.Image, typeof(frmArrayOutput).ToString());
            else if (menuItem == tsmiArrayFunction && AddElement != null)
                AddElement(menuItem.Image, typeof(frmArrayFunction).ToString());
            else if (menuItem == tsmiArrayOperation && AddElement != null)
                AddElement(menuItem.Image, typeof(frmArrayOperation).ToString());
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

        private void ComponentMenuItem(ToolStripMenuItem menuItem)
        {
            Color c = Color.FromArgb(255, 242, 132);

            if (menuItem == tsmiComponentDos && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDosComponent).ToString());
            else if (menuItem == tsmiComponentAbaqus && AddElement != null)
                AddElement(menuItem.Image, typeof(frmAbaqusComponent).ToString());
            else if (menuItem == tsmiComponentCalculiX && AddElement != null)
                AddElement(menuItem.Image, typeof(frmCalculiXComponent).ToString());
            else if (menuItem == tsmiComponentLSDyna && AddElement != null)
                AddElement(menuItem.Image, typeof(frmLsDynaComponent).ToString());
            else if (menuItem == tsmiComponentSge && AddElement != null)
                AddElement(menuItem.Image, typeof(frmSgeComponent).ToString());
            else if (menuItem == tsmiComponentSlurm && AddElement != null)
                AddElement(menuItem.Image, typeof(frmSlurmComponent).ToString());
            else if (menuItem == tsmiComponentExcel && AddElement != null)
                AddElement(menuItem.Image, typeof(frmExcelComponent).ToString());
            else if (menuItem == tsmiComponentDll && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDllComponent).ToString());
            else if (menuItem == tsmiComponentResponseSurface && AddElement != null)
                AddElement(menuItem.Image, typeof(frmResponseSurfaceComponent).ToString());
            else if (menuItem == tsmiComponentMatlab && AddElement != null)
                AddElement(menuItem.Image, typeof(frmMatlabComponent).ToString());
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

        private void DriverMenuItem(ToolStripMenuItem menuItem)
        {
            //if (menuItem == tsmiDriverDOE && AddElement != null)
            //    AddElement(menuItem.Image, typeof(frmDOEDriver).ToString());
            //else if (menuItem == tsmiDriverNM && AddElement != null)
            //    AddElement(menuItem.Image, typeof(frmNMDriver).ToString());
            //else if (menuItem == tsmiDriverGA && AddElement != null)
            //    AddElement(menuItem.Image, typeof(frmGADriver).ToString());
            //else if (menuItem == tsmiDriverAMQ && AddElement != null)
            //    AddElement(menuItem.Image, typeof(frmAMQDriver).ToString());
            //else if (menuItem == tsmiDriverLM && AddElement != null)
            //    AddElement(menuItem.Image, typeof(frmLMDriver).ToString());
            //else
            //    throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");

            if (menuItem == tsmiDriverDOE && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.DOEDriver("Driver", ""));
            else if (menuItem == tsmiDriverNM && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.NMDriver("Driver", ""));
            else if (menuItem == tsmiDriverGA && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.GADriver("Driver", ""));
            else if (menuItem == tsmiDriverAMQ && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.AMQDriver("Driver", ""));
            else if (menuItem == tsmiDriverLM && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.LMDriver("Driver", ""));
            else if (menuItem == tsmiDriverNSGAII && AddElement != null)
                AddElement(menuItem.Image, typeof(frmDriverEditor).ToString(), new OptiMaxFramework.NSGAIIDriver("Driver", ""));
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

        private void RecorderMenuItem(ToolStripMenuItem menuItem)
        {
            Color c = Color.FromArgb(159, 111, 191);

            if (menuItem == tsmiRecorderFile && AddElement != null)
                AddElement(menuItem.Image, typeof(frmFileRecorder).ToString());
            else if (menuItem == tsmiRecorderCsv && AddElement != null)
                AddElement(menuItem.Image, typeof(frmCSVRecorder).ToString());
            else if (menuItem == tsmiRecorderMem && AddElement != null)
                AddElement(menuItem.Image, typeof(frmMemRecorder).ToString());
            else
                throw new Exception("The menu item named '" + menuItem.Name + "' does not have an action assigned.");
        }

      
      

    }
}
