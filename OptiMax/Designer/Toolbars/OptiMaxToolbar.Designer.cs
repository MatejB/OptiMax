﻿namespace OptiMax.Designer.Toolbars
{
    partial class OptiMaxToolbar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsddbVariables = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiVariableInput = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariableConstant = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariableOutput = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariableFunction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariableFromArray = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariableFromCompareCurves = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVariablesContainer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddbArrays = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiArrayInput = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiArrayConstant = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiArrayOutput = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiArrayFunction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiArrayOperation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddbFileResources = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiFileResource = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFileResourceContainer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddbComponents = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiComponentDos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentAbaqus = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentCalculiX = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentLSDyna = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentSge = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentSlurm = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentDll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentResponseSurface = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComponentMatlab = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddbDrivers = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiDriverDOE = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDriverNM = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDriverGA = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDriverAMQ = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDriverLM = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDriverNSGAII = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddbRecorders = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiRecorderFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRecorderCsv = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRecorderMem = new System.Windows.Forms.ToolStripMenuItem();
            this.SuspendLayout();
            // 
            // tsddbVariables
            // 
            this.tsddbVariables.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbVariables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiVariableInput,
            this.tsmiVariableConstant,
            this.tsmiVariableOutput,
            this.tsmiVariableFunction,
            this.tsmiVariableFromArray,
            this.tsmiVariableFromCompareCurves,
            this.tsmiVariablesContainer});
            this.tsddbVariables.Image = global::OptiMax.Properties.Resources.VariableConstant;
            this.tsddbVariables.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbVariables.Name = "tsddbVariables";
            this.tsddbVariables.Size = new System.Drawing.Size(29, 159);
            this.tsddbVariables.Text = "Add variable";
            // 
            // tsmiVariableInput
            // 
            this.tsmiVariableInput.Image = global::OptiMax.Properties.Resources.VariableInput;
            this.tsmiVariableInput.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableInput.Name = "tsmiVariableInput";
            this.tsmiVariableInput.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableInput.Text = "Input variable";
            this.tsmiVariableInput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariableConstant
            // 
            this.tsmiVariableConstant.Image = global::OptiMax.Properties.Resources.VariableConstant;
            this.tsmiVariableConstant.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableConstant.Name = "tsmiVariableConstant";
            this.tsmiVariableConstant.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableConstant.Text = "Constant variable";
            this.tsmiVariableConstant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariableOutput
            // 
            this.tsmiVariableOutput.Image = global::OptiMax.Properties.Resources.VariableOutput;
            this.tsmiVariableOutput.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableOutput.Name = "tsmiVariableOutput";
            this.tsmiVariableOutput.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableOutput.Text = "Variable output";
            this.tsmiVariableOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariableFunction
            // 
            this.tsmiVariableFunction.Image = global::OptiMax.Properties.Resources.VariableFunction;
            this.tsmiVariableFunction.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableFunction.Name = "tsmiVariableFunction";
            this.tsmiVariableFunction.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableFunction.Text = "Variable function";
            this.tsmiVariableFunction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariableFromArray
            // 
            this.tsmiVariableFromArray.Image = global::OptiMax.Properties.Resources.VariableFromArray;
            this.tsmiVariableFromArray.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableFromArray.Name = "tsmiVariableFromArray";
            this.tsmiVariableFromArray.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableFromArray.Text = "Variable from array";
            this.tsmiVariableFromArray.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariableFromCompareCurves
            // 
            this.tsmiVariableFromCompareCurves.Image = global::OptiMax.Properties.Resources.VariableFromArray;
            this.tsmiVariableFromCompareCurves.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariableFromCompareCurves.Name = "tsmiVariableFromCompareCurves";
            this.tsmiVariableFromCompareCurves.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariableFromCompareCurves.Text = "Variable from compare curves";
            this.tsmiVariableFromCompareCurves.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiVariablesContainer
            // 
            this.tsmiVariablesContainer.Image = global::OptiMax.Properties.Resources.VariablesContainer;
            this.tsmiVariablesContainer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiVariablesContainer.Name = "tsmiVariablesContainer";
            this.tsmiVariablesContainer.Size = new System.Drawing.Size(190, 22);
            this.tsmiVariablesContainer.Text = "Variables container";
            this.tsmiVariablesContainer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsddbArrays
            // 
            this.tsddbArrays.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbArrays.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiArrayInput,
            this.tsmiArrayConstant,
            this.tsmiArrayOutput,
            this.tsmiArrayFunction,
            this.tsmiArrayOperation});
            this.tsddbArrays.Image = global::OptiMax.Properties.Resources.ArrayConstant;
            this.tsddbArrays.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsddbArrays.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbArrays.Name = "tsddbArrays";
            this.tsddbArrays.Size = new System.Drawing.Size(29, 159);
            this.tsddbArrays.Text = "Add array";
            // 
            // tsmiArraInput
            // 
            this.tsmiArrayInput.Image = global::OptiMax.Properties.Resources.ArrayInput;
            this.tsmiArrayInput.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsmiArrayInput.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiArrayInput.Name = "tsmiArrayInput";
            this.tsmiArrayInput.Size = new System.Drawing.Size(172, 22);
            this.tsmiArrayInput.Text = "Input array";
            this.tsmiArrayInput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiArrayConstant
            // 
            this.tsmiArrayConstant.Image = global::OptiMax.Properties.Resources.ArrayConstant;
            this.tsmiArrayConstant.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsmiArrayConstant.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiArrayConstant.Name = "tsmiArrayConstant";
            this.tsmiArrayConstant.Size = new System.Drawing.Size(172, 22);
            this.tsmiArrayConstant.Text = "Constant array";
            this.tsmiArrayConstant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiArrayOutput
            // 
            this.tsmiArrayOutput.Image = global::OptiMax.Properties.Resources.ArrayOutput;
            this.tsmiArrayOutput.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiArrayOutput.Name = "tsmiArrayOutput";
            this.tsmiArrayOutput.Size = new System.Drawing.Size(172, 22);
            this.tsmiArrayOutput.Text = "Output array";
            this.tsmiArrayOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiArrayFunction
            // 
            this.tsmiArrayFunction.Image = global::OptiMax.Properties.Resources.ArrayFunction;
            this.tsmiArrayFunction.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiArrayFunction.Name = "tsmiArrayFunction";
            this.tsmiArrayFunction.Size = new System.Drawing.Size(172, 22);
            this.tsmiArrayFunction.Text = "Array function";
            this.tsmiArrayFunction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiArrayOperation
            // 
            this.tsmiArrayOperation.Image = global::OptiMax.Properties.Resources.ArrayOperation;
            this.tsmiArrayOperation.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiArrayOperation.Name = "tsmiArrayOperation";
            this.tsmiArrayOperation.Size = new System.Drawing.Size(172, 22);
            this.tsmiArrayOperation.Text = "Array operation";
            this.tsmiArrayOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsddbFileResources
            // 
            this.tsddbFileResources.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbFileResources.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFileResource,
            this.tsmiFileResourceContainer});
            this.tsddbFileResources.Image = global::OptiMax.Properties.Resources.FileResource;
            this.tsddbFileResources.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbFileResources.Name = "tsddbFileResources";
            this.tsddbFileResources.Size = new System.Drawing.Size(29, 159);
            this.tsddbFileResources.Text = "Add file resource";
            // 
            // tsmiFileResource
            // 
            this.tsmiFileResource.Image = global::OptiMax.Properties.Resources.FileResource;
            this.tsmiFileResource.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiFileResource.Name = "tsmiFileResource";
            this.tsmiFileResource.Size = new System.Drawing.Size(172, 22);
            this.tsmiFileResource.Text = "File resource";
            this.tsmiFileResource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiFileResourceContainer
            // 
            this.tsmiFileResourceContainer.Image = global::OptiMax.Properties.Resources.FileResourceContainer;
            this.tsmiFileResourceContainer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiFileResourceContainer.Name = "tsmiFileResource";
            this.tsmiFileResourceContainer.Size = new System.Drawing.Size(172, 22);
            this.tsmiFileResourceContainer.Text = "File resource container";
            this.tsmiFileResourceContainer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsddbComponents
            // 
            this.tsddbComponents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbComponents.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiComponentDos,
            this.tsmiComponentAbaqus,
            this.tsmiComponentCalculiX,
            this.tsmiComponentLSDyna,
            this.tsmiComponentSge,
            this.tsmiComponentSlurm,
            this.tsmiComponentExcel,
            this.tsmiComponentDll,
            this.tsmiComponentResponseSurface,
            this.tsmiComponentMatlab});
            this.tsddbComponents.Image = global::OptiMax.Properties.Resources.Component;
            this.tsddbComponents.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsddbComponents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbComponents.Name = "tsddbComponents";
            this.tsddbComponents.Size = new System.Drawing.Size(29, 159);
            this.tsddbComponents.Text = "Add component";
            // 
            // tsmiComponentDos
            // 
            this.tsmiComponentDos.Image = global::OptiMax.Properties.Resources.ComponentDos;
            this.tsmiComponentDos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentDos.Name = "tsmiComponentDos";
            this.tsmiComponentDos.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentDos.Text = "Dos component";
            this.tsmiComponentDos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentAbaqus
            // 
            this.tsmiComponentAbaqus.Image = global::OptiMax.Properties.Resources.ComponentAbaqus;
            this.tsmiComponentAbaqus.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentAbaqus.Name = "tsmiComponentAbaqus";
            this.tsmiComponentAbaqus.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentAbaqus.Text = "Abaqus component";
            this.tsmiComponentAbaqus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             // 
            // tsmiComponentCalculix
            // 
            this.tsmiComponentCalculiX.Image = global::OptiMax.Properties.Resources.ComponentCalculiX;
            this.tsmiComponentCalculiX.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentCalculiX.Name = "tsmiComponentCalculiX";
            this.tsmiComponentCalculiX.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentCalculiX.Text = "CalculiX component";
            this.tsmiComponentCalculiX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentLSDyna
            // 
            this.tsmiComponentLSDyna.Image = global::OptiMax.Properties.Resources.ComponentLSDyna;
            this.tsmiComponentLSDyna.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentLSDyna.Name = "tsmiComponentLSDyna";
            this.tsmiComponentLSDyna.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentLSDyna.Text = "LS-Dyna component";
            this.tsmiComponentLSDyna.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentSge
            // 
            this.tsmiComponentSge.Image = global::OptiMax.Properties.Resources.ComponentSge;
            this.tsmiComponentSge.Name = "tsmiComponentSge";
            this.tsmiComponentSge.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentSge.Text = "Sge component";
            this.tsmiComponentSge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentSlurm
            // 
            this.tsmiComponentSlurm.Image = global::OptiMax.Properties.Resources.ComponentSge;
            this.tsmiComponentSlurm.Name = "tsmiComponentSlurm";
            this.tsmiComponentSlurm.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentSlurm.Text = "Slurm component";
            this.tsmiComponentSlurm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentExcel
            // 
            this.tsmiComponentExcel.Image = global::OptiMax.Properties.Resources.ComponentExcel;
            this.tsmiComponentExcel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentExcel.Name = "tsmiComponentExcel";
            this.tsmiComponentExcel.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentExcel.Text = "Excel component";
            this.tsmiComponentExcel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentDll
            // 
            this.tsmiComponentDll.Image = global::OptiMax.Properties.Resources.ComponentDll;
            this.tsmiComponentDll.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentDll.Name = "tsmiComponentDll";
            this.tsmiComponentDll.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentDll.Text = "Dll component";
            this.tsmiComponentDll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentResponseSurface
            // 
            this.tsmiComponentResponseSurface.Image = global::OptiMax.Properties.Resources.ComponentResponseSurface;
            this.tsmiComponentResponseSurface.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentResponseSurface.Name = "tsmiComponentResponseSurface";
            this.tsmiComponentResponseSurface.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentResponseSurface.Text = "Response surface component";
            this.tsmiComponentResponseSurface.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiComponentMatlab
            // 
            this.tsmiComponentMatlab.Image = global::OptiMax.Properties.Resources.ComponentMatlab;
            this.tsmiComponentMatlab.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiComponentMatlab.Name = "tsmiComponentMatlab";
            this.tsmiComponentMatlab.Size = new System.Drawing.Size(183, 22);
            this.tsmiComponentMatlab.Text = "Matlab component";
            this.tsmiComponentMatlab.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;// 
            // tsddbDrivers
            // 
            this.tsddbDrivers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbDrivers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDriverDOE,
            this.toolStripSeparator1,
            this.tsmiDriverNM,
            this.tsmiDriverGA,
            this.toolStripSeparator2,
            this.tsmiDriverAMQ,
            this.tsmiDriverLM,
            this.toolStripSeparator3,
            this.tsmiDriverNSGAII});
            this.tsddbDrivers.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsddbDrivers.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsddbDrivers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbDrivers.Name = "tsddbDrivers";
            this.tsddbDrivers.Size = new System.Drawing.Size(29, 159);
            this.tsddbDrivers.Text = "Add driver";
            // 
            // tsmiDriverDOE
            // 
            this.tsmiDriverDOE.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverDOE.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverDOE.Name = "tsmiDriverDOE";
            this.tsmiDriverDOE.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverDOE.Text = "DOE: Design of experiments";
            this.tsmiDriverDOE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(269, 6);
            //
            // tsmiDriverNM
            // 
            this.tsmiDriverNM.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverNM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverNM.Name = "tsmiDriverNM";
            this.tsmiDriverNM.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverNM.Text = "NM: Nelder-Mead simplex method";
            this.tsmiDriverNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiDriverGA
            // 
            this.tsmiDriverGA.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverGA.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverGA.Name = "tsmiDriverGA";
            this.tsmiDriverGA.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverGA.Text = "GA: Genetic algorithm";
            this.tsmiDriverGA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(269, 6);
            // 
            // tsmiDriverAMQ
            // 
            this.tsmiDriverAMQ.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverAMQ.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverAMQ.Name = "tsmiDriverAMQ";
            this.tsmiDriverAMQ.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverAMQ.Text = "AMQ: Convex approximation method";
            this.tsmiDriverAMQ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiDriverLM
            // 
            this.tsmiDriverLM.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverLM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverLM.Name = "tsmiDriverLM";
            this.tsmiDriverLM.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverLM.Text = "LM: Levenberg-Marquard method";
            this.tsmiDriverLM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(269, 6);
            // 
            // tsmiDriverNSGAII
            // 
            this.tsmiDriverNSGAII.Image = global::OptiMax.Properties.Resources.Driver;
            this.tsmiDriverNSGAII.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiDriverNSGAII.Name = "tsmiDriverNSGAII";
            this.tsmiDriverNSGAII.Size = new System.Drawing.Size(272, 22);
            this.tsmiDriverNSGAII.Text = "NSGAII";
            // 
            // tsddbRecorders
            // 
            this.tsddbRecorders.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsddbRecorders.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRecorderFile,
            this.tsmiRecorderCsv,
            this.tsmiRecorderMem});
            this.tsddbRecorders.Image = global::OptiMax.Properties.Resources.Recorder;
            this.tsddbRecorders.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsddbRecorders.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbRecorders.Name = "tsddbRecorders";
            this.tsddbRecorders.Size = new System.Drawing.Size(29, 159);
            this.tsddbRecorders.Text = "Add recorder";
            // 
            // tsmiRecorderFile
            // 
            this.tsmiRecorderFile.Image = global::OptiMax.Properties.Resources.RecorderFile;
            this.tsmiRecorderFile.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiRecorderFile.Name = "tsmiRecorderFile";
            this.tsmiRecorderFile.Size = new System.Drawing.Size(166, 22);
            this.tsmiRecorderFile.Text = "File recorder";
            this.tsmiRecorderFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiRecorderCsv
            // 
            this.tsmiRecorderCsv.Image = global::OptiMax.Properties.Resources.RecorderCsv;
            this.tsmiRecorderCsv.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiRecorderCsv.Name = "tsmiRecorderCsv";
            this.tsmiRecorderCsv.Size = new System.Drawing.Size(166, 22);
            this.tsmiRecorderCsv.Text = "Csv recorder";
            this.tsmiRecorderCsv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiRecorderMem
            // 
            this.tsmiRecorderMem.Image = global::OptiMax.Properties.Resources.RecorderMem;
            this.tsmiRecorderMem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiRecorderMem.Name = "tsmiRecorderMem";
            this.tsmiRecorderMem.Size = new System.Drawing.Size(166, 22);
            this.tsmiRecorderMem.Text = "Memory recorder";
            this.tsmiRecorderMem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OptiMaxToolbar
            // 
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsddbVariables,
            this.tsddbArrays,
            this.tsddbFileResources,
            this.tsddbComponents,
            this.tsddbDrivers,
            this.tsddbRecorders});
            this.Size = new System.Drawing.Size(338, 162);
            this.ResumeLayout(false);
        }

       

        #endregion

        private System.Windows.Forms.ToolStripDropDownButton tsddbVariables;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableInput;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableConstant;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableFunction;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableFromArray;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableFromCompareCurves;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariableOutput;
        private System.Windows.Forms.ToolStripMenuItem tsmiVariablesContainer;
        private System.Windows.Forms.ToolStripDropDownButton tsddbArrays;
        private System.Windows.Forms.ToolStripMenuItem tsmiArrayInput;
        private System.Windows.Forms.ToolStripMenuItem tsmiArrayConstant;
        private System.Windows.Forms.ToolStripMenuItem tsmiArrayOutput;
        private System.Windows.Forms.ToolStripMenuItem tsmiArrayFunction;
        private System.Windows.Forms.ToolStripMenuItem tsmiArrayOperation;
        private System.Windows.Forms.ToolStripDropDownButton tsddbFileResources;
        private System.Windows.Forms.ToolStripMenuItem tsmiFileResource;
        private System.Windows.Forms.ToolStripMenuItem tsmiFileResourceContainer;
        private System.Windows.Forms.ToolStripDropDownButton tsddbComponents;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentDos;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentAbaqus;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentCalculiX;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentLSDyna;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentSge;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentSlurm;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentExcel;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentDll;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentResponseSurface;
        private System.Windows.Forms.ToolStripMenuItem tsmiComponentMatlab;
        private System.Windows.Forms.ToolStripDropDownButton tsddbDrivers;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverDOE;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverNM;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverGA;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverAMQ;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverLM;
        private System.Windows.Forms.ToolStripDropDownButton tsddbRecorders;
        private System.Windows.Forms.ToolStripMenuItem tsmiRecorderFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiRecorderCsv;
        private System.Windows.Forms.ToolStripMenuItem tsmiRecorderMem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriverNSGAII;
    }
}

