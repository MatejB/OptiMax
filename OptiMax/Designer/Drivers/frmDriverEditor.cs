﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class frmDriverEditor : FormWithItem
    {
        // Variables                                                                                          
        private Component[] allComponents;


        // Properties                                                                                         
        public override Type GetItemType()
        {
            return typeof(BaseDriver);
        }
        public Driver Driver
        {
            get
            {
                return (Driver)Item;
            }
            set
            {
                Item = value;
            }
        }

       
        // Constructors                                                                                       
        public frmDriverEditor()
        {
            InitializeComponent();

            allComponents = null;
        }


        // Event handling                                                                                     
        private void frmDOEDriver_Load(object sender, EventArgs e)
        {
            ResetForm();

            if (Driver != null)
            {
                this.Text = "Driver Editor (" + Driver.GetType().Name + ")";

                // parameters
                Parameter[] parameters = Driver.Parameters;
                string name;
                for (int i = 0; i < parameters.Length; i++)
                {
                    name = parameters[i].Name;
                    foreach (DataGridViewRow row in dgvParameters.Rows)
                    {
                        if (name == (string)row.Cells["colName"].Value)
                        {
                            row.Cells["colInclude"].Value = true;
                            row.Cells["colMin"].Value = parameters[i].Min.ToString(OptiMaxFramework.Globals.nfi);
                            row.Cells["colMax"].Value = parameters[i].Max.ToString(OptiMaxFramework.Globals.nfi);
                            row.Cells["colValue"].Value = parameters[i].Value.ToString(OptiMaxFramework.Globals.nfi);
                            row.Cells["colDelta"].Value = parameters[i].Delta.ToString(OptiMaxFramework.Globals.nfi);
                            row.Cells["colNumOfValues"].Value = parameters[i].NumOfValues.ToString();
                        }
                    }
                }

                // workflow
                Component component;
                string[] workflow = Driver.Workflow.InputVariables.ToArray();

                for (int i = workflow.Length - 1; i >= 0; i--)
                {
                    component = (Component)Pairs.GetItemByNameIncludingFromContainers(workflow[i]);
                    if (component != null)
                    {
                        // reorder rows
                        foreach (DataGridViewRow row in dgvWorkflow.Rows)
                        {
                            if (component.Name == (string)row.Cells["colCompName"].Value)
                            {
                                row.Cells["colCompInclude"].Value = true;
                                dgvWorkflow.Rows.Remove(row);
                                dgvWorkflow.Rows.Insert(0, row);
                                break;
                            }
                        }
                    }
                }

                // objective functions
                Item[] resultItems = Pairs.GetAllValueItemsIncludingFromContainers();
                List<string> variableNames = new List<string>();
                List<string> arrayNames = new List<string>();
                foreach (Item item in resultItems)
                {
                    if (item is VariableBase && !(item is VariableInput))
                    {
                        variableNames.Add(item.Name);
                    }
                    if (item is ArrayBase)
                    {
                        arrayNames.Add(item.Name);
                    }
                }
                if (variableNames.Count + arrayNames.Count == 0 && !(Driver is DOEDriver))
                {
                    MessageBox.Show("The workflow does not contain any non-input variables or arrays.", "Error", MessageBoxButtons.OK);
                    this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                    return;
                }
                Driver.SetAListOfAllResultItemNames(variableNames, arrayNames);

                // propeties
                if (Driver is DOEDriver doe)
                {
                    ViewDOEDriver view = new ViewDOEDriver(doe, arrayNames.ToArray());
                    propDriver.SelectedObject = view;
                }
                else
                {
                    propDriver.SelectedObject = Driver;
                }
            }
        }
        private void propDriver_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            try
            {
                propDriver.Refresh();

                Driver driver;
                if (propDriver.SelectedObject is ViewDOEDriver view) driver = view.GetBase();
                else if (propDriver.SelectedObject is Driver d) driver = d;
                else throw new Exception();

                SetColumnsVisibility(driver);
            }
            catch (Exception ex)
            {
                ExceptionTools.Show(this, ex);
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Driver = GetDriver();

                allComponents = null;
                Pairs = null;
                dgvParameters.Rows.Clear();
                dgvWorkflow.Rows.Clear();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        // Methods                                                                                            
        private Driver GetDriver()
        {
            Driver driver;
            if (propDriver.SelectedObject is Driver d) driver = d;
            else if (propDriver.SelectedObject is ViewDOEDriver doe) driver = doe.GetBase();
            else throw new NotSupportedException();

            driver.ClearParameters();
            driver.Workflow.Clear();

            Item item = Pairs.GetItemByNameIncludingFromContainers(driver.Name);
            if (item != null && item != this.Driver) throw new Exception("The name '" + driver.Name + "' is allready in use or is a reserved name.");

            Parameter parameter;
            string name, min, max, value, delta, numOfValues;

            BaseDriver baseDriver = new BaseDriver();

            foreach (DataGridViewRow row in dgvParameters.Rows)
            {
                if ((bool)row.Cells["colInclude"].Value == true)
                {
                    name = (string)row.Cells["colName"].Value;
                    min = (string)row.Cells["colMin"].Value.ToString();
                    max = (string)row.Cells["colMax"].Value.ToString();
                    value = (string)row.Cells["colValue"].Value.ToString();
                    delta = (string)row.Cells["colDelta"].Value.ToString();
                    numOfValues = (string)row.Cells["colNumOfValues"].Value.ToString();

                    item = Pairs.GetItemByNameIncludingFromContainers(name);
                    if (item == null || !(item is VariableInput || item is ArrayInput)) 
                        throw new Exception("The variable or array named '" + name + "' does not exist or it is not an input variable or input array.");

                    if (min.Contains(',') || max.Contains(',') || value.Contains(',') || delta.Contains(',')) 
                        throw new Exception("Please use a dot as a decimal separator.");

                    if (item is VariableInput || item is ArrayInput)
                    {
                        parameter = new Parameter();
                        parameter.Name = name;
                        parameter.ValueType = item is VariableInput ? (item as VariableInput).ValueType : (item as ArrayInput).ValueType;
                        parameter.Min = double.Parse(min, OptiMaxFramework.Globals.nfi);
                        parameter.Max = double.Parse(max, OptiMaxFramework.Globals.nfi);
                        parameter.SetValue(double.Parse(value, OptiMaxFramework.Globals.nfi));
                        parameter.Delta = double.Parse(delta, OptiMaxFramework.Globals.nfi);
                        parameter.NumOfValues = int.Parse(numOfValues);

                        if (dgvParameters.Columns["colMin"].Visible &&
                            dgvParameters.Columns["colMax"].Visible && 
                            parameter.Max <= parameter.Min)
                            throw new Exception("The variable '" + name + "' has invalid min and max values.");
                        if (dgvParameters.Columns["colValue"].Visible &&
                            dgvParameters.Columns["colMin"].Visible &&
                            dgvParameters.Columns["colMax"].Visible &&
                            (parameter.Value < parameter.Min || parameter.Value > parameter.Max))
                            throw new Exception("The value of the " + name + "' variable must be between given min and max value.");
                        if (dgvParameters.Columns["colDelta"].Visible && parameter.Delta == 0)
                            throw new Exception("The delta of the '" + name + "' variable may not be equal to 0.");
                        if (dgvParameters.Columns["colNumOfValues"].Visible && parameter.NumOfValues < 1)
                            throw new Exception("The number of values of the '" + name + "' variable must be equal or greater than 1.");

                        driver.AddParameter(parameter);
                        baseDriver.AddParameter(parameter);
                    }
                }
            }
            if (driver.Parameters.Length == 0) throw new Exception("At least one parameter must be used.");

            foreach (DataGridViewRow row in dgvWorkflow.Rows)
            {
                if ((bool)row.Cells["colCompInclude"].Value == true)
                {
                    driver.Workflow.Add((string)row.Cells["colCompName"].Value);
                    baseDriver.Workflow.Add((string)row.Cells["colCompName"].Value);
                }
            }
            //if (driver.Workflow.InputVariables.Count == 0) throw new Exception("At least one component must be used.");

            bool test = driver.CorrectlyDefined(Pairs.GetItemNameItemPairsDictionaryIncludingFromContainers());

            Pairs.UpdateItem(OptiMaxFramework.Globals.baseDriverName, baseDriver);
            return driver;
        }
        override protected void ResetForm()
        {
            dgvParameters.Rows.Clear();

            // fill dgvParameters
            Item[] variables = Pairs.GetAllInputItemsIncludingFromContainers();
            foreach (var variable in variables)
            {
                dgvParameters.Rows.Add(new object[] { false, variable.Name, 0, 1, "0.5", "0.1", "1" });
            }

            // fill dgvWorkflow
            allComponents = Pairs.GetAllComponents();
            foreach (Component comp in allComponents)
            {
                dgvWorkflow.Rows.Add(new object[] { false, comp.Name, comp.Description, comp.Application });
            }

            // if this is a new driver, load parameter data from previous driver
            BaseDriver baseDriver = (BaseDriver)Pairs.GetItemByNameIncludingFromContainers(OptiMaxFramework.Globals.baseDriverName);
            Parameter[] parameters = baseDriver.Parameters;
            for (int i = 0; i < parameters.Length; i++)
            {
                foreach (DataGridViewRow row in dgvParameters.Rows)
                {
                    if (parameters[i].Name == (string)row.Cells["colName"].Value)
                    {
                        // existing parameter
                        row.Cells["colInclude"].Value = true;
                        row.Cells["colMin"].Value = parameters[i].Min.ToString(OptiMaxFramework.Globals.nfi);
                        row.Cells["colMax"].Value = parameters[i].Max.ToString(OptiMaxFramework.Globals.nfi);
                        row.Cells["colValue"].Value = parameters[i].Value.ToString(OptiMaxFramework.Globals.nfi);
                        row.Cells["colDelta"].Value = parameters[i].Delta.ToString(OptiMaxFramework.Globals.nfi);
                        row.Cells["colNumOfValues"].Value = parameters[i].NumOfValues.ToString();
                    }
                }
            }

            SetColumnsVisibility(Driver);

            Component component;
            string[] workflow = baseDriver.Workflow.InputVariables.ToArray();

            for (int i = workflow.Length - 1; i >= 0; i--)
            {
                component = (Component)Pairs.GetItemByNameIncludingFromContainers(workflow[i]);
                if (component != null)
                {
                    // reorder rows
                    foreach (DataGridViewRow row in dgvWorkflow.Rows)
                    {
                        if (component.Name == (string)row.Cells["colCompName"].Value)
                        {
                            row.Cells["colCompInclude"].Value = true;
                            dgvWorkflow.Rows.Remove(row);
                            dgvWorkflow.Rows.Insert(0, row);
                            break;
                        }
                    }
                }
            }
        }

        private void SetColumnsVisibility(Driver driver)
        {
            dgvParameters.Columns["colMin"].Visible = false;
            dgvParameters.Columns["colMax"].Visible = false;
            dgvParameters.Columns["colValue"].Visible = false;
            dgvParameters.Columns["colDelta"].Visible = false;
            dgvParameters.Columns["colNumOfValues"].Visible = false;

            if (Driver is DOEDriver doe)
            {
                if (doe.DOEGenerator != null)
                {
                    if (doe.DOEGenerator is FullFactorialGenerator)
                    {
                        dgvParameters.Columns["colMin"].Visible = true;
                        dgvParameters.Columns["colMax"].Visible = true;
                        dgvParameters.Columns["colNumOfValues"].Visible = true;
                    }
                    else if (doe.DOEGenerator is MonteCarloGenerator)
                    {
                        dgvParameters.Columns["colMin"].Visible = true;
                        dgvParameters.Columns["colMax"].Visible = true;
                    }
                    else if (doe.DOEGenerator is ParameterFactorialGenerator)
                    {
                        dgvParameters.Columns["colMin"].Visible = true;
                        dgvParameters.Columns["colMax"].Visible = true;
                        dgvParameters.Columns["colNumOfValues"].Visible = true;
                    }
                    else if (doe.DOEGenerator is SensitivityGenerator)
                    {
                        dgvParameters.Columns["colValue"].Visible = true;
                        dgvParameters.Columns["colDelta"].Visible = true;
                    }
                    else if (doe.DOEGenerator is OptimalLatinHypercubeGenerator)
                    {
                        dgvParameters.Columns["colMin"].Visible = true;
                        dgvParameters.Columns["colMax"].Visible = true;
                    }
                    else if (doe.DOEGenerator is RandomLatinHypercubeGenerator)
                    {
                        dgvParameters.Columns["colMin"].Visible = true;
                        dgvParameters.Columns["colMax"].Visible = true;
                    }
                    else if (doe.DOEGenerator is UserDefinedGenerator)
                    {
                    }
                    else throw new Exception();
                }
                else
                {
                    dgvParameters.Columns["colMin"].Visible = true;
                    dgvParameters.Columns["colMax"].Visible = true;
                    dgvParameters.Columns["colNumOfValues"].Visible = true;
                }
            }
            else if (Driver is NMDriver)
            {
                dgvParameters.Columns["colMin"].Visible = true;
                dgvParameters.Columns["colMax"].Visible = true;
                dgvParameters.Columns["colValue"].Visible = true;
                dgvParameters.Columns["colDelta"].Visible = true;
            }
            else if (Driver is GADriver)
            {
                dgvParameters.Columns["colMin"].Visible = true;
                dgvParameters.Columns["colMax"].Visible = true;
            }
            else if (Driver is AMQDriver)
            {
                dgvParameters.Columns["colMin"].Visible = true;
                dgvParameters.Columns["colMax"].Visible = true;
                dgvParameters.Columns["colValue"].Visible = true;
            }
            else if (Driver is LMDriver)
            {
                dgvParameters.Columns["colMin"].Visible = true;
                dgvParameters.Columns["colMax"].Visible = true;
                dgvParameters.Columns["colValue"].Visible = true;
            }
            else if (Driver is NSGAIIDriver)
            {
                dgvParameters.Columns["colMin"].Visible = true;
                dgvParameters.Columns["colMax"].Visible = true;
            }
            else throw new Exception();
        }

    }









}
