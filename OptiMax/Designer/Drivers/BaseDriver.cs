﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptiMaxFramework;

namespace OptiMax
{
    [Serializable]
    public class BaseDriver : Driver
    {
        // Constructors                                                                                       
        public BaseDriver()
            : base(OptiMaxFramework.Globals.baseDriverName, "")
        {

        }


        // Methods                                                                                            
        public override void RunEvaluation()
        {
            throw new NotImplementedException();
        }

    }
}
