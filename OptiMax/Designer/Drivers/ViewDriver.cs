﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DynamicTypeDescriptor;
using OptiMaxFramework;


namespace OptiMax
{
    [Serializable]
    public abstract class ViewDriver
    {
        // Variables                                                                                                                
        protected DynamicCustomTypeDescriptor _dctd = null;


        // Properties                                                                                                               
        [CategoryAttribute("Data")]
        [ReadOnly(false)]
        [OrderedDisplayName(0, 10, "Name")]
        [DescriptionAttribute("Enter driver name.")]
        [Id(1, 1)]
        public abstract string Name { get; set; }

        [CategoryAttribute("Misc")]
        [OrderedDisplayName(0, 10, "Description")]
        [DescriptionAttribute("Enter driver description.")]
        [Id(1, 1)]
        public abstract string Description { get; set; }

        [CategoryAttribute("Execution")]
        [OrderedDisplayName(0, 10, "Number of threads")]
        [DescriptionAttribute("Input a value larger than 1 to use parallel workflow execution.")]
        [DefaultValue(1)]
        [Id(1, 10)]
        public abstract int NumOfThreads { get; set; }

        [CategoryAttribute("Execution")]
        [OrderedDisplayName(1, 10, "Delete working directory")]
        [DescriptionAttribute("Select True to delete the working directory after workflow completition. All data that is not recorded will be lost.")]
        [DefaultValue(false)]
        [Id(1, 10)]
        public abstract bool DeleteWorkingDirectoryForSolution { get; set; }

        [CategoryAttribute("Execution")]
        [OrderedDisplayName(2, 10, "Number of retries")]
        [DescriptionAttribute("Set the number of times the workfolw will retry if it fails.")]
        [DefaultValue(0)]
        [Id(1, 10)]
        public abstract int NumOfRetries { get; set; }


        // Methods                                                                                                                  
        public abstract Driver GetBase();
    }
}
