﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DynamicTypeDescriptor;
using OptiMaxFramework;


namespace OptiMax
{
    [Serializable]
    public class ViewDOEDriver : ViewDriver
    {
        // Variables                                                                                                                
        private DOEDriver _driver;
        private string[] arrayNames;

        // Properties                                                                                                               
        public override string Name { get { return _driver.Name; } set { _driver.Name = value; } }
        public override string Description { get { return _driver.Description; } set { _driver.Description = value; } }

        [CategoryAttribute("Generator")]
        [OrderedDisplayName(0, 10, "Generator type")]
        [DescriptionAttribute("Select the experiments generator type.")]
        [Id(1, 2)]
        public string DOEGeneratorType
        {
            get { return _driver.DOEGeneratorName; }
            set
            {
                _driver.DOEGeneratorName = value;   // this sets the DOEGenerator
                SetPropertiesVisibility();
            }
        }

        [CategoryAttribute("Generator")]
        [OrderedDisplayName(1, 10, "Number of experiments")]
        [DescriptionAttribute("Enter the number of experiments.")]
        [DefaultValue(10)]
        [Id(1, 2)]
        public int NumberOfExperiments
        {
            get
            {
                if (_driver.DOEGenerator.DefineNumOfExperimentsDirectly)
                    return _driver.DOEGenerator.NumberOfExperiments;
                else
                    return -1;
            }
            set
            {
                if (_driver.DOEGenerator.DefineNumOfExperimentsDirectly)
                    _driver.DOEGenerator.NumberOfExperiments = value;
            }
        }

        [CategoryAttribute("Generator")]
        [OrderedDisplayName(2, 10, "Array of user defined values")]
        [DescriptionAttribute("Select the array for the user defined values. Each row of the array represents one experiment. Parameters must be separated by a space character.")]
        [Id(1, 2)]
        public string ArrayName
        {
            get
            {
                if (_driver.DOEGenerator is UserDefinedGenerator udg)
                {
                    if (udg.ArrayName == "" && arrayNames != null && arrayNames.Length > 0)
                        udg.ArrayName = arrayNames[0];
                    return udg.ArrayName;
                }
                else return "";
            }
            set
            {
                if (_driver.DOEGenerator is UserDefinedGenerator udg) udg.ArrayName = value;
            }
        }


        public override int NumOfThreads { get { return _driver.NumOfThreads; } set { _driver.NumOfThreads = value; } }
        public override bool DeleteWorkingDirectoryForSolution { get { return _driver.DeleteWorkingDirectoryForSolution; } set { _driver.DeleteWorkingDirectoryForSolution = value; } }
        public override int NumOfRetries { get { return _driver.NumberOfRetries; } set { _driver.NumberOfRetries = value; } }


        // Constructors                                                                                                             
        public ViewDOEDriver(DOEDriver doeDriver, string[] arrayNames)
        {
            _driver = doeDriver;                                    // 1 command
            this.arrayNames = arrayNames;
            _dctd = ProviderInstaller.Install(this);                // 2 command
            PopululateDropDownList();                               // 3 command
            SetPropertiesVisibility();                              // 4 command
        }


        // Methods                                                                                                                  
        public override Driver GetBase()
        {
            return _driver;
        }
        private void PopululateDropDownList()
        {
            List<string> typeNames = DOEDriver.GetAllDOEGeneratorNames();
            _dctd.PopulateProperty(() => this.DOEGeneratorType, typeNames.ToArray());
            _dctd.PopulateProperty(() => this.ArrayName, arrayNames);
        }
        private void SetPropertiesVisibility()
        {
            if (_driver.DOEGenerator.DefineNumOfExperimentsDirectly)
                _dctd.GetProperty("NumberOfExperiments").SetIsBrowsable(true);
            else
                _dctd.GetProperty("NumberOfExperiments").SetIsBrowsable(false);

            if (_driver.DOEGenerator is UserDefinedGenerator)
                _dctd.GetProperty("ArrayName").SetIsBrowsable(true);
            else
                _dctd.GetProperty("ArrayName").SetIsBrowsable(false);
        }
    }









}
