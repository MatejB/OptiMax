﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OptiMaxFramework;

namespace OptiMax
{
    public partial class FormWithItem : Form
    {
        // Variables
        protected ToolStripMenuItem[] toolStripMenuItems;


        // Properties                                                                                         
        public Item Item { get; set; }

        public PairsCollection Pairs { get; set; }

        public virtual Type GetItemType()
        {
            throw new Exception("The method GetItemType() must be overriden.");
        }

        public virtual String GetItemHelp()
        {
            return "The method GetItemHelp() must be overriden.";
        }

        public ToolStripMenuItem[] GetContextMenuItems
        {
            get { return toolStripMenuItems; }
        }

        // Constructors                                                                                       
        public FormWithItem()
        {
            InitializeComponent();

            toolStripMenuItems = null;
        }

        // Methods

        protected virtual void ResetForm()
        {
            throw new Exception("The method ResetForm() must be overriden.");
        }
    }
}
