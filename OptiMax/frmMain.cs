﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dalssoft.DiagramNet;
using OptiMaxFramework;
using System.IO;
using System.Diagnostics;

namespace OptiMax
{
    public partial class frmMain : Form
    {
        // Variables                                                                                                           
        private string settingsFile = "settings.om";
        private frmProjectSettings frm_ProjectSettings;
        private Framework framework;
        private string fileName;
        private DataTable memRecords;
        private DataTable activeJobs;

        // Properties
        public string FileName { get { return fileName; } set { fileName = value; } }

        // Constructors                                                                                                        
        public frmMain()
        {
            //LsDynaEloutReader.LsDynaEloutReader reader = new LsDynaEloutReader.LsDynaEloutReader(@"C:\Temp\Elout\elout");
            //string[] names = reader.GetAllVariableNames();
            //double[] result = reader.GetVariableValues("time");
            //result = reader.GetVariableValues("28933@sig-zz");


            //LsDynaMatsumReader.LsDynaMatsumReader reader = new LsDynaMatsumReader.LsDynaMatsumReader(@"D:\matsum");
            //string[] names = reader.GetAllVariableNames();
            //double[] result = reader.GetVariableValues("time");
            //result = reader.GetVariableValues("17@kinen");

            InitializeComponent();

            omDesigner.Dock = DockStyle.Fill;
            tbOutput.Dock = DockStyle.Fill;
            tabControl1.Dock = DockStyle.Fill;
            dvMemRecords.Dock = DockStyle.Fill;
            hcConvergance.Dock = DockStyle.Fill;
            splitContainer2.Dock = DockStyle.Fill;

            splitContainer1.SplitterDistance = splitContainer1.Height - 150;

            settingsFile = Path.Combine(Application.StartupPath, settingsFile);
            frm_ProjectSettings = new frmProjectSettings();
            framework = null;


            
            //List<double[]> points = new List<double[]>();
            //points.Add(new double[] { 0, 2, 2 });
            //points.Add(new double[] { 2, 2, 2 });
            //points.Add(new double[] { 2, 0, 2 });
            //double[] referencePoint = new double[] { 3, 3, 3};

            //HyperVolume hv = new HyperVolume();
            //double volume = hv.Evaluate(3, points.Count, points, referencePoint);
            //volume = volume;

            //memRecords = new DataTable();
            //memRecords.Columns.Add("String");
            //memRecords.Columns.Add("Double");

            //memRecords.Columns[1].DataType = typeof(double);

            //memRecords.Rows.Add(new object[] { "1", 1.2 });
            //memRecords.Rows.Add(new object[] { "2", 1.3 });
            //memRecords.Rows.Add(new object[] { "3", 1.4 });
            //memRecords.Rows.Add(new object[] { "4", 1.5678 });
            //memRecords.Rows.Add(new object[] { "5", 1234.5 });
            //memRecords.Rows.Add(new object[] { "6", 1.9E4 });

            //double result = 0;
            //bool test = double.TryParse("1.234E5", System.Globalization.NumberStyles.Float, OptiMaxFramework.Globals.nfi, out result);

            //double t = 2 * result;

            ////dataView1.DataSource = memRecords;

            //DataRow row = memRecords.Select("String=1").FirstOrDefault();

            //row["Double"] = 2;

            activeJobs = new DataTable("ActiveJobs");
            activeJobs.Columns.Add("ID", typeof(string));
            activeJobs.Columns.Add("Status", typeof(string));
            activeJobs.Columns.Add("Elapsed Time", typeof(string));
            //activeJobs.DefaultView.RowFilter = "Status NOT IN ('InQueue')";

            dgvActiveJobs.DataSource = activeJobs;
            dgvActiveJobs.Sort(dgvActiveJobs.Columns["Status"], ListSortDirection.Descending);
        }


        // Event handling                                                                                                      
        private void Form1_Load(object sender, EventArgs e)
        {
            SetDisabled();

            try
            {
                if (fileName != null)   // command line arguments
                {
                    OpenProject(fileName);
                }
            }
            catch
            {
                MessageBox.Show("Could not open file: " + fileName, "Error", MessageBoxButtons.OK);
                fileName = null;
                newProjectToolStripMenuItem_Click(null, null);
                return;
            }

            try
            {
                if (File.Exists(settingsFile))
                {
                    fileName = File.ReadAllText(settingsFile);
                    if (File.Exists(fileName)) OpenProject(fileName);
                    //else newProjectToolStripMenuItem_Click(null, null); this does not work since form is not yet shown
                }

                if (false)
                {
                    //bool test1 = Helpers.ExcelHelper.IsThisArrayCellRangeValid("A5:A6");
                    //bool test2 = Helpers.ExcelHelper.IsThisArrayCellRangeValid("A5:TZ34345");
                    //bool test3 = Helpers.ExcelHelper.IsThisArrayCellRangeValid("A5:U1");
                    //bool test4 = Helpers.ExcelHelper.IsThisArrayCellRangeValid("A5:C5");
                    //bool test5 = Helpers.ExcelHelper.IsThisArrayCellRangeValid("A5555555");

                    Framework fr = new Framework("Excel", "", @"E:\OptiMax\GradientConstrained");
                    fr.WorkingDirectory = @"E:\OptiMax\GradientConstrained\Work";
                    fr.ResultsDirectory = @"E:\OptiMax\GradientConstrained\Results";

                    fr.Add(new VariableInput("x", "", VariableValueType.Double));
                    fr.Add(new VariableFunction("y", "", "x*x"));

                    CsvRecorder recorder = new CsvRecorder("CsvResults", "", ",");
                    recorder.Add("x");
                    recorder.Add("y");
                    fr.Add(recorder);

                    //DOEDriver doe = new DOEDriver("driver", "");
                    //doe.DOEGenerator = new DOEFullFactorial();
                    //doe.AddParameter(new Parameter("x", -1, 1, 11));
                    ////doe.Workflow.Add(ec.Name);
                    //doe.NumOfThreads = 1;
                    //fr.Add(doe);

                    AMQDriver gc = new AMQDriver("driver", "", "y", null, 100, 0.001);
                    Parameter x = new Parameter("x", VariableValueType.Double, -1, 1);
                    x.SetValue(0.5);
                    gc.AddParameter(x);
                    gc.NumOfThreads = 1;
                    fr.Add(gc);
                    fr.Run();


                    //fr.Add(new FileResource("input", "", @"E:\OptiMax\Plosca\plosca.inp", FileIOType.Input));

                    //AbaqusComponent ac = new AbaqusComponent("Abaq", "", "input", "plosca", 1000 * 60 * 5);
                    //ac.MemorySizeGB = 1;
                    //ac.NumOfCpus = 2;
                    //ac.OdbReportType = ReportType.History;
                    //ac.ArgumentsToAdd = "";
                    //fr.Add(ac);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void frmMain_Shown(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.CenterToScreen();
            }

            if (omDesigner.Framework == null)
            {
                newProjectToolStripMenuItem_Click(null, null);
            }
        }
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (framework != null && framework.IsEventReportStatusDataSet) framework.ReportStatusData -= framework_ReportStatusData;
            if (framework != null && framework.IsEventReportConverganceDataSet) framework.ReportConverganceData -= framework_ReportCoverganceData;

            if (fileName != null) // nothing to save
            {
                var response = MessageBox.Show("Save project before exit?", "Exit project", MessageBoxButtons.YesNoCancel);

                if (response == System.Windows.Forms.DialogResult.Yes)
                {
                    saveProjectToolStripMenuItem_Click(null, null);
                    File.WriteAllText(settingsFile, fileName);
                }
                else if (response == System.Windows.Forms.DialogResult.No)
                {
                    File.WriteAllText(settingsFile, fileName);
                }
                else if (response == System.Windows.Forms.DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void SetDisabled()
        {
            tabControl1.Enabled = false;
            toolStrip1.Enabled = false;
            toolsToolStripMenuItem.Enabled = false;
        }
        private void SetEnabled()
        {
            tabControl1.Enabled = true;
            toolStrip1.Enabled = true;
            toolsToolStripMenuItem.Enabled = true;
        }
        private void ClearAll()
        {
            Clear();

            // Workflow
            omDesigner.Clear();

            fileName = null;
            tslProjectDirectory.Text = "Project directory: ";
            tslProjectName.Text = "Project name: ";
        }
        private void Clear()
        {
            tbOutput.Text = "";
            hcConvergance.Clear();
            
            // Active jobs
            dgvActiveJobs.DataSource = null;
            activeJobs.Rows.Clear();
            dgvActiveJobs.Rows.Clear();
            dgvActiveJobs.DataSource = activeJobs;
            tbJobsInqueue.Text = "";
            tbJobsRunning.Text = "";
            tbJobsComplete.Text = "";
            // Memory records
            if (memRecords != null)
            {
                memRecords.Rows.Clear();
                memRecords.Columns.Clear();
                dvMemRecords.Clear();
            }
            dvMemRecords.ClearGraphAndRecords();
        }


        // Toolbar
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileName != null && fileName != "" && File.Exists(fileName))
            {
                var response = MessageBox.Show("Save existing project before creating a new one?", "New project", MessageBoxButtons.YesNoCancel);
                if (response == System.Windows.Forms.DialogResult.Yes) { saveProjectToolStripMenuItem_Click(null, null); }
                else if (response == System.Windows.Forms.DialogResult.No) { }
                else if (response == System.Windows.Forms.DialogResult.Cancel) { return; }
            }

            ClearAll();
            SetDisabled();

            frm_ProjectSettings.SetForCreateNewProject();

            if (frm_ProjectSettings.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                omDesigner.Framework = frm_ProjectSettings.Framework;
                omDesigner.Invalidate();
                omDesigner.Focus();
                tbOutput.Text = "";

                tslProjectDirectory.Text = "Project directory: " + omDesigner.Framework.ProjectDirectory;
                tslProjectName.Text = "Project name: " + omDesigner.Framework.Name;
                fileName = Path.Combine(omDesigner.Framework.ProjectDirectory, omDesigner.Framework.Name + ".om");

                SetEnabled();
            }
        }
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (omDesigner.Framework == null) return;

                frm_ProjectSettings.SetForRenameProject(omDesigner.Framework);

                if (frm_ProjectSettings.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    omDesigner.Framework = frm_ProjectSettings.Framework;

                    tslProjectName.Text = "Project name: " + omDesigner.Framework.Name;
                    fileName = Path.Combine(omDesigner.Framework.ProjectDirectory, omDesigner.Framework.Name + ".om");

                    omDesigner.Invalidate();
                    SaveToFile();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void saveProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (framework != null && framework.IsEventReportStatusDataSet) framework.ReportStatusData -= framework_ReportStatusData;
                if (framework != null && framework.IsEventReportConverganceDataSet) framework.ReportConverganceData -= framework_ReportCoverganceData;

                if (fileName == null) MessageBox.Show("Nothing to save. This project is empty.", "Save project", MessageBoxButtons.OK);
                else SaveToFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in frmMain method saveProjectToolStripMenuItem_Click(...)." + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        private void SaveToFile()
        {
            Size frmSize = this.Size;
            if (this.WindowState == FormWindowState.Maximized)
            {
                Rectangle resolution = Screen.PrimaryScreen.Bounds;
                frmSize.Width = resolution.Width;
                frmSize.Height = resolution.Height;
            }
            omDesigner.SaveToFile(fileName, frmSize, memRecords, dvMemRecords.GraphPane, dvMemRecords.SelectedCells);
        }
        private void openProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "OptiMax file (*.om)|*.om";
                if (omDesigner != null && omDesigner.Framework != null)
                {
                    if (omDesigner.Framework.ProjectDirectory != null && Directory.Exists(omDesigner.Framework.ProjectDirectory))
                        openFileDialog1.InitialDirectory = omDesigner.Framework.ProjectDirectory;
                    if (omDesigner.Framework.Name != null)
                        openFileDialog1.FileName = omDesigner.Framework.Name + ".om";
                }

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ClearAll();
                    OpenProject(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void OpenProject(string projectFileName)
        {
            fileName = projectFileName;
            Size frmSize = new System.Drawing.Size();
            ZedGraph.GraphPane graph = null;
            SortedList<int, Point> selectedCells = null;
            omDesigner.LoadFromFile(fileName, ref frmSize, ref memRecords, ref graph, ref selectedCells);

            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            if (frmSize.Width == resolution.Width && frmSize.Height == resolution.Height) this.WindowState = FormWindowState.Maximized;
            else this.Size = frmSize;

            dvMemRecords.DataSource = memRecords;
            timUpdateMemRecords.Enabled = true;

            if (graph != null) dvMemRecords.GraphPane = graph;
            
            if (selectedCells != null) dvMemRecords.SelectedCells = selectedCells;

            tbOutput.Text = "";

            // check if the project was renamed
            if (omDesigner.Framework.Name.ToLower() != Path.GetFileNameWithoutExtension(projectFileName).ToLower())
                omDesigner.Framework.Name = Path.GetFileNameWithoutExtension(projectFileName);

            // check if the project was moved to different project directory
            if (omDesigner.Framework.ProjectDirectory != Path.GetDirectoryName(projectFileName))
                omDesigner.SetNewProjectDirectory(Path.GetDirectoryName(projectFileName));

            tslProjectDirectory.Text = "Project directory: " + omDesigner.Framework.ProjectDirectory;
            tslProjectName.Text = "Project name: " + omDesigner.Framework.Name;

            SetEnabled();
        }
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                frm_ProjectSettings.SetForEditProject(omDesigner.Framework);

                if (frm_ProjectSettings.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    omDesigner.Framework = frm_ProjectSettings.Framework;
                    
                    tslProjectDirectory.Text = "Project directory: " + omDesigner.Framework.ProjectDirectory;
                    tslProjectName.Text = "Project name: " + omDesigner.Framework.Name;
                    fileName = Path.Combine(omDesigner.Framework.ProjectDirectory, omDesigner.Framework.Name + ".om");

                    omDesigner.Invalidate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void loadMemRecordsFromcsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "Csv file (*.csv)|*.csv";
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (tssbStart.Enabled)
                    {
                        LoadMemRecordsFromFile(openFileDialog1.FileName);
                        tabControl1.SelectedIndex = 2;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Globals.ProgramName + Environment.NewLine + Environment.NewLine + "by Matej Borovinšek", "About");
        }


        // Report data
        void framework_ReportStatusData(string data)
        {
            UpdateOutput(data);
        }
        void framework_ReportCoverganceData(double[] data)
        {
            try
            {
                if (this.hcConvergance.InvokeRequired)
                {
                    // This is a worker thread so delegate the task.
                    this.BeginInvoke(new MethodInvoker(() => framework_ReportCoverganceData(data)));
                }
                else
                {
                    if (data != null && data.Length == 2)
                    {
                        hcConvergance.Add(data[0], data[1]);
                    }
                }
            }
            catch
            {
            }
        }
        void framework_ReportMemRecorderData(object[] data)
        {
            try
            {
                if (this.dvMemRecords.InvokeRequired)
                {
                    // This is a worker thread so delegate the task.
                    this.BeginInvoke(new MethodInvoker(() => framework_ReportMemRecorderData(data)));
                }
                else
                {
                    if (memRecords == null) return; //

                    if (memRecords.Rows.Count == 0)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            memRecords.Columns[i].DataType = data[i].GetType();
                        }
                    }

                    memRecords.Rows.Add(data);

                    if (!timUpdateMemRecords.Enabled)
                    {
                        dvMemRecords.Update();
                        timUpdateMemRecords.Enabled = true;
                    }
                }
            }
            catch
            {
            }
        }

        // Active jobs
        private void timUpdateStatus_Tick(object sender, EventArgs e)
        {
            try
            {
                if (tabControl1.SelectedIndex != 1) return;

                UpdateActiveJobs();
            }
            catch
            { }
        }
        private void UpdateActiveJobs()
        {
            if (framework.Driver != null && framework.Driver.JobStatusData != null)
            {
                TimeSpan span;
                string time;
                JobStatusData[] data = framework.Driver.JobStatusData;

                // run in the first iteration after the start
                if (activeJobs.Rows.Count == 0)
                {
                    foreach (var status in data)
                    {
                        span = DateTime.Now - status.startTime;
                        time = span.Hours.ToString().PadLeft(2, '0') + ":"
                               + span.Minutes.ToString().PadLeft(2, '0') + ":"
                               + span.Seconds.ToString().PadLeft(2, '0');
                        if (status.jobStatus == JobStatus.InQueue) time = "";
                        activeJobs.Rows.Add(new object[] { status.ID, status.jobStatus.ToString(), time });
                    }
                    dgvActiveJobs.ClearSelection(); // allways clear after the data change
                }
                // run in all following iterations
                else
                {
                    DataRow row;
                    string selectedRowID = "";

                    DataGridViewSelectedRowCollection rows = dgvActiveJobs.SelectedRows;

                    // save the selected row ID and deselect it
                    if (rows != null && rows.Count > 0)
                    {
                        selectedRowID = rows[0].Cells["ID"].Value.ToString();
                    }

                    // change the data
                    int i = 0;
                    int jobsInQueue = 0;
                    int jobsRunning = 0;
                    int jobsComplete = 0;
                    foreach (var status in data)
                    {
                        row = activeJobs.Rows[i++];
                        row["ID"] = status.ID;
                        row["Status"] = status.jobStatus.ToString();
                        if (status.jobStatus == JobStatus.Running || status.jobStatus == JobStatus.Retrying)
                        {
                            span = DateTime.Now - status.startTime;
                            time = span.Hours.ToString().PadLeft(2, '0') + ":"
                                   + span.Minutes.ToString().PadLeft(2, '0') + ":"
                                   + span.Seconds.ToString().PadLeft(2, '0');
                            if (status.jobStatus == JobStatus.InQueue) time = "";
                            row["Elapsed time"] = time;
                        }
                        
                        if (status.jobStatus == JobStatus.InQueue) jobsInQueue++;
                        else if (status.jobStatus == JobStatus.Running) jobsRunning++;
                        else if (status.jobStatus == JobStatus.OK) jobsComplete++;
                    }
                    tbJobsInqueue.Text = jobsInQueue.ToString();
                    tbJobsRunning.Text = jobsRunning.ToString();
                    tbJobsComplete.Text = jobsComplete.ToString();

                    dgvActiveJobs.ClearSelection(); // allways clear after the data change

                    // reselect the selected row
                    if (selectedRowID != "")
                    {
                        foreach (DataGridViewRow dgvRow in dgvActiveJobs.Rows)
                        {
                            if (dgvRow.Cells["ID"].Value.ToString() == selectedRowID)
                            {
                                dgvRow.Selected = true;
                                break;
                            }
                        }
                    }

                }
            }
        }
        private void dgvActiveJobs_MouseUp(object sender, MouseEventArgs e)
        {
            //See if the left mouse button was clicked
            if (e.Button == MouseButtons.Left)
            {
                //Check the HitTest information for this click location
                if (dgvActiveJobs.HitTest(e.X, e.Y) == DataGridView.HitTestInfo.Nowhere)
                {
                    dgvActiveJobs.ClearSelection();
                }
            }
        }


        // Menubar                                                                                                            
        private void tsbNew_Click(object sender, EventArgs e)
        {
            newProjectToolStripMenuItem_Click(null, null);
        }
        private void tsbOpen_Click(object sender, EventArgs e)
        {
            openProjectToolStripMenuItem_Click(null, null);
        }
        private void tsbSave_Click(object sender, EventArgs e)
        {
            saveProjectToolStripMenuItem_Click(null, null);
        }
        private void tssbStart_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                Framework frameWork = omDesigner.AssembledFramework;

                if (frameWork == null)
                    throw new Exception("The name and the directories of the project are not set.");

                if (!omDesigner.CheckFrameworkValidity())
                    throw new Exception("There are errors in the framework. Eliminate the errors and try again.");

                framework = null;
                framework = frameWork.DeepClone();  // deepClone: else a serialization error of OptiMax.frmMain due to memRecorder
                RunFramework("Job started.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        
        private void tsmiTry_Click(object sender, EventArgs e)
        {
            try
            {
                framework = omDesigner.AssembledFramework;

                if (framework == null)
                    throw new Exception("The name and the directories of the project are not set.");

                if (!omDesigner.CheckFrameworkValidity())
                    throw new Exception("There are errors in the framework. Eliminate the errors and try again.");

                Driver oldDriver = framework.Driver;
                if (oldDriver != null)
                {
                    DOEDriver newDriver = new DOEDriver(oldDriver.Name, "try");
                    newDriver.DOEGenerator = new FullFactorialGenerator();

                    // Set Parameters
                    int N = oldDriver.Parameters.Length;
                    List<string> names = new List<string>();
                    List<string> values = new List<string>();

                    string name;
                    for (int i = 0; i < N; i++)
                    {
                        name = oldDriver.Parameters[i].Name;
                        names.Add(name);
                        values.Add(oldDriver.Parameters[i].Min.ToString(OptiMaxFramework.Globals.nfi));
                    }

                    frmTry frmTry = new OptiMax.frmTry();
                    frmTry.SetParameter(names.ToArray(), values.ToArray());
                    if (frmTry.ShowDialog() == DialogResult.OK)
                    {
                        Parameter newParameter;
                        for (int i = 0; i < N; i++)
                        {
                            newParameter = new Parameter();
                            newParameter.Name = names[i];
                            newParameter.ValueType = VariableValueType.Double;
                            newParameter.Min = frmTry.Values[i];
                            newParameter.Max = newParameter.Min + 1;
                            newParameter.NumOfValues = 1;
                            newDriver.AddParameter(newParameter);
                        }

                        // Set workflow and recorders to record all results
                        foreach (string component in oldDriver.Workflow.InputVariables)
                        {
                            newDriver.Workflow.Add(component);
                        }

                        newDriver.NumOfThreads = 1;

                        framework = null;
                        framework = omDesigner.AssembledFramework.DeepClone();  // deepClone: else a serialization error of OptiMax.frmMain due to memRecorder
                        foreach (Recorder rec in framework.Recorders) rec.RecordAll = true;
                        framework.ReplaceDriver(newDriver);
                        RunFramework("Try started.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        private async void tsbStop_Click(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(() =>
                {
                    framework.Kill();
                    UpdateOutput("Kill command sent.");
                }
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void RunFramework(string outputText)
        {
            framework.ReportStatusData = framework_ReportStatusData;

            Clear();

            if (framework.HasMemRecorder)
            {
                PrepareMemDataTable();
                framework.ReportMemRecorderData = framework_ReportMemRecorderData;
                framework.ReportConverganceData = framework_ReportCoverganceData;
            }

            using (BackgroundWorker bwStart = new BackgroundWorker())
            {
                bwStart.DoWork += bwStart_DoWork;
                bwStart.RunWorkerCompleted += bw_WorkerCompleted;
                if (!bwStart.IsBusy)
                {
                    bwStart.RunWorkerAsync();
                    UpdateOutput(outputText);
                    UpdateActiveJobs();
                }
            }
        }

        // Toolbar
        private void omtToolbar_AddElement(Image image, string propertiesFormTypeName, Item item)
        {
            int elWidth = 32;
            int elHeight = 32;

            RectangleIconNode el = new RectangleIconNode(image, 0, 0, elWidth, elHeight);

            el.Label.AutoSize = true;
            el.Label.Text = "";
            el.Label.Font = new Font(FontFamily.GenericSansSerif, 10);
            el.Label.DoAutoSize();
            el.PropertieFormTypeName = propertiesFormTypeName;

            omDesigner.AddElement(el, item);
        }


        // Worker                                                                                                             
        void bwStart_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Active(true);
                framework.Run();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        void bw_WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Active(false);

            if (framework.Status == JobStatus.OK)
                UpdateOutput("Job finished." + Environment.NewLine);
            else if (framework.Status == JobStatus.Killed)
                UpdateOutput("Job killed." + Environment.NewLine);
            else if (framework.Status == JobStatus.Failed)
                UpdateOutput("Job failed." + Environment.NewLine);

            UpdateActiveJobs();
        }
        private void Active(bool active)
        {
            if (this.omDesigner.InvokeRequired)
            {
                // This is a worker thread so delegate the task.
                this.BeginInvoke(new MethodInvoker(() => Active(active)));
            }
            else
            {
                tssbStart.Enabled = !active;
                tsbPause.Enabled = active;
                tsbStop.Enabled = active;
                timUpdateStatus.Enabled = active;
                if (active)
                {
                    tsProgress.Style = ProgressBarStyle.Marquee;
                }
                else
                {
                    tsProgress.Style = ProgressBarStyle.Continuous;
                    timUpdateStatus_Tick(null, null);
                }
            }

        }


        // Mem data set                                                                                                       
        private void timUpdateMemRecords_Tick(object sender, EventArgs e)
        {
            dvMemRecords.Update();
            timUpdateMemRecords.Enabled = false;
        }
        private void PrepareMemDataTable()
        {
            string[] titles = framework.GetMemRecorderTitleRow;
            PrepareMemDataTable(titles);
        }
        private void PrepareMemDataTable(string[] titles)
        {
            if (titles != null)
            {
                memRecords = new DataTable("Records");

                foreach (string title in titles)
                {
                    memRecords.Columns.Add(title, typeof(double));
                }

                dvMemRecords.DataSource = memRecords;
            }
        }
        public void LoadMemRecordsFromFile(string file)
        {
            string[] lines = File.ReadAllLines(file);
            string[] titles;
            string[] data;

            int countCommas = 0;
            int countDots = 0;
            for (int i = 1; i < lines.Length && i <= 10; i++)
            {
                countCommas += lines[i].Split(',').Length;
                countDots += lines[i].Split('.').Length;
            }

            bool useComma = false;
            if (countCommas > countDots)
            {
                if (MessageBox.Show("It seems that the decimal separator is a comma.\nUse comma as a decimal separator?", "Decimal separator",
                                MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    useComma = true;
                }
            }

            if (lines.Length > 0)
            {
                titles = lines[0].Split(new string[] {";", "\""}, StringSplitOptions.RemoveEmptyEntries);
                PrepareMemDataTable(titles);

                for (int i = 1; i < lines.Length; i++)
                {
                    if (useComma) lines[i] = lines[i].Replace(',', '.');
                    
                    data = lines[i].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    framework_ReportMemRecorderData(data);
                }
            }

        }


        // Output                                                                                                             
        private void UpdateOutput(string data)
        {
            if (this.tbOutput.InvokeRequired)
            {
                // This is a worker thread so delegate the task.
                this.BeginInvoke(new MethodInvoker(() => UpdateOutput(data)));
            }
            else
            {
                // It's on the same thread, no need for Invoke
                tbOutput.AppendText(DateTime.Now.ToString() + "   " + data + Environment.NewLine);

                tbOutput.Select(tbOutput.TextLength, 0);
                tbOutput.ScrollToCaret();

                Application.DoEvents();
            }
        }
        private void tbOutput_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (splitContainer1.Height - splitContainer1.SplitterDistance <= 100)
                splitContainer1.SplitterDistance = 100;
            else //if (splitContainer1.SplitterDistance <= 100)
                splitContainer1.SplitterDistance = splitContainer1.Height - 100;
        }

       
    }
}
